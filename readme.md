The "core" components required to run a snkeng app. 

Usually loaded by composer and added during the composer "create-project" command.

For more information visit: https://snako.dev.

