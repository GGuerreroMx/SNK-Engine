#!/usr/bin/env bash

# Get variables
echo -n "Site URL: "
read -r SITE_URL

#
echo -n "Short Name: "
read -r SHORT_NAME

#
if [[ -z "$SITE_URL" ]]; then
  echo "NO SITE DETECTED (FORMAT: domain.bigdomain)"
  exit 1
fi
#
if [[ -z "$SHORT_NAME" ]]; then
  echo "NO DB NAME DETECTED (FORMAT: shortname)"
  exit 1
fi


# server folder
mkdir -p /var/www/$SITE_URL/public_html
chmod -R 700 /var/www/$SITE_URL/public_html

# index file for confirmation
cat > /var/www/$SITE_URL/public_html/index.html << EOF
<html>
	<head>
		<title>INDEX FILE FOR: $SITE_URL</title>
	</head>
	<body>
		<h1>SUCCESS</h1>
		$SITE_URL
	</body>
</html>
EOF


# building begin
DB_NAME="db_$SHORT_NAME"

# Random data
COOKIE_RAND=$(cat /dev/urandom | tr -dc 'a-zA-Z0-9' | fold -w 48 | head -n 1)
SYNC_RAND=$(cat /dev/urandom | tr -dc 'a-zA-Z0-9' | fold -w 64 | head -n 1)

# config file
cat > /var/www/$SITE_URL/.env << EOF
MYSQL_SERVER="localhost"
MYSQL_DB_NAME="$DB_NAME"
MYSQL_USR_USR="PUT_DATA"
MYSQL_USR_PWD="PUT_DATA"
MYSQL_ADM_USR="PUT_DATA"
MYSQL_ADM_PWD="PUT_DATA"
# SE variables, all required
SE_TYPE="dev"
# Must be literaly "true" for it to be a debuging environment.
SE_DEBUG="true"
SE_SYNC_CODE="$SYNC_RAND"
SE_COOKIE="$COOKIE_RAND"
SE_MAIL_CONTACT="PLACE_EMAIL"
# CUSTOM PER SITE
GOOGLE_ANALYTICS_V3="TESTENV"
GOOGLE_CAPTCHA_SECRET="TESTENV"
GOOGLE_CAPTCHA_KEY="TESTENV"
EOF

# Mysql operations
mysql -uroot -p <<EOFMYSQL
CREATE SCHEMA IF NOT EXISTS $DB_NAME DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci;
EOFMYSQL
#
