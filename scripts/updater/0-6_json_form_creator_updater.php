<?php

$origArray = <<<JSON
[
				{"ftype":"text", "vname":"title", "fname":"Nombre", "fdesc":"", "dtype":"str", "regexp":"simpleTitle", "lMin":3, "lMax":50, "requiered":true},
				{"ftype":"text", "vname":"fullUrl", "fname":"URL", "fdesc":"", "dtype":"str", "regexp":"urlPath", "requiered":true}
			]
JSON;


// Do ops
$objectArray = json_decode($origArray, true);

$finalArray = [];

foreach ( $objectArray as $cObj ) {
	$cArray = [];

	// General
	$cArray['field_type'] = $cObj['ftype'] ?? '';
	$cArray['data_type'] = $cObj['dtype'] ?? '';
	$cArray['default_value'] = $cObj['dval'] ?? null;
	$cArray['required'] = $cObj['required'] ?? false;
	$cArray['info_name'] = $cObj['fname'] ?? 'PENDIENTE';
	$cArray['info_description'] =  $cObj['fdesc'] ?? '';
	$cArray['attributes'] = [];

	//
	switch ( $cObj['ftype'] ) {
		case 'text':
			$cArray['field_type'] = 'input';
			$cArray['data_type'] = 'string';

			//
			$cArray['attributes']['minlength'] = $cObj['lMin'] ?? 0;
			$cArray['attributes']['maxlength'] = $cObj['lMax'] ?? 255;
			$cArray['attributes']['data-regexp'] = $cObj['regexp'] ?? null;
			break;

		//
		case 'number':
			$cArray['field_type'] = 'input';
			$cArray['data_type'] = 'number';

			//
			$cArray['attributes']['type'] = 'number';
			$cArray['attributes']['min'] = $cObj['min'] ?? 0;
			$cArray['attributes']['max'] = $cObj['max'] ?? 4294967295;
			break;

		//
		case 'tel':
			$cArray['field_type'] = 'input';
			$cArray['data_type'] = 'number';

			//
			$cArray['attributes']['type'] = 'tel';
			break;

		//
		case 'email':
			$cArray['field_type'] = 'input';
			$cArray['data_type'] = 'string';
			$cArray['attributes']['type'] = 'email';

			//
			$cArray['attributes']['minlength'] = $cObj['lMin'] ?? 6;
			$cArray['attributes']['maxlength'] = $cObj['lMax'] ?? 100;
			break;

		//
		case 'checkbox':
			$cArray['data_type'] = 'bool';
			unset($cArray['required']);

			$cArray['attributes']['value'] = 1;
			break;
		//
		case 'list':
			$cArray['attributes']['options'] = $cObj['opts'];
			break;
		//
		case 'hidden':
			unset($cArray['required']);
			unset($cArray['info_name']);
			unset($cArray['info_description']);
			break;
		//
		case 'datetime-local':
			$cArray['field_type'] = 'input';
			$cArray['data_type'] = 'datetime';
			$cArray['attributes']['type'] = 'datetime-local';

			//
			$cArray['attributes']['minlength'] = 19;
			$cArray['attributes']['maxlength'] = 25;
			$cArray['attributes']['regexp'] = 'datetime';
			break;
		//
		case 'date':
			$cArray['field_type'] = 'input';
			$cArray['data_type'] = 'date';
			$cArray['attributes']['type'] = 'date';

			//
			$cArray['attributes']['minlength'] = 10;
			$cArray['attributes']['maxlength'] = 12;
			$cArray['attributes']['regexp'] = 'date';
			break;
		//
		case 'file':
			$cArray['attributes']['accept'] = $cObj['accept'] ?? '';
			$cArray['attributes']['max_size'] = $cObj['fsize'] ?? 512;
			break;
	}

	//
	foreach ( $cArray as $cEl => $cVal ) {
		if ( $cVal === null || empty($cVal) ) {
			unset($cArray[$cEl]);
		}
	}


	$finalArray[$cObj['vname']] = $cArray;
}

$varExport = json_encode($finalArray, JSON_UNESCAPED_SLASHES | JSON_UNESCAPED_UNICODE | JSON_PRETTY_PRINT);
$varExport = str_replace('    ', "\t", $varExport);
$varExport = str_replace("\n", "\n\t\t\t", $varExport);

echo <<<TEXT
$varExport
TEXT;
