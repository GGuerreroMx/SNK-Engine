<?php
//
$http_query_data = [
	'url' => 'https://snako.cora.snako.dev/server/snkeng/versions/core/upload',
	'auth' => [
		'user' => 'abc',
		'pass' => 'def'
	]
];

//
$rootDir = __DIR__ . '/../../';

//
if ( !is_dir($rootDir) ) {
	echo "INVALID PROJECT DIRECTORY ($rootDir)";
	exit();
}

//
echo <<<TEXT
START

CREATE ZIP FILE: 'se_core_patch.zip'.\n
TEXT;

// Get real path for our folder
$rootPathLen = strlen(realpath($rootDir)) + 1;

$coreDir = $rootDir .'se_core/';
$corePath = realpath($coreDir);

// Initialize archive object
$zip = new ZipArchive();
$zip->open("se_core_patch.zip", ZipArchive::CREATE | ZipArchive::OVERWRITE);

// Create recursive directory iterator
$files = new RecursiveIteratorIterator(
	new RecursiveDirectoryIterator($corePath),
	RecursiveIteratorIterator::LEAVES_ONLY
);


//
foreach ( $files as $name => $file )
{
	// Skip directories (they would be added automatically)
	if ( !$file->isDir() )
	{
		// Get real and relative path for current file
		$filePath = $file->getRealPath();
		$relativePath = substr($filePath, $rootPathLen);

		// convert to compatible linux op
		$relativePath = str_replace('\\', '/', $relativePath);

		// Add current file to archive
		$zip->addFile($filePath, $relativePath);
	}
}

// Add "root files"
$zip->addFile($rootDir . ".htaccess", ".htaccess");
$zip->addFile($rootDir . "se_index.php", "se_index.php");

// Properties
$engineProperties = [
	'version' => "0.11.100.0",
	'date' => date("Y-m-d H:m:s")
];

// Log
// $zip->addFromString("info.json", \json_encode($engineProperties, JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES));

// Zip archive will be created only after closing object
$zip->close();
//

//
echo <<<TEXT
FILE CREATED.

UPLOAD TO:{$http_query_data['url']}\n
TEXT;

// upload file?


// Authorization
$hash = base64_encode($http_query_data['auth']['user'] . ":" . $http_query_data['auth']['pass']);

$cFile = curl_file_create("se_core_patch.zip");

// Headers
$headers = [
	"Content-Type: multipart/form-data",
	"Authorization: Basic $hash"
];



$options = array(
	CURLOPT_RETURNTRANSFER => true,                 // return web page
	CURLOPT_HEADER         => false,                // don't return headers
	CURLOPT_FOLLOWLOCATION => true,                 // follow redirects
	CURLOPT_ENCODING       => "",                   // handle all encodings
	CURLOPT_USERAGENT      => "spider",             // who am i
	CURLOPT_AUTOREFERER    => true,                 // set referer on redirect
	CURLOPT_CONNECTTIMEOUT => 120,                  // timeout on connect
	CURLOPT_TIMEOUT        => 120,                  // timeout on response
	CURLOPT_MAXREDIRS      => 1,                    // stop after 1 redirects
	CURLOPT_HTTPHEADER      => $headers,
	CURLOPT_POST            => 1,                   // i am sending post data
	CURLOPT_POSTFIELDS     => ['file' => $cFile],   // this are my post vars
	CURLOPT_SSL_VERIFYHOST => 0,                    // don't verify ssl
	CURLOPT_SSL_VERIFYPEER => false,                //
	CURLOPT_VERBOSE        => 1                     //
);

$ch      = curl_init($http_query_data['url']);
curl_setopt_array($ch, $options);
$content = curl_exec($ch);
$err     = curl_errno($ch);
$errmsg  = curl_error($ch) ;
$header  = curl_getinfo($ch);
curl_close($ch);

echo <<<TEXT
RESULTADO DEL QUERY:
---
{$content}
---
{$err}
---
{$errmsg}
---
{$header}
---
FIN\n
TEXT;

// */