<?php

$origArray = [
	'title' => ['name' => 'Nombre', 'type' => 'str', 'lMin' => 0, 'lMax' => 75, 'filter' => 'simpleTitle', 'process' => false, 'validate' => true],
	'modId' => ['name' => 'ID Modulo', 'type' => 'int', 'process' => false, 'validate' => true, 'notNull' => true],
	'dbName' => ['name' => 'Nombre DB', 'type' => 'str', 'lMin' => 0, 'lMax' => 75, 'filter' => 'urlTitles', 'process' => false, 'validate' => true],
	'dbNick' => ['name' => 'Nick DB', 'type' => 'str', 'lMin' => 0, 'lMax' => 10, 'filter' => 'urlTitles', 'process' => false, 'validate' => true],
	'dbType' => ['name' => 'Tipo DB', 'type' => 'str', 'lMin' => 0, 'lMax' => 10, 'filter' => 'urlTitles', 'process' => false, 'validate' => true],
	'addId' => ['name' => 'Add Id', 'type' => 'bool'],
	'addDtAdd' => ['name' => 'Add dt add', 'type' => 'bool'],
	'addDtMod' => ['name' => 'Add dt mod', 'type' => 'bool'],
	'addTitle' => ['name' => 'ADD title', 'type' => 'bool'],
];


// Do ops
$objectArray = [];

foreach ( $origArray as $cName => $cObj ) {
	$dataOptionsArr = [];

	$altValidationList = [];

	//
	$dataOptionsArr['name'] = $cObj['name'];
	$dataOptionsArr['type'] = $cObj['type'];
	$dataOptionsArr['required'] = ( !empty($cObj['valid'] ) && $cObj['valid'] );

	//
	switch ( $cObj['type'] ) {
		case 'int':
		case 'float':
			$altValidationList['min'] = $cObj['min'] ?? 0;
			$altValidationList['max'] = $cObj['max'] ?? 255;
			$altValidationList['not_null'] = $cObj['notNull'] ?? 0;
			break;

		//
		case 'str':
			$altValidationList['minlength'] = $cObj['lMin'] ?? 0;
			$altValidationList['maxlength'] = $cObj['lMax'] ?? 255;
			$altValidationList['filter'] = $cObj['filter'] ?? '';

			//
			switch ( $altValidationList['filter'] ) {
				case 'email':
					$dataOptionsArr['type'] = 'email';
					unset($altValidationList['filter']);
					break;
			}
			break;
		//
		case 'bool':
			unset($dataOptionsArr['required']);
			break;
		//
		case 'color':
			$cObj['type'] = 'hexcolor';

			$altValidationList['minlength'] = 3;
			$altValidationList['maxlength'] = 6;
			break;
		//
		case 'url':
		case 'email':
			$altValidationList['minlength'] = 5;
			$altValidationList['maxlength'] = $cObj['lMax'] ?? 255;
			break;
		//
		case 'moneyInt':
			$altValidationList['min'] = $cObj['min'] ?? 0;
			$altValidationList['max'] = $cObj['max'] ?? 42949672;
			break;
		//
		case 'datetime':
			$altValidationList['minlength'] = 10;
			$altValidationList['maxlength'] = $cObj['lMax'] ?? 12;
			break;
		//
		case 'date':
			$altValidationList['minlength'] = 19;
			$altValidationList['maxlength'] = $cObj['lMax'] ?? 20;
			break;
		//
		case 'file':
			$altValidationList['file_types'] = $cObj['fType'] ?? [];
			$altValidationList['max_size'] = $cObj['mSize'] ?? 512;
			break;
	}

	//
	if ( !empty($altValidationList) ) {
		$dataOptionsArr['validation'] = $altValidationList;
	}

	//
	$objectArray[$cName] = $dataOptionsArr;
}

//
$varExport = var_export($objectArray, true);
$varExport = str_replace(
	['array (', '),', '  '],
	['[', '],', "\t"],
	$varExport
);
$varExport = substr_replace($varExport, ']', -1);

echo <<<TEXT
$varExport
TEXT;
