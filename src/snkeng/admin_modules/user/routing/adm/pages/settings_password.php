<?php
// Pre-check
\snkeng\core\engine\nav::cacheCheckFile(__FILE__);
\snkeng\core\engine\nav::cacheFinalCheck();

//
\snkeng\core\engine\nav::pageFileModuleAdd('admin_modules', 'user', '/pages/user-adm-pages-settings-password.mjs');

//
$page['head']['title'] = 'Contraseña';

//
$page['body'].= <<<HTML
<div class="grid">
<div class="gr_sz04 gr_ps04">

	<div class="pageSubTitle">Contraseña</div>
	
	<user-adm-pages-settings-password>
	
		<form class="se_form" method="post" action="/api/user/settings/password/" is="se-async-form">
			<label class="separator required">
				<div class="cont"><span class="title">Contraseña original</span><span class="desc">Si no tiene, puede estar vacío.</span></div>
				<input type="password" name="pass_orig" minlength="4" maxlength="32" autocomplete="old-password" placeholder="Contraseña actual" required />
			</label>
			<label class="separator required">
				<div class="cont"><span class="title">Contraseña nueva</span><span class="desc">Entre 8 y 32 caracteres. Intente combinar mayúsculas, minúsculas y números. Es recomendable usar el sistema integrado del explorador.</span></div>
				<input type="password" name="pass_new1" minlength="8" maxlength="32" autocomplete="new-password" placeholder="Nueva contraseña" required />
			</label>
			<label class="separator required">
				<div class="cont"><span class="title">Repetir contraseña</span><span class="desc"></span></div>
				<input type="password" name="pass_new2" minlength="8" maxlength="32" autocomplete="new-password" placeholder="Repetir nueva contraseña" required />
			</label>
		
			<button type="submit"><svg class="icon inline mr"><use xlink:href="#fa-save"></use></svg>Guardar</button>
			<output></output>
		</form>
	
	</user-adm-pages-settings-password>

</div>
</div>
HTML;
//
