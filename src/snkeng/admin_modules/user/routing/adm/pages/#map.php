<?php
//
$params['page']['ajax'] = $appData['url_json'];

//
switch ( \snkeng\core\engine\nav::next() ) {
	//
	case 'notifications':
		require __DIR__ . '/notifications.php';
		break;
	//
	case 'settings':
		//
		$params['page']['main'].= '/' . \snkeng\core\engine\nav::current();
		$params['page']['ajax'].= '/' . \snkeng\core\engine\nav::current();

		//
		\snkeng\core\engine\nav::pageFileModuleAdd('admin_modules', 'user', '/pages/user-adm-pages-settings.css');

		//
		switch ( \snkeng\core\engine\nav::next() ) {
			//
			case 'password':
				//
				$params['page']['main'].= '/' . \snkeng\core\engine\nav::current();
				$params['page']['ajax'].= '/' . \snkeng\core\engine\nav::current();

				//
				require __DIR__ . '/settings_password.php';
				break;

			//
			case 'photo':
				//
				$params['page']['main'].= '/' . \snkeng\core\engine\nav::current();
				$params['page']['ajax'].= '/' . \snkeng\core\engine\nav::current();

				//
				require __DIR__ . '/settings_photo.php';
				break;

			//
			case 'preferences':
				//
				$params['page']['main'].= '/' . \snkeng\core\engine\nav::current();
				$params['page']['ajax'].= '/' . \snkeng\core\engine\nav::current();

				//
				require __DIR__ . '/settings_preferences.php';
				break;

			//
			case 'emails':
				//
				$params['page']['main'].= '/' . \snkeng\core\engine\nav::current();
				$params['page']['ajax'].= '/' . \snkeng\core\engine\nav::current();

				//
				require __DIR__ . '/settings_email.php';
				break;

			//
			case 'profile':
				//
				$params['page']['main'].= '/' . \snkeng\core\engine\nav::current();
				$params['page']['ajax'].= '/' . \snkeng\core\engine\nav::current();

				//
				require __DIR__ . '/settings_profile.php';
				break;

			// Página principal
			case '':
			case null:
				require __DIR__ . '/settings_#index.php';
				break;

			// Otros
			default:
				\snkeng\core\engine\nav::invalidPage();
				break;
		}
		break;

	// Página principal
	case '':
	case null:
		require __DIR__ . '/#index.php';
		break;

	// Otros
	default:
		\snkeng\core\engine\nav::invalidPage();
		break;
}
//
