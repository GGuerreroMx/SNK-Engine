<?php
//
switch ( \snkeng\core\engine\nav::next() ) {
	//
	case 'personal':
		$pData = [
			'fName' => ['name' => 'Nombre', 'type' => 'str', 'lMin' => 3, 'lMax' => 50, 'filter' => 'names', 'process' => false, 'validate' => true],
			'lName' => ['name' => 'Apellido', 'type' => 'str', 'lMin' => 3, 'lMax' => 50, 'filter' => 'names', 'process' => false, 'validate' => true],
			'uName' => ['name' => 'Nombre completo', 'type' => 'str', 'lMin' => 7, 'lMax' => 50, 'filter' => 'names', 'process' => false, 'validate' => true],
		];
		$rData = \snkeng\core\general\saveData::postParsing($response, $pData);

		//
		$sql_upd = <<<SQL
UPDATE sb_objects_obj
SET a_fname='{$rData['fName']}', a_lname='{$rData['lName']}', a_obj_name='{$rData['uName']}'
WHERE a_id={$siteVars['user']['data']['id']}
LIMIT 1;
SQL;
		\snkeng\core\engine\mysql::submitQuery($sql_upd);
		break;

	//
	case 'profile':
		$pData = [
			'sn_fb_id' => ['name' => 'FB Id', 'type' => 'str', 'lMin' => 0, 'lMax' => 32, 'filter' => 'numeric', 'validate' => false],
			'sn_fb_nm' => ['name' => 'FB Nm', 'type' => 'str', 'lMin' => 0, 'lMax' => 32, 'filter' => 'simpleText', 'validate' => false],
			'sn_tw_id' => ['name' => 'TW Id', 'type' => 'str', 'lMin' => 0, 'lMax' => 32, 'filter' => 'numeric', 'validate' => false],
			'sn_tw_nm' => ['name' => 'TW Nm', 'type' => 'str', 'lMin' => 0, 'lMax' => 32, 'filter' => 'simpleText', 'validate' => false],
			'desc' => ['name' => 'Description', 'type' => 'str', 'lMin' => 0, 'lMax' => 300, 'filter' => 'textSimple', 'process' => true, 'validate' => false],
		];
		$rData = \snkeng\core\general\saveData::postParsing($response, $pData);
		//
		\snkeng\core\socnet\object_properties::saveArray($siteVars['user']['data']['id'], 'snkeng', 'profile', $rData);
		break;

	// Picture
	case 'profile_upload':
		\snkeng\core\socnet\object_picture::picture_upload($response, $siteVars['user']['data']['id']);
		break;

	//
	case 'profile_crop':
		\snkeng\core\socnet\object_picture::picture_crop($response, $siteVars['user']['data']['id']);
		break;

	// Banner
	case 'banner_upload':
		\snkeng\core\socnet\object_picture::banner_upload($response, $siteVars['user']['data']['id']);
		break;

	//
	case 'banner_crop':
		\snkeng\core\socnet\object_picture::banner_crop($response, $siteVars['user']['data']['id']);
		break;

	//
	default:
		\snkeng\core\engine\nav::invalidPage();
		break;
}
//
