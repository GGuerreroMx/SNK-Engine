import * as domManipulator from '/snkeng/core/res/modules/library/dom-manipulation.mjs';
import * as formActions from '/snkeng/core/res/modules/library/forms.mjs';
import * as simpleForm from '/snkeng/core/res/modules/components-simple/se-async-form.mjs';

//
customElements.define('user-adm-pages-settings-email', class extends HTMLElement {
	//
	mailForm = this.querySelector('form');
	mailTable = this.querySelector('table');
	mailTableTemplate = this.mailTable.querySelector('template');
	mailTableContent = this.mailTable.querySelector('tbody');

	//
	mainUrl = '/api/user/settings/email';

	//
	constructor() {
		super();
	}

	//
	connectedCallback() {

		// Set form callback to add function
		window.customElements.whenDefined('se-async-form').then(() => {
			this.mailForm.dispatchEvent(
				new CustomEvent("setCallBacks", {
					detail: {
						onSuccess: this.formSubmitSuccess.bind(this)
					},
				}),
			);
		});

		// Enable buttons
		this.addEventListener('click', this.onClickCallBacks.bind(this));
	}

	//
	onClickCallBacks(e) {
		let cBtn = e.target.closest('button[data-action]');
		if ( !cBtn ) {
			return;
		}

		//
		let operation = 'onClick_' + cBtn.dataset['action'];

		//
		if ( typeof this[operation] !== 'function' ) {
			console.error("operación no definida.", operation, cBtn);
			return;
		}

		//
		this[operation](e, cBtn);
	}

	//
	onClick_mailValidate(e, cBtn) {
		if ( !confirm('Se enviará un correo electrónico para validar este correo.') ) {
			return;
		}

		let cRow = cBtn.closest('tr'),
			mailId = intVal(cRow.dataset['id']);

		//
		formActions.postRequest(
			this.mainUrl + '/' + mailId+ '/' + mailId+ '/validate',
			null,
				{
					method:'patch',
				onSuccess:(msg) => {
					cBtn.remove();
				},
				onFail:(msg) => {
					alert(msg.title);
				}
			}
		);
	}

	//
	onClick_mailDelete(e, cBtn) {
		if ( !confirm('¿Desea eliminar el presente correo?') ) {
			return;
		}

		//
		let cRow = cBtn.closest('tr'),
			mailId = intVal(cRow.dataset['id']);

		//
		formActions.postRequest(
			this.mainUrl + '/' + mailId+ '/delete',
			null,
{
				method:'delete',
				onSuccess:(msg) => {
					cRow.remove();
				},
				onFail:(msg) => {
					alert(msg.title);
				}
			}
		);
	}

	//
	onClick_mailPrimary(e, cBtn) {
		if ( !confirm('¿Desea convertir este correo en su correo principal?') ) {
			return;
		}

		let cRow = cBtn.closest('tr'),
			mailId = intVal(cRow.dataset['id']);

		//
		formActions.postRequest(
			this.mainUrl + '/' + mailId+ '/primary',
			null,
			{
				method:'patch',
				onSuccess:(msg) => {
					this.mailTable.querySelectorAll('span[data-type="primary"]').dataset['value'] = 0;
					this.mailTable.querySelectorAll('tr').dataset['primary'] = 0;
					cRow.dataset['primary'] = 1;
					cRow.querySelector('span[data-type="primary"]').dataset['value'] = 1;
				},
				onFail:(msg) => {
					alert(msg.title);
				}
			}
		);
	}

	//
	formSubmitSuccess(msg) {
		domManipulator.insertContentFromTemplate(
			this.mailTableContent,
			"beforeend",
			this.mailTableTemplate.innerHTML,
			msg.d
		);
	}

});
