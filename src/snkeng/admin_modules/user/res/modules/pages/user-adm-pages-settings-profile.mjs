import * as formActions from '/snkeng/core/res/modules/library/forms.mjs';
import * as domManipulator from '/snkeng/core/res/modules/library/dom-manipulation.mjs';
import * as simpleForm from '/snkeng/core/res/modules/components-simple/se-async-form.mjs';


//
customElements.define('user-adm-pages-settings-profile', class extends HTMLElement {
	//
	constructor() {
		super();
	}

	//
	connectedCallback() {}
});

//
customElements.define('user-profile-photo', class extends HTMLElement {
	objPhoto = this.querySelector('img');
	//
	photoUploadForm = this.querySelector('form[data-elem="imageUpload"]');
	photoCropForm = this.querySelector('form[data-element="crop"]');

	//
	constructor() {
		super();
	}

	//
	connectedCallback() {
		this.mainUrl = '/api/user/settings/image/';

		// Set form callback to add function
		window.customElements.whenDefined('se-async-form').then(() => {
			this.photoUploadForm.dispatchEvent(
				new CustomEvent("setCallBacks", {
					detail: {
						onSuccess: this.updateImages.bind(this)
					},
				}),
			);
		});

		// Enable buttons
		this.addEventListener('click', this.onClickCallBacks.bind(this));
	}

	//
	onClickCallBacks(e) {
		let cBtn = e.target.closest('button[data-action]');
		if ( !cBtn ) {
			return;
		}

		//
		let operation = 'onClick_' + cBtn.dataset['action'];

		//
		if ( typeof this[operation] !== 'function' ) {
			console.error("operación no definida.", operation, cBtn);
			return;
		}

		//
		this[operation](e, cBtn);
	}


	updateImages(msg) {
		let nUrl = '/res/image/objects',
			imageEnd = msg.d.img_crop + '.webp?' + Date.now();

		//
		switch ( this.dataset['type'] ) {
			case 'banner':
				//
				this.objPhoto.src = nUrl + '/w_450/' + imageEnd;
				break;
			case 'profile':
				//
				this.objPhoto.src = nUrl + '/w_160/' + imageEnd;
				// Modify general site image
				$('#se_user img').src = nUrl + '/w_35/' + imageEnd;
				break;
			default:
				console.error();
				break;
		}
	}
});

/*

// Plugin menu
se.plugin.se_usr_settings_photo = function (plugElem, plugSettings) {
	const //
		mainUrl = '/api/user/settings/' + '/image',
		//
		actionMain = this.querySelector('div.action[data-name="main"]'),
		objPhoto = actionMain.querySelector('img[se-type="userPhoto"]'),
		//
		actionUpload = this.querySelector('div.action[data-name="photoUpload"]'),
		photoUploadForm = actionUpload.querySelector('form'),
		//
		actionAdjust = this.querySelector('div.action[data-name="photoAdjust"]'),
		photoAdjustForm = actionAdjust.querySelector('form'),
		photoCanvas = actionAdjust.querySelector('canvas');

	//
	function init() {
		// Plugin
		photoCanvas.se_plugin('cropper', {
			onNewSel:(data) => {
				photoAdjustForm.se_formElVal('img_x', data.x);
				photoAdjustForm.se_formElVal('img_y', data.y);
				photoAdjustForm.se_formElVal('img_w', data.w);
				photoAdjustForm.se_formElVal('img_h', data.h);
			},
			ratio:'1:1',
			min:'160x160'
		});
		//
		photoUploadForm.se_plugin('simpleForm', {
			save_url: mainUrl + '/upload',
			onSuccess:(msg) => {
				updateImages(msg.d.img_crop);
			}
		});
		photoAdjustForm.se_plugin('simpleForm', {
			save_url: mainUrl + '/crop',
			onSuccess:(msg) => {
				updateImages(msg.d.img_crop);
			}
		});

		//
		this.se_on('click', 'button[se-act]', bntAction);
	}

	//
	function bntAction(e, cBtn) {
		e.preventDefault();
		//
		switch ( cBtn.getAttribute('se-act') ) {
			//
			case 'actionSwitch':
				actionSwitch(cBtn.dataset['action']);
				break;
			//
			default:
				console.error("op not valid.");
				break;
		}
	}


	//
	function updateImages(imgName) {
		let nUrl = '/res/image/objects',
			imageEnd = imgName + '?' + uniqId();

		//
		objPhoto.src = nUrl + '/w_160/' + imageEnd;
		$('#se_user img').src = nUrl + '/w_35/' + imageEnd;
	}


	//
	function actionSwitch(actName) {
		//
		this.querySelectorAll('div.action').dataset['active'] = 0;
		this.querySelectorAll('div.action[data-name="' + actName + '"]').dataset['active'] = 1;

		if ( actName === 'photoAdjust' ) {
			photoCanvas.cropper.canvasReDraw();
		}
	}

	//
	init();
	//
	return {};
};
//
*/