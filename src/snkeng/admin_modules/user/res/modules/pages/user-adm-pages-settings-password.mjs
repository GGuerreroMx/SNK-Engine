import * as simpleForm from '/snkeng/core/res/modules/components-simple/se-async-form.mjs';

//
customElements.define('user-adm-pages-settings-password', class extends HTMLElement {
	structureForm = this.querySelector('form');

	//
	constructor() {
		super();
	}

	//
	connectedCallback() {
		this.structureForm.se_on('change', this.passCheck.bind(this));
	}

	//
	passCheck() {
		const
			inputNewPass01 = this.querySelector('input[name="pass_new1"]'),
			inputNewPass02 = this.querySelector('input[name="pass_new2"]');

		//
		console.log("checking password", inputNewPass01.value, inputNewPass02.value, inputNewPass01.checkValidity(), inputNewPass02.checkValidity());

		//
		inputNewPass02.setCustomValidity('');
		if ( inputNewPass01.checkValidity() && inputNewPass02.value !== '' && inputNewPass01.value !== inputNewPass02.value ) {
			console.log("test faailed, report");
			inputNewPass02.setCustomValidity("Las nuevas contraseñas no coinciden.");
		}
		//
	}
});
