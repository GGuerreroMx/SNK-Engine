// Plugin menu
se.plugin.se_usr_settings_mail = function (plugElem, plugSettings) {
	let defaults = {},
		settings = se.object.merge(defaults, plugSettings),
		//
		mainUrl = apps.usr_settings.url + '/email',
		//
		mailForm = plugElem.querySelector('form'),
		mailTable = plugElem.querySelector('table'),
		mailTableTemplate = mailTable.querySelector('template'),
		mailTableContent = mailTable.querySelector('tbody');

	//
	function init() {
		//
		mailForm.se_plugin('simpleForm', {
			save_url:mainUrl + '/add',
			onSuccess:mailFormAddSuccess
		});
		plugElem.se_on('click', 'button[se-act]', buttonAction);
	}

	//
	function buttonAction(ev, cBtn) {
		ev.preventDefault();
		//
		switch ( cBtn.se_attr('se-act') ) {
			//
			case 'primary':
				if ( confirm('¿Desea convertir este correo en su correo principal?') ) {
					mailPrimary(cBtn);
				}
				break;
			//
			case 'activate':
				if ( confirm('Se enviará un correo electrónico para validar este correo.') ) {
					mailValidate(cBtn);
				}
				break;
			//
			case 'delete':
				if ( confirm('¿Eliminar este correo?') ) {
					mailDelete(cBtn);
				}
				break;
			//
			default:
				console.error("op not valid.");
				break;
		}
	}

	//
	function mailFormAddSuccess(msg) {
		mailTableContent.se_site_coreend(se.struct.stringPopulate(mailTableTemplate.se_html(), msg.d));
	}

	//
	function mailPrimary(cBtn) {
		let cRow = cBtn.se_closest('tr'),
			mailId = intVal(cRow.se_data('id'));

		//
		se.ajax.json(mainUrl + '/primary', {mailId:mailId}, {
			onSuccess:(msg) => {
				mailTable.querySelectorAll('span[data-type="primary"]').se_data('value', 0);
				mailTable.querySelectorAll('tr').se_data('primary', 0);
				cRow.se_data('primary', 1);
				cRow.querySelector('span[data-type="primary"]').se_data('value', 1);
			},
			onFail:(msg) => {
				se.struct.errorAlert(msg);
			}
		});
	}

	//
	function mailValidate(cBtn) {
		let cRow = cBtn.se_closest('tr'),
			mailId = intVal(cRow.se_data('id'));

		//
		se.ajax.json(mainUrl + '/validate', {mailId:mailId}, {
			onSuccess:(msg) => {
				cBtn.se_remove();
			},
			onFail:(msg) => {
				se.struct.errorAlert(msg);
			}
		});
	}

	//
	function mailDelete(cBtn) {
		let cRow = cBtn.se_closest('tr'),
			mailId = intVal(cRow.se_data('id'));

		//
		se.ajax.json(mainUrl + '/delete', {mailId:mailId}, {
			onSuccess:(msg) => {
				cRow.se_remove();
			},
			onFail:(msg) => {
				se.struct.errorAlert(msg);
			}
		});
	}

	//
	init();
	//
	return {};
};
//
