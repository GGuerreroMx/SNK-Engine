<?php
$header = [];
$header['title'] = 'Usuario';
$header['menu'] = <<<HTML
<div class="nav3_menu_parent">
	<div class="nav3_menu_button"><a se-nav="app_content" href="{$appData['url']}/settings/">Inicio</a></div>
</div>
<div class="nav3_menu_parent">
	<div class="nav3_menu_button"><a se-nav="app_content" href="{$appData['url']}/settings/profile/">Perfil online</a></div>
</div>
<div class="nav3_menu_parent">
	<div class="nav3_menu_button"><a se-nav="app_content" href="{$appData['url']}/settings/preferences/">Preferencias</a></div>
</div>
<div class="nav3_menu_parent">
	<div class="nav3_menu_button"><a se-nav="app_content" href="{$appData['url']}/settings/password/">Contraseña</a></div>
</div>
<div class="nav3_menu_parent">
	<div class="nav3_menu_button"><a se-nav="app_content" href="{$appData['url']}/settings/emails/">Correos</a></div>
</div>
HTML;

return $header;
