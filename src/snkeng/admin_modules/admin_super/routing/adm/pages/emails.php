<?php
// Cache Check
\snkeng\core\engine\nav::cacheCheckFile(__FILE__);
\snkeng\core\engine\nav::cacheCheckFile($_SERVER['DOCUMENT_ROOT'] . '/snkeng/core/class/general/dynamicTable.php');
\snkeng\core\engine\nav::cacheFinalCheck();

//
$params['page']['ajax'].= '/emails';



// Estructura
$an_struct = <<<HTML
<tr data-element="row" data-objid="!id;" data-objtitle="!name;">
	<td>!id;</td>
	<td>!email;</td>
	<td><span class="fakeCheck" data-value="!mailStatus;"></span></td>
	<td>!mailType;</td>
	<td>!userId;</td>
	<td>!userName;</td>
	<td>!userUrlName;</td>
	<td>!dtAdd;</td>
	<td>!userLevel;</td>
	<td>!userType;</td>
</tr>
HTML;
//
$an_empty = "No hay elementos que mostrar.";

//
$exData = [
	'js_url' => $params['page']['ajax'].'/readAll',
	
	'printType' => 'table',
	'tableWide' => true,
	'tableHead'=> [
		['name' => 'ID', 'filter' => 1, 'fName'=>'id'],
		['name' => 'Email', 'filter' => 1, 'fName'=>'email'],
		['name' => 'Validado', 'filter' => 'checkbox', 'fName' => 'mailStatus'],
		['name' => 'Tipo'],
		['name' => 'ID Usuario'],
		['name' => 'Nombre', 'filter' => 1, 'fName'=>'usrName'],
		['name' => 'Nombre URL', 'filter' => 1, 'fName' => 'usrUrl'],
		['name' => 'Creado'],
		['name' => 'Nivel', 'filter' => 'list', 'fName' => 'usrAdmLevel', 'data' => [
			'0' => 'U. Admin',
			'1' => 'S. Admin',
			'2' => 'Admin',
			'3' => 'Usuario',
		]],
		['name' => 'Sub Nivel (adm)', 'filter' => 1, 'fName' => 'usrAdmSubLevel'],
	],
	'settings' => ['nav'=>true],
	'actions' => <<<JSON
{
	"del":{
		"action":"del",
		"ajax-elem":"obj",
		"del_url":"{$params["page"]["ajax"]}/del"
	}
}
JSON
];

// debugVariable($post->sql_qry, '', true);
$tableStructure = \snkeng\core\general\dynamicTable::createElement($an_struct, $exData);

//
$page['head']['title'] = "Correos electrónicos";

// Page
$page['body'] = <<<HTML
<div class="pageSubTitle">Correos electrónicos</div>

{$tableStructure}
HTML;
//
