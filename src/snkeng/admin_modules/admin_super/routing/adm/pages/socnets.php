<?php
// Estructura
$an_struct = <<<HTML
<tr data-element="row" data-objid="!id;" data-objtitle="!name;">
	<td>!id;</td>
	<td><svg class="icon"><use xlink:href="#fa-!snType;" /></svg></td>
	<td>!snId;</td>
	<td>!userId;</td>
	<td>!userName;</td>
	<td>!userUrlName;</td>
	<td>!dtAdd;</td>
	<td>!userLevel;</td>
	<td>!userType;</td>
</tr>
HTML;
//

//
$exData = [
	'js_url' => $params['page']['ajax'].'/socnets/readAll',
	'printType'=>'table',
	'tableWide' => true,
	'printContent' => false,
	'tableHead'=>[
		['name' => 'ID', 'filter' => 1, 'fName'=>'id'],
		['name' => 'SN', 'filter'=>'list', 'fName' => 'snType', 'data' => [
			'1' => 'FB',
			'2' => 'Tw',
			'3' => 'G+',
		]],
		['name' => 'SN Id', 'filter' => 1, 'fName'=>'snId'],
		['name' => 'ID Usuario'],
		['name' => 'Nombre', 'filter' => 1, 'fName'=>'name'],
		['name' => 'Nombre URL', 'filter' => 1, 'fName' => 'nameurl'],
		['name' => 'Creado'],
		['name' => 'Nivel', 'filter' => 'list', 'fName' => 'usrAdmLevel', 'data' => [
			'0' => 'U. Admin',
			'1' => 'S. Admin',
			'2' => 'Admin',
			'3' => 'Usuario',
		]],
		['name' => 'Sub Nivel (adm)', 'filter' => 1, 'fName' => 'usrAdmSubLevel'],
	],
	'settings'=>['nav'=>true],
	'actions' => <<<JSON
{
	"del":{"action":"del", "ajax-elem":"obj", "del_url":"{$params["page"]["ajax"]}/del"}
}
JSON
];

// debugVariable($post->sql_qry, '', true);
$tableStructure = \snkeng\core\general\dynamicTable::createElement($an_struct, $exData);

$page['head']['title'] = "Redes sociales";

// Page
$page['body'] = <<<HTML
<div class="pageTitle">Redes sociales</div>

{$tableStructure}
HTML;
//
