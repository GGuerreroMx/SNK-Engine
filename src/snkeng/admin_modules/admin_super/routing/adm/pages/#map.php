<?php
// Permission
\snkeng\core\engine\login::kill_loginLevel('sadmin');

//
$params['page']['main'] = $appData['url'];
$params['page']['ajax'] = $appData['url_json'];

//
switch ( \snkeng\core\engine\nav::next() ) {
	//
	case 'users':
		$params['page']['ajax'].= '/' . \snkeng\core\engine\nav::current();
		$params['page']['main'].= '/' . \snkeng\core\engine\nav::current();

		//
		$params['vars']['type'] = \snkeng\core\engine\nav::current();
		$params['vars']['typeName'] = 'Usuarios';

		//
		if ( !is_numeric(\snkeng\core\engine\nav::next(false)) ) {
			require __DIR__ . '/users_#index.php';
		} else {
			$params['vars']['objId'] = intval(\snkeng\core\engine\nav::next());

			//
			$params['page']['ajax'].= '/' . \snkeng\core\engine\nav::current();
			$params['page']['main'].= '/' . \snkeng\core\engine\nav::current();

			//
			require __DIR__ . '/users_#map.php';
		}
		break;
	//
	case 'objects':
		$params['page']['ajax'].= '/' . \snkeng\core\engine\nav::current();
		$params['page']['main'].= '/' . \snkeng\core\engine\nav::current();

		//
		$params['vars']['type'] = \snkeng\core\engine\nav::current();
		$params['vars']['typeName'] = 'Objetos';

		//
		if ( !is_numeric(\snkeng\core\engine\nav::next(false)) ) {
			require __DIR__ . '/objects_#index.php';
		} else {
			$params['vars']['objId'] = intval(\snkeng\core\engine\nav::next());

			//
			$params['page']['ajax'].= '/' . \snkeng\core\engine\nav::current();
			$params['page']['main'].= '/' . \snkeng\core\engine\nav::current();

			//
			require __DIR__ . '/objects_#map.php';
		}
		break;
	//
	case 'emails':
		require __DIR__ . '/emails.php';
		break;
	//
	case 'socnets':
		require __DIR__ . '/socnets.php';
		break;
	//
	case 'sendmail':
		require __DIR__ . '/sendmail.php';
		break;
	//
	case 'adminLevels':
		require __DIR__ . '/adminlevels.php';
		break;
	//
	case 'smartLinks':
		require __DIR__ . '/smartLinks.php';
		break;
	//
	case 'notifications':
		require __DIR__ . '/+objects_[id]_notifications.php';
		break;

	//
	case 'uadmin':
		//
		\snkeng\core\engine\login::kill_loginLevel('uadmin');

		//
		require __DIR__ . '/uadmin/#map.php';
		break;

	// Página principal
	case '':
	case null:
		require __DIR__ . '/#index.php';
		break;

	// Otros
	default:
		\snkeng\core\engine\nav::invalidPage();
		break;
	//</editor-fold>
}
//
