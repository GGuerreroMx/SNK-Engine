<?php

//
switch ( \snkeng\core\engine\nav::next() ) {
	//
	case '':
	case null:
		require __DIR__ . '/+objects_[id]_index.php';
		break;

	//
	case 'properties':
		\snkeng\core\engine\nav::pageFileGroupAdd(['objects']);
		require __DIR__ . '/+objects_[id]_properties.php';
		break;

	//<editor-fold desc="User only">

	//
	case 'emails':
		require __DIR__ . '/+objects_[id]_user_emails.php';
		break;
	//
	case 'socnets':
		require __DIR__ . '/+objects_[id]_user_socnets.php';
		break;

	//</editor-fold>

	//
	case 'settings':
		require __DIR__ . '/+objects_[id]_settings.php';
		break;

	//
	case 'notifications':
		require __DIR__ . '/+objects_[id]_notifications.php';
		break;

	//
	case 'relationships_in':
		require __DIR__ . '/+objects_[id]_relationships_in.php';
		break;

	//
	case 'relationships_out':
		require __DIR__ . '/+objects_[id]_relationships_out.php';
		break;

	//
	default:
		\snkeng\core\engine\nav::invalidPage();
		break;
}

// Single
$page['body'] = <<<HTML
<div class="pageTitle">Usuarios</div>

<div class="nav4">
	<div class="breadcrumbs">
		<a se-nav="app_content" href="{$appData['url']}/users">Usuarios</a>
	</div>
	<div class="normal">
		<a se-nav="app_content" href="{$appData['url']}/users/{$params['vars']['objId']}/properties">Propiedades</a>
		<a se-nav="app_content" href="{$appData['url']}/users/{$params['vars']['objId']}/emails">Correos</a>
		<a se-nav="app_content" href="{$appData['url']}/users/{$params['vars']['objId']}/socnets">Redes Sociales</a>
		<a se-nav="app_content" href="{$appData['url']}/users/{$params['vars']['objId']}/notifications">Notificaciones</a>
		<a se-nav="app_content" href="{$appData['url']}/users/{$params['vars']['objId']}/settings">Configuración</a>
		<a se-nav="app_content" href="{$appData['url']}/users/{$params['vars']['objId']}/relationships_in">Relaciones Internas</a>
		<a se-nav="app_content" href="{$appData['url']}/users/{$params['vars']['objId']}/relationships_out">Relaciones Externas</a>
	</div>
</div>

{$page['body']}\n
HTML;
//