<?php

//
$permList = '';
$user_perms_file = $_SERVER['DOCUMENT_ROOT'] . '/snkeng/site_core/specs/admin_permits.php';
if ( file_exists($user_perms_file) ) {
	$usr_perms_data = (require $user_perms_file);
	$first = true;
	foreach ( $usr_perms_data as $index => $data ) {
		$permList.= ( $first ) ? '' : ',';
		$permList.= "{$index}:'{$data['name']}'";
		$first = false;
	}
} else {
	$permList = "0:'No hay niveles.'";
}

// Estructura
$an_struct = <<<HTML
<tr data-element="row" data-objid="!id;" data-objtitle="!name;">
	<td>!id;</td>
	<td>!fullName;</td>
	<td>!urlName;</td>
	<td>!dtAdd;</td>
	<td>!level;</td>
	<td>!sublevel;</td>
	<td><span class="fakeCheck" data-value="!status;"></span></td>
	<td class="actions">
		<a class="btn small" se-nav="app_content" href="{$appData['url']}/users/!id;/" title="Ver"><svg class="icon inline"><use xlink:href="#fa-eye"/></svg></a>
		<button class="btn small" data-dyntab-onclick="element_op" data-action="name" title="Nombres"><svg class="icon inline"><use xlink:href="#fa-pencil"/></svg></button>
		<button class="btn small" data-dyntab-onclick="element_op" data-action="url" title="URL"><svg class="icon inline"><use xlink:href="#fa-chain"/></svg></button>
		<button class="btn small" data-dyntab-onclick="element_op" data-action="permits" title="Permisos"><svg class="icon inline"><use xlink:href="#fa-gear"/></svg></button>
		<button class="btn small" data-dyntab-onclick="element_op" data-action="password" title="Contraseña"><svg class="icon inline"><use xlink:href="#fa-key"/></svg></button>
		<button class="btn small" data-dyntab-onclick="element_op" data-action="del" title="Borrar"><svg class="icon inline"><use xlink:href="#fa-trash-o"/></svg></button>
	</td>
</tr>
HTML;
//

//
$exData = [
	'js_url' => $params['page']['ajax'].'/readAll',
	
	'printType'=>'table',
	'tableWide' => true,
	'tableHead'=>[
		['name' => 'ID', 'filter' => 1, 'fName'=>'id'],
		['name' => 'Nombre', 'filter' => 1, 'fName'=>'name'],
		['name' => 'Nombre URL', 'filter' => 1, 'fName' => 'urlname'],
		['name' => 'Creado'],
		['name' => 'Nivel', 'filter' => 'list', 'fName' => 'admLevel', 'data' => [
			'0' => 'U. Admin',
			'1' => 'S. Admin',
			'2' => 'Admin',
			'3' => 'Usuario',
		]],
		['name' => 'Sub Nivel (adm)', 'filter' => 1, 'fName' => 'admSubLevel'],
		['name' => 'Habilitado', 'filter' => 'checkbox', 'fName' => 'status'],
		['name' => 'Acciones'],
	],
	'settings' => ['nav' => true ],
	'actions' => <<<JSON
{
	"add":
	{
		"ajax-action":"add",
		"ajax-elem":"none",
		"menu":{"title":"Agregar", "icon":"fa-plus"},
		"print":false,
		"action":"form",
		"form":{
			"title":"Nuevo",
			"save_url":"{$params["page"]["ajax"]}/add",
			"actName":"Agregar",
			"elems":{
				"fname": {
					"field_type": "input",
					"data_type": "string",
					"info_name": "Nombre",
					"attributes": {
						"minlength": 3,
						"maxlength": 50,
						"data-regexp": "names"
					}
				},
				"lname": {
					"field_type": "input",
					"data_type": "string",
					"info_name": "Apellido",
					"attributes": {
						"minlength": 3,
						"maxlength": 50,
						"data-regexp": "names"
					}
				},
				"email": {
					"field_type": "input",
					"data_type": "string",
					"info_name": "Correo Elecrónico",
					"attributes": {
						"type": "email",
						"minlength": 6,
						"maxlength": 100
					}
				},
				"password": {
					"field_type": "input",
					"data_type": "string",
					"info_name": "Contraseña",
					"attributes": {
						"minlength": 6,
						"maxlength": 32,
						"data-regexp": "password"
					}
				},
				"level": {
					"field_type": "list",
					"default_value": 3,
					"info_name": "Nivel",
					"attributes": {
						"options": [
							"Uber Administrador",
							"Super Administrador",
							"Administrador",
							"Usuario"
						]
					}
				},
				"sendmail": {
					"field_type": "checkbox",
					"data_type": "bool",
					"info_name": "Enviar correo electrónico",
					"attributes": {
						"value": 1
					}
				}
			}
		}
	},
	"name":
	{
		"ajax-action":"edit",
		"ajax-elem":"obj",
		"action":"form",
		"form":{
			"title":"Nombre",
			"load_url":"{$params["page"]["ajax"]}/{id}/properties/nameRead",
			"save_url":"{$params["page"]["ajax"]}/{id}/properties/nameUpd",
			"actName":"Guardar",
			"elems":{
				"id": {
					"field_type": "hidden",
					"data_type": "int"
				},
				"fName": {
					"field_type": "input",
					"data_type": "string",
					"info_name": "Nombre",
					"attributes": {
						"minlength": 0,
						"maxlength": 255,
						"data-regexp": "names"
					}
				},
				"lName": {
					"field_type": "input",
					"data_type": "string",
					"info_name": "Apellido",
					"attributes": {
						"minlength": 0,
						"maxlength": 255,
						"data-regexp": "names"
					}
				},
				"uName": {
					"field_type": "input",
					"data_type": "string",
					"info_name": "Nombre completo",
					"attributes": {
						"minlength": 0,
						"maxlength": 255,
						"data-regexp": "names"
					}
				}
			}
		}
	},
	"url":
	{
		"ajax-action":"edit",
		"ajax-elem":"obj",
		"action":"form",
		"form":{
			"title":"URL",
			"load_url":"{$params["page"]["ajax"]}/{id}/properties/urlRead",
			"save_url":"{$params["page"]["ajax"]}/{id}/properties/urlUpd",
			"actName":"Guardar",
			"elems":{
				"id": {
					"field_type": "hidden",
					"data_type": "int"
				},
				"url": {
					"field_type": "input",
					"data_type": "string",
					"info_name": "URL",
					"attributes": {
						"minlength": 0,
						"maxlength": 255,
						"data-regexp": null
					}
				}
			}
		}
	},
	"permits":
	{
		"ajax-action":"edit",
		"ajax-elem":"obj",
		"ajax-update":false,
		"action":"form",
		"form":{
			"title":"Permisos",
			"load_url":"{$params["page"]["ajax"]}/{id}/properties/permitsRead",
			"save_url":"{$params["page"]["ajax"]}/{id}/properties/permitsUpd",
			"actName":"Guardar",
			"elems":{
				"id": {
					"field_type": "hidden",
					"data_type": "int"
				},
				"level": {
					"field_type": "list",
					"default_value": 3,
					"info_name": "Nivel",
					"attributes": {
						"options": {
							"0": "Uber Administrador",
							"1": "Super Administrador",
							"2": "Administrador",
							"3": "Usuario"
						}
					}
				},
				"sublevel": {
					"field_type": "list",
					"data_type": "int",
					"info_name": "Tipo (Admin)",
					"attributes": {
						"options": {{$permList}}
					}
				},
				"status": {
					"field_type": "list",
					"data_type": "str",
					"info_name": "Condición",
					"attributes": {
						"options": {
							"1": "Normal",
							"2": "Bloqueado"
						}
					}
				}
			}
		}
	},
	"password":
	{
		"ajax-action":"edit",
		"ajax-elem":"obj",
		"ajax-update":false,
		"action":"form",
		"form":{
			"title":"Contraseña",
			"save_url":"{$params["page"]["ajax"]}/{id}/properties/password",
			"actName":"Guardar",
			"elems": {
				"password": {
					"field_type": "input",
					"data_type": "string",
					"info_name": "Contraseña nueva",
					"attributes": {
						"minlength": 0,
						"maxlength": 255,
						"data-regexp": null
					}
				},
				"sendmail": {
					"field_type": "checkbox",
					"data_type": "bool",
					"info_name": "Enviar correo electrónico",
					"attributes": {
						"value": 1
					}
				}
			}
		}
	},
	"del":{
		"action":"del",
		"ajax-elem":"obj",
		"del_url":"{$params["page"]["ajax"]}/{id}/del"
	}
}
JSON
];

//
$tableStructure = \snkeng\core\general\dynamicTable::createElement($an_struct, $exData);

// debugVariable($post->sql_qry, '', true);

$page['head']['title'] = "Usuarios";

// Page
$page['body'] = <<<HTML
<div class="pageTitle">Usuarios</div>

{$tableStructure}
HTML;
//
