<?php
// Pre-check
\snkeng\core\engine\nav::cacheCheckFile(__FILE__);
\snkeng\core\engine\nav::cacheFinalCheck();

//
$params['page']['ajax'].= '/relationships';
$params['page']['main'].= '/relationships';

// Estructura
$an_struct = <<<HTML
<tr data-element="row" data-objid="!id;" data-objtitle="!objName;">
	<td>!id;</td>
	<td>!objName;</td>
	<td>!status;</td>
	<td><span class="fakeCheck" data-value="!isBlocked;"></span></td>
	<td><span class="fakeCheck" data-value="!isSubscribed;"></span></td>
	<td><span class="fakeCheck" data-value="!isCommended;"></span></td>
	<td>!admLevel;</td>
	<td>!admType;</td>
	<td>!dtAdd;</td>
	<td>!dtMod;</td>
	<td class="actions">
		<a class="btn" href="/@!objUrl;/" target="_blank"><svg class="icon inline"><use xlink:href="#fa-eye" /></svg></a>
		<button class="btn small" data-dyntab-onclick="element_op" data-action="edit" title="Editar"><svg class="icon inline"><use xlink:href="#fa-pencil" /></svg></button>
		<button class="btn" se-act="del"><svg class="icon inline"><use xlink:href="#fa-trash-o" /></svg></button>
	</td>
</tr>
HTML;
//

//
$exData = [
	'js_url' => $params['page']['ajax'] . '/readSingleIn',
	
	'printType'=>'table',
	'tableWide' => true,
	'tableHead'=>[
		['name' => 'ID', 'filter' => 1, 'fName'=>'id'],
		['name' => 'Nombre', 'filter' => 1, 'fName'=>'objName'],
		['name' => 'Status'],
		['name' => 'Bloqueado'],
		['name' => 'Suscrito'],
		['name' => 'Recomendado'],
		['name' => 'Adm. Nivel'],
		['name' => 'Adm. Tipo'],
		['name' => 'Dt Add'],
		['name' => 'Dt Mod'],
		['name' => 'Acciones'],
	],
	'settings'=>['nav'=>true],
	'actions' => <<<JSON
{
	"add":
	{
		"ajax-action":"add",
		"ajax-elem":"none",
		"menu":{"title":"Agregar", "icon":"fa-plus"},
		"print":false,
		"action":"form",
		"form":{
			"title":"Nueva",
			"save_url":"{$params["page"]["ajax"]}/add",
			"actName":"Agregar",
			"elems":{
				"objId_target": {
					"field_type": "hidden",
					"data_type": "int",
					"default_value": "{$params["vars"]["objId"]}",
					"attributes": []
				},
				"objId_from": {
					"field_type": "input",
					"data_type": "number",
					"required": false,
					"info_name": "ID del Objeto",
					"info_description": "Objeto para agregar la relación",
					"attributes": {
						"type": "number",
						"min": 0,
						"max": 4294967295
					}
				}
			}
		}
	},
	"edit":
	{
		"ajax-action":"edit",
		"ajax-elem":"obj",
		"action":"form",
		"form":{
			"title":"Editar",
			"load_url":"{$params["page"]["ajax"]}/readSingle",
			"save_url":"{$params["page"]["ajax"]}/upd",
			"defaults":{},
			"actName":"Guardar",
			"elems":{
				"status": {
					"field_type": "input",
					"data_type": "number",
					"required": false,
					"info_name": "Status",
					"info_description": "Status general de la relación (creo qeu ya no se usa)",
					"attributes": {
						"type": "number",
						"min": 0,
						"max": 10
					}
				},
				"isBlocked": {
					"field_type": "checkbox",
					"data_type": "bool",
					"info_name": "Bloqueado",
					"info_description": "",
					"attributes": {
						"value": 1
					}
				},
				"isCommend": {
					"field_type": "checkbox",
					"data_type": "bool",
					"info_name": "Recomendado",
					"info_description": "",
					"attributes": {
						"value": 1
					}
				},
				"isSubscribed": {
					"field_type": "checkbox",
					"data_type": "bool",
					"info_name": "Subscrito",
					"info_description": "",
					"attributes": {
						"value": 1
					}
				},
				"admLevel": {
					"field_type": "list",
					"data_type": "",
					"default_value": 0,
					"required": false,
					"info_name": "Admin Level",
					"info_description": "",
					"attributes": {
						"options": null
					}
				},
				"admType": {
					"field_type": "input",
					"data_type": "string",
					"required": false,
					"info_name": "Admin Type",
					"info_description": "",
					"attributes": {
						"minlength": 0,
						"maxlength": 10,
						"data-regexp": "simpleTitle"
					}
				}
			}
		}
	},
	"del":{"action":"del", "ajax-elem":"obj", "del_url":"{$params["page"]["ajax"]}/del"}
}
JSON
];
//

//
$tableStructure = \snkeng\core\general\dynamicTable::createElement($an_struct, $exData);

// Page
$page['body'] = <<<HTML
<div class="pageSubTitle">Relaciones Internas</div>
<div>Relaciones que recibe este objeto de otros. Admin.</div>

{$tableStructure}
HTML;
//
