<?php
// Pre-check
\snkeng\core\engine\nav::cacheCheckFile(__FILE__);
\snkeng\core\engine\nav::cacheFinalCheck();

//
$params['page']['ajax'].= '/' . \snkeng\core\engine\nav::current();
$params['page']['main'].= '/' . \snkeng\core\engine\nav::current();

// Estructura
$an_struct = <<<HTML
<tr data-element="row" data-objid="!id;" data-objtitle="!name;">
	<td>!id;</td>
	<td>!userName;</td>
	<td>!appName;</td>
	<td>!actName;</td>
	<td>!actId;</td>
	<td><time datetime="!dtAddFull;" title="!dtAddFull;">!dtAdd;</time></td>
	<td><time datetime="!dtModFull;" title="!dtModFull;">!dtMod;</time></td>
	<td></td>
</tr>
HTML;
//
$exData = [
	'js_url' => $params['page']['ajax'].'/readSingle',
	'printType'=>'table',
	'tableWide' => true,
	'tableHead'=>[
		['name' => 'ID', 'filter' => 1, 'fName'=>'id'],
		['name' => 'Usuario', 'filter' => 1, 'fName'=>'userName'],
		['name' => 'App'],
		['name' => 'Act'],
		['name' => 'Act Id'],
		['name' => 'Agregado'],
		['name' => 'Modificato'],
		['name' => 'Acciones'],
	],
	'settings'=>['nav'=>true],
	'actions' => <<<JSON
{
	"del":{"action":"del", "ajax-elem":"obj", "del_url":"{$params["page"]["ajax"]}/del"}
}
JSON
];

//
$tableStructure = \snkeng\core\general\dynamicTable::createElement($an_struct, $exData);

// Page
$page['body'] = <<<HTML
<div class="pageSubTitle">Notificaciones</div>

{$tableStructure}
HTML;
//
