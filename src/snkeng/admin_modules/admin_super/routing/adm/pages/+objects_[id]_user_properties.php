<?php
// Pre-check
\snkeng\core\engine\nav::cacheCheckFile(__FILE__);
\snkeng\core\engine\nav::cacheFinalCheck();

//
$params['page']['ajax'].= '/' . \snkeng\core\engine\nav::current();
$params['page']['main'].= '/' . \snkeng\core\engine\nav::current();

// OBTENER DATOS
$sql_qry = <<<SQL
SELECT
	user.a_fname AS fName, user.a_lname AS lName, user.a_obj_name AS fullName,
	user.a_adm_level AS level, user.a_adm_type AS sublevel, user.a_status AS status,
	user.a_url_name_full AS objUrl,
	user.a_img_full AS imgFull, user.a_img_crop AS imgCrop,
	user.a_banner_full AS bannerFull, user.a_banner_crop AS bannerCrop
FROM sb_objects_obj AS user
WHERE a_id={$params['vars']['objId']}
LIMIT 1;
SQL;
$userData = \snkeng\core\engine\mysql::singleRowAssoc($sql_qry, [], [
	'errorKey' => '',
	'errorDesc' => '',
]);


$userPicture = '';
if ( $userData['imgFull'] !== 'default' ) {
	$fullFile = '/se_files/user_upload/img/objects/'.$userData['imgFull'];
	$autoLoadPicture = ( file_exists($_SERVER['DOCUMENT_ROOT'].$fullFile) ) ? $fullFile : '';

	$userPicture = '/res/image/objects/w_160/'.$userData['imgCrop'].'.jpg';
}

$userBanner = '';
if ( $userData['bannerFull'] !== 'default' ) {
	$fullFile = '/se_files/user_upload/img/objects/'.$userData['bannerFull'];
	$autoLoadBanner = ( file_exists($_SERVER['DOCUMENT_ROOT'].$fullFile) ) ? $fullFile : '';

	$userBanner = '/res/image/objects/w_450/'.$userData['bannerCrop'].'.jpg';
}

// Permisos
$permList = '';
$user_perms_file = $_SERVER['DOCUMENT_ROOT'] . '/snkeng/site_core/specs/admin_permits.php';
if ( file_exists($user_perms_file) ) {
	$usr_perms_data = (require $user_perms_file);
	foreach ( $usr_perms_data as $index => $data ) {
		$permList.= "<option value='{$index}'>{$data['name']}</option>";
	}
} else {
	$permList = '<option value="">No hay niveles.</option>';
}

// Page
$page['body'] = <<<HTML
<div class="pageSubTitle">Propiedades generales</div>

<div class="grid">

	<div class="gr_sz04">
		
		<div class="gOMB">
			<h2>Foto Banner</h2>
			<div id="objectPhoto" se-plugin="object_image" data-type="banner" data-url="{$params['page']['ajax']}/image" data-image="{$userBanner}" data-id="{$params['vars']['objId']}" data-ekey="">
				<img class="userBanner" se-elem="mainPicture" src="{$userBanner}" width="450" height="150" />
				<div class="options">
					<form se-elem="imageUpload">
						<label class="btn wide blue">
							<input type="file" name="image" style="display:none;" accept=".jpeg, .jpg, .png" />
							<svg class="icon inline mr"><use xlink:href="#fa-upload" /></svg>Subir nueva foto
						</label>
					</form>
					<button class="btn wide blue" se-act="photoCrop"><svg class="icon inline mr"><use xlink:href="#fa-crop" /></svg>Ajustar foto</button>
				</div>
				<dialog>
					<div class="photoCrop">
						<h2>Editar</h2>
						<div class="options">
						<form se-elem="imageCrop">
							<input type="hidden" name="img_x" value="0" />
							<input type="hidden" name="img_y" value="0" />
							<input type="hidden" name="img_w" value="160" />
							<input type="hidden" name="img_h" value="160" />
							<button class="btn blue wide" type="submit"><svg class="icon inline mr"><use xlink:href="#fa-save" /></svg>Actualizar</button>
							<output se-type="response"></output>
						</form>
						<button class="btn wide blue" se-act="cropClose"><svg class="icon inline mr"><use xlink:href="#fa-arrow-left" /></svg>Regresar</button>
						</div>
					</div>
					
					<div class="cropImage">
						<canvas data-autoload="{$autoLoadBanner}"></canvas>
					</div>
				</dialog>
			</div>
		</div>

		<div class="gOMB">
			<h2>Foto Perfil</h2>
			<div id="objectPhoto" se-plugin="object_image" data-type="profile" data-url="{$params['page']['ajax']}/image" data-image="{$userPicture}" data-id="{$params['vars']['objId']}" data-ekey="">
				<img class="userPicture" se-elem="mainPicture" src="{$userPicture}" />
				<div class="options">
					<form se-elem="imageUpload">
						<label class="btn wide blue">
							<input type="file" name="image" style="display:none;" accept=".jpeg, .jpg, .png" />
							<svg class="icon inline mr"><use xlink:href="#fa-upload" /></svg>Subir nueva foto
						</label>
					</form>
					<button class="btn wide blue" se-act="photoCrop"><svg class="icon inline mr"><use xlink:href="#fa-crop" /></svg>Ajustar foto</button>
				</div>
				<dialog>
					<div class="photoCrop">
						<h2>Editar</h2>
						<div class="options">
						<form se-elem="imageCrop">
							<input type="hidden" name="img_x" value="0" />
							<input type="hidden" name="img_y" value="0" />
							<input type="hidden" name="img_w" value="160" />
							<input type="hidden" name="img_h" value="160" />
							<button class="btn blue wide" type="submit"><svg class="icon inline mr"><use xlink:href="#fa-save" /></svg>Actualizar</button>
							<output se-type="response"></output>
						</form>
						<button class="btn wide blue" se-act="cropClose"><svg class="icon inline mr"><use xlink:href="#fa-arrow-left" /></svg>Regresar</button>
						</div>
					</div>
					
					<div class="cropImage">
						<canvas data-autoload="{$autoLoadPicture}"></canvas>
					</div>
				</dialog>
			</div>
		</div>

	</div>

	<div class="gr_sz04">

		<div class="title">Nombre</div>
		<form class="se_form" se-plugin="simpleForm" action="{$params['page']['ajax']}/nameUpd">
			<input type="hidden" name="id" value="{$params['vars']['objId']}">
			<label class="separator required">
				<div class="cont"><span class="title">Nombre</span><span class="desc"></span></div>
				<input type="text" name="fName" value="{$userData['fName']}" required pattern="[A-z\\u00C0-\\u00ff ]{3,50}">
			</label>
			<label class="separator required">
				<div class="cont"><span class="title">Apellido</span><span class="desc"></span></div>
				<input type="text" name="lName" value="{$userData['lName']}" required pattern="[A-z\\u00C0-\\u00ff ]{3,50}">
			</label>
			<label class="separator required">
				<div class="cont"><span class="title">Nombre de usaurio</span><span class="desc"></span></div>
				<input type="text" name="uName" value="{$userData['fullName']}" required pattern="[A-z\\u00C0-\\u00ff ]{3,50}">
			</label>
			<button type="submit">Guardar</button>
			<output se-elem="response"></output>
		</form>

		<div class="title">Nombre URL</div>
		<form class="se_form" se-plugin="simpleForm" action="{$params['page']['ajax']}/urlUpd">
			<input type="hidden" name="id" value="{$params['vars']['objId']}">
			<label class="separator required">
				<div class="cont"><span class="title">Nombre de usaurio</span><span class="desc"></span></div>
				<input type="text" name="url" value="{$userData['objUrl']}" required pattern="[A-z\\u00C0-\\u00ff ]{3,50}">
			</label>
			<button type="submit">Guardar</button>
			<output se-elem="response"></output>
		</form>

	</div>

	<div class="gr_sz04">
		<div class="title">Permisos</div>
		<form class="se_form" se-plugin="simpleForm" action="{$params['page']['ajax']}/permitsUpd">
			<input type="hidden" name="id" value="{$params['vars']['objId']}">
			<label class="separator required">
				<div class="cont"><span class="title">Nivel</span><span class="desc"></span></div>
				<select name="level" required se-fix="{$userData['level']}">
					<option value="0">Uber Administrador</option>
					<option value="1">Super Administrador</option>
					<option value="2">Administrador</option>
					<option value="3">Usuario</option>
				</select>
			</label>
			<label class="separator required">
				<div class="cont"><span class="title">Tipo (Admin)</span><span class="desc"></span></div>
				<select name="sublevel" required se-fix="{$userData['sublevel']}">{$permList}</select>
			</label>
			<label class="separator required">
				<div class="cont"><span class="title">Condición</span><span class="desc"></span></div>
				<select name="status" required se-fix="{$userData['status']}">
					<option value="1">Normal</option>
					<option value="2">Bloqueado</option>
				</select>
			</label>
			<button type="submit">Guardar</button>
			<output se-elem="response"></output>
		</form>
		
		<div class="title">Contraseña</div>
		<form class="se_form" se-plugin="simpleForm" action="{$params['page']['ajax']}/password">
			<input type="hidden" name="id" value="{$params['vars']['objId']}">
			<label class="separator required">
				<div class="cont"><span class="title">Contraseña</span><span class="desc"></span></div>
				<input type="text" name="password" value="" required pattern="[A-z\\u00C0-\\u00ff ]{3,50}">
			</label>
			<button type="submit">Guardar</button>
			<output se-elem="response"></output>
		</form>

	</div>

</div>

HTML;
//
