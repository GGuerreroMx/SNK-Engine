<?php
//
switch ( \snkeng\core\engine\nav::next() )
{
	// Editar
	case 'nameUpd':
		// Validar
		$rData = \snkeng\core\general\saveData::postParsing(
			$response,
			[
				'uName' => ['name' => 'Nombre Completo', 'type' => 'str', 'lMin' => 7, 'lMax' => 50, 'filter' => 'names', 'process' => false, 'validate' => true],
			]
		);

		//
		$sql_upd = <<<SQL
UPDATE sb_objects_obj
SET a_obj_name='{$rData['uName']}'
WHERE a_id='{$params['vars']['objId']}'
LIMIT 1;
SQL;
		\snkeng\core\engine\mysql::submitQuery(
			$sql_upd,
			[
				'errorKey' => '',
				'errorDesc' => ''
			]
		);

		$response['d'] = [
			'id' => $params['vars']['objId'],
			'name' => $rData['uName']
		];
		break;

	// Leer
	case 'nameRead':
		\snkeng\core\general\saveData::sql_table_readRow(
			$response,
			[
				'elems' => [
					'a_fname' => 'fName',
					'a_lname' => 'lName',
					'a_obj_name' => 'uName',
					'a_url_name_full' => 'url',
				],
				'tName' => 'sb_objects_obj',
				'tId' => ['nm' => 'id', 'db' => 'a_id'],
				'dtAdd' => 'a_dt_add',
				'dtMod' => 'a_dt_mod',
			],
			$params['vars']['objId']
		);
		break;

	// Editar
	case 'urlUpd':
		// Validar
		$rData = \snkeng\core\general\saveData::postParsing(
			$response,
			[
				'url' => ['name' => 'URL', 'type' => 'str', 'lMin' => 4, 'lMax' => 50, 'filter' => 'urlTitles', 'process' => false, 'validate' => true],
			]
		);

		//
		\snkeng\core\user\management::objectUrlUpdate($response, $params['vars']['objId'], $rData['url']);
		break;

	// Leer
	case 'urlRead':
		\snkeng\core\general\saveData::sql_table_readRow(
			$response,
			[
				'elems' => [
					'a_url_name_full' => 'url',
				],
				'tName' => 'sb_objects_obj',
				'tId' => ['nm' => 'id', 'db' => 'a_id'],
				'dtAdd' => 'a_dt_add',
				'dtMod' => 'a_dt_mod',
			],
			$params['vars']['objId']
		);
		break;

	//
	case 'image':
		//
		switch ( \snkeng\core\engine\nav::next() )
		{
			// Picture
			case 'profile_upload':
				\snkeng\core\socnet\object_picture::picture_upload($response, $params['vars']['objId']);
				break;

			//
			case 'profile_crop':
				\snkeng\core\socnet\object_picture::picture_crop($response, $params['vars']['objId']);
				break;

			// Banner
			case 'banner_upload':
				\snkeng\core\socnet\object_picture::banner_upload($response, $params['vars']['objId']);
				break;

			//
			case 'banner_crop':
				\snkeng\core\socnet\object_picture::banner_crop($response, $params['vars']['objId']);
				break;
			//
			default:
				\snkeng\core\engine\nav::invalidPage();
				break;
		}
		break;

	//
	default:
		\snkeng\core\engine\nav::invalidPage();
		break;
}
//