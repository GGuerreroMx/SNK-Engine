<?php

//

\snkeng\core\engine\login::kill_loginLevel('sadmin');

//
switch ( \snkeng\core\engine\nav::next() )
{
	//
	case 'objects':
		if ( !is_numeric(\snkeng\core\engine\nav::next(false)) ) {
			require __DIR__ . '/objects_object.php';
		} else {
			//
			$params['vars']['objId'] = intval(\snkeng\core\engine\nav::next());

			//
			switch ( \snkeng\core\engine\nav::next() ) {
				//
				case 'properties':
					require __DIR__ . '/objects_properties.php';
					break;

				//
				case 'relationships':
					require __DIR__ . '/relationships.php';
					break;
				//
				case 'notifications':
					require __DIR__ . '/notifications.php';
					break;

				//
				default:
					\snkeng\core\engine\nav::invalidPage();
					break;
			}
		}
		break;

	//
	case 'users':
		if ( !is_numeric(\snkeng\core\engine\nav::next(false)) ) {
			require __DIR__ . '/users_object.php';
		} else {
			//
			$params['vars']['objId'] = intval(\snkeng\core\engine\nav::next());

			//
			switch ( \snkeng\core\engine\nav::next() ) {
				//
				case 'properties':
					require __DIR__ . '/users_properties.php';
					break;

				//
				case 'relationships':
					require __DIR__ . '/relationships.php';
					break;

				//
				case 'notifications':
					require __DIR__ . '/notifications.php';
					break;

				//
				case 'emails':
					require __DIR__ . '/emails.php';
					break;

				//
				case 'socnets':
					require __DIR__ . '/socnets.php';
					break;

				//
				default:
					\snkeng\core\engine\nav::invalidPage();
					break;
			}
		}
		break;
	//
	case 'emails':
		require __DIR__ . '/emails.php';
		break;
	//
	case 'socnets':
		require __DIR__ . '/socnets.php';
		break;
	//
	case 'relationships':
		require __DIR__ . '/relationships.php';
		break;
	//
	case 'notifications':
		require __DIR__ . '/notifications.php';
		break;
	//
	case 'sendmail':
		require __DIR__ . '/sendmail.php';
		break;
	//
	case 'smartLinks':
		require __DIR__ . '/smartLinks.php';
		break;

	//
	case 'uadmin':
		//
		\snkeng\core\engine\login::kill_loginLevel('uadmin');

		require __DIR__ . '/uadmin/#map.php';
		break;

	//
	default:
		\snkeng\core\engine\nav::invalidPage();
		break;
}
//
