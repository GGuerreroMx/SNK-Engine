<?php

//
switch ( \snkeng\core\engine\nav::next() )
{
	// Leer todas
	case 'readAll':
		// Preparar estructura
		\snkeng\core\general\asyncDataJson::printJson(
			[
				'sel' => "n.n_id AS id,
					n.n_dt_add AS dtAddFull, n.n_dt_mod AS dtMod,
					DATE(n.n_dt_add) AS dtAdd, DATE(n.n_dt_mod) AS dtMod,
					n.n_app AS appName, n.n_action_type AS actName",
				'from' => 'sb_notifications AS n
					INNER JOIN sb_objects_obj AS obj ON obj.a_id=n.a_id',
				'lim' => 20,
				'where' => [
					'id' => ['name' => 'ID', 'db' => 'n.n_id', 'vtype' => 'int', 'stype' => 'eq'],
					'appName' => ['name' => 'Nombre APP', 'db' => 'n.n_app', 'vtype' => 'str', 'stype' => 'like'],
					'actName' => ['name' => 'Nombre Acción', 'db' => 'n.n_action_type', 'vtype' => 'str', 'stype' => 'like']
				],
				'order' => [
					'id' => ['Name' => 'ID', 'db' => 'n.n_id']
				],
				'default_order' => [
					['id', 'DESC']
				],
			]
		);
		break;

	// Leer todas
	case 'readSingle':
		// Preparar estructura
		\snkeng\core\general\asyncDataJson::printJson(
			[
				'sel' => "n.n_id AS id,
					n.n_dt_add AS dtAddFull, n.n_dt_mod AS dtMod,
					DATE(n.n_dt_add) AS dtAdd, DATE(n.n_dt_mod) AS dtMod,
					n.n_app AS appName, n.n_action_type AS actName",
				'from' => 'sb_notifications AS n',
				'lim' => 20,
				'where' => [
					'id' => ['name' => 'ID', 'db' => 'n.n_id', 'vtype' => 'int', 'stype' => 'eq'],
					'objId' => ['name' => 'ID', 'db' => 'n.n_obj_id', 'vtype' => 'int', 'stype' => 'eq'],
					'appName' => ['name' => 'Nombre APP', 'db' => 'n.n_app', 'vtype' => 'str', 'stype' => 'like'],
					'actName' => ['name' => 'Nombre Acción', 'db' => 'n.n_action_type', 'vtype' => 'str', 'stype' => 'like']
				],
				'order' => [
					'id' => ['Name' => 'ID', 'db' => 'n.n_id']
				],
				'default_order' => [
					['id', 'DESC']
				]
			]
		);
		break;

	//
	default:
		\snkeng\core\engine\nav::invalidPage();
		break;
}
//
