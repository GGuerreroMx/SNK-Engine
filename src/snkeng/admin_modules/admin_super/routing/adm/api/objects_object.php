<?php
//

switch ( \snkeng\core\engine\nav::next() )
{
	// Agregar
	case 'add':
		$rData = \snkeng\core\general\saveData::postParsing(
			$response,
			[
				'name' => ['name' => 'Nombre', 'type' => 'str', 'lMin' => 3, 'lMax' => 50, 'filter' => 'names', 'process' => false, 'validate' => true],
				'type' => ['name' => 'Tipo', 'type' => 'str', 'lMin' => 3, 'lMax' => 50, 'filter' => 'names', 'process' => false, 'validate' => true],
			]
		);

		// Verificación inicial
		if ( !\snkeng\core\engine\login::check_loginLevel('sadmin') ) {
			\snkeng\core\engine\nav::killWithError("El usuario actual no puede crear usuarios.");
		}

		// Revisar que no cree uber administradores
		if ( $rData['type'] === 'user' ) {
			\snkeng\core\engine\nav::killWithError('No se puede crear un usuario en esta sección.');
		}

		//
		\snkeng\core\user\management::objectCreate($response, $rData['name'], $rData['type']);
		break;

	// Leer Uno
	case 'readSingle':
		$objId = intval($_POST['id']);
		if ( $objId === 0 ) {
			\snkeng\core\engine\nav::killWithError('No se obtuvo el id de la consulta.,');
		}
		//
		elementReturnSingle($objId);
		break;

	// Borrar
	case 'del':
		\snkeng\core\general\saveData::sql_table_delRow(
			$response,
			[
				'tName' => 'st_site_textedit',
				'tId' => ['db' => 'ste_id']
			],
			'id'
		);
		break;

	// Leer todas
	case 'readAll':
		// Preparar estructura
		\snkeng\core\general\asyncDataJson::printJson(
			[
				'sel' => "obj.a_id AS id,
				obj.a_obj_name AS fullName, obj.a_url_name_full AS urlNameFull, obj.a_url_name_simple AS urlNameSimple,
				obj.a_status AS status,
				DATE(obj.a_dt_add) as dtAdd, DATE(obj.a_dt_mod) as dtMod,
				obj.a_type AS type",
				'from' => 'sb_objects_obj AS obj',
				'lim' => 30,
				'where' => [
					'id' => ['name' => 'ID', 'db' => 'obj.a_id', 'vtype' => 'int', 'stype' => 'eq'],
					'name' => ['name' => 'Nombre', 'db' => 'obj.a_obj_name', 'vtype' => 'str', 'stype' => 'like'],
					'urlname' => ['name' => 'Nombre URL', 'db' => 'obj.a_url_name_full', 'vtype' => 'str', 'stype' => 'like'],
					'urlnamesimple' => ['name' => 'Nombre URL', 'db' => 'obj.a_url_name_simple', 'vtype' => 'str', 'stype' => 'like'],
					'type' => ['name' => 'Tipo', 'db' => 'obj.a_type', 'vtype' => 'str', 'stype' => 'eq']
				],
				'order' => [
					'id' => ['Name' => 'ID', 'db' => 'obj.a_id']
				],
				'default_order' => [
					['id', 'DESC']
				]
			]
		);
		break;

	//
	default:
		\snkeng\core\engine\nav::invalidPage();
		break;
}
//

//
// INIT: jsonSelection
//	Operaciones que regresan json
function elementReturnSingle($objId)
{
	$tData = [
		'elems' => [
			'ste_title' => 'title',
		],
		'tName' => 'st_site_textedit',
		'tId' => ['nm' => 'id', 'db' => 'ste_id'],
		'dtAdd' => 'ste_dtadd',
		'dtMod' => 'ste_dtmod',
	];
	return sql_table_readRow($tData, $objId);
}
// END: jsonSelection
//

//</editor-fold>
