<?php

//
switch ( \snkeng\core\engine\nav::next() )
{
	// Leer todas
	case 'readAll':
		// Preparar estructura
		\snkeng\core\general\asyncDataJson::printJson(
			[
				'sel' => "admail.am_id AS id, admail.am_mail AS email,
				admail.am_dt_add AS dtAdd, admail.am_status AS mailStatus, admail.am_type AS mailType,
				user.a_id AS userId, user.a_obj_name AS userName, user.a_url_name_full AS userUrlName,
				user.a_adm_level AS userLevel, user.a_adm_type AS userType",
				'from' => 'sb_users_mail AS admail
				INNER JOIN sb_objects_obj AS user ON user.a_id=admail.a_id',
				'lim' => 25,
				'where' => [
					'id' => ['name' => 'ID Usuario', 'db' => 'admail.a_id', 'vtype' => 'int', 'stype' => 'eq'],
					'email' => ['name' => 'Email', 'db' => 'admail.am_mail', 'vtype' => 'str', 'stype' => 'like'],
					'mailStatus' => ['name' => 'Mail Status', 'db' => 'admail.am_status', 'vtype' => 'int', 'stype' => 'eq'],
					'usrName' => ['name' => 'Usr Name', 'db' => 'user.a_obj_name', 'vtype' => 'str', 'stype' => 'like'],
					'usrUrl' => ['name' => 'Usr Url', 'db' => 'user.a_url_name_full', 'vtype' => 'str', 'stype' => 'like'],
					'usrAdmLevel' => ['name' => 'Nivel Administración', 'db' => 'user.a_adm_level', 'vtype' => 'int', 'stype' => 'eq'],
					'usrAdmSubLevel' => ['name' => 'Sub nivel administración', 'db' => 'user.a_adm_type', 'vtype' => 'str', 'stype' => 'eq'],
				],
				'order' => [
					'am_id' => ['Name' => 'ID', 'db' => 'admail.am_id']
				],
				'default_order' => [
					['am_id', 'DESC']
				],
				'toSimple' => [
					'replace' => [
						'mailType' => [
							'0' => 'N/D',
							'1' => 'Pri',
							'2' => 'Sec'
						],
						'userLevel' => [
							'0' => 'U. Admin',
							'1' => 'S. Admin',
							'2' => 'Admin',
							'3' => 'Usuario',
							'4' => 'Objeto',
						]
					]
				]
			]
		);
		break;

	// Leer todas
	case 'readSingle':
		// Preparar estructura
		\snkeng\core\general\asyncDataJson::printJson(
			[
				'sel' => "admail.am_id AS id, admail.am_mail AS email,
							admail.am_status AS status, admail.am_type AS type, admail.am_dt_add AS dtAdd
					",
				'from' => 'sb_users_mail AS admail',
				'lim' => 20,
				'where' => [
					'objId' => ['name' => 'ID Usuario', 'db' => 'admail.a_id', 'vtype' => 'int', 'stype' => 'eq', 'set' => $params['vars']['objId'] ],
				],
				'order' => [
					'am_id' => ['Name' => 'ID', 'db' => 'admail.am_id']
				],
				'default_order' => [
					['am_id', 'DESC']
				],
			]
		);
		break;

	//
	default:
		\snkeng\core\engine\nav::invalidPage();
		break;
}
//
