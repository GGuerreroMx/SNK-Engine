<?php

//
switch ( \snkeng\core\engine\nav::next() )
{
	// Leer todas
	case 'send':
		$pData = [
			'fName' => ['name' => 'Nombre', 'type' => 'str', 'filter' => 'name', 'process' => false, 'validate' => true],
			'lName' => ['name' => 'Apellido', 'type' => 'str', 'filter' => 'name', 'process' => false, 'validate' => true],
			'eMail' => ['name' => 'E-Mail', 'type' => 'str', 'filter' => 'email', 'process' => false, 'validate' => true],
			'title' => ['name' => 'Título', 'type' => 'str', 'process' => false, 'validate' => true],
			'content' => ['name' => 'Contenido', 'type' => 'str', 'process' => false, 'validate' => true],
		];
		$rData = \snkeng\core\general\saveData::postParsing($response, $pData);

		// Variables
		$host = str_replace("www.", "", $_SERVER['HTTP_HOST']);

		// Datos
		$to = "{$rData['fName']} {$rData['lName']} <{$rData['eMail']}>";
		$title = $rData['title'];
		$message = $rData['content'];

		// Operaciones
		$mail = \snkeng\core\general\mail::sendText($to, $title, $message);

		//
		if ( $_ENV['SE_DEBUG'] ) {
			//
			$response['debug']['mail'] = <<<HTML
<pre>
EMAIL:
----
{$mail['debug']['mail']}
----
</pre>
HTML;
			//
		}
		break;
	//</editor-fold>

	//
	default:
		\snkeng\core\engine\nav::invalidPage();
		break;
}
