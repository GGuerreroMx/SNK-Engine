import * as formActions from '/snkeng/core/res/modules/library/forms.mjs';

//
customElements.define('super_adm_pages_object_images', class extends HTMLElement {
	structureForm = this.querySelector('form');

	//
	constructor() {
		super();
	}

	//
	connectedCallback() {
		//
		this.structureForm.se_on('change', ['textarea', 'input'], function(ev) {
			let cForm = ev.target.se_closest('form'),
				svg = this.getElementById('testIcon'),
				xi = cForm.se_formElVal('xi'),
				yi = cForm.se_formElVal('yi'),
				xp = cForm.se_formElVal('xp'),
				yp = cForm.se_formElVal('yp'),
				svgData = cForm.se_formElVal('svg');
			//
			svg.setAttribute('viewBox', '' + xi + ',' + yi + ',' + xp + ',' + yp);
			svg.innerHTML = svgData;
		});

		//
		this.structureForm.addEventListener(
			'submit',
			(e) => {
				e.preventDefault();
				formActions.formSubmit(this.structureForm, {
					onComplete:(msg) => {
						this.querySelector('output[se-elem="response"]').innerText = msg.d;
					}
				});
			}
		);
	}
});
