<?php
//

$params['vars'] = [];
$params['vars']['title'] = 'Administración';
$params['vars']['menu'] = <<<HTML
<div class="nav3_menu_parent">
	<div class="nav3_menu_button"><a se-nav="app_content" href="{$appData['url']}">Inicio</a></div>
</div>
<div class="nav3_menu_parent">
	<div class="nav3_menu_button"><a se-nav="app_content" href="{$appData['url']}/users/">Usuarios</a></div>
</div>
<div class="nav3_menu_parent">
	<div class="nav3_menu_button"><a se-nav="app_content" href="{$appData['url']}/objects/">Objetos</a></div>
</div>
<div class="nav3_menu_parent">
	<div class="nav3_menu_button"><a se-nav="app_content" href="{$appData['url']}/emails/">Correos</a></div>
</div>
<div class="nav3_menu_parent">
	<div class="nav3_menu_button"><a se-nav="app_content" href="{$appData['url']}/socnets/">Redes Sociales</a></div>
</div>
<div class="nav3_menu_parent">
	<div class="nav3_menu_button"><a se-nav="app_content" href="{$appData['url']}/sendmail/">Enviar correo</a></div>
</div>
<div class="nav3_menu_parent">
	<div class="nav3_menu_button"><a se-nav="app_content" href="{$appData['url']}/notifications/">Notificaciones</a></div>
</div>
<div class="nav3_menu_parent">
	<div class="nav3_menu_button"><a se-nav="app_content" href="{$appData['url']}/smartLinks/">Smart Links</a></div>
</div>
<div class="nav3_menu_parent">
	<div class="nav3_menu_button"><a se-nav="app_content" href="{$appData['url']}/adminLevels/">Permisos</a></div>
</div>
HTML;


if ( \snkeng\core\engine\login::check_loginLevel('uadmin') ) {
	$params['vars']['menu'].= <<<HTML
	<div class="nav3_menu_parent">
		<div class="nav3_menu_button"><span class="text">U. Admin.</span></div>
		<div class="nav3_menu_hidden">
			<a se-nav="app_content" href="{$appData['url']}/uadmin/">Empty</a>
			<a se-nav="app_content" href="{$appData['url']}/uadmin/scripts/">Scripts</a>
		</div>
	</div>\n
HTML;
}

//
return $params['vars'];
