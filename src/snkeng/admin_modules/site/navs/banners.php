<?php
//
$nav = [
	'banners' => [
		'sel' => "ban.ban_id AS id, ban.ban_title AS title, ban.ban_size AS bSize,
				ban.ban_type AS bTypeNum, ban.ban_type AS bTypeTxt,
				ban.ban_active AS bPublished,
				ban.ban_size AS bSizeTxt, ban.ban_size AS bSizeNum,
				IF(
					ban_active=1 &&
					( now()>ban.ban_dtini || ban.ban_dtini IS NULL )&&
					( now()<ban.ban_dtend || ban.ban_dtini IS NULL ) &&
					( ban.ban_maxclicks=0 || ban.ban_curclicks<ban.ban_maxclicks ) &&
					( ban.ban_maxprints=0 || ban.ban_curprints<ban.ban_maxprints )
				, 1, 0 ) AS bStatus",
		'from' => 'sc_site_banners AS ban',
		'lim' => 20,
		'where' => [
			'id' => ['name' => 'ID', 'db' => 'ban.ban_id', 'vtype' => 'int', 'stype' => 'eq'],
			'title' => ['name' => 'Título', 'db' => 'ban.ban_title', 'vtype' => 'str', 'stype' => 'like']
		],
		'order' => [
			['Name' => 'ID', 'db' => 'ban.ban_id', 'set' => 'DESC']
		],
		'toSimple' => [
			'replace' => [
				'bSizeTxt' => [
					'1' => 'Banner Principal',
					'2' => 'Secundario Lateral Vertical',
					'3' => 'Secundario Horizontal',
					'4' => 'Secundario',
					'5' => 'Cuadrado',
				],
				'bTypeTxt' => [
					'1' => 'Imagen',
					'2' => 'Código',
					'3' => 'Video'
				]
			]
		]
	]
];
//
return $nav;
