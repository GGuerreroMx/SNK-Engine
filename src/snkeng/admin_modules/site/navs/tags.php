<?php
//
$nav = [
	'tags' => [
		'sel' => "tag.tag_id AS id, tag.tag_title AS title, tag.tag_urltitle AS urltitle",
		'from' => 'sc_site_tags AS tag',
		'lim' => 20,
		'where' => [
			'cType' => ['name' => 'Tipo', 'db' => 'tag.tag_mode_type', 'vtype' => 'str', 'stype' => 'eq'],
			'id' => ['name' => 'ID', 'db' => 'tag.tag_id', 'vtype' => 'int', 'stype' => 'eq'],
			'title' => ['name' => 'Título', 'db' => 'tag.tag_title', 'vtype' => 'str', 'stype' => 'like'],
		],
		'order' => [
			['Name' => 'Título', 'db' => 'tag.tag_title', 'set' => 'DESC']
		]
	]
];
return $nav;
