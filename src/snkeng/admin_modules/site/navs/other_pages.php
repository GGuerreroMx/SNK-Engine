<?php
//
$nav = [
	'blog' => [
		'sel' => "art.art_id AS id, art.art_title AS title, art.art_urltitle AS urltitle,
				art.art_type_primary AS artType, art.art_type_secondary AS artTypeSub,
				DATE(art.art_dt_pub) AS dtPub, art.art_dt_add AS dtAdd, art.art_dt_mod AS dtMod,
				art.art_status_published AS isPub,
				art.art_count_views AS views,
				art.art_url AS fullUrl",
		'from' => 'sc_site_articles AS art',
		'lim' => 20,
		'where' => [
			'artTypeSub' => ['name' => 'Tipo sub', 'db' => 'art.art_type_secondary', 'vtype' => 'str', 'stype' => 'like'],
			'title' => ['name' => 'Título', 'db' => 'art.art_title', 'vtype' => 'str', 'stype' => 'like'],
			'id' => ['name' => 'ID', 'db' => 'art.art_id', 'vtype' => 'int', 'stype' => 'eq'],
			'artType' => ['name' => 'Tipo', 'db' => 'art.art_type_primary', 'vtype' => 'str', 'stype' => 'like'],
		],
		'order' => [
			['Name' => 'ID', 'db' => 'art.art_id', 'set' => 'ASC']
		]
	],
];
//
return $nav;
