<?php
//
\snkeng\core\engine\nav::cacheCheckFile(__FILE__);
\snkeng\core\engine\nav::cacheFinalCheck();

// Estructura
$an_struct = <<<HTML
<form data-element="row" data-action="upd" data-objid="!id;" data-objtitle="!title;">
	<span>!id;</span>
	<span><input name="title" value="!title;" /></span>
	<span>
		<button class="btn small" type="submit" title="Guardar"><svg class="icon inline"><use xlink:href="#fa-save" /></svg></button>
		<button class="btn small" data-dyntab-onclick="element_op" data-action="del" type="button" title="Borrar"><svg class="icon inline"><use xlink:href="#fa-trash-o" /></svg></button>
	</span>
</form>
HTML;
//

//
$exData = [
	'js_url' => $params['page']['ajax'].'/readAll',
	'printElements' => false,
	'printType' => 'tableMod',
	'tableWide' => true,
	'tableHead'=>[
		['name' => 'ID', 'filter' => 1, 'fName'=>'id'],
		['name' => 'Nombres', 'filter' => 1, 'fName'=>'title'],
		['name' => 'Acciones'],
	],
	'settings' => ['nav' => true],
	'actions' => <<<JS
{
	'add':
	{
		'action':'form',
		'ajax-action':'add',
		'ajax-elem':'none',
		'menu':{'title':'Agregar', 'icon':'fa-plus'},
		'print':false,
		'form':{
			'title':'Nuevo texto',
			'save_url':'{$params['page']['ajax']}/add',
			'actName':'Agregar',
			'elems':[
			{'ftype':'text', 'vname':'title', 'fname':'Nombre', 'fdesc':'', 'dtype':'str', 'regexp':'urlSimple', 'lMin':3, 'lMax':50, 'requiered':true},
			]
		}
	},
	'upd':
	{
		'action':'direct',
		'ajax-action':'upd',
		'save_url':'{$params['page']['ajax']}/upd',
	},
	'del':{'action':'del', 'ajax-elem':'obj', 'del_url':'{$params['page']['ajax']}/del'}
}
JS
];

$tableStructure = \snkeng\core\general\dynamicTable::createElement($an_struct, $exData);

// Page
$page['body'] = <<<HTML
<div class="pageSubTitle">Textos simples</div>

{$tableStructure}
HTML;
//
