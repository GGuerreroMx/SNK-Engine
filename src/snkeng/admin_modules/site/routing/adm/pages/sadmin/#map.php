<?php

//
$params['page']['main'].= '/' . \snkeng\core\engine\nav::current();
$params['page']['ajax'].= '/' . \snkeng\core\engine\nav::current();

// Root
\snkeng\core\admin\navBuild::navBreadcrumbAdd($params['page']['main'], 'Documentaciones');
\snkeng\core\admin\navBuild::navLevelSet([
	[$params['page']['main'].'/', 'Inicio'],
	[$params['page']['main'].'/texts', 'Textos simples'],
	[$params['page']['main'].'/pages', 'Páginas'],
	[$params['page']['main'].'/categories', 'Categorías'],
	[$params['page']['main'].'/tags', 'Tags'],
	[$params['page']['main'].'/images', 'Imágenes'],
	[$params['page']['main'].'/files', 'Archivos'],
]);

//
switch ( \snkeng\core\engine\nav::next() ) {
	//
	case 'texts':
		//
		$params['page']['main'].= '/' . \snkeng\core\engine\nav::current();
		$params['page']['ajax'].= '/' . \snkeng\core\engine\nav::current();

		//
		require __DIR__ . '/texts.php';
		break;

	//
	case 'pages':
		//
		$params['page']['main'].= '/' . \snkeng\core\engine\nav::current();
		$params['page']['ajax'].= '/' . \snkeng\core\engine\nav::current();

		//
		require __DIR__ . '/pages.php';
		break;

	//
	case 'categories':
		//
		$params['page']['main'].= '/' . \snkeng\core\engine\nav::current();
		$params['page']['ajax'].= '/' . \snkeng\core\engine\nav::current();

		//
		require __DIR__ . '/../general/categories.php';
		break;

	//
	case 'tags':
		//
		$params['page']['main'].= '/' . \snkeng\core\engine\nav::current();
		$params['page']['ajax'].= '/' . \snkeng\core\engine\nav::current();

		//
		require __DIR__ . '/../general/tags.php';
		break;

	//
	case 'images':
		//
		$params['page']['main'].= '/' . \snkeng\core\engine\nav::current();
		$params['page']['ajax'].= '/' . \snkeng\core\engine\nav::current();

		//
		\snkeng\admin_modules\site\image_window::printNormal($params['page']['ajax'], []);
		break;

	//
	case 'files':
		//
		$params['page']['main'].= '/' . \snkeng\core\engine\nav::current();
		$params['page']['ajax'].= '/' . \snkeng\core\engine\nav::current();

		//
		\snkeng\admin_modules\site\file_window::printNormal($params['page']['ajax'], []);
		break;

	// Otros
	default:
		\snkeng\core\engine\nav::invalidPage();
		break;
}
//

//
$nav = \snkeng\core\admin\navBuild::navCreate();

// Menu
$page['body'] = <<<HTML
<div class="pageTitle">Super administración</div>

{$nav}

{$page['body']}
HTML;
//
