<?php
//
\snkeng\core\engine\nav::pageFileModuleAdd('admin_modules', 'site', '/pages/site-adm-pages-gallerySelect.css');
\snkeng\core\engine\nav::pageFileModuleAdd('admin_modules', 'site', '/pages/site-adm-pages-gallerySelect.mjs');

//
$sql_qry = <<<SQL
SELECT
	gal.gal_id AS id,
	gal.gal_title AS title, gal.gal_description AS description,
	gal.gal_structure AS structure,
	gal.gal_ammount AS ammount
FROM sc_site_gallery AS gal
WHERE gal.gal_id='{$params['vars']['galId']}';
SQL;

$galDataTemplate = <<<HTML
<div class="photo" data-id="!id;">
	<img src="/res/image/site/w_160/!fName;" />
	<div> 
		<input name="title" value="!title;" />
		<textarea name="description">!description;</textarea>
		<div class="options">
		<button class="btn small" se-act="del"><svg class="icon inline"><use xlink:href="#fa-trash-o"/></svg></button>
	</div>
	</div>
</div>
HTML;

$galData = \snkeng\core\engine\mysql::singleRowAssoc($sql_qry, [
	'json' => ['structure']
]);

// debugVariable($galData);

//
$galDataTxt = '';
if ( isset($galData['structure']['d']) ) {
	//
	foreach ( $galData['structure']['d'] as $cEl ) {
		$galDataTxt.= stringPopulate($galDataTemplate, $cEl);
	}
}

//
$page['head']['title'] = "Galería: {$params['vars']['galId']}";
//
$page['body'] .= <<<HTML
<div class="pageSubTitle">Galería:{$galData['title']}</div>
<div class="site_adm_gallery" se-plugin="adm_gallerySelect" data-galid="{$params['vars']['galId']}" data-url="{$params['page']['ajax']}">
<div class="grid">
	<div class="gr_sz07" se-elem="photos">
		<template>{$galDataTemplate}</template>
		<div se-elem="content">{$galDataTxt}</div>
	</div>
	<div class="gr_sz05" se-elem="menu">
		<button class="btn blue wide" se-act="add"><svg class="icon inline mr"><use xlink:href="#fa-plus"/></svg>Agregar existente</button>
		<div>
			<h3>Subir archivos</h3>
			<form se-elem="uploadImages">
				<input type="hidden" name="galId" value="{$params['vars']['galId']}" />
				<input type="file" accept="image/jpeg" name="files[]" multiple required />
				<div se-elem="imagesPreview">
					<template><div><img class="thumbnail" src="!result;" alt="temporal" /></div></template>
					<div class="previewPhotos" se-elem="content"></div>
				</div>
				<button class="btn blue wide" type="submit"><svg class="icon inline mr"><use xlink:href="#fa-upload"/></svg>Subir</button>
				<output se-elem="response"></output>
			</form>
		</div>
	</div>
</div>
</div>
HTML;
//
