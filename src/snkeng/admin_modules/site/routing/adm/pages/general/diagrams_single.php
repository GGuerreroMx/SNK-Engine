<?php
//
\snkeng\core\engine\nav::pageFileModuleAdd('admin_modules', 'site', '/pages/site-adm-pages-diagram.css');
\snkeng\core\engine\nav::pageFileModuleAdd('admin_modules', 'site', '/pages/site-adm-pages-diagram.mjs');

//
$page['head']['title'].= ' Diagrama';

// Page
$page['body'].= <<<HTML
<div class="pageSubTitle">Diagrama</div>\n\n
<se-site-diagram-build class="" data-id="">
	<div>
		<form class="se_form">
			<textarea>
graph TD
	A[Christmas] -->|Get money| B(Go shopping)
	B --> C{Let me think}
	C -->|One| D[Laptozp]
	C -->|Two| E[iPhone]
	C -->|Three| F[fa:fa-car Car]
			</textarea>
			<button type="submit"><svg class="icon inline mr"><use xlink:href="#fa-save" /></svg>Guardar</button>
			<output></output>
		</form>
	</div>
	<div>
		<div id="graphElement"></div>
	</div>
</se-site-diagram-build>
HTML;
//
