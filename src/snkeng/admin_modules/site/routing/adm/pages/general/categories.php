<?php
//
\snkeng\core\engine\nav::cacheCheckFile(__FILE__);
\snkeng\core\engine\nav::cacheFinalCheck();

// Estructura
$an_struct = <<<HTML
<form data-element="row" data-action="upd" data-objid="!id;" data-objtitle="!title;">
	<span>!id;</span>
	<span><input name="title" value="!title;" size="40" /></span>
	<span><input name="urltitle" value="!urltitle;" size="40" /></span>
	<span>!cType;</span>
	<span>
		<button class="btn small" type="submit" title="Guardar"><svg class="icon inline"><use xlink:href="#fa-save" /></svg></button>
		<button class="btn small" data-dyntab-onclick="element_op" data-action="del" title="Borrar"><svg class="icon inline"><use xlink:href="#fa-trash-o" /></svg></button>
	</span>
</form>
HTML;
//
$exData = [
	'js_id' => 'pagElements',
	'js_url' => $params['page']['ajax'].'/readAll',
	'printElements' => false,
	'printType' => 'tableMod',
	'tableWide' => true,
	'tableHead' => [
		['name' => 'ID', 'filter' => 1, 'fName' => 'id'],
		['name' => 'Nombres', 'filter' => 1, 'fName' => 'title'],
		['name' => 'URL'],
		['name' => 'Tipo'],
		['name' => 'Acciones'],
	],
	'settings' => ['nav' => true],
	'actions' => <<<JSON
{
	"add":
	{
		"ajax-action":"add",
		"ajax-elem":"none",
		"print":false,
		"menu":{"title":"Agregar", "icon":"fa-plus"},
		"action":"form",
		"form":{
			"title":"Nueva categoría",
			"save_url":"{$params["page"]["ajax"]}/add",
			"actName":"Agregar",
			"elems":{
				"title": {
					"field_type": "input",
					"data_type": "string",
					"info_name": "Nombre",
					"attributes": {
						"minlength": 3,
						"maxlength": 50,
						"data-regexp": "simpleTitle"
					}
				},
				"urltitle": {
					"field_type": "input",
					"data_type": "string",
					"info_name": "URL",
					"attributes": {
						"minlength": 3,
						"maxlength": 30,
						"data-regexp": "urlTitle"
					}
				}
			}
		}
	},
	"upd":
	{
		"action":"direct",
		"ajax-action":"upd",
		"save_url":"{$params["page"]["ajax"]}/upd",
	},
	"del":{"action":"del", "ajax-elem":"obj", "del_url":"{$params["page"]["ajax"]}/del"}
}
JSON
];
//

//
$tableStructure = \snkeng\core\general\dynamicTable::createElement($an_struct, $exData);

// $post->debugQuery();

// Page
$page['body'] = <<<HTML
<div class="pageSubTitle">Categorías</div>

{$tableStructure}
HTML;
//
