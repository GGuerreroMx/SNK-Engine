<?php
//
\snkeng\core\engine\nav::cacheCheckFile(__FILE__);
\snkeng\core\engine\nav::cacheFinalCheck();

//
$params['page']['main'].= '/' . \snkeng\core\engine\nav::current();
$params['page']['ajax'].= '/' . \snkeng\core\engine\nav::current();

//
\snkeng\core\engine\nav::pageFileModuleAdd('admin_modules', 'site', '/pages/site-adm-pages-banners.css');

// Estructura
$an_struct = <<<HTML
<tr data-element="row" data-objid="!id;" data-objtitle="!title;" data-btype="!bTypeNum;">
	<td>!id;</td>
	<td>!title;</td>
	<td>!bSizeTxt;</td>
	<td>!bTypeTxt;</td>
	<td><span class="fakeCheck" data-value="!bPublished;"></span></td>
	<td><span class="fakeCheck" data-value="!bStatus;"></span></td>
	<td>
		<button class="btn small" data-filter="1" data-dyntab-onclick="element_op" data-action="configImage" title="Configuración"><svg class="icon inline"><use xlink:href="#fa-gear" /></svg></button>
		<button class="btn small" data-filter="2" data-dyntab-onclick="element_op" data-action="configCode" title="Configuración"><svg class="icon inline"><use xlink:href="#fa-gear" /></svg></button>
		<button class="btn small" data-filter="3" data-dyntab-onclick="element_op" data-action="configVideo" title="Configuración"><svg class="icon inline"><use xlink:href="#fa-gear" /></svg></button>
		<button class="btn small" data-filter="1" data-dyntab-onclick="element_op" data-action="imageUpload" title="Subir archivo de imagen"><svg class="icon inline"><use xlink:href="#fa-upload" /></svg></button>
		<button class="btn small" data-filter="3" data-dyntab-onclick="element_op" data-action="videoUpload" title="Subir archivo de video"><svg class="icon inline"><use xlink:href="#fa-upload" /></svg></button>
	</td>
	<td>
		<button class="btn small" data-dyntab-onclick="element_op" data-action="edit" title="Editar"><svg class="icon inline"><use xlink:href="#fa-pencil" /></svg></button>
		<button class="btn small" data-dyntab-onclick="element_op" data-action="publish" title="Publicación"><svg class="icon inline"><use xlink:href="#fa-calendar" /></svg></button>
		<button class="btn small" data-dyntab-onclick="element_op" data-action="del" title="Borrar"><svg class="icon inline"><use xlink:href="#fa-trash-o" /></svg></button>
	</td>
</tr>
HTML;
//

//
$exData = [
	'js_id' => 'pagElements',
	'js_url' => $params['page']['ajax'].'/readAll',
	'printElements' => false,
	'printType' => 'table',
	'tableWide' => true,
	'contentClass' => 'siteAdm_banners',
	'tableHead'=>[
		['name' => 'ID', 'filter' => 1, 'fName'=>'id'],
		['name' => 'Nombres', 'filter' => 1, 'fName'=>'title'],
		['name' => 'Tamaño'],
		['name' => 'Tipo'],
		['name' => 'Activado'],
		['name' => 'Publicando'],
		['name' => 'Configuración'],
		['name' => 'Acciones'],
	],
	'settings' => ['nav' => true],
	'actions' => <<<JSON
{
	"add":
	{
		"ajax-action":"add",
		"ajax-elem":"none",
		"menu":{"title":"Agregar", "icon":"fa-plus"},
		"print":false,
		"action":"form",
		"form":{
			"title":"Nuevo Banner",
			"save_url":"{$params["page"]["ajax"]}/add",
			"actName":"Agregar",
			"elems":{
				"title": {
					"field_type": "input",
					"data_type": "string",
					"info_name": "Nombre",
					"attributes": {
						"minlength": 5,
						"maxlength": 75,
						"data-regexp": "simpleTitle"
					}
				},
				"size": {
					"field_type": "input",
					"data_type": "number",
					"info_name": "Tamaño",
					"attributes": {
						"type": "number",
						"min": 0,
						"max": 255
					}
				},
				"type": {
					"field_type": "list",
					"default_value": 1,
					"info_name": "Tipo",
					"attributes": {
						"options": {
							"1": "Imagen",
							"2": "Código",
							"3": "Video"
						}
					}
				}
			}
		}
	},
	"edit":
	{
		"ajax-action":"edit",
		"ajax-elem":"obj",
		"action":"form",
		"form":{
			"title":"Editar",
			"load_url":"{$params["page"]["ajax"]}/{id}/generalData",
			"save_url":"{$params["page"]["ajax"]}/{id}/generalData",
			"actName":"Editar",
			"elems":{
				"title": {
					"field_type": "input",
					"data_type": "string",
					"info_name": "Nombre",
					"attributes": {
						"minlength": 5,
						"maxlength": 75,
						"data-regexp": "simpleTitle"
					}
				},
				"size": {
					"field_type": "list",
					"default_value": 1,
					"info_name": "Tamaño",
					"attributes": {
						"options": {
							"1": "Banner",
							"2": "Sec. Vertical",
							"3": "Sec. Horizontal",
							"4": "Otro",
							"5": "Cuadrado"
						}
					}
				},
				"type": {
					"field_type": "list",
					"default_value": 1,
					"info_name": "Tipo",
					"attributes": {
						"options": {
							"1": "Imagen",
							"2": "Código",
							"3": "Video"
						}
					}
				}
			}
		}
	},
	"publish":
	{
		"ajax-action":"edit",
		"ajax-elem":"obj",
		"action":"form",
		"form":{
			"title":"Publicación",
			"load_url":"{$params["page"]["ajax"]}/{id}/publishData",
			"save_url":"{$params["page"]["ajax"]}/{id}/publishData",
			"actName":"Editar",
			"elems":{
				"id": {
					"field_type": "hidden",
					"data_type": "int"
				},
				"dtini": {
					"field_type": "input",
					"data_type": "datetime",
					"info_name": "Fecha de inicio",
					"attributes": {
						"type": "datetime-local",
						"minlength": 19,
						"maxlength": 25,
						"regexp": "datetime"
					}
				},
				"dtend": {
					"field_type": "input",
					"data_type": "datetime",
					"info_name": "Fecha de final",
					"attributes": {
						"type": "datetime-local",
						"minlength": 19,
						"maxlength": 25,
						"regexp": "datetime"
					}
				},
				"clicksMax": {
					"field_type": "input",
					"data_type": "number",
					"info_name": "Clicks Máximos",
					"info_description": "Cantidad máxima de clicks permitidos (archivo). 0=Ilimitados",
					"attributes": {
						"type": "number",
						"min": 0,
						"max": 4294967295
					}
				},
				"printsMax": {
					"field_type": "input",
					"data_type": "number",
					"info_name": "Impresiones Máximos",
					"info_description": "Cantidad máxima de impresiones permitidas. 0=Ilimitados",
					"attributes": {
						"type": "number",
						"min": 0,
						"max": 4294967295
					}
				},
				"published": {
					"field_type": "checkbox",
					"data_type": "bool",
					"info_name": "Publicado",
					"attributes": {
						"value": 1
					}
				}
			}
		}
	},
	"configCode":
	{
		"ajax-action":"edit",
		"ajax-elem":"obj",
		"ajax-update":false,
		"action":"form",
		"form":{
			"title":"Editar",
			"load_url":"{$params["page"]["ajax"]}/{id}/codeData",
			"save_url":"{$params["page"]["ajax"]}/{id}/codeData",
			"actName":"Editar",
			"elems":{
				"code": {
					"field_type": "textarea",
					"data_type": "str",
					"info_name": "Código"
				}
			}
		}
	},
	"configImage":
	{
		"ajax-action":"edit",
		"ajax-elem":"obj",
		"ajax-update":false,
		"action":"form",
		"form":{
			"title":"Editar",
			"load_url":"{$params["page"]["ajax"]}/{id}/mediaData",
			"save_url":"{$params["page"]["ajax"]}/{id}/mediaData",
			"actName":"Editar",
			"elems":{
				"url": {
					"field_type": "input",
					"data_type": "string",
					"info_name": "URL",
					"attributes": {
						"minlength": 0,
						"maxlength": 255,
						"data-regexp": null
					}
				},
				"alttext": {
					"field_type": "textarea",
					"data_type": "str",
					"info_name": "Texto alternativo",
					"info_description": "Describir la publicidad"
				}
			}
		}
	},
	"configVideo":
	{
		"ajax-action":"edit",
		"ajax-elem":"obj",
		"ajax-update":false,
		"action":"form",
		"form":{
			"title":"Editar",
			"load_url":"{$params["page"]["ajax"]}/{id}/mediaData",
			"save_url":"{$params["page"]["ajax"]}/{id}/mediaData",
			"actName":"Editar",
			"elems":{
				"url": {
					"field_type": "input",
					"data_type": "string",
					"info_name": "URL",
					"attributes": {
						"minlength": 0,
						"maxlength": 255,
						"data-regexp": null
					}
				},
				"alttext": {
					"field_type": "textarea",
					"data_type": "str",
					"info_name": "Texto alternativo",
					"info_description": "Describir la publicidad"
				}
			}
		}
	},
	"imageUpload":
	{
		"ajax-action":"edit",
		"ajax-elem":"obj",
		"ajax-update":false,
		"action":"form",
		"form":{
			"title":"Nueva imagen",
			"save_url":"{$params["page"]["ajax"]}/{id}/imageUpload",
			"actName":"Editar",
			"elems":{
				"id": {
					"field_type": "hidden",
					"data_type": "int"
				},
				"file": {
					"field_type": "file",
					"info_name": "Archivo",
					"attributes": {
						"accept": "image/*",
						"max_size": 512
					}
				}
			}
		}
	},
	"videoUpload":
	{
		"ajax-action":"edit",
		"ajax-elem":"obj",
		"ajax-update":false,
		"action":"form",
		"form":{
			"title":"Nuevo video",
			"save_url":"{$params["page"]["ajax"]}/{id}/videoUpload",
			"actName":"Editar",
			"elems":{
				"id": {
					"field_type": "hidden",
					"data_type": "int"
				},
				"file": {
					"field_type": "file",
					"info_name": "Archivo",
					"attributes": {
						"accept": "video/mp4",
						"max_size": 512
					}
				}
			}
		}
	},
	"del":{"save_url":"del", "ajax-elem":"obj", "del_url":"{$params["page"]["ajax"]}/{id}/del"}
}
JSON
];

//
$tableStructure = \snkeng\core\general\dynamicTable::createElement($an_struct, $exData);

// Page
$page['body'] = <<<HTML
<div class="pageTitle">Banners</div>

{$tableStructure}
HTML;
//