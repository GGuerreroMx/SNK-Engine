<?php

//
$params['page']['main'].= '/' . \snkeng\core\engine\nav::current();
$params['page']['ajax'].= '/' . \snkeng\core\engine\nav::current();

//
switch ( \snkeng\core\engine\nav::next() ) {
	//
	case 'scripts':
		require __DIR__ . '/uadmin_scripts.php';
		break;
	//
	case '':
	case null:
		require __DIR__ . '/#index.php';
		break;
	// Otros
	default:
		\snkeng\core\engine\nav::invalidPage();
		break;
}