<?php
// Add contact
$params['page']['main'].= '/' . \snkeng\core\engine\nav::current();
$params['page']['ajax'].= '/' . \snkeng\core\engine\nav::current();

//
if ( !is_numeric(\snkeng\core\engine\nav::next(false)) ) {
	require __DIR__ . '/contact_#index.php';
} else {
	//
	$params['vars']['cId'] = intval(\snkeng\core\engine\nav::next());

	// Add number
	$params['page']['ajax'].= '/' . \snkeng\core\engine\nav::current();
	$params['page']['main'].= '/' . \snkeng\core\engine\nav::current();

	//
	require __DIR__ . '/contact_[id].php';
}
//

// Nav get
$nav = \snkeng\core\admin\navBuild::navCreate();

// Menu
$page['body'] = <<<HTML
<div class="pageTitle">Contacto</div>

{$nav}

{$page['body']}
HTML;
//
