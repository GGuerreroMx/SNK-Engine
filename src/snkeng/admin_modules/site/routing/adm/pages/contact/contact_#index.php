<?php
//
\snkeng\core\engine\nav::cacheCheckFile(__FILE__);
\snkeng\core\engine\nav::cacheFinalCheck();

// Estructura
$an_struct = <<<HTML
<tr data-element="row" data-objid="!id;" data-objtitle="!title;" data-btype="!bTypeNum;">
	<td>!id;</td>
	<td>!dtAdd;</td>
	<td>!usrName;</td>
	<td>!usrMail;</td>
	<td>!usrIp;</td>
	<td><span class="fakeCheck" data-value="!statusRevised;"></span></td>
	<td><span class="fakeCheck" data-value="!statusSpam;"></span></td>
	<td>
		<a class="btn small" se-nav="app_content" href="./!id;/" title="Editar"><svg class="icon inline"><use xlink:href="#fa-eye" /></svg></a>
	</td>
</tr>
HTML;
//

//
$exData = [
	'js_id' => 'pagElements',
	'js_url' => $params['page']['ajax'].'/readAll',
	'printElements' => false,
	'printType' => 'table',
	'tableWide' => true,
	'contentClass' => 'siteAdm_banners',
	'tableHead'=>[
		['name' => 'ID', 'filter' => 1, 'fName'=>'id'],
		['name' => 'Dt Add'],
		['name' => 'Nombre', 'filter' => 1, 'fName'=>'title'],
		['name' => 'Correo'],
		['name' => 'IP'],
		['name' => 'Spam'],
		['name' => 'Revisado'],
		['name' => 'Acciones'],
	],
	'settings' => ['nav' => true],
	'actions' => null
];

//
$tableStructure = \snkeng\core\general\dynamicTable::createElement($an_struct, $exData);

// Page
$page['body'] = <<<HTML
<div class="pageTitle">Contactos</div>

{$tableStructure}
HTML;
//