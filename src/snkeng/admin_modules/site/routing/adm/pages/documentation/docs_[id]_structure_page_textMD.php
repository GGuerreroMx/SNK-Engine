<?php
//
\snkeng\core\engine\nav::cacheCheckFile(__FILE__);

// Markdown post edition
$markDownForm = \snkeng\core\site\markdown_form::formPrint(
	$params['vars']['cms_data']['name'],
	$params['vars']['cms_data']['type'],
	$params['vars']['pId'],
	$params['vars']['cms_data']['lang'],
	$params['page']['ajax'],
	$params['vars']['windows']['pages'],
	[
		'extraInput' => [
			'dId' => $params['vars']['dId']
		]
	]
);

// Close checks
\snkeng\core\engine\nav::cacheFinalCheck();

// Page
$page['body'].= <<<HTML
<div class="pageSubTitle">Texto</div>

{$markDownForm}
HTML;
//
