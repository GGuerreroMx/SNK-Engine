<?php
//
\snkeng\core\engine\nav::cacheCheckFile(__FILE__);
\snkeng\core\engine\nav::cacheFinalCheck();

//
\snkeng\core\engine\nav::pageFileModuleAdd('admin_modules', 'site', '/pages/site-adm-pages-docs-structure.css');
\snkeng\core\engine\nav::pageFileModuleAdd('admin_modules', 'site', '/pages/site-adm-pages-docs-structure.mjs');

//
$sql_qry = <<<SQL
SELECT
	docs.docs_id AS id, docs.img_id AS imgId,
	docs.docs_dt_add AS dtAdd, docs.docs_dt_mod AS dtMod,
	docs.docs_title_normal AS title, docs.docs_title_url AS urlTitle, docs.docs_description,
	docs.docs_img_struct, docs.docs_access_level, docs.docs_access_link_req
FROM sc_site_documents AS docs
WHERE docs.docs_id={$params['vars']['dId']}
SQL;
$docData = \snkeng\core\engine\mysql::singleRowAssoc(
	$sql_qry,
	[],
	[
		'errorKey' => '',
		'errorDesc' => '',
	]
);

//
$sql_qry = <<<SQL
SELECT
	art.art_id AS id,
	art.art_title AS title, art.art_urltitle AS urlTitle, art.art_url AS fullUrl,
	CONV(art.art_order_current , 36, 10) AS orderCur, art.art_order_level AS orderLvl,
	art.art_order_hierarchy AS orderHierarchy,
	IF(art.art_status_indexable, 'checked', '') AS indexable,
	IF(art.art_status_published, 'checked', '') AS published
FROM sc_site_articles AS art
WHERE art.art_type_primary='docs' AND art.cat_id='{$docData['id']}'
ORDER BY art.art_order_hierarchy ASC
LIMIT 150;
SQL;

$struct = <<<HTML
<div class="elem" se-type="elem" data-id="!id;" data-order="!orderCur;" data-level="!orderLvl;" data-url="!urlTitle;" data-title="!title;">
<div class="content">
	<div class="title"><input type="text" name="title" value="!title;" maxlength="60" /></div>
	<div>
		<label>
			<span>P:</span>
			<input type="checkbox" name="published" value="1" !published;/>
		</label>
		<label>
			<span>I:</span>
			<input type="checkbox" name="indexable" value="1" !indexable;/>
		</label>
		<a class="btn" se-nav="app_content" href="{$params['page']['main']}/!id;/text/"><svg class="icon"><use xlink:href="#fa-pencil" /></svg></a>
		<a class="btn" data-action="navigation" data-target="app_content" href="{$params['page']['main']}/!id;/props/"><svg class="icon"><use xlink:href="#fa-photo" /></svg></a>
		<button se-btn="delete"><svg class="icon"><use xlink:href="#fa-trash-o" /></svg></button>
	</div>
</div>
</div>
HTML;

$content = \snkeng\core\engine\mysql::printSimpleQuery($sql_qry, $struct);

// Content
$page['body'].= <<<HTML

<div class="pageSubTitle">Documentación: {$docData['title']}</div>

<site-adm-pages-docs-structure id="site_adm_docs_sorter" data-docid="{$docData['id']}" data-saveurl="{$params['page']['ajax']}/upd_order_many/">

<script se-elem="defaults" type="application/json5">
{
	save_url:'{$params['page']['ajax']}/upd_order_many',
	actions:{
		'del':{
			'action':'del',
			'ajax-elem':'obj',
			'action':'{$params['page']['ajax']}/del'
		}
	}
}
</script>

<div class="grid">

	<div class="gr_sz04">
		<div class="action_menu">
			<form se-elem="add" class="se_form" method="post" action="{$params['page']['ajax']}/add" is="se-async-form">
				<input type="hidden" name="dId" value="{$docData['id']}" />
				<input type="hidden" name="orderLvl" />
				<input type="hidden" name="orderCur" />
				<input type="hidden" name="orderHierarchy" />
				<input type="hidden" name="baseUrl" />
				<div class="separator required"><label>
					<div class="cont"><span class="title">Nombre</span><span class="desc"></span></div>
					<input name="title" value="" required="" pattern="[A-z\u00C0-\u00ff0-9\-., ]{3,60}$" type="text">
				</label></div>
				<button type="submit"><svg class="icon inline"><use xlink:href="#fa-plus" /></svg>Agregar</button><span se-elem="response"></span>
			</form>
			<div class="menu">
				<div class="nav">
				<button class="btn" data-action="navigation" data-target="up"><svg class="icon"><use xlink:href="#fa-caret-up" /></svg></button>
				<button class="btn" data-action="navigation" data-target="down"><svg class="icon"><use xlink:href="#fa-caret-down" /></svg></button>
				<button class="btn" data-action="navigation" data-target="left"><svg class="icon"><use xlink:href="#fa-caret-left" /></svg></button>
				<button class="btn" data-action="navigation" data-target="right"><svg class="icon"><use xlink:href="#fa-caret-right" /></svg></button>
				</div>
				<div class="act">
				<button class="btn" data-action="save"><svg class="icon"><use xlink:href="#fa-save" /></svg></button>
				</div>
			</div>
		</div>
	</div>

	<div class="gr_sz08">
		<template>{$struct}</template>
		<div se-elem="content">{$content}</div>
	</div>

</div>

</site-adm-pages-docs-structure>\n
HTML;
//
