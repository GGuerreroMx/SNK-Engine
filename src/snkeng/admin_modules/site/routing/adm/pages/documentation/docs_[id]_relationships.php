<?php
//
\snkeng\core\engine\nav::cacheCheckFile(__FILE__);
\snkeng\core\engine\nav::cacheFinalCheck();

// Estructura
$an_struct = <<<HTML
<tr data-element="row" data-objid="!id;" data-objtitle="!userName;">
	<td>!id;</td>
	<td>!userName;</td>
	<td>!docTitle;</td>
	<td>!dtAdd;</td>
	<td>!dtMod;</td>
	<td>
		<button class="btn small" data-dyntab-onclick="element_op" data-action="edit" title="Editar"><svg class="icon inline"><use xlink:href="#fa-list" /></svg></button>
	</td>
</tr>
HTML;
//

//
$exData = [
	'js_url' => $params['page']['ajax'] . '/readAll',
	'js_defaults' => "{docId:{$params['vars']['dId']}}",
	'printType' => 'table',
	'tableWide' => true,
	'tableHead' => [
		['name' => 'ID', 'filter' => 1, 'fName' => 'id'],
		['name' => 'Usuario', 'filter' => 1, 'fName' => 'userName'],
		['name' => 'Documentación', 'filter' => 1, 'fName' => 'docTitle'],
		['name' => 'DT Add'],
		['name' => 'DT Mod'],
		['name' => 'Acciones'],
	],
	'settings' => ['nav' => true],
	'actions' => <<<JSON
{
	"add":
	{
		"ajax-action":"add",
		"ajax-elem":"none",
		"menu":{"title":"Agregar", "icon":"fa-plus"},
		"print":false,
		"action":"form",
		"form":{
			"title":"Nueva relación",
			"save_url":"{$params["page"]["ajax"]}/add",
			"actName":"Agregar",
			"elems":{
				"userId": {
					"field_type": "input",
					"data_type": "number",
					"info_name": "ID Usuario",
					"attributes": {
						"type": "number",
						"min": 0,
						"max": 4294967295
					}
				}
			}
		}
	},
	"edit":
	{
		"ajax-action":"edit",
		"ajax-elem":"obj",
		"action":"form",
		"form":{
			"title":"Propiedades",
			"load_url":"{$params["page"]["ajax"]}/statusRead",
			"save_url":"{$params["page"]["ajax"]}/statusUpd",
			"actName":"Actualizar",
			"elems":{
				"id": {
					"field_type": "hidden",
					"data_type": "int"
				},
				"dStatus": {
					"field_type": "checkbox",
					"data_type": "bool",
					"info_name": "Activo",
					"attributes": {
						"value": 1
					}
				}
			}
		}
	}
}
JSON
	//
];
//

//
$tableStructure = \snkeng\core\general\dynamicTable::createElement($an_struct, $exData);

// Page
$page['body'] = <<<HTML
<div class="pageSubTitle">Relación</div>

{$tableStructure}
HTML;
//
