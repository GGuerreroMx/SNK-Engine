<?php
// Contenido
$sql_qry = <<<SQL
SELECT
	art_id AS id, art_content AS content
FROM sc_site_articles AS art
WHERE art_id={$params['vars']['pId']}
LIMIT 1;
SQL;
$postData = \snkeng\core\engine\mysql::singleRowAssoc($sql_qry, [
	'int' => ['id']
]);

// Contenido
$page['body'] .= <<<HTML
<div class="pageSubTitle">Texto (HTML)</div>

<div class="grid">

	<div class="gr_sz09">
		<form class="se_form" action="{$params['page']['ajax']}/textHTMLUpd" is="se-async-form">
			<input type="hidden" name="id" value="{$postData['id']}" />
			<input type="hidden" name="dId" value="{$params['vars']['dId']}" />
			<label class="separator required adv_text">
				<div class="cont"><span class="title">Contenido</span><span class="desc"></span></div>
				<textarea se-plugin="wysiwyg" name="content">{$postData['content']}</textarea>
			</label>
			<button type="submit"><svg class="icon inline mr"><use xlink:href="#fa-save" /></svg>Guardar</button>
			<output se-elem="response"></output>
		</form>
	</div>

	<div class="gr_sz03"></div>

</div>
HTML;
//
