<?php
// Set parameters
switch ( \snkeng\core\engine\nav::next() ) {
	//
	case 'add':
		$rData = \snkeng\core\general\saveData::postParsing(
			$response,
			[
				'title' => ['name' => 'Nombre',   'type' => 'str', 'lMin' => 0, 'lMax' => 75, 'filter' => 'simpleTitle', 'process' => false, 'validate' => true],
				'fullUrl' => ['name' => 'Full URL',   'type' => 'str', 'lMin' => 0, 'lMax' => 75, 'filter' => 'urlPath', 'process' => false, 'validate' => true],
			]
		);

		// Fix URL
		if ( $rData['fullUrl'][0] !== '/' ) { $rData['fullUrl'] = '/'.$rData['fullUrl']; }
		if ( $rData['fullUrl'][-1] !== '/' ) { $rData['fullUrl'].= '/'; }

		//
		\snkeng\admin_modules\site\page::addPage(
			$response,
			'page',
			'',
			$rData['title'],
			'',
			'<div class="wpContent"><p>Insertar contenido de la página.</p></div>',
			[
				'fullUrl' => $rData['fullUrl']
			]
		);
		break;

	// Actualizar
	case 'upd':
		//
		\snkeng\core\general\saveData::sql_complex_rowUpd(
			$response,
			[
				'id' => ['name' => 'ID Objeto',   'type' => 'int', 'validate' => true],
				'title' => ['name' => 'Nombre',   'type' => 'str', 'lMin' => 0, 'lMax' => 75, 'filter' => 'simpleTitle', 'process' => false, 'validate' => true],
				'fullUrl' => ['name' => 'Nombre URL',   'type' => 'str', 'lMin' => 0, 'lMax' => 75, 'filter' => 'urlPath', 'process' => false, 'validate' => true],
				'isPub' => ['name' => 'Publicado',   'type' => 'bool'],
				'isIndexed' => ['name' => 'Indexable',   'type' => 'bool'],
			],
			[
				'elems' => [
					'art_title' => 'title',
					'art_url' => 'fullUrl',
					'art_status_published' => 'isPub',
					'art_status_indexable' => 'isIndexed',
				],
				'tName' => 'sc_site_articles',
				'tId' => ['nm' => 'id', 'db' => 'art_id'],
			],
			[

			]
		);
		break;

	// Borrar
	case 'del':
		//
		\snkeng\core\general\saveData::sql_complex_rowDel(
			$response,
			[
				'tName' => 'sc_site_articles',
				'tId' => ['db' => 'art_id']
			],
			'id'
		);
		break;

	// Leer todas
	case 'readAll':
		// Preparar estructura
		\snkeng\core\general\asyncDataJson::printJson(
			[
				'sel' => "art.art_id AS id, art.art_title AS title, art.art_urltitle AS urltitle,
					art.art_type_primary AS artType, art.art_type_secondary AS artTypeSub,
					DATE(art.art_dt_pub) AS dtPub, art.art_dt_add AS dtAdd, art.art_dt_mod AS dtMod,
					art.art_status_published AS isPub,
					art.art_status_indexable AS isIndexed,
					art.art_count_views AS views,
					art.art_url AS fullUrl
				",
				'from' => 'sc_site_articles AS art',
				'lim' => 30,
				'where' => [
					'artType' => ['name' => 'Tipo', 'db'=>'art.art_type_primary', 'vtype'=>'str', 'stype'=>'eq', 'set'=> 'page'],
					'id' => ['name' => 'ID', 'db' => 'art.art_id', 'vtype' => 'int', 'stype' => 'eq'],
					'title' => ['name' => 'Título', 'db' => 'art.art_title', 'vtype' => 'str', 'stype' => 'like'],
				],
				'order' => [
					'urlTitle' => ['Name' => 'ID', 'db' => 'art.art_url']
				],
				'default_order' => [
					['urlTitle', 'ASC']
				]
			]
		);
		break;

	//
	default:
		\snkeng\core\engine\nav::invalidPage();
		break;
}
//
