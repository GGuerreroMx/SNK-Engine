<?php

//
switch ( \snkeng\core\engine\nav::next() ) {
	//
	case 'context':
		\snkeng\core\general\saveData::sql_complex_uri_update_read(
			$response,
			$params['vars']['cms_data']['parentId'],
			[
				'elems' => [
					'author_id' => 'authorId',
					'docs_mode_type' => 'docType',
				],
				'tName' => 'sc_site_documents',
				'tId' => ['nm' => 'id', 'db' => 'docs_id']
			],
			[
				'authorId' => ['name' => 'ID Usuario', 'type' => 'int', 'validate' => true],
				'docType' => ['name' => 'Nombre', 'type' => 'str', 'lMin' => 4, 'lMax' => 10, 'filter' => 'simpleTitle', 'process' => false, 'validate' => true],
			]
		);
		break;

	//
	case 'titles':
		\snkeng\core\general\saveData::sql_complex_uri_update_read(
			$response,
			$params['vars']['cms_data']['parentId'],
			[
				'elems' => [
					'cat_id' => 'catId',
					'docs_title_normal' => 'title',
					'docs_title_url' => 'urlTitle',
					'docs_content_bundles' => 'contentBundles',
					'docs_content_body_mod' => 'contentBodyMod',
					'docs_description' => 'shortDescription',
					'docs_content' => 'content',
				],
				'tName' => 'sc_site_documents',
				'tId' => ['nm' => 'id', 'db' => 'docs_id']
			],
			[
				'catId' => ['name' => 'ID Categoría', 'type' => 'int', 'validate' => true],
				'title' => ['name' => 'Nombre', 'type' => 'str', 'lMin' => 4, 'lMax' => 75, 'filter' => 'simpleTitle', 'process' => false, 'validate' => true],
				'urlTitle' => ['name' => 'Nombre', 'type' => 'str', 'lMin' => 4, 'lMax' => 75, 'filter' => 'urlTitle', 'process' => false, 'validate' => true],
				'contentBundles' => ['name' => 'Bundles', 'type' => 'str', 'lMax' => 75, 'filter' => 'simpleTitle', 'process' => false, 'validate' => false],
				'contentBodyMod' => ['name' => 'Budy Mod', 'type' => 'str', 'lMax' => 75, 'filter' => 'simpleTitle', 'process' => false, 'validate' => false],
				'shortDescription' => ['name' => 'Descripción', 'type' => 'str', 'lMin' => 0, 'lMax' => 230],
				'content' => ['name' => 'Contenido', 'type' => 'str', 'lMin' => 0, 'lMax' => 60000],
			]
		);
		break;

	//
	case 'permits':
		\snkeng\core\general\saveData::sql_complex_uri_update_read(
			$response,
			$params['vars']['cms_data']['parentId'],
			[
				'elems' => [
					'docs_published' => 'isPub',
					'docs_access_level' => 'accessLevel',
					'docs_access_link_req' => 'isLinkReq',
				],
				'tName' => 'sc_site_documents',
				'tId' => ['nm' => 'id', 'db' => 'docs_id']
			],
			[
				'isPub' => ['name' => 'Acceso', 'type' => 'bool'],
				'accessLevel' => ['name' => 'Acceso', 'type' => 'int', 'validate' => true],
				'isLinkReq' => ['name' => 'Acceso', 'type' => 'bool'],
			]
		);
		break;

	// Imagen
	case 'imageUpd':
		//
		$rData = \snkeng\core\general\saveData::postParsing(
			$response,
			[
				'imgId' => ['name' => 'ID imagen', 'type' => 'int', 'process' => false, 'validate' => true]
			]
		);

		// Seleccionar
		$sql_qry = "SELECT img.img_id AS id, img.img_fname AS fName, img.img_name AS gName, img.img_width AS imgWidth, img.img_height AS imgHeight FROM sc_site_images AS img WHERE img.img_id={$rData['imgId']};";
		$imgData = \snkeng\core\engine\mysql::singleRowAssoc($sql_qry, ['int' => ['id']]);

		//
		$upd_qry = <<<SQL
UPDATE sc_site_documents SET img_id='{$imgData['id']}', docs_img_struct='{$imgData['fName']}', docs_img_alt_text='{$imgData['gName']}', docs_img_width='{$imgData['imgWidth']}', docs_img_height='{$imgData['imgHeight']}' WHERE docs_id={$params['vars']['cms_data']['parentId']};
SQL;
		\snkeng\core\engine\mysql::submitQuery($upd_qry);

		$response['d'] = [
			'img_file' => "/res/image/site/w_320/{$imgData['fName']}"
		];
		break;

	//
	default:
		\snkeng\core\engine\nav::invalidPage();
		break;
}
//
