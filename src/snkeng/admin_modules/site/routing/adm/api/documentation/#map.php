<?php
// Set parameters
$params['vars']['cms_data']['name'] = 'site';
$params['vars']['cms_data']['type'] = 'docs';
$params['vars']['cms_data']['parentId'] = 0;

//
switch ( \snkeng\core\engine\nav::next() )
{
	//
	case 'docs':
		//
		if ( !is_numeric(\snkeng\core\engine\nav::next(false)) ) {
			require __DIR__ . '/docs_object.php';
		}
		else {
			//
			$params['vars']['cms_data']['parentId'] = intval(\snkeng\core\engine\nav::next());

			//
			switch ( \snkeng\core\engine\nav::next() ) {
				//
				case 'properties':
					require __DIR__ . '/docs_single_properties.php';
					break;
				//
				case 'structure':
					if ( !is_numeric(\snkeng\core\engine\nav::next(false)) ) {
						require __DIR__ . '/docs_single_structure_general.php';
					}
					else {
						//
						$params['vars']['pageId'] = intval(\snkeng\core\engine\nav::next());
						require __DIR__ . '/docs_single_structure_pages.php';
					}
					break;

				//
				case 'relationships':
					require __DIR__ . '/docs_single_relationships.php';
					break;

				//
				case 'files':
					require __DIR__ . '/../shared/files.php';
					break;

				//
				case 'images':
					require __DIR__ . '/../shared/images.php';
					break;

				//
				case '':
				case null:
					require __DIR__ . '/polls_a_index.php';
					break;
				//
				default:
					\snkeng\core\engine\nav::invalidPage();
					break;
			}
		}
		break;

	//
	case 'categories':
		require __DIR__ . '/../shared/categories.php';
		break;

	//
	case 'tags':
		require __DIR__ . '/../shared/tags.php';
		break;

	//
	default:
		\snkeng\core\engine\nav::invalidPage();
		break;
}
//