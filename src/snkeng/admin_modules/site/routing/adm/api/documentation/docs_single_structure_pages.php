<?php

//
switch ( \snkeng\core\engine\nav::next() ) {
	//
	case 'properties':
		// Most operations are done here
		\snkeng\admin_modules\site\page::propertiesUpdate(
			$response,
			'docs',
			$params['vars']['pageId'],
			\snkeng\core\engine\nav::next(),
			[
				'metadata' => 1,
				'title' => 0,
				'category' => 0,
				'image' => 1,
				'imageAltText' => 1,
				'publish' => 1,
				'behaviour' => 0,
				'tags' => 0,
				'rating' => 0,
			]
		);
		break;

	case 'textMD':
		switch ( $_SERVER['REQUEST_METHOD'] ) {
			//
			case 'GET':
				//
				$response = \snkeng\admin_modules\site\markdown_page::md_text_get(
					$params['vars']['cms_data']['name'],
					$params['vars']['cms_data']['type'],
					$params['vars']['pageId'],
					$params['vars']['cms_data']['lang']
				);
				break;

			//
			case 'POST':
				//
				$rData = \snkeng\core\general\saveData::postParsing(
					$response,
					[
						'txtId' => ['name'=>'ID Texto', 'type'=>'int', 'process'=>false, 'validate'=>true],
						'lang' => ['name'=>'Idioma', 'type'=>'str', 'process'=>false, 'validate'=>true],
						'content' => ['name'=>'Idioma', 'type'=>'straightStr', 'process'=>false, 'validate'=>true],
					]
				);

				//
				$content = \snkeng\core\site\texts::upd(
					$params['vars']['cms_data']['name'],
					$params['vars']['cms_data']['type'],
					$params['vars']['pageId'],
					$params['vars']['cms_data']['lang'], // Ignoring sent lang...?
					$rData['txtId'],
					$rData['content'],
					[],
					['html']
				);

				// Build navigation headers


				$doc = new DOMDocument('1.0', 'utf-8');
				$doc->formatOutput = true;
				$doc->strictErrorChecking = false;

				// To skip uncompatible html5 tags... apparently a bug from 2016
				libxml_use_internal_errors(true);

				//
				$doc->loadHTML(mb_convert_encoding($content['html'], 'HTML-ENTITIES', 'UTF-8'));
				$xpath = new DOMXPath($doc);
				libxml_clear_errors();

				//
				$altDoc = new DOMDocument('1.0', 'utf-8');
				$altDoc->formatOutput = true;

				//
				$frag = $altDoc->createDocumentFragment();
				$head = &$frag;

				//
				$last = 1;
				$cLevel = 0;
				$cOrder = [0, 0, 0, 0, 0, 0];

				// Buscar títulos
				foreach ( $xpath->query('//*[self::h1 or self::h2 or self::h3 or self::h4 or self::h5 or self::h6]') as $headline ) {
					// get level of current headline
					sscanf($headline->tagName, 'h%u', $curr);

					//
					$curOrder = ++$cOrder[$curr-1];

					// move head reference if necessary
					if ( $curr < $last ) {
						for ( $i = $curr; $i < 6; $i++ ) {
							$cOrder[$i] = 0;
						}
					}
					$last = $curr;

					// Create ID
					$idNumber = '';
					$first = true;
					for ( $i = 0; $i < 6; $i++ ) {
						if ( $cOrder[$i] === 0 ) { break; }
						$idNumber.= ( $first ) ? '' : '.';
						$idNumber.= $cOrder[$i];
						$first = false;
					}
					$id = 'nav_sec_' . $idNumber;

					// add anchor to headline
					$headline->setAttribute('id', $id);

					// add list item
					$a = $altDoc->createElement('a', $headline->textContent);
					$a->setAttribute('href', '#' . $id);
					$a->setAttribute('data-level', $curr);
					$a->setAttribute('data-order', $curOrder);

					//
					$head->appendChild($a);
				}

				//
				if ( $frag->firstChild ) {
					$altDoc->appendChild($frag);
				}

				// lista de títulos
				$navParse = $altDoc->saveHTML();
				$navParse = \snkeng\core\engine\mysql::real_escape_string($navParse);

				// Se modifican los títulos, por lo que hay que guardar la nueva versión
				$modContent = $doc->saveHTML($doc->getElementsByTagName('body')->item(0)); // Extract body information
				$modContent = trim(substr($modContent, 6, -7)); // Removing body tags
				$modContent = \snkeng\core\engine\mysql::real_escape_string($modContent);

				//
				\snkeng\core\general\saveData::sql_table_updRow(
					$response,
					[
						'elems' => [
							'art_content' => 'content',
							'art_content_extra' => 'contentExtra',
						],
						'tName' => 'sc_site_articles',
						'tId' => ['nm' => 'id', 'db' => 'art_id'],
					],
					[
						'id' => $params['vars']['pageId'],
						'content' => $modContent,
						'contentExtra' => $navParse
					]
				);
				break;

			//
			default:
				\snkeng\core\engine\nav::invalidMethod();
				break;
		}
		break;

	// texto update
	case 'textHTMLUpd':
		//
		$rData = \snkeng\core\general\saveData::postParsing($response, [
			'content' => ['name'=>'Idioma', 'type'=>'straightStr', 'process'=>false, 'validate'=>true],
		]);

		//
		$doc = new DOMDocument('1.0', 'utf-8');
		$doc->formatOutput = true;
		$doc->strictErrorChecking = false;

		//
		$doc->loadHTML(mb_convert_encoding($rData['content'], 'HTML-ENTITIES', 'UTF-8'));
		$xpath = new DOMXPath($doc);

		//
		$altDoc = new DOMDocument('1.0', 'utf-8');
		$altDoc->formatOutput = true;

		//
		$frag = $altDoc->createDocumentFragment();
		$head = &$frag;

		//
		$last = 1;
		$cLevel = 0;
		$cOrder = [0, 0, 0, 0, 0, 0];

		// Buscar títulos
		foreach ( $xpath->query('//*[self::h1 or self::h2 or self::h3 or self::h4 or self::h5 or self::h6]') as $headline ) {
			// get level of current headline
			sscanf($headline->tagName, 'h%u', $curr);

			//
			$curOrder = ++$cOrder[$curr-1];

			// move head reference if necessary
			if ( $curr < $last ) {
				for ( $i = $curr; $i < 6; $i++ ) {
					$cOrder[$i] = 0;
				}
			}
			$last = $curr;

			// Create ID
			$idNumber = '';
			$first = true;
			for ( $i = 0; $i < 6; $i++ ) {
				if ( $cOrder[$i] === 0 ) { break; }
				$idNumber.= ( $first ) ? '' : '.';
				$idNumber.= $cOrder[$i];
				$first = false;
			}
			$id = 'nav_sec_' . $idNumber;

			// add anchor to headline
			$headline->setAttribute('id', $id);

			// add list item
			$a = $altDoc->createElement('a', $headline->textContent);
			$a->setAttribute('href', '#' . $id);
			$a->setAttribute('data-level', $curr);
			$a->setAttribute('data-order', $curOrder);

			//
			$head->appendChild($a);
		}

		//
		if ( $frag->firstChild ) {
			$altDoc->appendChild($frag);
		}

		// lista de títulos
		$navParse = $altDoc->saveHTML();
		$navParse = \snkeng\core\engine\mysql::real_escape_string($navParse);

		// Se modifican los títulos, por lo que hay que guardar la nueva versión
		$modContent = $doc->saveHTML($doc->getElementsByTagName('body')->item(0)); // Extract body information
		$modContent = trim(substr($modContent, 6, -7)); // Removing body tags
		$modContent = \snkeng\core\engine\mysql::real_escape_string($modContent);

		//
		\snkeng\core\general\saveData::sql_table_updRow(
			$response,
			[
				'elems' => [
					'art_content' => 'content',
					'art_content_extra' => 'contentExtra',
				],
				'tName' => 'sc_site_articles',
				'tId' => ['nm' => 'id', 'db' => 'art_id'],
			],
			[
				'id' => $params['vars']['pageId'],
				'content' => $modContent,
				'contentExtra' => $navParse
			]
		);
		break;


	// Borrar
	case 'del':
		$tData = [
			'tName' => 'sc_site_articles',
			'tId' => ['db' => 'art_id']
		];
		// \snkeng\core\general\saveData::sql_table_delRow($response, $tData, 'id');
		break;

	// Leer todas
	case 'readAll':
		// Preparar estructura
		\snkeng\core\general\asyncDataJson::printJson(
			[
				'sel' => "art.art_id AS id, art.art_title AS title, art.art_urltitle AS urltitle,
						art.art_type_primary AS artType, art.art_type_secondary AS artTypeSub,
						DATE(art.art_dt_pub) AS dtPub, art.art_dt_add AS dtAdd, art.art_dt_mod AS dtMod,
						art.art_status_published AS isPub,
						art.art_status_indexable AS isIndexed,
						art.art_count_views AS views,
						art.art_url AS fullUrl",
				'from' => 'sc_site_articles AS art',
				'lim' => 30,
				'where' => [
					'artType' => ['name' => 'Tipo', 'db'=>'art.art_type_primary', 'vtype'=>'str', 'stype'=>'eq', 'set'=> 'page'],
					'id' => ['name' => 'ID', 'db' => 'art.art_id', 'vtype' => 'int', 'stype' => 'eq'],
					'title' => ['name' => 'Título', 'db' => 'art.art_title', 'vtype' => 'str', 'stype' => 'like'],
				],
				'order' => [
					'urlTitle' => ['Name' => 'ID', 'db' => 'art.art_url']
				],
				'default_order' => [
					['urlTitle', 'ASC']
				]
			]
		);
		break;

	//
	default:
		\snkeng\core\engine\nav::invalidPage();
		break;
}
//
