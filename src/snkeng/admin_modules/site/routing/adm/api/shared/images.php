<?php
if ( !is_numeric(\snkeng\core\engine\nav::next(false)) ) {
	//
	switch ( \snkeng\core\engine\nav::next() )
	{
		//
		case 'imgAdd':
			// Validar
			$rData = \snkeng\core\general\saveData::postParsing(
				$response,
				[
					'imgName' => ['name' => 'Nombre', 'type' => 'str', 'lMin' => 4, 'lMax' => 75, 'filter' => 'simpleTitle', 'validate' => true],
					'imgDesc' => ['name' => 'Descripción', 'type' => 'str', 'lMin' => 0, 'lMax' => 250, 'filter' => 'simpleText', 'validate' => false],
					'imgType' => ['name' => 'Tipo', 'type' => 'int', 'validate' => true],
					'imgPropEditable' => ['name' => 'Editable', 'type' => 'bool'],
					'imgPropWidth' => ['name' => 'Propiedad ancho', 'type' => 'int', 'validate' => true],
					'imgPropHeight' => ['name' => 'Propiedad alto', 'type' => 'int', 'validate' => true],
					'imgFile' => ['name' => 'Image', 'type' => 'file', 'fType'=>['jpg', 'jpeg', 'webp', 'png', 'svg'], 'mSize'=>2048, 'validate' => true],
				]
			);

			//
			\snkeng\admin_modules\site\image::add($response, $rData, 'site', $params['vars']['cms_data']['type'], $params['vars']['cms_data']['parentId']);
			break;

		// Leer todas
		case 'readAll':
			$params['vars']['cms_data']['parentId'] = ( $params['vars']['cms_data']['parentId'] !== 0 ) ? $params['vars']['cms_data']['parentId'] : null;

			// Preparar estructura
			\snkeng\core\general\asyncDataJson::printJson(
				[
					'sel' => "img.img_id AS id,
				img.img_mode_type AS modeType, img.img_mode_id AS modeId,
				img.img_dt_add AS dtAdd, img.img_dt_mod AS dtMod, 
				img.img_name AS name, img.img_desc AS content,
				img.img_floc AS floc, img.img_fname AS fname,
				img.img_width AS width, img.img_height as height,
				IF(img.img_prop_adjustable, CONCAT('/res/image/site/w_160/', img.img_fname, '.webp'), img.img_floc) AS imgFileLoc,
				IF(img.img_prop_adjustable, CONCAT('/res/image/site/original/', img.img_fname, '.webp'), img.img_floc) AS floc,
				img.img_prop_adjustable AS propAdjustable",
					'from' => 'sc_site_images AS img',
					'lim' => 25,
					'where' => [
						'id' => ['name' => 'ID', 'db' => 'img.img_id', 'vtype' => 'int', 'stype' => 'eq'],
						'text' => ['name' => 'Nombre', 'db' => 'img.img_name, img.img_desc', 'vtype' => 'str', 'stype' => 'match'],
						'type' => ['name' => 'Tipo', 'db' => 'img.img_ftype', 'vtype' => 'int', 'stype' => 'eq'],
						'modeType' => ['name' => 'Tipo', 'db' => 'img.img_mode_type', 'vtype' => 'str', 'stype' => 'eq', 'set' => $params['vars']['cms_data']['type']],
						'modeId' => ['name' => 'Tipo', 'db' => 'img.img_mode_id', 'vtype' => 'int', 'stype' => 'eq', 'set' => $params['vars']['cms_data']['parentId']]
					],
					'order' => [
						'id' => ['Name' => 'ID', 'db' => 'img.img_id']
					],
					'default_order' => [
						['id', 'DESC']
					]
				]
			);
			break;

		//
		default:
			\snkeng\core\engine\nav::invalidPage();
			break;
	}
} else {
	//
	$imgId = intval(\snkeng\core\engine\nav::next());

	if ( $imgId === 0 ) { \snkeng\core\engine\nav::killWithError('ID no válido.'); }

	//
	switch ( \snkeng\core\engine\nav::next() )
	{
		//
		case 'dataUpd':
			// Validar
			$rData = \snkeng\core\general\saveData::postParsing(
				$response,
				[
					'imgName' => ['name' => 'Nombre', 'type' => 'str', 'lMin' => 5, 'lMax' => 75, 'filter' => 'simpleTitle', 'validate' => true],
					'imgDesc' => ['name' => 'Descripción', 'type' => 'str', 'lMin' => 0, 'lMax' => 250, 'filter' => 'simpleText', 'validate' => false],
				]
			);

			//
			\snkeng\admin_modules\site\image::dataUpd($response, $imgId, $rData);
			break;

		//
		case 'imgReplace':
			// Validar
			$rData = \snkeng\core\general\saveData::postParsing(
				$response,
				[
					'imgPropWidth' => ['name' => 'Propiedad ancho', 'type' => 'int', 'validate' => true],
					'imgPropHeight' => ['name' => 'Propiedad alto', 'type' => 'int', 'validate' => true],
					'imgFile' => ['name' => 'Image', 'type' => 'file', 'fType'=>['jpg', 'jpeg', 'webp', 'png', 'svg'], 'mSize'=>2048, 'validate' => true],
				]
			);

			//
			\snkeng\admin_modules\site\image::imageReplace($response, $imgId, $rData, 'site');
			break;

		//
		case 'imgUpdate':
			// Validar
			$rData = \snkeng\core\general\saveData::postParsing($response,
				[
					'imgPropWidth' => ['name' => 'Propiedad ancho', 'type' => 'int', 'validate' => true],
					'imgPropHeight' => ['name' => 'Propiedad alto', 'type' => 'int', 'validate' => true],
					'imgFile' => ['name' => 'Image', 'type' => 'file', 'fType'=>['jpg', 'jpeg', 'webp', 'png', 'svg'], 'mSize'=>2048, 'validate' => true],
				]
			);

			// TODO: check
			\snkeng\admin_modules\site\image::imageUpdate($response, $imgId, $rData, 'site');
			break;

		//
		case 'imgDelete':
			// Operación
			\snkeng\admin_modules\site\image::delete($response, $imgId);
			break;

		//
		case 'imgCopy':
			\snkeng\admin_modules\site\image::copy($response, $imgId);
			break;

		//
		case 'imgResize':
			$imgType = intval($_POST['imgType']);
			$imgSizeX = intval($_POST['imgSizeX']);
			$imgSizeY = intval($_POST['imgSizeY']);

			// Revisión
			if ( empty($imgId) ) {
				\snkeng\core\engine\nav::killWithError('ID de imagen no válido.');
			}
			if ( empty($imgType) ) {
				\snkeng\core\engine\nav::killWithError('Falta tipo de imagen.');
			}
			if ( empty($imgSizeX) ) {
				\snkeng\core\engine\nav::killWithError('Tamaño X no válido.');
			}
			if ( empty($imgSizeY) ) {
				\snkeng\core\engine\nav::killWithError('Tamaño Y no válido.');
			}

			//
			\snkeng\admin_modules\site\image::resize($response, $imgId, $imgType, $imgSizeX, $imgSizeY);
			break;

		//
		case 'imgCut':
			$imgOriginX = intval($_POST['imgOriginX']);
			$imgSizeX = intval($_POST['imgSizeX']);
			$imgOriginY = intval($_POST['imgOriginY']);
			$imgSizeY = intval($_POST['imgSizeY']);
			$imgEndSizeX = intval($_POST['imgEndSizeX']);
			$imgEndSizeY = intval($_POST['imgEndSizeY']);
			// Revisión
			if ( empty($imgId) ) {
				\snkeng\core\engine\nav::killWithError('ID de imagen no válido.');
			}
			if ( empty($imgSizeX) ) {
				\snkeng\core\engine\nav::killWithError('Tamaño X no válido.');
			}
			if ( empty($imgSizeY) ) {
				\snkeng\core\engine\nav::killWithError('Tamaño Y no válido.');
			}
			if ( empty($imgEndSizeX) ) {
				\snkeng\core\engine\nav::killWithError('Tamaño X final no válido.');
			}
			if ( empty($imgEndSizeY) ) {
				\snkeng\core\engine\nav::killWithError('Tamaño Y final no válido.');
			}
			//
			\snkeng\admin_modules\site\image::cut($response, $imgId, $imgOriginX, $imgSizeX, $imgOriginY, $imgSizeY, $imgEndSizeX, $imgEndSizeY);
			break;

		//
		default:
			\snkeng\core\engine\nav::invalidPage();
			break;
	}
}
//
