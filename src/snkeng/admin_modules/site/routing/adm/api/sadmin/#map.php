<?php
// Páginas

$params['vars']['cms_data']['type'] = '';
$params['vars']['cms_data']['parentId'] = 0;

//
switch ( \snkeng\core\engine\nav::next() ) {
	//
	case 'texts':
		require __DIR__ . '/texts.php';
		break;
	//
	case 'pages':
		require __DIR__ . '/pages.php';
		break;
	//
	case 'categories':
		require __DIR__ . '/../shared/categories.php';
		break;

	//
	case 'tags':
		require __DIR__ . '/../shared/tags.php';
		break;

	//
	case 'images':
		require __DIR__ . '/../shared/images.php';
		break;

	//
	case 'files':
		require __DIR__ . '/../shared/files.php';
		break;

	// Otros
	default:
		\snkeng\core\engine\nav::invalidPage();
		break;
}
//
