<?php
// Páginas
switch ( \snkeng\core\engine\nav::next() )
{
	//
	case 'add':
		$rData = \snkeng\core\general\saveData::postParsing(
			$response,
			[
				'title' => ['name' => 'Nombre',   'type' => 'str', 'lMin' => 0, 'lMax' => 75, 'filter' => 'simpleTitle', 'process' => false, 'validate' => true],
				'fullUrl' => ['name' => 'Full URL',   'type' => 'str', 'lMin' => 0, 'lMax' => 75, 'filter' => 'urlPath', 'process' => false, 'validate' => true],
			]
		);
		//
		$rData['artType'] = 'page';
		$rData['artTypeSub'] = '';

		//
		if ( $rData['fullUrl'][0] !== '/' ) { $rData['fullUrl'] = '/'.$rData['fullUrl']; }
		if ( $rData['fullUrl'][-1] !== '/' ) { $rData['fullUrl'].= '/'; }

		//
		$rData['urltitle'] = \snkeng\core\general\saveData::buildFriendlyTitle($rData['title']);
		$rData['content'] = '<p>Insertar nuevo contenido.</p>';
		$rData['author'] = $siteVars['user']['data']['id'];

		//
		$nId = \snkeng\core\engine\mysql::getNextId('sc_site_articles');

		//
		\snkeng\core\general\saveData::sql_table_addRow(
			$response,
			[
				'elems' => [
					'art_type_primary' => 'artType',
					'art_type_secondary' => 'artTypeSub',
					'a_id' => 'author',
					'art_title' => 'title',
					'art_urltitle' => 'urltitle',
					'art_url' => 'fullUrl',
					'art_content' => 'content',
				],
				'tName' => 'sc_site_articles',
				'tId' => ['nm' => 'id', 'db' => 'art_id'],

			],
			$rData
		);
		break;

	// Actualizar
	case 'upd':
		//
		\snkeng\core\general\saveData::sql_complex_rowUpd(
			$response,
			[
				'id' => ['name' => 'ID Objeto',   'type' => 'int', 'validate' => true],
				'title' => ['name' => 'Nombre',   'type' => 'str', 'lMin' => 0, 'lMax' => 75, 'filter' => 'simpleTitle', 'process' => false, 'validate' => true],
				'fullUrl' => ['name' => 'Nombre URL',   'type' => 'str', 'lMin' => 0, 'lMax' => 75, 'filter' => 'urlPath', 'process' => false, 'validate' => true],
				'isPub' => ['name' => 'Publicado',   'type' => 'bool'],
				'isIndexed' => ['name' => 'Indexable',   'type' => 'bool'],
				'artType' => ['name' => 'Tipo',   'type' => 'str', 'lMin' => 0, 'lMax' => 10, 'filter' => 'simpleURL', 'process' => false, 'validate' => true],
				'artTypeSub' => ['name' => 'Sub Tipo',   'type' => 'str', 'lMin' => 0, 'lMax' => 10, 'filter' => 'simpleURL', 'process' => false, 'validate' => false],
			],
			[
				'elems' => [
					'art_title' => 'title',
					'art_url' => 'fullUrl',
					'art_status_published' => 'isPub',
					'art_status_indexable' => 'isIndexed',
					'art_type_primary' => 'artType',
					'art_type_secondary' => 'artTypeSub',
				],
				'tName' => 'sc_site_articles',
				'tId' => ['nm' => 'id', 'db' => 'art_id'],
			]
		);
		break;

	// Actualizar
	case 'metaTextUpd':
		//
		\snkeng\core\general\saveData::sql_complex_rowUpd(
			$response,
			[
				'id' => ['name' => 'ID Objeto',   'type' => 'int', 'validate' => true],
				'metaWord' => ['name' => 'Meta Word',   'type' => 'str', 'lMin' => 0, 'lMax' => 240, 'filter' => 'simpleTitle', 'process' => false, 'validate' => true],
				'metaDesc' => ['name' => 'Meta Desc',   'type' => 'str', 'lMin' => 0, 'lMax' => 240, 'filter' => 'simpleTitle', 'process' => false, 'validate' => true],
			],
			[
				'elems' => [
					'art_meta_word' => 'metaWord',
					'art_meta_desc' => 'metaDesc',
				],
				'tName' => 'sc_site_articles',
				'tId' => ['nm' => 'id', 'db' => 'art_id'],
			]
		);
		break;

	// Leer Uno
	case 'metaTextRead':
		//
		\snkeng\core\general\saveData::sql_complex_rowRead(
			$response,
			[
				'elems' => [
					'art_meta_word' => 'metaWord',
					'art_meta_desc' => 'metaDesc',
				],
				'tName' => 'sc_site_articles',
				'tId' => ['nm' => 'id', 'db' => 'art_id'],
			],
			'id'
		);
		break;

	//
	case 'updLive':
		//
		\snkeng\core\general\saveData::sql_complex_rowUpd(
			$response,
			[
				'id' => ['name' => 'ID Objeto',   'type' => 'int', 'validate' => true],
				'content' => ['name' => 'Contenido',   'type' => 'str', 'process' => true, 'validate' => true],
			],
			[
				'elems' => [
					'art_content' => 'content',
				],
				'tName' => 'sc_site_articles',
				'tId' => ['nm' => 'id', 'db' => 'art_id'],
			]
		);
		break;

	// Leer Uno
	case 'readSingle':
		//
		\snkeng\core\general\saveData::sql_complex_rowRead(
			$response,
			[
				'elems' => [
					'art_title' => 'title',
					'art_urltitle' => 'urltitle',
					'art_status_published' => 'isPub',
				],
				'tName' => 'sc_site_articles',
				'tId' => ['nm' => 'id', 'db' => 'art_id'],
			],
			'id'
		);
		break;

	// Borrar
	case 'del':
		//
		\snkeng\core\general\saveData::sql_complex_rowDel(
			$response,
			[
				'tName' => 'sc_site_articles',
				'tId' => ['db' => 'art_id']
			],
			'id'
		);
		break;

	// Leer todas
	case 'readAll':
		// Preparar estructura
		\snkeng\core\general\asyncDataJson::printJson(
			[
				'sel' => "art.art_id AS id, art.art_title AS title, art.art_urltitle AS urltitle,
						art.art_type_primary AS artType, art.art_type_secondary AS artTypeSub,
						DATE(art.art_dt_pub) AS dtPub, art.art_dt_add AS dtAdd, art.art_dt_mod AS dtMod,
						art.art_status_published AS isPub,
						art.art_status_indexable AS isIndexed,
						art.art_count_views AS views,
						art.art_url AS fullUrl
				",
				'from' => 'sc_site_articles AS art',
				'lim' => 20,
				'where' => [
					'id' => ['name' => 'ID', 'db' => 'art.art_id', 'vtype' => 'int', 'stype' => 'eq'],
					'title' => ['name' => 'Título', 'db' => 'art.art_title', 'vtype' => 'str', 'stype' => 'like'],
					'artType' => ['name' => 'Tipo', 'db' => 'art.art_type_primary', 'vtype' => 'str', 'stype' => 'eq'],
				],
				'order' => [
					'id' => ['Name' => 'ID', 'db' => 'art.art_id']
				],
				'default_order' => [
					['id', 'ASC']
				]
			]
		);
		break;

	//
	default:
		\snkeng\core\engine\nav::invalidPage();
		break;
}
//
