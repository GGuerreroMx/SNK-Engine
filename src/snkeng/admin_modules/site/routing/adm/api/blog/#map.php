<?php
// Set parameters

$params['vars']['cms_data']['name'] = 'site';
$params['vars']['cms_data']['type'] = 'blog';
$params['vars']['cms_data']['lang'] = 'es';
$params['vars']['cms_data']['parentId'] = 0;

//
switch ( \snkeng\core\engine\nav::next() )
{
	//
	case 'posts':
		if ( !is_numeric(\snkeng\core\engine\nav::next(false)) ) {
			require __DIR__ . '/blog_posts_general.php';
		}
		else {
			$params['vars']['postId'] = intval(\snkeng\core\engine\nav::next());

			if ( $params['vars']['postId'] === 0 ) { \snkeng\core\engine\nav::killWithError('ID no válido.'); }

			\snkeng\core\engine\login::permitsCheckKill('site', 'blog_upd');

			require __DIR__ . '/blog_posts_properties.php';
		}
		break;

	//
	case 'categories':
		require __DIR__ . '/../shared/categories.php';
		break;

	//
	case 'tags':
		require __DIR__ . '/../shared/tags.php';
		break;

	//
	case 'files':
		require __DIR__ . '/../shared/files.php';
		break;

	//
	case 'diagrams':
		require __DIR__ . '/../shared/diagrams.php';
		break;

	//
	case 'images':
		require __DIR__ . '/../shared/images.php';
		break;

	//
	default:
		\snkeng\core\engine\nav::invalidPage();
		break;
}
//