<?php
// Banners
switch ( \snkeng\core\engine\nav::next() )
{
	//
	case 'generalData':
		\snkeng\core\general\saveData::sql_complex_uri_update_read(
			$response,
			$params['vars']['bannerId'],
			[
				'elems' => [
					'ban_title' => 'title',
					'ban_size' => 'size',
					'ban_type' => 'type',
				],
				'tName' => 'sc_site_banners',
				'tId' => ['nm' => 'id', 'db' => 'ban_id'],
			],
			[
				'title' => ['name' => 'Nombre', 'type' => 'str', 'lMin' => 5, 'lMax' => 75, 'filter' => 'simpleTitle', 'process' => false, 'validate' => true],
				'size' => ['name' => 'Tamaño', 'type' => 'int', 'validate' => true],
				'type' => ['name' => 'Tipo', 'type' => 'int', 'validate' => true],
			]
		);
		break;

	//
	case 'publishData':
		\snkeng\core\general\saveData::sql_complex_uri_update_read(
			$response,
			$params['vars']['bannerId'],
			[
				'elems' => [
					'ban_dtini' => 'dtini',
					'ban_dtend' => 'dtend',
					'ban_maxclicks' => 'clicksMax',
					'ban_maxprints' => 'printsMax',
					'ban_active' => 'published',
				],
				'tName' => 'sc_site_banners',
				'tId' => ['nm' => 'id', 'db' => 'ban_id'],
			],
			[
				'dtini' => ['name' => 'Fecha inicio', 'type' => 'str', 'filter' => 'datetime-local', 'process' => false, 'validate' => false],
				'dtend' => ['name' => 'Fecha final', 'type' => 'str', 'filter' => 'datetime-local', 'process' => false, 'validate' => false],
				'clicksMax' => ['name' => 'Clicks máximos', 'type' => 'int', 'validate' => false],
				'printsMax' => ['name' => 'Impresones máximas', 'type' => 'int', 'validate' => false],
				'published' => ['name' => 'Publicado', 'type' => 'int', 'validate' => false],
			]
		);
		break;

	//
	case 'codeData':
		\snkeng\core\general\saveData::sql_complex_uri_update_read(
			$response,
			$params['vars']['bannerId'],
			[
				'elems' => [
					'ban_code' => 'code',
				],
				'tName' => 'sc_site_banners',
				'tId' => ['nm' => 'id', 'db' => 'ban_id']
			],
			[
				'code' => ['name' => 'Código', 'type' => 'str', 'filter' => '', 'process' => false, 'validate' => false],
			]
		);
		break;

	//
	case 'mediaData':
		\snkeng\core\general\saveData::sql_complex_uri_update_read(
			$response,
			$params['vars']['bannerId'],
			[
				'elems' => [
					'ban_alt' => 'alttext',
					'ban_url' => 'url',
				],
				'tName' => 'sc_site_banners',
				'tId' => ['nm' => 'id', 'db' => 'ban_id']
			],
			[
				'url' => ['name' => 'URL', 'type' => 'str', 'process' => false, 'validate' => false],
				'alttext' => ['name' => 'Texto alternativo', 'type' => 'str', 'process' => false, 'validate' => false],
			]
		);
		break;

	// Subir archivo
	case 'imageUpload':
		$rData = \snkeng\core\general\saveData::postParsing(
			$response,
			[
				'file' => ['name' => 'Archivo', 'type' => 'file', 'fType'=>['jpeg', 'jpg', 'gif', 'png'], 'mSize'=>512, 'validate' => true]
			]
		);

		// Revisar si hay archivo previo y remover
		$fileData = \snkeng\core\engine\mysql::singleRowAssoc("SELECT ban_title AS name, ban_floc AS path FROM sc_site_banners WHERE ban_id={$params['vars']['bannerId']};");
		if ( !empty($fileData['path']) && file_exists($_SERVER['DOCUMENT_ROOT'].$fileData['path']) ) {
			unlink($_SERVER['DOCUMENT_ROOT'].$fileData['path']);
		}

		// Re-crear nombre
		$name = \snkeng\core\general\saveData::buildFriendlyTitle($fileData['name']);
		$filePad = str_pad($params['vars']['bannerId'], 6, '0', STR_PAD_LEFT);
		$timeStr = se_timeString();
		$fileName = $filePad.'_'.$name.'_'.$timeStr;

		$fileData = [];
		if ( !\snkeng\core\general\saveData::fileManager($rData['file'], $fileData, $fileName, '/se_files/user_upload/img/banners/', 524288, ['jpeg', 'jpg', 'gif', 'png']) ) {
			\snkeng\core\engine\nav::killWithError('No fue posible mover el archivo.');
		}

		//
		$ins_qry = <<<SQL
UPDATE sc_site_banners SET
ban_ftype='{$fileData['ext']}', ban_fname='{$fileName}',
ban_floc='{$fileData['path']}', ban_fsize='{$fileData['size']}'
WHERE ban_id={$params['vars']['bannerId']};
SQL;
		\snkeng\core\engine\mysql::submitQuery($ins_qry,
			[
				'errorKey' => 'SiteAdmBannerVideoUp',
				'errorDesc' => 'No pudo ser guardado en la base de datos.',
				'onError' => function () use ($fileData) {
					unlink($_SERVER['DOCUMENT_ROOT'].$fileData['path']);
				}
			]
		);

		//
		$response['d'] = [
			'id'=>$params['vars']['bannerId'],
			'title' => $name,
			'fileName' => $fileData['name'],
			'fileExt' => $fileData['ext'],
			'fileSize' => $fileData['size']
		];
		break;

	// Subir archivo
	case 'videoUpload':
		$rData = \snkeng\core\general\saveData::postParsing(
			$response,
			[
				'file' => ['name' => 'Archivo', 'type' => 'file', 'fType'=>['mp4'], 'mSize'=>1024, 'validate' => true]
			]
		);

		// Revisar si hay archivo previo y remover
		$fileData = \snkeng\core\engine\mysql::singleRowAssoc("SELECT ban_title AS name, ban_floc AS path FROM sc_site_banners WHERE ban_id={$params['vars']['bannerId']};");
		if ( !empty($fileData['path']) && file_exists($_SERVER['DOCUMENT_ROOT'].$fileData['path']) ) {
			unlink($_SERVER['DOCUMENT_ROOT'].$fileData['path']);
		}

		// Re-crear nombre
		$name = \snkeng\core\general\saveData::buildFriendlyTitle($fileData['name']);
		$filePad = str_pad($params['vars']['bannerId'], 6, '0', STR_PAD_LEFT);
		$fileName = $filePad.'_'.$name;

		$fileData = [];
		if ( !\snkeng\core\general\saveData::fileManager($rData['file'], $fileData, $fileName, '/se_files/user_upload/image/banners/', 1048576, ['mp4']) ) {
			\snkeng\core\engine\nav::killWithError('No fue posible mover el archivo.');
		}
		 // debugVariable($fileData, 'test 2');
		$ins_qry = <<<SQL
UPDATE sc_site_banners SET
ban_ftype='{$fileData['ext']}', ban_fname='{$fileData['name']}',
ban_floc='{$fileData['path']}', ban_fsize='{$fileData['size']}'
WHERE ban_id={$params['vars']['bannerId']};
SQL;

		\snkeng\core\engine\mysql::submitQuery($ins_qry,
			[
				'errorKey' => 'SiteAdmBannerVideoUp',
				'errorDesc' => 'No pudo ser guardado en la base de datos.',
				'onError' => function () use ($fileData) {
					unlink($_SERVER['DOCUMENT_ROOT'].$fileData['path']);
				}
			]
		);

		//
		$response['d'] = [
			'id'=>$params['vars']['bannerId'],
			'title' => $name,
			'fileName' => $fileData['name'],
			'fileExt' => $fileData['ext'],
			'fileSize' => $fileData['size']
		];
		break;

	// Borrar
	case 'del':
		$tData = [
			'tName' => 'sc_site_banners',
			'tId' => ['db' => 'ban_id']
		];
		// \snkeng\core\general\saveData::sql_table_delRow($response, $tData, 'id');
		break;

	//
	default:
		\snkeng\core\engine\nav::invalidPage();
		break;
}
//
