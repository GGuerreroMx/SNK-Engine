<?php

if ( !is_numeric(\snkeng\core\engine\nav::next(false)) ) {
	require __DIR__ . '/contact_general.php';
}
else {
	$params['vars']['bannerId'] = intval(\snkeng\core\engine\nav::next());

	//
	require __DIR__ . '/contact_properties.php';
}