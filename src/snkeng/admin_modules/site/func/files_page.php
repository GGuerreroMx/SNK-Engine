<?php
//
\snkeng\core\engine\nav::cacheCheckFile(__FILE__);
\snkeng\core\engine\nav::cacheFinalCheck();

// Operation defaults:
$opDefaults = ( !empty($params['vars']['files']['defaults']) ) ? \json_encode($params['vars']['files']['defaults']) : '{}';


// Query

// Query
$an_qry = (require __DIR__ . '/../navs/files.php');
//
$an_empty = "No hay elementos que mostrar.";
// Estructura
$an_struct = <<<HTML
<tr data-element="row" data-objid="!id;" data-objtitle="!title;">
	<td>!id;</td>
	<td>!title;</td>
	<td>!dtAdd;</td>
	<td>!dtMod;</td>
	<td>!fileName;</td>
	<td>!fileType;</td>
	<td>!fileSize;</td>
	<td>
		<button class="btn small" data-dyntab-onclick="element_op" data-action="upd" title="Editar información"><svg class="icon inline"><use xlink:href="#fa-pencil" /></svg></button>
		<button class="btn small" data-dyntab-onclick="element_op" data-action="newfile" title="Cambiar archivo"><svg class="icon inline"><use xlink:href="#fa-file" /></svg></button>
		<a class="btn small" href="!fileLoc;" target="_blank" download="!title;.!fileType;" title="Descargar"><svg class="icon inline"><use xlink:href="#fa-download" /></svg></a>
		<button class="btn small" data-dyntab-onclick="element_op" data-action="del" title="Borrar"><svg class="icon inline"><use xlink:href="#fa-trash-o" /></svg></button>
	</td>
</tr>
HTML;
//
$exData = [
	'js_id' => 'pagElements',
	'js_url' => $params['page']['ajax'].'/readAll',
	'js_defaults' => $opDefaults,
	'printElements' => false,
	'printType' => 'table',
	'tableWide' => true,
	'tableHead'=>[
		['name' => 'ID', 'filter' => 1, 'fName'=>'id'],
		['name' => 'Nombre', 'filter' => 1, 'fName'=>'title'],
		['name' => 'Dt Add'],
		['name' => 'Dt Mod'],
		['name' => 'Archivo', 'filter' => 1, 'fName' => 'fileName'],
		['name' => 'Tipo'],
		['name' => 'Tamaño (KBs)'],
		['name' => 'Acciones'],
	],
	'settings' => ['nav' => true],
	'actions' => <<<JS
{
	'add':
	{
		'action':'form',
		'ajax-action':'add',
		'ajax-elem':'none',
		'menu':{'title':'Agregar', 'icon':'fa-plus'},
		'print':false,
		'form':{
			'title':'Nuevo archivo',
			'save_url':'{$params['page']['ajax']}/add',
			'actName':'Agregar',
			'elems':[
			{'ftype':'file', 'vname':'file', 'fname':'Archivo', 'fdesc':'', 'requiered':true},
			{'ftype':'text', 'vname':'title', 'fname':'Nombre', 'fdesc':'', 'dtype':'str', 'regexp':'simpleTitle', 'lMin':5, 'lMax':50, 'requiered':true},
			{'ftype':'textarea', 'vname':'content', 'fname':'Descripción', 'fdesc':'', 'dtype':'str', 'dval':'', 'requiered':true},
			]
		}
	},
	'upd':
	{
		'ajax-action':'edit',
		'ajax-elem':'obj',
		'action':'form',
		'form':{
			'title':'Editar contenido',
			'load_url':'{$params['page']['ajax']}/readSingle',
			'save_url':'{$params['page']['ajax']}/upd',
			'actName':'Modificar',
			'defaults':{},
			'elems':[
				{'ftype':'hidden', 'vname':'id', 'fname':'ID Serie', 'dtype':'int', 'dval':'', 'requiered':true},
				{'ftype':'text', 'vname':'title', 'fname':'Nombre', 'fdesc':'', 'dtype':'str', 'regexp':'simpleTitle', 'lMin':5, 'lMax':50, 'requiered':true},
				{'ftype':'textarea', 'vname':'content', 'fname':'Descripción', 'fdesc':'', 'dtype':'str', 'requiered':true},
			]
		}
	},
	'newfile':
	{
		'ajax-action':'edit',
		'ajax-elem':'obj',
		'action':'form',
		'form':{
			'title':'Cambiar archivo',
			'load_url':'{$params['page']['ajax']}/readSingle',
			'save_url':'{$params['page']['ajax']}/fileUpd',
			'actName':'Cambiar archivo',
			'defaults':{},
			'elems':[
				{'ftype':'hidden', 'vname':'id', 'fname':'ID Serie', 'dtype':'int', 'dval':'', 'requiered':true},
				{'ftype':'file', 'vname':'file', 'fname':'Archivo', 'fdesc':'', 'dtype':'str', 'dval':'', 'requiered':true},
			]
		}
	},
	'del':{'action':'del', 'ajax-elem':'obj', 'del_url':'{$params['page']['ajax']}/del'}
}
JS
];

// debugVariable($post->sql_qry, '', true);
$tableStructure = \snkeng\core\general\dynamicTable::createElement($an_struct, $exData);

// Page
$page['body'] = <<<HTML
<div class="pageSubTitle">Archivos</div>

{$tableStructure}
HTML;
//


return $page;