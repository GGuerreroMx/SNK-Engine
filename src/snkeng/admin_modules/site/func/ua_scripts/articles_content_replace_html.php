<?php
//
$params = explode(',', $_POST['params']);

if ( count($params) !== 2 ) {
	//
	$response['d'] = "Parámetros no válidos, requiere dos.\n";
	return;
}

//
$upd_qry = '';
$first = true;

//
$sql_qry = <<<SQL
SELECT
	art_id AS id, art_content AS content
FROM sc_site_articles;
SQL;
if ( \snkeng\core\engine\mysql::execQuery($sql_qry) ) {
	while ( $datos = \snkeng\core\engine\mysql::$result->fetch_array() ) {
		$content = \snkeng\core\engine\mysql::real_escape_string(str_replace($params[0], $params[1], $datos['content']));

		//
		$upd_qry.= <<<SQL
UPDATE sc_site_articles SET art_content='{$content}' WHERE art_id='{$datos['id']}';\n
SQL;
	}
}

//
$response['d'] = 'EXCECUTED: ';

//
if ( $_POST['exec'] ) {
	//
	\snkeng\core\engine\mysql::submitMultiQuery($upd_qry, [
		'errorKey' => 'admin',
		'errorDesc' => 'asdf'
	]);

	$response['d'].= "YES\n\n";
} else {
	$response['d'].= "NO\n\n";
}

//
$response['d'].= $upd_qry;
