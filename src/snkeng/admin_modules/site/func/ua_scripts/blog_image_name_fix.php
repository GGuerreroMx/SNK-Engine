<?php
//
$appData = (require $_SERVER['DOCUMENT_ROOT'] . '/snkeng/site_modules/site/params.php');

//
$upd_qry = '';
$first = true;

//
$sql_qry = <<<SQL
SELECT
	art.art_id AS id, art.art_type_primary AS artType,
    ssi.img_id AS imgId,
	ssi.img_fname AS imgFName, ssi.img_name_url AS imgUrl, ssi.img_name AS imgAlt, ssi.img_width AS imgWidth, ssi.img_height AS imgHeigth
FROM sc_site_articles AS art
INNER JOIN sc_site_images ssi ON art.img_id = ssi.img_id;
SQL;
//
if ( \snkeng\core\engine\mysql::execQuery($sql_qry) ) {
	while ( $datos = \snkeng\core\engine\mysql::$result->fetch_array() ) {
		//
		$upd_qry.= <<<SQL
UPDATE sc_site_articles SET
	art_img_struct='{$datos['imgFName']}', art_img_name_url='{$datos['imgUrl']}', art_img_alt_text='{$datos['imgAlt']}',
    art_img_heigth='{$datos['imgHeigth']}', art_img_width='{$datos['imgWidth']}' 
WHERE art_id='{$datos['id']}';
UPDATE sc_site_images SET img_mode_type='{$datos['artType']}' WHERE img_id={$datos['imgId']};\n
SQL;
		//
	}
}

$response['d'] = 'EXCECUTED: ';

//
if ( $_POST['exec'] ) {
	//
	\snkeng\core\engine\mysql::submitMultiQuery($upd_qry, [
		'errorKey' => 'admin',
		'errorDesc' => 'asdf'
	]);

	$response['d'].= "YES\n\n";
} else {
	$response['d'].= "NO\n\n";
}

//
$response['d'].= $upd_qry;