<?php
//
$appData = (require $_SERVER['DOCUMENT_ROOT'] . '/snkeng/site_modules/site/params.php');

//
$converter = new \snkeng\core\site\htmlToMarkDown(true);


//
$upd_qry = '';
$first = true;

//
$sql_qry = <<<SQL
SELECT
	art.art_id AS id, art.art_type_primary AS type, art.art_content AS content
FROM sc_site_articles AS art;
SQL;
//
$pageList = \snkeng\core\engine\mysql::returnArray($sql_qry);

//
foreach ( $pageList as $page ) {
	// Search
	$sql_qry = "SELECT txt_id AS id FROM sc_site_texts WHERE txt_app_mode='{$page['type']}' AND txt_app_id='{$page['id']}' LIMIT 1;";
	$txtId = \snkeng\core\engine\mysql::singleNumber($sql_qry);

	// Get text
	$cText = $converter->convert($page['content']);
	$cText = \snkeng\core\engine\mysql::real_escape_string($cText);

	// Save
	if ( $txtId ) {
		$upd_qry.= "UPDATE sc_site_texts SET txt_content='{$cText}' WHERE txt_id={$txtId};\n\n";
	} else {
		$appName = ( in_array($page['type'], ['blog', 'page', 'docs']) ) ? 'site' : 'unknown';
		$upd_qry.= "INSERT INTO sc_site_texts (txt_id_parent, txt_app_name, txt_app_mode, txt_app_id, txt_lang, txt_type, txt_content) VALUES (0, '{$appName}', '{$page['type']}', {$page['id']}, 'es', 'md', '{$cText}');\n\n";
	}
}


//
$response['d'] = 'EXCECUTED: ';

//
if ( $_POST['exec'] ) {
	//
	\snkeng\core\engine\mysql::submitMultiQuery($upd_qry, [
		'errorKey' => 'admin',
		'errorDesc' => 'asdf'
	]);

	$response['d'].= "YES\n\n";
} else {
	$response['d'].= "NO\n\n";
}

//
$response['d'].= $upd_qry;