<?php
//
namespace snkeng\admin_modules\site;

//
class page {
	//
	/**
	 * addPage - Create a page from variables
	 * @param array $response response data returned in ajax
	 * @param string $pageType the page type, defines basic page behaviour
	 * @param string $pageSubType additional variation of pagetype
	 * @param string $pageTitle the title, urlTitle will be created from this information
	 * @param string $urlStruct url structure, allows !id;, !urlTitle; and date information (!year;, !month;, !day;)
	 * @param string $pageContent default content.
	 * @param array $params additional modifiers (ignoring defaults)
	 */
	public static function addPage(array &$response, string $pageType, string $pageSubType, string $pageTitle, string $urlStruct, string $pageContent, array $params = []) {
		global $mysql, $siteVars;

		$cData = [];

		//
		$cData['type'] = $pageType;
		$cData['subtype'] = $pageSubType;

		//
		$cData['title'] = $pageTitle;
		$cData['urltitle'] = \snkeng\core\general\saveData::buildFriendlyTitle($pageTitle);
		//
		if ( !empty($urlStruct) ) {
			$nId = \snkeng\core\engine\mysql::getNextId('sc_site_articles');
			$cData['fullUrl'] = self::urlBuild($nId, $cData['urltitle'], time(), $urlStruct);
		} else {
			$cData['fullUrl'] = '';
		}


		//
		$cData['content'] = $pageContent;

		// Defaults?
		$cData['catId'] = ( !empty($params['catId']) ) ? $params['catId'] : 0;
		$cData['lang'] = ( !empty($params['lang']) ) ? $params['lang'] : $siteVars['site']['lang'];
		$cData['author'] = ( !empty($params['author']) ) ? $params['author'] : $siteVars['user']['data']['id'];

		// Docs
		$cData['orderLvl'] = ( !empty($params['orderLvl']) ) ? $params['orderLvl'] : 0;
		$cData['orderCur'] = ( !empty($params['orderCur']) ) ? $params['orderCur'] : 0;
		$cData['orderHierarchy'] = ( !empty($params['orderHierarchy']) ) ? $params['orderHierarchy'] : '';

		// Override (currently for pages)
		$cData['fullUrl'] = ( !empty($params['fullUrl']) ) ? $params['fullUrl'] : $cData['fullUrl'];

		//
		\snkeng\core\general\saveData::sql_table_addRow($response,
			[
				'elems' => [
					'art_type_primary' => 'type',
					'art_type_secondary' => 'subtype',

					'art_title' => 'title',
					'art_urltitle' => 'urltitle',
					'art_url' => 'fullUrl',
					'art_content' => 'content',

					'cat_id' => 'catId',

					'art_lang' => 'lang',
					'a_id' => 'author',

					'art_order_level' => 'orderLvl',
					'art_order_current' => 'orderCur',
					'art_order_hierarchy' => 'orderHierarchy',
				],
				'tName' => 'sc_site_articles',
				'tId' => ['nm' => 'id', 'db' => 'art_id'],

			],
			$cData
		);
	}

	//
	public static function urlBuild($id, $urlTitle, $date, $urlStruct) {
		$datos = [
			'id' => $id,
			'idPad' => str_pad($id, 4, "0", STR_PAD_LEFT),
			'urlTitle' => $urlTitle,
			'year' => date('Y', $date),
			'month' => date('m', $date),
			'day' => date('d', $date),
		];
		$string = stringPopulate($urlStruct, $datos);
		return $string;
	}

	//
	public static function hasPermitions() {

	}

	//
	public static function updateTitle(array &$response, string $type, int $id, string $title, string $urlTitle, string $urlStruct) {
				//
		$sql_qry = "SELECT DATE(art_dt_pub) AS dtPub, art_type_primary AS contentType FROM sc_site_articles WHERE art_id={$id};";
		$objInformation = \snkeng\core\engine\mysql::singleRowAssoc($sql_qry);

		//
		if ( $objInformation['contentType'] !== $type ) {
			\snkeng\core\engine\nav::killWithError('Tipo de objeto no corresponde al objeto a actualizar.');
		}

		$date = ( !empty($objInformation['dtPub']) ) ? strtotime($objInformation['dtPub']) : time();
		$fullUrl = self::urlBuild($id, $urlTitle, $date, $urlStruct);

		//
		\snkeng\core\general\saveData::sql_table_updRow(
			$response,
			[
				'elems' => [
					'art_title' => 'title',
					'art_urltitle' => 'urltitle',
					'art_url' => 'fullUrl',
				],
				'tName' => 'sc_site_articles',
				'tId' => ['nm' => 'id', 'db' => 'art_id'],

			],
			[
				'id' => $id,
				'title' => $title,
				'urltitle' => $urlTitle,
				'fullUrl' => $fullUrl,
			]
		);
	}

	//

	/**
	 * Change image for page
	 * @param array $response
	 * @param int $pageId
	 * @param int $imgId
	 */
	public static function updateImage(array &$response, int $pageId, int $imgId) {
		global $response;
		// Seleccionar
		$sql_qry = <<<SQL
SELECT
    img.img_fname AS fName, img.img_name AS fTitle, img.img_height AS heigth, img.img_width AS width
FROM sc_site_images AS img WHERE img.img_id={$imgId};
SQL;
		$imgStruct = \snkeng\core\engine\mysql::singleRowAssoc($sql_qry, [], [
			'errorEmpty' => 'Image ID not found'
		]);

		//
		$upd_qry = <<<SQL
UPDATE sc_site_articles
SET img_id='{$imgId}',
    art_img_struct='{$imgStruct['fName']}', art_img_alt_text='{$imgStruct['fTitle']}',
    art_img_heigth='{$imgStruct['heigth']}', art_img_width='{$imgStruct['width']}' 
WHERE art_id={$pageId};
SQL;
		\snkeng\core\engine\mysql::submitQuery($upd_qry);

		$response['d'] = [
			'img_file' => "/res/image/site/w_320/{$imgStruct['fName']}"
		];
	}


	//
	public static function propertiesUpdate(array &$response, string $artType, int $artId, string $action, array $options) {
		if ( empty($options[$action]) ) {
			\snkeng\core\engine\nav::killWithError('Operación no disponible en este tipo de artículo', "ACT: {$action}.");
		}

		//
		switch ( $action ) {
			//
			case 'metadata':
				\snkeng\core\general\saveData::sql_complex_rowUpd(
					$response,
					[
						'metaword' => ['name' => 'Meta Words', 'type' => 'str', 'lMin' => 0, 'lMax' => 200, 'filter' => 'simpleText', 'process' => false, 'validate' => false],
						'metadesc' => ['name' => 'Meta Desc', 'type' => 'str', 'lMin' => 0, 'lMax' => 200, 'filter' => 'simpleText', 'process' => false, 'validate' => false],
					],
					[
						'elems' => [
							'art_meta_word' => 'metaword',
							'art_meta_desc' => 'metadesc',
						],
						'tName' => 'sc_site_articles',
						'tId' => ['nm' => 'id', 'db' => 'art_id'],
					],
					[
						'setData' => [ 'id' => $artId ]
					]
				);
				break;

			//
			case 'category':
				//
				\snkeng\core\general\saveData::sql_complex_rowUpd(
					$response,
					[
						'id' => ['name' => 'ID Post', 'type' => 'int', 'process' => false, 'validate' => true],
						'catId' => ['name' => 'ID Categoría', 'type' => 'int', 'process' => false, 'validate' => true],
					],
					[
						'elems' => [
							'cat_id' => 'catId',
						],
						'tName' => 'sc_site_articles',
						'tId' => ['nm' => 'id', 'db' => 'art_id'],
					],
					[
						'setData' => [ 'id' => $artId ]
					]
				);
				break;

			//
			case 'image':
				//
				$rData = \snkeng\core\general\saveData::postParsing(
					$response,
					[
						'imgId' => ['name' => 'ID imagen', 'type' => 'int', 'process' => false, 'validate' => true]
					]
				);

				self::updateImage($response, $artId, $rData['imgId']);
				break;

			//
			case 'imageAltText':
				//
				\snkeng\core\general\saveData::sql_complex_rowUpd($response,
					[
						'imageAltText' => ['name' => 'Descripción alternativa', 'type' => 'str', 'lMin' => 0, 'lMax' => 200, 'filter' => 'simpleText', 'required' => true],
					],
					[
						'elems' => [
							'art_img_alt_text' => 'imageAltText',
						],
						'tName' => 'sc_site_articles',
						'tId' => ['nm' => 'id', 'db' => 'art_id'],
					],
					[
						'setData' => [ 'id' => $artId ]
					]
				);
				break;

			//
			case 'publish':
				//
				\snkeng\core\general\saveData::sql_complex_rowUpd($response,
					[
						'dtPub' => ['name' => 'Fecha de publicación', 'type' => 'str', 'lMin' => 0, 'lMax' => 32, 'filter' => 'datetime', 'process' => true, 'validate' => true],
						'isPub' => ['name' => 'Publicado', 'type' => 'bool', 'process' => false, 'validate' => false],
						'isIndexed' => ['name' => 'Indexable', 'type' => 'bool', 'process' => false, 'validate' => false],
					],
					[
						'elems' => [
							'art_dt_pub' => 'dtPub',
							'art_status_published' => 'isPub',
							'art_status_indexable' => 'isIndexed',
						],
						'tName' => 'sc_site_articles',
						'tId' => ['nm' => 'id', 'db' => 'art_id'],
					],
					[
						'setData' => [ 'id' => $artId ]
					]
				);
				break;

			//
			case 'behaviour':
				//
				$rData = \snkeng\core\general\saveData::postParsing(
					$response,
					[
						'headerType' => ['name' => 'Header Type', 'type' => 'str', 'lMin' => 0, 'lMax' => 10, 'filter' => 'simpleText', 'process' => false, 'validate' => false],
						'headerContent' => ['name' => 'Header Content', 'type' => 'str', 'lMin' => 0, 'lMax' => 100, 'filter' => 'simpleText'],
						'bundles' => ['name' => 'Bundles', 'type' => 'str', 'lMin' => 0, 'lMax' => 100, 'filter' => 'simpleText'],
						'webComponent' => ['name' => 'Web component', 'type' => 'str', 'lMin' => 0, 'lMax' => 20, 'filter' => 'simpleText'],
						'redirect' => ['name' => 'Redirect', 'type' => 'str', 'lMin' => 0, 'lMax' => 255, 'filter' => 'simpleText'],
					]
				);

				//
				$rData['id'] = $artId;

				//
				$rData['contentExtra'] = '';

				// Procesar
				switch ( $rData['headerType'] ) {
					// Video
					case 'video':
						if ( strpos($rData['headerContent'], 'youtube.com/watch') !== false ) {
							//
							$vidId = strstr($rData['headerContent'], 'v=');
							$vidId = substr($vidId, 2);
							if ( strpos($vidId, '&') !== false ) {
								$vidId = strstr($vidId, '&', true);
							}
							//
							$rData['contentExtra'] = '<iframe src="https://www.youtube.com/embed/'.$vidId .'?rel=0" allowFullScreen></iframe>';
						} elseif ( strpos($rData['headerContent'],'youtu.be/') !== false ) {
							$vidId = strstr($rData['headerContent'], '.be/');
							$vidId = substr($vidId, 4);
							//
							$rData['contentExtra'] = '<iframe src="https://www.youtube.com/embed/'.$vidId .'?rel=0" allowFullScreen></iframe>';
						} elseif ( strpos($rData['headerContent'],'vimeo.com') !== false ) {
							$vidId = strstr($rData['headerContent'], '.com/');
							$vidId = substr($vidId, 4);
							//
							$rData['contentExtra'] = '<iframe src="http://player.vimeo.com/video/'.$vidId .'" allowFullScreen></iframe>';
						} else {
							\snkeng\core\engine\nav::killWithError('Sitio no reconocido.');
						}
						// debugVariable($rData);
						break;

					// Default
					default:
						$rData['typeSub'] = ( !empty($rData['typeSub']) ) ? $rData['typeSub'] : 'normal';
						$rData['contentExtra'] = '';
						break;
				}

				//
				\snkeng\core\general\saveData::sql_table_updRow(
					$response,
					[
						'elems' => [
							'art_content_header_type'       => 'headerType',
							'art_content_header_content'    => 'headerContent',
							'art_content_bundles'           => 'bundles',
							'art_content_body_component'    => 'webComponent',
							'art_content_redirect'          => 'redirect',
							'art_content_extra '            => 'contentExtra'
						],
						'tName' => 'sc_site_articles',
						'tId'   => ['nm' => 'id', 'db' => 'art_id']
					],
					$rData
				);
				break;

			//
			case 'tagAdd':
				\snkeng\core\general\saveData::sql_complex_rowAdd($response,
					[
						'aId' => ['name' => 'ID Objeto', 'type' => 'int', 'process' => false, 'validate' => true],
						'tId' => ['name' => 'ID Tag', 'type' => 'int', 'process' => false, 'validate' => true]
					],
					[
						'elems' => [
							'tag_id' => 'tId',
							'art_id' => 'aId',
						],
						'tName' => 'sc_site_tagrel',
						'tId' => ['nm' => 'rId', 'db' => 'tagr_id']
					],
					[
						'setData' => [ 'id' => $artId ]
					]
				);
				break;

			//
			case 'tagDel':
				\snkeng\core\general\saveData::sql_table_delRow($response,
					[
						'aId' => ['name' => 'ID Objeto', 'type' => 'int', 'process' => false, 'validate' => true],
						'rId' => ['name' => 'ID Relation', 'type' => 'int', 'process' => false, 'validate' => true]
					],
					'rId'
				);
				break;
			//
			case 'rating':
				break;
			//
			default:
				break;
		}
	}

	//
	public static function propertiesPrintForm(string $articleType, string $ajaxUrl, string $windowsUrl, array $options, array $pageData, array $extraData = null) {
		
		//
		$column_2 = '';
		$column_3 = '';
		$extraDataForm = '';
		//
		$extraDataJSON = \json_encode($extraData, JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES);

		//
		if ( $extraData ) {
			foreach ( $extraData as $name => $value ) {
				$extraDataForm.= "\n\t\t\t\t\t<input type='hidden' name='{$name}' value='{$value}' />";
			}
		}

		//
		if ( !empty($options['title']) ) {
			$column_2.= <<<HTML
		<div class="sideModule">
			<div class="title">Título</div>
			<div class="content">
				<form class="se_form" method="post"  action="{$ajaxUrl}/titleUpd" method="post" class="se_form" is="se-async-form" >
					<input type="hidden" name="id" value="{$pageData['id']}" />{$extraDataForm}
					<label class="separator required">
						<div class="cont"><span class="title">Título</span><span class="desc"></span></div>
						<input type="text" name="title" value="{$pageData['title']}" required pattern="[A-z\u00C0-\u00ff0-9_\-\.,!¡¿?=\(\)\[\]:; ]{4,75}">
					</label>
					<label class="separator required">
						<div class="cont"><span class="title">URL</span><span class="desc"></span></div>
						<input type="text" name="urltitle" value="{$pageData['urltitle']}" required pattern="[A-z\u00C0-\u00ff0-9_\-\.,!¡¿?=\(\)\[\]:;]{4,75}">
					</label>
					<button type="submit">Actualizar</button>
					<output se-elem="response"></output>
				</form>
			</div>
		</div>\n\n
HTML;
			//

		}

		//
		if ( !empty($options['category']) ) {
			// Categorías
			$catOptions = \snkeng\core\engine\mysql::printSimpleQuery(
				"SELECT cat_id AS value, cat_title AS name FROM sc_site_category WHERE cat_mode_type='{$articleType}' ORDER BY name ASC LIMIT 20;",
				'<option value="!value;">!name;</option>'
			);

			//
			$column_2.= <<<HTML
		<div class="sideModule">
			<div class="title">Categoría</div>
			<div class="content">
				<form class="se_form" method="post" action="{$ajaxUrl}/properties/category" is="se-async-form" >
					<input type="hidden" name="id" value="{$pageData['id']}" />{$extraDataForm}
					<label class="separator">
						<div class="cont"><span class="title">Categoría</span><span class="desc"></span></div>
						<select name="catId" se-fix="{$pageData['catId']}">{$catOptions}</select>
					</label>
					<button type="submit">Actualizar</button>
					<span se-elem="response"></span>
				</form>
			</div>
		</div>\n\n
HTML;
			//
		}

		//
		if ( !empty($options['image']) ) {
			//
			$column_2.= <<<HTML
		<div class="sideModule">
			<div class="title">Imágen</div>
			<div class="content">
				<img se-elem="image" src="/res/image/site/w_320/{$pageData['imageStruct']}.webp" />
				<button class="btn blue wide" data-action="imageSelect"><svg class="icon inline mr"><use xlink:href="fa-image-o" /></svg>Seleccionar</button>
				<output se-elem="image"></output>
				
				<form class="se_form" method="post" action="{$ajaxUrl}/properties/imageAltText" is="se-async-form">
					<input type="hidden" name="id" value="{$pageData['id']}" />
					<label class="separator">
                        <div class="cont"><span class="title">Descripción de la imagen</span><span class="desc"></span></div>
                        <textarea se-plugin="autoSize" name="imageAltText">{$pageData['imageAltText']}</textarea>
                    </label>
					<button type="submit">Actualizar</button>
					<output se-elem="response"></output>
				</form>
			</div>
		</div>\n\n
HTML;
			//
		}

		//
		if ( !empty($options['publish']) ) {
			//
			$column_2.= <<<HTML
		<div class="sideModule">
			<div class="title">Publicar</div>
			<div class="content">
				<form class="se_form" method="post" action="{$ajaxUrl}/properties/publish" is="se-async-form">
					<input type="hidden" name="id" value="{$pageData['id']}" />{$extraDataForm}
					<label class="separator">
						<div class="cont"><span class="title">Fecha de publicación</span><span class="desc"></span></div>
						<input type="datetime-local" name="dtPub" value="{$pageData['dtPub']}" se-plugin="calendarInput" pattern="([0-9]{4})-([0-1][0-9])-([0-3][0-9])\s?(?:([0-2][0-9]):([0-5][0-9]):([0-5][0-9]))?" / required>
					</label>
					<label class="separator checkbox">
						<input type="checkbox" name="isPub" value="1" se-fix="{$pageData['isPublished']}" />
						<div class="cont"><span class="title">Publicado</span><span class="desc"></span></div>
					</label>
					<label class="separator checkbox">
						<input type="checkbox" name="isIndexed" value="1" se-fix="{$pageData['isIndexed']}" />
						<div class="cont"><span class="title">Indexable</span><span class="desc"></span></div>
					</label>
					
					<button type="submit">Actualizar</button>
					<output se-elem="response"></output>
				</form>
			</div>
		</div>\n\n
HTML;
			//
		}

		//
		if ( !empty($options['behaviour']) ) {
			$typeSubOptions = "";

			switch ( $articleType ) {
				case 'page':
					$typeSubOptions = <<<HTML
							<option value="normal">Normal</option>
							<option value="image">Imagen</option>
							<option value="video">Video</option>
HTML;
					break;
				case 'post':
					$typeSubOptions = <<<HTML
							<option value="normal">Normal</option>
							<option value="video">Video</option>
HTML;
					break;
			}


			//
			$column_3.= <<<HTML
		<div class="sideModule">
			<div class="title">Propiedades</div>
			<div class="content">
				<form class="se_form" method="post" action="{$ajaxUrl}/properties/behaviour" is="se-async-form" >
					<input type="hidden" name="id" value="{$pageData['id']}" />{$extraDataForm}
					<label class="separator">
						<div class="cont"><span class="title">Tipo de Encabezado</span><span class="desc">Variantes en el tipo de contenido extra.</span></div>
						<select name="headerType" se-fix="{$pageData['contentHeaderType']}">
{$typeSubOptions}
						</select>
					</label>
					<label class="separator">
						<div class="cont"><span class="title">Contenido encabezado</span><span class="desc">Ex: video link</span></div>
						<input type="text" name="headerContent" value="{$pageData['contentHeaderContent']}" maxlength="100" />
					</label>
					<label class="separator">
						<div class="cont"><span class="title">Bundles</span><span class="desc">Separados por coma</span></div>
						<input type="text" name="bundles" value="{$pageData['contentBundles']}" maxlength="100" />
					</label>
					<label class="separator">
						<div class="cont"><span class="title">Web component</span><span class="desc">Replace content div to a webcomponent tag (use bundles)</span></div>
						<input type="text" name="webComponent" value="{$pageData['contentBodyComponent']}" maxlength="20" />
					</label>
					<label class="separator">
						<div class="cont"><span class="title">Redirección</span><span class="desc">Si no está vació, este toma prioridad siempre.</span></div>
						<input type="text" name="redirect" value="{$pageData['contentRedirect']}" maxlength="100" />
					</label>
					<button type="submit">Actualizar</button>
					<output se-elem="response"></output>
				</form>
			</div>
		</div>\n\n
HTML;
			//
		}

		//
		if ( !empty($options['tags']) ) {
			// Tags

			// Tag list
			$tagList = '';
			$sql_qry = <<<SQL
SELECT
    tag.tag_id AS id, tag.tag_title AS title
FROM sc_site_tags AS tag
ORDER BY tag.tag_title DESC
LIMIT 20;
SQL;
			$tagList = \snkeng\core\engine\mysql::printSimpleQuery($sql_qry, "{id:'!id;',title:'!title;'}", [
				'mergeChar' => ','
			]);

			// Tag Cur
			$tagCur = '';
			$sql_qry = <<<SQL
SELECT
	tagr.tagr_id AS rId, tagr.tag_id AS tId
FROM sc_site_tagrel AS tagr
WHERE tagr.art_id={$pageData['id']}
LIMIT 5;
SQL;
			$tagCur = \snkeng\core\engine\mysql::printSimpleQuery($sql_qry, "{rId:'!rId;',tId:'!tId;'}", [
				'mergeChar' => ','
			]);

			//
			$column_3.= <<<HTML
		<div class="sideModule" se-elem="tagList">
			<div class="title">Tags</div>
			<div class="content">
				<form class="se_form">
					<input type="hidden" name="id" value="{$pageData['id']}" />{$extraDataForm}
					<label class="separator required">
						<div class="cont"><span class="title">Título</span><span class="desc"></span></div>
						<div class="modInput">
							<input type="text" name="tagName" list="tagList" pattern="[A-z\u00C0-\u00ff0-9\-\_\.\,\ ]{5,75}" required>
							<button type="submit"><svg class="icon inline"><use xlink:href="#fa-plus" /></svg></button>
						</div>
						<datalist id="tagList"></datalist>
					</label>
				</form>
				<div>
					<template>
					<div class="tag" data-rid="!rId;" data-tid="!tId;"><span>!title;</span><button data-act="del"><svg class="icon inline"><use xlink:href="#fa-times" /></svg></button></div>
					</template>
					<div se-type="content" class="tagList"></div>
				</div>
			</div>
		</div>\n\n
HTML;
			//
		}

		//
		if ( !empty($options['rating']) ) {
			//
			$column_2.= <<<HTML
		<div class="sideModule">
			<div class="title">Rating</div>
			<div class="content">
				<form class="se_form" method="post" action="{$ajaxUrl}/properties/rating" is="se-async-form" >
					<input type="hidden" name="id" value="{$pageData['id']}" />{$extraDataForm}
					<label class="separator">
						<div class="cont"><span class="title">Rating</span><span class="desc"></span></div>
						<select name="catId" se-fix="{$pageData['catId']}">{$catOptions}</select>
					</label>
					<button type="submit">Actualizar</button>
					<span se-elem="response"></span>
				</form>
			</div>
		</div>\n\n
HTML;
			//
		}

		//
		\snkeng\core\engine\nav::pageFileModuleAdd('admin_modules', 'site', '/pages/site-adm-pages-articles.css');
		\snkeng\core\engine\nav::pageFileModuleAdd('admin_modules', 'site', '/pages/site-adm-pages-articles.mjs');

		//
		return <<<HTML
<site-adm-pages-articles data-pageid="{$pageData['id']}" data-apiurl="{$ajaxUrl}" data-windowurl="{$windowsUrl}" data-apiurl="{$ajaxUrl}">

<script se-elem="defaults" type="application/json">
{
	'pageId':{$pageData['id']},
	'ajaxPage':'{$ajaxUrl}',
	'windowsUrl':'$windowsUrl',
    'extraPost':{$extraDataJSON}
}
</script>

<div class="grid">

	<div class="gr_sz04">
		<div class="sideModule">
			<div class="title">Meta datos</div>
			<div class="content">
				<form class="se_form" method="post" action="{$ajaxUrl}/properties/metadata" is="se-async-form">
					<input type="hidden" name="id" value="{$pageData['id']}" />{$extraDataForm}
					<label class="separator">
                        <div class="cont"><span class="title">Palabras clave</span><span class="desc"></span></div>
                        <textarea se-plugin="autoSize" name="metaword">{$pageData['metaWord']}</textarea>
                    </label>
					<label class="separator">
                        <div class="cont"><span class="title">Descripción corta</span><span class="desc"></span></div>
    					<textarea se-plugin="autoSize" name="metadesc">{$pageData['metaDesc']}</textarea>
					</label>
					<button type="submit"><svg class="icon inline mr"><use xlink:href="#fa-save"></use></svg>Guardar</button>
					<output se-elem="response"></output>
				</form>
			</div>
		</div>
	</div>

	<div class="gr_sz04">
{$column_2}
	</div>

	<div class="gr_sz04">
	
		<div class="sideModule">
			<div class="title">Estátus</div>
			<div class="content">
				<table class="st_dualtab"><tbody>
					<tr><td>URL</td><td>{$pageData['urlfull']} <a href="{$pageData['urlfull']}" target="_blank">[ Ver ]</a></td></tr>
					<tr><td>Publicado:</td><td><span class="hasProp s{$pageData['isPublished']}"></span></td></tr>
					<tr><td>Editable:</td><td><span class="hasProp s{$pageData['isEditable']}"></span></td></tr>
				</tbody></table>
			</div>
		</div>
	
{$column_3}
	</div>
</div>
</site-adm-pages-articles>
HTML;
		//
	}
}
//
