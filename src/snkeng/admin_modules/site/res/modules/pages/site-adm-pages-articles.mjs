import * as formCreator from '/snkeng/core/res/modules/library/form-creator.mjs';
import * as formActions from '/snkeng/core/res/modules/library/forms.mjs';
//
import * as simpleForm from '/snkeng/core/res/modules/components-simple/se-async-form.mjs';


//
customElements.define('site-adm-pages-articles', class extends HTMLElement {
	//
	tagList = this.querySelector('div[se-elem="tagList"]');
	imageElement = this.querySelector('img[se-elem="image"]');
	imageResponse = this.querySelector('output[se-elem="image"]');

	//
	mainUrl = '/api/user/settings/email';

	//
	constructor() {
		super();
	}

	//
	connectedCallback() {
		// TODO: not working
		if ( this.tagList ) {
			// Set form callback to add function
			window.customElements.whenDefined('se-async-form').then(() => {
				this.tagList.dispatchEvent(
					new CustomEvent("setCallBacks", {
						detail: {
							// onSuccess: this.formSubmitSuccess.bind(this)
						},
					}),
				);
			});
		}

		// Enable buttons
		this.addEventListener('click', this.onClickCallBacks.bind(this));
	}

	//
	onClickCallBacks(e) {
		let cBtn = e.target.closest('button[data-action]');
		if ( !cBtn ) {
			return;
		}

		//
		let operation = 'onClick_' + cBtn.dataset['action'];

		//
		if ( typeof this[operation] !== 'function' ) {
			console.error("operación no definida.", operation, cBtn);
			return;
		}

		//
		this[operation](e, cBtn);
	}

	//
	async onClick_imageSelect(e, cBtn) {
		e.preventDefault();

		//
		let imageDialog = await formCreator.Modal.page(this.dataset['windowurl'] + '/images_window', true, () => {
			// Wait for actuall definition of the object.
			window.customElements.whenDefined('site-adm-pages-image-picker').then(() => {
				let cElement = document.body.querySelector("site-adm-pages-image-picker");


				// Only do something if the element exists on the page.
				if ( !cElement ) {
					console.error("bad appending");
					return;
				}

				//
				cElement.dispatchEvent(
					new CustomEvent("imageSelectSet", {
						detail: {
							callback:(imgData) => {
								// AJAX
								formActions.postRequest(
									this.dataset['apiurl'] + '/properties/image',
									Object.assign({'id':this.dataset['pageid'], 'imgId':imgData['id']}),
									{
										onSuccess:(msg) => {
											this.imageElement.setAttribute('src', msg.d.img_file + '.webp');
										}
									}
								);

								// Close the current window (it destroys it)
								imageDialog.forceClose();
							}
						},
					}),
				);
			});
		});
	}
});