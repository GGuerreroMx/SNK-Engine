import * as domManipulator from '/snkeng/core/res/modules/library/dom-manipulation.mjs';
import * as formActions from '/snkeng/core/res/modules/library/forms.mjs';
import * as simpleForm from '/snkeng/core/res/modules/components-simple/se-async-form.mjs';

//
customElements.define('user-adm-pages-settings-email', class extends HTMLElement {
	//
	mailForm = this.querySelector('form');
	mailTable = this.querySelector('table');
	mailTableTemplate = this.mailTable.querySelector('template');
	mailTableContent = this.mailTable.querySelector('tbody');

	//
	mainUrl = '/api/user/settings/email';

	//
	constructor() {
		super();
	}

	//
	connectedCallback() {

		// Set form callback to add function
		window.customElements.whenDefined('se-async-form').then(() => {
			this.mailForm.dispatchEvent(
				new CustomEvent("setCallBacks", {
					detail: {
						onSuccess: this.formSubmitSuccess.bind(this)
					},
				}),
			);
		});

		// Enable buttons
		this.addEventListener('click', this.onClickCallBacks.bind(this));
	}

	//
	onClickCallBacks(e) {
		let cBtn = e.target.closest('button[data-action]');
		if ( !cBtn ) {
			return;
		}

		//
		let operation = 'onClick_' + cBtn.dataset['action'];

		//
		if ( typeof this[operation] !== 'function' ) {
			console.error("operación no definida.", operation, cBtn);
			return;
		}

		//
		this[operation](e, cBtn);
	}

	//
	onClick_mailValidate(e, cBtn) {
		if ( !confirm('Se enviará un correo electrónico para validar este correo.') ) {
			return;
		}

		let cRow = cBtn.closest('tr'),
			mailId = intVal(cRow.dataset['id']);

		//
		formActions.postRequest(
			this.mainUrl + '/' + mailId+ '/' + mailId+ '/validate',
			null,
			{
				method:'patch',
				onSuccess:(msg) => {
					cBtn.remove();
				},
				onFail:(msg) => {
					alert(msg.title);
				}
			}
		);
	}

	//
	formSubmitSuccess(msg) {
		domManipulator.insertContentFromTemplate(
			this.mailTableContent,
			"beforeend",
			this.mailTableTemplate.innerHTML,
			msg.d
		);
	}

});



'use strict';
// Galería seleccionar imagenes
se.plugin.adm_gallerySelect = function(plugElem, plugOptions) {
	// Binds
	let pDefaults = {
			callback:null
		},
		pSettings = se.object.merge(pDefaults, plugOptions),
		//
		ajaxUrl = plugElem.dataset['url'],
		postData = {
			galId:plugElem.dataset['galid']
		},
		// Upload object
		uploadForm = plugElem.querySelector('form[se-elem="uploadImages"]'),
		uploadInput = uploadForm.querySelector('input[name="files[]"]'),
		uploadTemplate = uploadForm.querySelector('template'),
		uploadContent = uploadForm.querySelector('div[se-elem="content"]'),
		// Actual list
		photoObj = plugElem.querySelector('div[se-elem="photos"]'),
		photoTemplate = photoObj.querySelector('template'),
		photoList = photoObj.querySelector('div[se-elem="content"]');

	//
	function init() {
		// Bindings
		plugElem.se_on('click', 'button[se-act]', btn_actions);
		photoList.se_on('change', ['input', 'textarea'], upd_content);
		//Check File API support
		if ( window.File && window.FileList && window.FileReader ) {
			uploadInput.se_on("change", images_preview);
		}
		//
		uploadForm.se_plugin('simpleForm', {
			save_url:ajaxUrl + '/gallery/upload',
			onSuccess:images_upload
		});
	}

	//
	function images_upload(msg) {
		uploadContent.se_empty();
		photoList.se_site_coreend(se.struct.stringPopulateMany(photoTemplate.innerHTML, msg.d));
	}

	//
	function images_preview(event) {
		uploadContent.se_empty();
		//
		let files = event.target.files; //FileList object
		for ( let i = 0; i < files.length; i++ ) {
			let file = files[i];
			//Only pics
			if ( !file.type.match('image') ) { continue; }

			let picReader = new FileReader();
			picReader.addEventListener("load",
				(event) => {
					let picFile = event.target;
					uploadContent.se_site_coreend(se.struct.stringPopulate(uploadTemplate.innerHTML, {'result':picFile.result}));
				}
			);
			//Read the image
			picReader.readAsDataURL(file);
		}
	}

	//
	function upd_content(e, cEl) {
		let imgEl = cEl.closest('div.photo'),
			imgData = {
				imgId:imgEl.dataset['id'],
				title:imgEl.querySelector('input[name="title"]').value,
				description:imgEl.querySelector('textarea[name="description"]').value
			};


		// AJAX
		formActions.postRequest(ajaxUrl + '/gallery/upd', se.object.merge(postData, imgData), {
			onSuccess:(msg) => {
				// imgEl.se_replaceWith(se.struct.stringPopulate(photoTemplate.innerHTML, imgData));
				console.log("content updated");
			}
		});
	}

	//
	function btn_actions(e, cEl) {
		//
		switch ( cEl.getAttribute('se-act') ) {
			//
			case 'add':
				se.addon.floatWin({
					iniUrl:'/api/module_adm/core/site/images/win_images_sel',
					fullScreen:true,
					id:'admin_photoSel',
					onLoad:() => {
						$('#photoNav').adm_imageEditor.setup({
							onSelect:img_callBack
						});
					}
				});
				break;

			//
			case 'del':
				if ( confirm('¿Eliminar foto de la galería? (No se remueve del servidor)') ) {
					let imgEl = cEl.closest('div.photo');
					formActions.postRequest(ajaxUrl + '/gallery/del', se.object.merge(postData, {id: imgEl.dataset['id']}), {
						onSuccess: (msg) => {
							imgEl.remove();
						}
					});
				}
				break;
			//
			default:
				console.log("Acción no definida.", cEl);
				break;
		}
	}

	//
	function img_callBack(imgData) {
		let imgDataFinal = {
			imgId:imgData.id,
			fName:imgData.fname,
			title:imgData.title,
			description:imgData.desc
		};
		//
		console.log("IMAGE SELECTED DATA", imgData, imgDataFinal);

		// AJAX
		formActions.postRequest(ajaxUrl+'/gallery/add', se.object.merge(postData, imgDataFinal), {
			onSuccess:(msg) => {
				photoList.se_site_coreend(se.struct.stringPopulate(photoTemplate.innerHTML, imgDataFinal));
			}
		});
	}

	// Expose
	init();
	return {};
};