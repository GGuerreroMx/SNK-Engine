<?php
//
$params['page']['ajax'] = '/api/user/';


//
switch ( \snkeng\core\engine\nav::next() ) {
	//
	case 'recovery':
		$params['page']['ajax'].= '/recovery';

		$params['vars']['key'] = ( isset($_GET['key'])) ? strval($_GET['key']) : '';
		if ( $params['vars']['key'] ) {
			$params['vars']['linkData'] = \snkeng\core\user\extlink::linkValidate( $params['vars']['key'] );
			if ( empty($params['vars']['linkData']) || $params['vars']['linkData']['app'] !== 'core' || $params['vars']['linkData']['act'] !== 'adm_recov' ) {
				\snkeng\core\engine\nav::invalidPage();
			}

			//
			require __DIR__ . '/recovery_password.php';
		} else {
			require __DIR__ . '/recovery_request.php';
		}
		break;

	// Página principal
	case '':
	case null:
		require __DIR__ . '/#index.php';
		break;

	// Otros
	default:
		\snkeng\core\engine\nav::invalidPage();
		break;
}
//
