<?php

$page['head']['title'] = 'Recuperación de contraseña';

//
$page['body'].= <<<HTML
<style>
.container { display:flex; align-items:center; justify-content:center; height:100vh; width:100vw; }
.microPage { background-color:#FFF; border:thin solid #999; border-radius:5px; width:500px; box-shadow:0 3px 5px #e2e2e2; }
.microPage > .fullTitle { padding:10px; margin:0; color:#FFFFFF; background-color:#999999; font-size:1.8rem; font-weight:bold; }
.microPage > .content { padding:10px; }
p + p { margin-top:1em; }
/* */
@media only screen and (max-width:767px) {
	.container { display:block; }
	#microPage { display:block; margin:10px auto; width:calc(100% - 20px); }
}
</style>
<div class="microPage">
	<div class="fullTitle">Recuperación de contraseña</div>
	<div class="content">
		<div class="gOMB">
			<p>Si ya tiene una cuenta con nosotros pero ha olvidado su contraseña, por favor llene el siguiente formulario.</p>
			<p>Si tiene su contraseña, <a se-nav="se_template_root" href="/admin">inicie sesión</a></p>
		</div>
		<form id="acc_recovery_1" class="se_form" method="post" action="/api/user/recovery/create" se-plugin="simpleForm">
			<input type="hidden" name="type" value="adm" />
			<label class="separator required">
				<div class="cont"><span class="title">E-Mail</span><span class="desc"></span></div>
				<input type="email" name="eMail" title="Correo electrónico de registro." required />
			</label>
			<div class="separator" id="g_captcha"></div>
			<button type="submit">Enviar</button>
			<output se-elem="response"></output>
		</form>
	</div>
</div>
HTML;
//

// Request captcha (supports internal ang google)
$localCaptcha = new \snkeng\core\general\captcha();
$localCaptcha->load();