<?php
//
$nav = [
	'tables' => [
		'sel' => "t.t_id AS id, t.t_name AS title,
		t.t_dbname AS dbName, t.t_dbnick AS dbNick,
		m.mod_name AS modTitle,
		IF (gt.gt_id IS NOT NULL && gt_dt_mod>= t.t_dt_mod, 1, 0) AS upToDate
		",
		'from' => 'sa_table_object AS t
		INNER JOIN sa_modules AS m ON t.mod_id=m.mod_id
		LEFT OUTER JOIN sb_ghost_table_object AS gt ON gt.t_id=t.t_id',
		'lim' => 50,
		'where' => [
			'modTitle' => ['name' => 'Mod Nombre', 'db' => 'm.mod_name', 'vtype' => 'str', 'stype' => 'like'],
			'title' => ['name' => 'Nombre', 'db' => 't.t_name', 'vtype' => 'str', 'stype' => 'like'],
			'dbName' => ['name' => 'DB Nombre', 'db' => 't.t_dbname', 'vtype' => 'str', 'stype' => 'like'],
			'dbNick' => ['name' => 'DB Nick', 'db' => 't.t_dbnick', 'vtype' => 'str', 'stype' => 'like']
		],
		'order' => [
			['Name' => 'Módulo', 'db' => 'mod_name', 'set' => 'ASC'],
			['Name' => 'Nombre', 'db' => 't_name', 'set' => 'ASC']
		]
	],
	'tEl_simple' => [
		'sel' => "
		te_id AS id, te_name AS title, te_desc AS content, te_pos AS cOrder,
		te_db_name AS dbName, te_db_type AS dbType, te_db_typedesc AS dbTypeDesc,
		te_db_notnull AS dbNotNull, te_db_unsigned AS dbUnsigned, te_db_autoinc AS dbAutoInc, te_db_prikey AS dbPrikey, te_db_default AS dbDefault, te_db_fulltext AS dbFullText,
		te_d_nick AS dNick, te_d_optional AS sysOpc, te_d_special AS sysEsp, te_d_parenttable AS sysParent,
		te_f_dtype AS fType, te_f_minl AS fMinl, te_f_maxl AS fMaxl, te_f_maxli AS fMaxlim
		",
		'from' => 'sa_table_column',
		'lim' => 50,
		'where' => [
			'tId' => ['name' => 'ID Tabla', 'db' => 't_id', 'vtype' => 'int', 'stype' => 'eq'],
		],
		'order' => [
			'tePos' => ['Name' => 'Orden', 'db' => 'te_pos', 'set' => 'ASC']
		]
	],
	'tKeys' => [
		'sel' => "
			tabkey_id AS id, te_id AS teId,
			tabkey_name AS tkName, tabkey_parent_table AS tkParentTable, tabkey_parent_tab_element AS tkParentElement
		",
		'from' => 'sa_table_column',
		'lim' => 50,
		'where' => [
			'tId' => ['name' => 'ID Tabla', 'db' => 't_id', 'vtype' => 'int', 'stype' => 'eq'],
		],
		'order' => [
			'tePos' => ['Name' => 'Orden', 'db' => 'te_pos', 'set' => 'ASC']
		]
	]
];
return $nav;
