<?php
//
$nav = [
	'tables' => [
		'sel' => "t.t_id AS id, t.t_name AS title,
		t.t_dbname AS dbName, t.t_dbnick AS dbNick,
		m.mod_name AS modTitle,
		IF (gt.gt_id IS NOT NULL && gt_dt_mod>= t.t_dt_mod, 1, 0) AS upToDate
		",
		'from' => 'sa_table_object AS t
		INNER JOIN sa_modules AS m ON t.mod_id=m.mod_id
		LEFT OUTER JOIN sb_ghost_table_object AS gt ON gt.t_id=t.t_id',
		'lim' => 50,
		'where' => [
			'modTitle' => ['name' => 'Mod Nombre', 'db' => 'm.mod_name', 'vtype' => 'str', 'stype' => 'like'],
			'title' => ['name' => 'Nombre', 'db' => 't.t_name', 'vtype' => 'str', 'stype' => 'like'],
			'dbName' => ['name' => 'DB Nombre', 'db' => 't.t_dbname', 'vtype' => 'str', 'stype' => 'like'],
			'dbNick' => ['name' => 'DB Nick', 'db' => 't.t_dbnick', 'vtype' => 'str', 'stype' => 'like']
		],
		'order' => [
			['Name' => 'Módulo', 'db' => 'mod_name', 'set' => 'ASC'],
			['Name' => 'Nombre', 'db' => 't_name', 'set' => 'ASC']
		]
	],
];
return $nav;
