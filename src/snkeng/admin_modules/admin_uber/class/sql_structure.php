<?php
//
namespace snkeng\admin_modules\admin_uber;

//
class sql_structure {
	//
	public static function tableReturn(&$response) {
		//
		$response['d'] = json_encode(self::dbStructGet(), JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES );
	}

	//
	public static function tableUpload(&$response, $cData) {
		$currentData = self::dbStructGet();

		//
		$response['d'] = self::tablesCompare($cData, $currentData);
	}

	//
	public static function dbStructGet() {
		//
		
		//
		$tableData = [];
		$tables = [];

		// Revisar SQL
		$sql_qry = "SHOW TABLES;";
		if ( \snkeng\core\engine\mysql::execQuery($sql_qry) ) {
			while ( $data = \snkeng\core\engine\mysql::$result->fetch_array() ) {
				$table = $data[0];
				if ( substr( $table, 0, 3 ) !== "st_" ) {
					$tables[] = $table;
				}
			}
		}

		//
		foreach ( $tables as $table ) {
			$cTableData = \snkeng\core\engine\mysql::returnArray("DESCRIBE {$table};");
			$tableData[$table] = [];
			foreach ( $cTableData as $cRow ) {
				$cName = $cRow['Field'];
				//
				unset($cRow['Field']);
				$cRow['Type'] = strtoupper($cRow['Type']);
				//
				$tableData[$table][$cName] = $cRow;

			}
		}

		return $tableData;
	}

	//
	public static function tableDownload() {
		$tableData = self::dbStructGet();

		$tableJson = json_encode($tableData, JSON_PRETTY_PRINT | JSON_UNESCAPED_SLASHES);

		$debug = ( isset($_GET['debug']) && $_GET['debug'] );

		if ( $debug ) {
			header('Content-Type: text/plain; charset=utf-8');

		} else {
			header("Content-Disposition: attachment; filename=dbStruct.txt");
			header("Content-type: application/force-download");
		}
		echo $tableJson;
		exit();
	}

	//
	public static function tablesGet() {
		//
		
		// Filtro para sólo seleccionar las que inicien con sa (las únicas oficialmente editables)
		$strArray = ['sa_', 'sb_', 'sc_'];
		$tables = [];
		$sql_qry = "SHOW TABLES LIKE 's__%';";
		if ( \snkeng\core\engine\mysql::execQuery($sql_qry) )
		{
			while ( $datos = \snkeng\core\engine\mysql::$result->fetch_array() )
			{
				if ( in_array(substr($datos[0],0, 3), $strArray) ) {
					$tables[] = $datos[0];
				}
			}

		}

		//
		$allTableData = [];

		//
		foreach ( $tables AS $cTable ) {
			//
			$cTabData = \snkeng\core\engine\mysql::returnArray("DESCRIBE {$cTable};", [
				'id' => 'Field'
			]);
			//

			//
			$allTableData[$cTable] = $cTabData;
		}

		//
		return $allTableData;
	}

	//
	private static function columnBuild($cCol) {
		//
		$cColString = $cCol['Type'];

		//
		switch ( $cCol['Key'] )
		{
			case 'PRI':
				$cColString .= " PRIMARY KEY";
				break;
			//
			default:
				//
				if ( $cCol['Null'] === "NO" ) {
					$cColString .= " NOT";
				}

				//
				$cColString .= " NULL";
				break;
		}

		//
		if ( strlen($cCol['Default']) ) {
			switch ( $cCol['Default'] ) {
				//
				case 'auto_increment':
					$cColString.= " AUTO_INCREMENT";
					break;
				//
				case 'CURRENT_TIMESTAMP':
					$cColString.= " DEFAULT CURRENT_TIMESTAMP";
					break;
				//
				default:
					$cColString.= " DEFAULT '{$cCol['Default']}'";
					break;
			}
		}

		//
		if ( !empty($cCol['Extra']) ) {
			$cColString.= " ". str_replace('DEFAULT_GENERATED', '', $cCol['Extra']);
		}

		return $cColString;
	}

	//
	public static function tablesCompare($tableNewData, $tableOldData)
	{
		$result = '';

		// Check for new tables
		$result.= "# TABLES TO ADD:\n";
		$tablesToAdd = array_diff_key($tableNewData, $tableOldData);
		if ( $tablesToAdd ) {
			foreach ( $tablesToAdd AS $cTableName => $cColumns ) {
				$result.= "CREATE TABLE `{$cTableName}` (\n";
				$first = true;
				foreach ( $cColumns AS $cColName => $colData ) {
					if ( !$first ) {
						$result.= ",\n";
					}
					//
					$result.= "\t`{$cColName}` " . self::columnBuild($colData);

					//
					$first = false;
				}
				$result.= "\n) ENGINE=INNODB;\n";
			}
			$result.= "# END OF ADDING\n\n";
		} else {
			$result.= "# NONE\n\n";
		}
		// For memory freeing
		unset($tablesToAdd);


		// Remove old tables
		$result.= "# TABLES TO REMOVE:\n";
		$tablesToDel = array_diff_key($tableOldData, $tableNewData);
		if ( $tablesToDel ) {
			foreach ( $tablesToDel AS $cTableName => $cTabContent ) {
				$result.= "DROP TABLE IF EXISTS `{$cTableName}`;\n";
			}
			$result.= "# END OF REMOVING\n\n";
		} else {
			$result.= "# NONE\n\n";
		}
		// For memory freeing
		unset($tablesToDel);

		// Check existing table with elements
		$result.= "# TABLES TO CHECK:\n";
		$tablesToCheck = array_intersect_key($tableNewData, $tableOldData);
		if ( $tablesToCheck ) {
			foreach ( $tablesToCheck AS $cTableName => $cTabContent ) {
				$hasChanges = false;
				$result.= "# TABLE: {$cTableName}";

				$cChanges = [];

				//
				$colsDel = array_diff_key($tableOldData[$cTableName], $tableNewData[$cTableName]);
				if ( $colsDel ) {
					foreach ( $colsDel AS $cColName => $colProps ) {
						$cChanges[] = "DROP `{$cColName}`";
					}
					$hasChanges = true;
				}


				// TODO: PONER LO DEL ORDEN... PARA MANTENER CONSISTENCIA
				$colsAdd = array_diff_key($tableNewData[$cTableName], $tableOldData[$cTableName]);
				if ( $colsAdd ) {
					foreach ( $colsAdd AS $cColName => $cCol ) {
						$cChanges[] = "ADD `{$cColName}` " . self::columnBuild($cCol);
					}
					$hasChanges = true;
				}

				//
				$colsSim = array_intersect_key($tableNewData[$cTableName], $tableOldData[$cTableName]);
				if ( $colsSim ) {
					$colNames = array_keys($colsSim);
					foreach ( $colNames AS $cColName ) {
						if ( array_diff_assoc($tableNewData[$cTableName][$cColName], $tableOldData[$cTableName][$cColName]) ) {
							/*
							echo "$cColName:\n";
							debugVariable($tableNewData[$cTableName][$cColName], 'new data', false);
							debugVariable($tableOldData[$cTableName][$cColName], 'old data', false);
							exit();
							*/

							$cChanges[] = "CHANGE `{$cColName}` `{$cColName}` " . self::columnBuild($tableNewData[$cTableName][$cColName]);
							$hasChanges = true;
						}
					}
				}

				//
				if ( $hasChanges ) {
					$result.= ":\n";
					$result.= "ALTER TABLE {$cTableName}\n";
					$result.= implode(",\n", $cChanges);
					$result.= ";\n\n";
				} else {
					$result.= ": no changes.\n\n";
				}
			}
			$result.= "# END OF CHECK.\n\n";
		} else {
			$result.= "# NONE (WHAT?)\n\n";
		}

		//
		return $result;
	}
}
//