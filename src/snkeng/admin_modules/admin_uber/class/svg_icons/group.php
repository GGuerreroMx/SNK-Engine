<?php
namespace snkeng\admin_modules\admin_uber\svg_icons;

// Icon management
class group
{
	public static function select($fileName, $list) {
		// Directories
		$dir_editable   = $_SERVER['DOCUMENT_ROOT'] . '/se_files/site_generated/svg-icons/groups';
		$dir_libraries  = $_SERVER['DOCUMENT_ROOT'] . '/se_files/site_generated/svg-icons/libraries';
		$dir_final      = $_SERVER['DOCUMENT_ROOT'] . '/se_files/site_generated/svg-icons/final';

		//
		$currentFile = $dir_editable . "/{$fileName}.json";
		$svg_final = $dir_final . "/{$fileName}";

		if ( !file_exists($currentFile) ) {
			\snkeng\core\engine\nav::killWithError('Target file not found.');
		}

		// Save current data
		file_put_contents($currentFile, json_encode($list, JSON_UNESCAPED_SLASHES | JSON_UNESCAPED_UNICODE));

		//
		$svgData = '';
		//
		foreach ( $list as $libName => $elements ) {
			$librarySvgFile = $dir_libraries . "/{$libName}.svg";

			//
			if ( !file_exists($librarySvgFile) ) {
				\snkeng\core\engine\nav::killWithError("Library file not found {$libName}");
			}

			// Load
			$libraryDom = simplexml_load_file($librarySvgFile);

			//
			foreach ( $elements as $cElem ) {
				$symbolXML = $libraryDom->xpath('//*[@id="' . $cElem . '"]');
				if ( empty($symbolXML) ) {
					\snkeng\core\engine\nav::killWithError("Library file not found {$libName}, {$cElem}");
				}
				//
				$svgData.= $symbolXML[0]->asXML() . "\n";
			}

		}

		// Attach svg
		$svg_struct = <<<HTML
<?xml version="1.0" encoding="UTF-8" standalone="no"?>
<svg xmlns="http://www.w3.org/2000/svg" style="display:none;">
{$svgData}</svg>
HTML;

		// Save normal
		file_put_contents($svg_final . '.svg', $svg_struct);
		// gZip
		$fileZip = gzencode($svg_struct, 9);
		// Save Zip
		file_put_contents($svg_final . '.svgz', $fileZip);
	}
}
//
