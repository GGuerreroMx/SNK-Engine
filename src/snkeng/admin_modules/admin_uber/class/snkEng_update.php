<?php
namespace snkeng\admin_modules\admin_uber;
//
class snkEng_update
{
	//
	// INI: Variables
	public $hasChanged = false;
	
	public $tables = [];
	public $getContent = false;
	
	public $fileName = "";
	public $txtFile = "";

	//
	// INI: construct
	public function __construct()
	{
	}
	// END: construct
	//

	//
	// INI: updateAllSite
	//	Revisa los cambios en todas las tablas y objetos del sistema.
	public function updateAllSite()
	{
		$upd_str = "";
		
		// Lectura de tablas
		echo("<h2>Tablas</h2>\n");
		$objArray = [];
		$sql_qry = "SELECT t_id, t_name, t_dbname FROM sa_table_object;";
		$response = [];
		if ( \snkeng\core\engine\mysql::execQuery($sql_qry) )
		{
			while ( $data = \snkeng\core\engine\mysql::$result->fetch_row() )
			{
				if ( !empty($data[2]) && preg_match("/st_/", $data[2]) )
				{
					$objArray[] = array('id'=>intval($data[0]), 'name'=>$data[1], 'dbname'=>$data[2]);
				}
			}
			foreach ( $objArray as $obj )
			{
				echo("<h3>".$obj['name']."(".$obj['dbname'].")</h3>\n");
				$this->updateTable($response, $obj['id']);
			}
		} else {
			\snkeng\core\engine\mysql::killIfError("No se pudieron leer las tablas.", "full");
		}

	}
	// END: updateAllSite
	//

	/**
	 * Update a single table. It checks status with ghost tables and database.
	 * @param array $response return values for async call
	 * @param number $tId the id of table (in sa_table_object) to update.
	 */
	public function updateTable(&$response, $tId)
	{
		$sql_qry = "SELECT t_id FROM sb_ghost_table_object WHERE t_id={$tId};";
		$gTabId = \snkeng\core\engine\mysql::singleNumber($sql_qry);
		// Determinar la tabla existe (basado en la tabla ghost)
		if ( $gTabId === 0 ) {
			$data = $this->createTable($tId);
		} else {
			$data = $this->alterTable($tId);
		}

		// Insertar tabla
		if ( $data['changed'] )
		{
			$response['debug']['table_qry'] = $data['qry'];
			// debugVariable($data['qry'], 'changed');
			\snkeng\core\engine\mysql::submitQuery($data['qry'], [
				'errorKey' => 'SADBUPD',
				'errorDesc' => 'No se pudo actualizar la tabla.'
			]);

			// Llevar a cabo operaciones de actualización.
			$this->updateOps($response, $tId, $data['qry']);
		} else {
			// Actualizar fechas de last update, sólo para evitar que existan problemas
			$sql_qry = "UPDATE sa_table_object SET t_dt_mod=NOW() WHERE t_id={$tId};\nUPDATE sb_ghost_table_object SET gt_dt_mod=NOW() WHERE t_id={$tId};";
			\snkeng\core\engine\mysql::submitMultiQuery($sql_qry, [
				'errorKey' => 'SE_sa_table_objectUPDATE',
				'errorDesc' => 'No fue posible actualizar el cambio de las tablas.'
			]);

			//
			\snkeng\core\engine\nav::killWithError("No database structure changes detected.");
		}
	}

	/**
	 * Update an array of tables.
	 * @param array $response the return of actual content.
	 * @param array $tables array list containing the tables to update.
	 */
	public function updateTables(&$response, $tables)
	{
		foreach ( $tables as $tId ) {
			//
			$sql_qry = "SELECT t_id FROM sb_ghost_table_object WHERE t_id={$tId};";
			$gTableId = \snkeng\core\engine\mysql::singleNumber($sql_qry,
				[
					'errorKey' => 'READ SUBMIT',
					'errorDesc' => 'Actualizar elementos.'
				]
			);

			// Determinar la tabla existe (basado en la tabla ghost)
			if ( $gTableId === 0 ) {
				$data = $this->createTable($tId);
			} else {
				$data = $this->alterTable($tId);
			}

			// Insertar tabla
			if ( $data['changed'] ) {
				\snkeng\core\engine\mysql::submitMultiQuery($data['qry'], [
					'errorKey' => '',
					'errorDesc' => ''
				]);
				// Llevar a cabo operaciones de actualización.
				$this->updateOps($response, $tId, $data['qry']);
			}
		}
	}

	//
	public function gTableClean(&$response) {
		$sql_qry = <<<SQL
SELECT gt_id AS cData
FROM sb_ghost_table_object AS gTab
WHERE gTab.t_id NOT IN (
	SELECT t_id
	FROM sa_table_object
);
SQL;
		//
		$gtIds = [];
		if ( \snkeng\core\engine\mysql::execQuery($sql_qry) )
		{
			while ( $data = \snkeng\core\engine\mysql::$result->fetch_assoc() )
			{
				$gtIds[] = $data['cData'];
			}
		}


		//
		\snkeng\core\engine\mysql::killIfError('SE_SA_gTableCleanRead', 'No fue posible leer las tablas adicionales.');

		$gtIdsTxt = implode(',', $gtIds);

		//
		$upd_sql = "DELETE FROM sb_ghost_table_object WHERE gt_id IN ({$gtIdsTxt});\nDELETE FROM sb_ghost_table_column WHERE gt_id IN ({$gtIdsTxt});";

		/*
		debugVariable($upd_sql);
		//
		\snkeng\core\engine\mysql::submitMultiQuery($upd_sql);
		\snkeng\core\engine\mysql::killIfError2('SE_SA_gTableCleanSubmit', 'No fue posible realizar la limpieza.');
		*/
		$response['d'] = $upd_sql;
	}

	//
	public function tableCheck(&$response) {
		$gTables = [];
		$rTables = [];

		// Revisar ghost
		$sql_qry = "SELECT gt_dbname
					FROM sb_ghost_table_object
					WHERE gt_dbname LIKE 'st_%'
					ORDER BY gt_dbname ASC;";
		if ( \snkeng\core\engine\mysql::execQuery($sql_qry) ) {
			while ( $data = \snkeng\core\engine\mysql::$result->fetch_array() ) {
				$gTables[] = $data['gt_dbname'];
			}
		} else {
			\snkeng\core\engine\mysql::killIfError('', '');
		}

		// Revisar SQL
		$sql_qry = "SHOW TABLES LIKE 'st_%';";
		if ( \snkeng\core\engine\mysql::execQuery($sql_qry) ) {
			while ( $data = \snkeng\core\engine\mysql::$result->fetch_array() ) {
				$rTables[] = $data[0];
			}
		} else {
			\snkeng\core\engine\mysql::killIfError('', '');
		}


		$grDiff = array_diff($gTables, $rTables);
		$rgDiff = array_diff($rTables, $gTables);

		//
		$dSql = '';
		foreach ( $rgDiff as $i ) {
			$dSql.= "DROP TABLE IF EXISTS `{$i}`;\n";
		}
		$response['d'] = $dSql;

		$response['debug'] = $grDiff;

		// comparar
		/*
		debugVariable($gTables, 'GHOST', false);
		debugVariable($rTables, 'REAL', false);
		debugVariable($grDiff, 'DIFF GHOST-REAL', false);
		debugVariable($rgDiff, 'DIFF REAL-GHOST', false);
		exit();
		*/
	}

	/**
	 * Create a table from the sa_table_object id.
	 * @param int $tId ID of the table
	 * @return mixed Information about the operation, includes query.
	 */
	private function createTable($tId)
	{
		// Hacer a partir de insert.
		$newSQL = "";
		$sql_qry = "SELECT t_dbname, t_dbtype FROM sa_table_object WHERE t_id={$tId};";
		if ( \snkeng\core\engine\mysql::execQuery($sql_qry) )
		{
			$this->hasChanged = true;
			while ( $tabla = \snkeng\core\engine\mysql::$result->fetch_array() )
			{
				$newSQL.= "CREATE TABLE `".$tabla['t_dbname']."`\n(\n";
				$sql_qry = "SELECT
								te_db_name, te_db_type, te_db_typedesc,
								te_db_notnull, te_db_unsigned, te_db_autoinc, te_db_prikey, te_db_default,
								te_db_fulltext
							FROM sa_table_column
							WHERE t_id={$tId}
							ORDER BY te_pos ASC;";
				if ( \snkeng\core\engine\mysql::execQuery($sql_qry) )
				{
					$fullText = [];
					$first = true;
					while ( $datos = \snkeng\core\engine\mysql::$result->fetch_assoc() )
					{
						if ($first) {
							$first = false;
						} else {
							$newSQL.= ",\n";
						}
						$newSQL.= "  `".$datos['te_db_name']."` ".$datos['te_db_type'];
						if ( !empty( $datos['te_db_typedesc'] ) )		{ $newSQL.= "(".$datos['te_db_typedesc'].")"; }
						if ( intval($datos['te_db_unsigned'] ) == 1 )	{ $newSQL.= " UNSIGNED"; }
						if ( intval($datos['te_db_autoinc'] ) == 1 )	{ $newSQL.= " AUTO_INCREMENT"; }
						if ( intval($datos['te_db_prikey'] ) == 1 )		{ $newSQL.= " PRIMARY KEY"; }
						if ( intval($datos['te_db_notnull'] ) == 1 )	{ $newSQL.= " NOT NULL"; }
						if ( $datos['te_db_default'] !== null && strlen($datos['te_db_default']) > 0 ) {
							$defValue = ( in_array($datos['te_db_type'], ['DATE', 'DATETIME', 'TIME', 'TIMESTAMP']) ) ? $datos['te_db_default'] : "'{$datos['te_db_default']}'";
							$newSQL.= " DEFAULT {$defValue}";
						}
						// Ver si es fulltext
						if ( $datos['te_db_fulltext'] == 1 ) {
							$fullText[] = $datos['te_db_name'];
						}
					}
					if ( !empty($fullText) )
					{
						$newSQL.= ",\nFULLTEXT (";
						$first = true;
						foreach ( $fullText as $x )
						{
							if ($first) {
								$first = false;
							} else {
								$newSQL.= ",";
							}
							$newSQL.= $x;
						}
						$newSQL.= ")";
					}
				}
				\snkeng\core\engine\mysql::killIfError('SA_SE_', 'asdfasdf');
				$newSQL.= "\n)\nENGINE = ".$tabla['t_dbtype'].";\n";
			}
		}
		//
		\snkeng\core\engine\mysql::killIfError('SA_SE_', 'asdfasdf');
		// Fin operación insert table
		$return['changed'] = true;
		$return['qry'] = $newSQL;
		return $return;
	}

	/**
	 * Updates a table from the sa_table_object. In theory the exisance in ghost tables is checked. Check for real table and abort if fail.
	 * @param $tId
	 * @return mixed
	 */
	private function alterTable($tId)
	{
		// Operación de alter table
		$abort = false;
		$hasChanges = false;
		$error = '';
		$uptqry = "";

		// Descripción tabla nueva.
		$sql_qry = "SELECT t_dbname AS name, t_dbtype AS type FROM sa_table_object WHERE t_id={$tId};";
		$tableNew = \snkeng\core\engine\mysql::singleRowAssoc($sql_qry, [
			'errorKey' => 'SAdminEngUpdateAlter',
			'errorDesc' => 'No fue posible leer la forma de la tabla nueva.'
		]);

		// Descripción tabla vieja.
		$sql_qry = "SELECT gt_dbname AS name, gt_dbtype AS type FROM sb_ghost_table_object WHERE t_id={$tId};";
		$tableOld = \snkeng\core\engine\mysql::singleRowAssoc($sql_qry, [
			'errorKey' => 'SAdminEngUpdateAlter',
			'errorDesc' => 'No fue posible leer la forma de la tabla vieja.'
		]);

		// Revisar el nombre
		if ( $tableNew['name'] !== $tableOld['name'] ) {
			$hasChanges = true;
		}
		// Revisar tipo
		if ( $tableNew['type'] !== $tableOld['type'] ) {
			$hasChanges = true;
		}

		// TABLA de sistema
		$sql_qry = "DESCRIBE {$tableOld['name']};";
		$tableSystemArray = \snkeng\core\engine\mysql::returnArray($sql_qry);
		if ( empty($tableSystemArray) ) {
			\snkeng\core\engine\nav::killWithError("Tabla no disponible en la base de datos. Tabla: '{$tableOld['name']}'");
		}

		// Contenido tabla vieja
		$contentOld = [];
		$oldIndex = [];
		$oldFullText = [];
		$sql_qry = <<<SQL
SELECT
gte_id AS gteId,
te_id,
gte_db_name, gte_db_type, gte_db_typedesc,
gte_db_notnull, gte_db_unsigned, gte_db_autoinc, gte_db_prikey, gte_db_default,
gte_db_fulltext
FROM sb_ghost_table_column
WHERE gt_id={$tId};
SQL;
		if ( \snkeng\core\engine\mysql::execQuery($sql_qry) )
		{
			while ( $datos = \snkeng\core\engine\mysql::$result->fetch_array() )
			{
				$tArray = [];
				$teid = $datos['te_id'];
				$tArray['name'] = $datos['gte_db_name'];
				$tArray['type'] = $datos['gte_db_type'];
				$tArray['typed'] = $datos['gte_db_typedesc'];
				$tArray['notnull'] = $datos['gte_db_notnull'];
				$tArray['unsigned'] = $datos['gte_db_unsigned'];
				$tArray['autoinc'] = $datos['gte_db_autoinc'];
				$tArray['prikey'] = $datos['gte_db_prikey'];
				$tArray['default'] = $datos['gte_db_default'];
				$tArray['fullText'] = $datos['gte_db_fulltext'];
				$contentOld[$teid] = $tArray;
				$oldIndex[] = $teid;
				if ( $datos['gte_db_fulltext'] == 1 )
				{
					$oldFullText[] = $datos['gte_db_name'];
				}


				// Validación con sistema
				$elExists = false;
				//
				foreach ( $tableSystemArray as $index => $curElem ) {
					if ( $curElem['Field'] === $tArray['name'] ) {
						$elExists = true;
						// Validation
						if ( $curElem['Type'] === 'tinyint(1)' && $tArray['type'] === 'BOOLEAN' ) {
							continue;
						}
						//
						$tElType = (
							( strstr($curElem['Type'], '(') ) ?
								strstr($curElem['Type'], '(', true) :
								(
									( strstr($curElem['Type'], ' ') ) ?
										strstr($curElem['Type'], ' ', true) :
										$curElem['Type']
								)
						);

						//
						if ( strtolower($tArray['type']) !== strtolower($tElType) ) {
							\snkeng\core\engine\nav::killWithError('Diferencias entre tabla en el sistema y gtable.', "Name:'{$tArray['name']}'. Gtable:[ID: {$datos['te_id']}. - Type:'{$tArray['type']}']. - System:'{$curElem['Type']}' - $tElType");
						}

						//
						switch ( $tElType ) {
							//
							case 'boolean':
								break;
							//
							case 'tinyint':
							case 'smallint':
							case 'mediumint':
							case 'int':
							case 'bigint':
								// Checar atributos... negativo etc
								break;
							//
							case 'varchar':
							case 'char':
								// Revisar tamaño
								break;
							//
							case 'varbinary':
								break;
							//
							case 'tinytext':
							case 'smalltext':
							case 'mediumtext':
							case 'text':
							case 'longtext':
								break;
							//
							case 'time':
							case 'datetime':
							case 'date':
							case 'year':
							case 'timestamp':
								break;
							//
							case 'float':
							case 'double':
							case 'decimal':
								break;

							//
							default:
								\snkeng\core\engine\nav::killWithError('System table type not defined (in code)', $tElType);
								break;
						}

						// Clean up
						unset($tableSystemArray[$index]);
						break;
					}
				}

				//
				if ( !$elExists ) {
					\snkeng\core\engine\nav::killWithError("Un elemento gtable ('{$tArray['name']}', '{$datos['gteId']}') no se encuentra en la base de datos del sistema.");
				}
			}

			// Si se extiende
		}
		//
		\snkeng\core\engine\mysql::killIfError('SAdminEngUpdateAlter', 'No fue posible obtener elementos de la tabla original (gtable).');

		// Contenido tabla nueva
		$newIndex = [];
		$contentNew = [];
		$newFullText = [];
		$sql_qry = <<<SQL
SELECT
te_id,
te_db_name, te_db_type, te_db_typedesc,
te_db_notnull, te_db_unsigned, te_db_autoinc, te_db_prikey, te_db_default,
te_db_fulltext
FROM sa_table_column
WHERE t_id={$tId};
SQL;

		if ( \snkeng\core\engine\mysql::execQuery($sql_qry) )
		{
			while ( $datos = \snkeng\core\engine\mysql::$result->fetch_assoc() )
			{
				$tArray = [];
				$teid = $datos['te_id'];
				$tArray['name'] = $datos['te_db_name'];
				$tArray['type'] = $datos['te_db_type'];
				$tArray['typed'] = $datos['te_db_typedesc'];
				$tArray['notnull'] = $datos['te_db_notnull'];
				$tArray['unsigned'] = $datos['te_db_unsigned'];
				$tArray['autoinc'] = $datos['te_db_autoinc'];
				$tArray['prikey'] = $datos['te_db_prikey'];
				$tArray['default'] = $datos['te_db_default'];
				$tArray['fullText'] = $datos['te_db_fulltext'];
				$contentNew[$teid] = $tArray;
				$newIndex[] = $teid;
				// FullText
				if ( $datos['te_db_fulltext'] == 1 )
				{
					$newFullText[] = $datos['te_db_name'];
				}
			}
		}
		//
		\snkeng\core\engine\mysql::killIfError('SAdminEngUpdateAlter', 'No fue posible obtener elementos de la tabla nueva.');


		// Iniciar análisis de cambios
		$uptqry = '';
		// Ver si quitar index FULLTEXT inicial
		if ( $oldFullText !== $newFullText )
		{
			$sql_qry = "SHOW INDEX FROM ".$tableOld['name'];
			$dropName = "";
			if ( \snkeng\core\engine\mysql::execQuery($sql_qry) )
			{
				while ( $datos = \snkeng\core\engine\mysql::$result->fetch_array() )
				{
					if ( $datos['Index_type'] == "FULLTEXT" )
					{
						$dropName = $datos['Key_name'];
						$hasChanges = true;
					}
				}
			}
			if ( !empty($dropName) )
			{
				$uptqry.= "ALTER TABLE `".$tableOld['name']."` DROP INDEX `$dropName`;\n";
			}
		}



		$opts = [];
		$uptqry.= "ALTER TABLE ".$tableOld['name']."\n";
		// Rename?
		if ( $tableOld['name'] !== $tableNew['name'] ) { $opts[] = "RENAME ".$tableNew['name']; $hasChanges = true; }

		// Elementos a agregar
		$tempArray = array_diff($newIndex, $oldIndex);
		foreach ( $tempArray as $i )
		{
			$hasChanges = true;
			$temp = "ADD `".$contentNew[$i]['name']."`\t".$contentNew[$i]['type'];
			if ( !empty( $contentNew[$i]['typed'] ) )			{ $temp.= "(".$contentNew[$i]['typed'].")"; }
			if ( intval($contentNew[$i]['unsigned'] ) === 1 )	{ $temp.= " UNSIGNED"; }
			if ( intval($contentNew[$i]['autoinc'] ) === 1 )	{ $temp.= " AUTO_INCREMENT"; }
			if ( intval($contentNew[$i]['prikey'] ) === 1 )		{ $temp.= " PRIMARY KEY"; }
			if ( intval($contentNew[$i]['notnull'] ) === 1 )	{ $temp.= " NOT NULL"; }
			if ( $contentNew[$i]['default'] !== null && strlen($contentNew[$i]['default']) > 0 ) {
				$defValue = ( in_array($contentNew[$i]['type'], ['DATE', 'DATETIME', 'TIME', 'TIMESTAMP']) ) ? $contentNew[$i]['default'] : "'{$contentNew[$i]['default']}'";
				$temp.= " DEFAULT {$defValue}";
			}
			//
			$opts[] = $temp;
		}

		// Elementos a borrar - I <3 U Katze :)
		$tempArray = array_diff($oldIndex, $newIndex);
		foreach ( $tempArray as $i )
		{
			$hasChanges = true;
			$opts[] = "DROP ".$contentOld[$i]['name'];
		}

		// Posible actualización :)
		$tempArray = array_intersect($oldIndex, $newIndex);
		foreach ( $tempArray as $i )
		{
			$tArray = array_diff_assoc($contentOld[$i], $contentNew[$i]);
			// echo("<br />\n");
			// print_r($contentOld[$i]);
			// echo("<br />\n");
			// print_r($contentNew[$i]);
			// echo("<br />\n");
			// print_r($tArray);
			// echo("<br />\n");
			if ( !empty($tArray) )
			{
				$hasChanges = true;
				$temp = "CHANGE `".$contentOld[$i]['name']."`\t`".$contentNew[$i]['name']."`\t".$contentNew[$i]['type'];
				if ( !empty( $contentNew[$i]['typed'] ) )			{ $temp.= "(".$contentNew[$i]['typed'].")"; }
				if ( intval( $contentNew[$i]['unsigned'] ) == 1 )	{ $temp.= " UNSIGNED"; }
				// if ( intval( $contentNew[$i]['autoinc'] ) == 1 )	{ $temp.= " AUTO_INCREMENT"; }
				// if ( intval( $contentNew[$i]['prikey'] ) == 1 )		{ $temp.= " PRIMARY KEY"; }
				if ( intval( $contentNew[$i]['notnull'] ) == 1 )	{ $temp.= " NOT NULL"; }
				if (  $contentNew[$i]['default'] !== null && strlen($contentNew[$i]['default']) > 0 ) {
					$defValue = ( in_array($contentNew[$i]['type'], ['DATE', 'DATETIME', 'TIME', 'TIMESTAMP']) ) ? $contentNew[$i]['default'] : "'{$contentNew[$i]['default']}'";
					$temp.= " DEFAULT {$defValue}";
				}
				$opts[] = $temp;
			}
		}

		// Agregar FullIndex (si cambió)
		if ( $oldFullText != $newFullText )
		{
			$temp = "ADD FULLTEXT(";
			$first = true;
			foreach ( $newFullText as $x=>$y )
			{
				$temp.= ($first) ? "" : ", ";
				$first = false;
				$temp.= "`$y`";
			}
			$temp.= ")";
			$opts[] = $temp;
		}

		// Revisar si hay que cambiar de nombre a la tabla
		if ( $tableOld['type'] !== $tableNew['type'] ) { $opts[] = "ENGINE=".$tableNew['type']; $hasChanges = true; }

		// Juntar
		$uptqry.= implode(",\n", $opts);

		$return['changed'] = $hasChanges;
		$return['qry'] = $uptqry.';';
		//
		return $return;
	}
	// END: alterTable
	//

	//
	// INI: reorderTable
	//	Crear query para reordernar elementos (la tabla debe existir y no deben existir modificaciones dado que gData no se alterará)
	public function reorderDBStruct($tId)
	{
		// Operación de alter table
		$abort = false;
		$error = '';
		$uptqry = '';
		// Descripción tabla nueva.
		$sql_qry = "SELECT t_dbname, t_dbtype FROM sa_table_object WHERE t_id=\"$tId\"";
		if ( \snkeng\core\engine\mysql::execQuery($sql_qry) )
		{
			while ( $tabla = \snkeng\core\engine\mysql::$result->fetch_array() )
			{
				$tableNew['name'] = $tabla['t_dbname'];
				$tableNew['type'] = $tabla['t_dbtype'];
			}
		} else {
			$abort = true;
			$error = "No se llevo a cabo consulta tabla nueva.";
		}

		// Contenido tabla nueva
		$curItems = [];
		$sql_qry = "SELECT
						te_id,
						te_db_name, te_db_type, te_db_typedesc,
						te_db_notnull, te_db_unsigned, te_db_default
					FROM sa_table_column
					WHERE t_id={$tId}
					ORDER BY te_pos ASC";
		if ( \snkeng\core\engine\mysql::execQuery($sql_qry) )
		{
			$first = true;
			$lastName = "";
			$uptqry = "ALTER TABLE ".$tableNew['name']."\n";
			while ( $datos = \snkeng\core\engine\mysql::$result->fetch_array() )
			{
				if ( !$first ) { $uptqry.= ",\n"; }
				
				$uptqry.= "CHANGE `".$datos['te_db_name']."` `".$datos['te_db_name']."` ".$datos['te_db_type'];
				if ( !empty( $datos['te_db_typedesc'] ) )		{ $uptqry.= " (".$datos['te_db_typedesc'].")"; }
				if ( intval( $datos['te_db_unsigned'] ) == 1 )	{ $uptqry.= " UNSIGNED"; }
				if ( intval( $datos['te_db_notnull'] ) == 1 )	{ $uptqry.= " NOT NULL"; }
				if ( $datos['te_db_default'] !== null && strlen($datos['te_db_default']) > 0 ) {
					$defValue = ( in_array($datos['te_db_type'], ['DATE', 'DATETIME', 'TIME', 'TIMESTAMP']) ) ? $datos['te_db_type']['default'] : "'{$datos['te_db_type']['default']}'";
					$uptqry.= " DEFAULT {$defValue}";
				}
				if ( $first )
				{
					$uptqry.= " FIRST";
				} else {
					$uptqry.= " AFTER ".$lastName;
				}
				$lastName = '`'.$datos['te_db_name'].'`';
				$first = false;
			}
			$uptqry.= ";";
		} else {
			$abort = true;
			$error = "No se llevo a cabo consulta contenido tabla nueva.";
		}
		if ( $error )
		{
			return $error;
		} else {
			return $uptqry;
		}
	}
	// END: reorderTable
	//

	//
	// INI: updateOps
	//	Actualiza core de sistema de actualización de tablas
	//	Actualiza las opciones disponibles para los formularios, y searches. (Parte 2)
	private function updateOps(&$response, $tId, $upd_qry)
	{

		// Checar que la tabla existe
$response['d'] = <<<HTML
<h4>Actualización de tabla</h4>
<table>
<thead><tr><th>#</th><th>Operación</th><th>Resultado</th></tr></thead>
<tbody>\n
HTML;
		$string = "<tr><td>%s</td><td>%s</td><td>%s</td></tr>\n";
		$opTitle = "<tr><td colspan=\"3\">%s</td></tr>\n";

		// ---------------------------------------------
		//	OPERACIONES BÃSICAS
		// ---------------------------------------------

		$response['d'].= sprintf($opTitle, "Operaciones iniciales");

		// Verificar existencia tabla
		$opNum = 1;
		$opName = "Existencia tabla";
		$sql_qry = "SELECT t_id FROM sb_ghost_table_object WHERE t_id={$tId};";
		if ( !\snkeng\core\engine\mysql::notNullQuery($sql_qry) )
		{
			// La tabla no existe, crear.
			$sql_qry = "INSERT INTO sb_ghost_table_object (t_id, gt_dt_add) VALUES ($tId, now())";
			if ( \snkeng\core\engine\mysql::submitQuery($sql_qry) )
			{
				$opResult = "INSERTADA";
			} else {
				$opResult = "ERROR";
			}
		} else { 
			$opResult = "EXISTE";
		}
		$response['d'].= sprintf($string, $opNum, $opName, $opResult);
		
		
		// Actualizar propiedades de la tabla
		$opNum = 2;
		$opName = "Actualizar propiedades de la tabla";

		$sql_qry = "SELECT t_dbname, t_dbtype FROM sa_table_object WHERE t_id={$tId};";
		if ( \snkeng\core\engine\mysql::execQuery($sql_qry) )
		{
			while ( $tabla = \snkeng\core\engine\mysql::$result->fetch_array() )
			{
				$name = $tabla['t_dbname'];
				$type = $tabla['t_dbtype'];
			}
		}
		$sql_qry = "UPDATE sa_table_object SET t_dt_mod=now() WHERE t_id={$tId};\nUPDATE sb_ghost_table_object SET gt_dbname='{$name}', gt_dbtype='{$type}', gt_dt_mod=now() WHERE t_id={$tId};";
		if ( \snkeng\core\engine\mysql::submitMultiQuery($sql_qry) )
		{
			$opResult = "OK";
		} else {
			$opResult = "Error";
		}
		$response['d'].= sprintf($string, $opNum, $opName, $opResult);
		
		// ---------------------------------------------
		//	ACTUALIZACIÓN TABLA OCULTA
		// ---------------------------------------------

		$response['d'].= sprintf($opTitle, "Actualización tabla oculta");

		//
		// Cargar datos
		//

		// Contenido tabla nueva
		$opNum = 3;
		$opName = "Datos tabla nueva";

		$newIndex = [];
		$contentNew = [];
		$sql_qry = <<<SQL
SELECT
te_id,
te_db_name, te_db_type, te_db_typedesc,
te_db_notnull, te_db_unsigned, te_db_autoinc, te_db_prikey, te_db_default,
te_db_fulltext
FROM sa_table_column
WHERE t_id={$tId};
SQL;
		if ( \snkeng\core\engine\mysql::execQuery($sql_qry) )
		{
			while ( $datos = \snkeng\core\engine\mysql::$result->fetch_array() )
			{
				$tArray = [];
				$teid = $datos['te_id'];
				$tArray['name'] = $datos['te_db_name'];
				$tArray['type'] = $datos['te_db_type'];
				$tArray['typed'] = $datos['te_db_typedesc'];
				$tArray['notnull'] = $datos['te_db_notnull'];
				$tArray['unsigned'] = $datos['te_db_unsigned'];
				$tArray['autoinc'] = $datos['te_db_autoinc'];
				$tArray['prikey'] = $datos['te_db_prikey'];
				$tArray['default'] = $datos['te_db_default'];
				$tArray['fullText'] = $datos['te_db_fulltext'];
				$contentNew[$teid] = $tArray;
				$newIndex[] = $datos['te_id'];
			}
			$opResult = "OK";
		} else {
			$abort = true;
			$opResult = "No se llevo a cabo consulta contenido tabla nueva.";
		}
		$response['d'].= sprintf($string, $opNum, $opName, $opResult);

		
		// Contenido tabla vieja
		$opNum = 4;
		$opName = "Datos tabla vieja";

		$contentOld = [];
		$oldIndex = [];
		$sql_qry = <<<SQL
SELECT
te_id,
gte_db_name, gte_db_type, gte_db_typedesc,
gte_db_notnull, gte_db_unsigned, gte_db_autoinc, gte_db_prikey, gte_db_default,
gte_db_fulltext
FROM sb_ghost_table_column
WHERE gt_id={$tId};
SQL;
		if ( \snkeng\core\engine\mysql::execQuery($sql_qry) )
		{
			while ( $datos = \snkeng\core\engine\mysql::$result->fetch_array() )
			{
				$tArray = [];
				$teid = $datos['te_id'];
				$tArray['name'] = $datos['gte_db_name'];
				$tArray['type'] = $datos['gte_db_type'];
				$tArray['typed'] = $datos['gte_db_typedesc'];
				$tArray['notnull'] = $datos['gte_db_notnull'];
				$tArray['unsigned'] = $datos['gte_db_unsigned'];
				$tArray['autoinc'] = $datos['gte_db_autoinc'];
				$tArray['prikey'] = $datos['gte_db_prikey'];
				$tArray['default'] = $datos['gte_db_default'];
				$tArray['fullText'] = $datos['gte_db_fulltext'];
				$contentOld[$teid] = $tArray;
				$oldIndex[] = $datos['te_id'];
			}
			$opResult = "OK";
		} else {
			$opResult = "Error";
		}
		$response['d'].= sprintf($string, $opNum, $opName, $opResult);
		//

		//
		// Iniciar operaciones
		//

		// Elementos a agregar
		$opNum = 5;
		$opName = "Agregar";
		
		$tempArray = array_diff($newIndex, $oldIndex);
		$first = true;
		if ( !empty($tempArray ) )
		{	
			$ins_qry = "INSERT INTO sb_ghost_table_column 
						(te_id, gt_id, gte_db_name, gte_db_type, gte_db_typedesc, gte_db_notnull, gte_db_unsigned, gte_db_autoinc, gte_db_prikey, gte_db_default, gte_db_fulltext)
						VALUES\n";
			
			foreach ( $tempArray as $i )
			{		
				if ( $first )
				{
					$first = false;
				} else {
					$ins_qry.= ",\n";
				}

				$ins_qry.= "\t(".$i.", $tId, \"";
				$ins_qry.= $contentNew[$i]['name']."\", \"".$contentNew[$i]['type']."\", \"".$contentNew[$i]['typed']."\", ";
				$ins_qry.= $contentNew[$i]['notnull'].", ".$contentNew[$i]['unsigned'].", ".$contentNew[$i]['autoinc'].", ";
				$ins_qry.= $contentNew[$i]['prikey'].", \"".$contentNew[$i]['default']."\", ".$contentNew[$i]['fullText'];
				$ins_qry.= ")";
			}
			if ( \snkeng\core\engine\mysql::submitQuery($ins_qry) )
			{
				$opResult = "OK";
			} else {
				$opResult = "ERROR";
			}
		} else {
			$opResult = "NULL";
		}
		$response['d'].= sprintf($string, $opNum, $opName, $opResult);

		// Elementos a borrar (I <3 U)
		$opNum = 6;
		$opName = "Borrar";

		$tempArray = array_diff($oldIndex, $newIndex);
		$first = true;
		if ( !empty($tempArray ) )
		{	
			$del_qry = "DELETE FROM sb_ghost_table_column WHERE ";
			foreach ( $tempArray as $i )
			{
				if ( $first )
				{
					$first = false;
				} else {
					$del_qry.= " OR ";
				}
				$del_qry.= "te_id=$i";
			}
			// $response['d'].= ("\n<br />\n<br />\n".$del_qry."\n<br />\n<br />\n");
			if ( \snkeng\core\engine\mysql::submitQuery($del_qry) )
			{
				$opResult = "OK";
			} else {
				$opResult = "ERROR";
			}
		} else {
			$opResult = "NULL";
		}
		$response['d'].= sprintf($string, $opNum, $opName, $opResult);

		// Actualización (es indiscriminado) :)
		$opNum = 7;
		$opName = "Actualizar";
		
		$tempArray = array_intersect($oldIndex, $newIndex);
		$first = true;
		$upt_qry = '';
		if ( !empty($tempArray ) )
		{	
			
			foreach ( $tempArray as $i )
			{
				
				if ( $first )
				{
					$first = false;
				} else {
					$upt_qry.= "\n";
				}
				$upt_qry.= "UPDATE sb_ghost_table_column SET ";
				$upt_qry.= "gte_db_name=\"".$contentNew[$i]['name']."\", gte_db_type=\"".$contentNew[$i]['type']."\", gte_db_typedesc=\"";
				$upt_qry.= $contentNew[$i]['typed']."\", gte_db_notnull=".$contentNew[$i]['notnull'].", gte_db_unsigned=";
				$upt_qry.= $contentNew[$i]['unsigned'].", gte_db_autoinc=".$contentNew[$i]['autoinc'].", gte_db_prikey=";
				$upt_qry.= $contentNew[$i]['prikey'].", gte_db_default=\"".$contentNew[$i]['default']."\", gte_db_fulltext=";
				$upt_qry.= $contentNew[$i]['fullText']." WHERE te_id=$i;";
			}
			// $response['d'].= ("\n<br />\n<br />\n".$upt_qry."\n<br />\n<br />\n");
			if ( \snkeng\core\engine\mysql::submitMultiQuery($upt_qry) )
			{
				$opResult = "OK";
			} else {
				$opResult = "ERROR";
			}
		} else {
			$opResult = "NULL";
		}
		$response['d'].= sprintf($string, $opNum, $opName, $opResult);

		// --------------------------------------------
		//	AUTONAVS, AUTOTABLES Y FORM
		// --------------------------------------------

		/*
		$response['d'].= sprintf($opTitle, "Actualización elementos autonav, autotable y form");

		// Variables
		$tIds = [];
		$anids = [];
		$dtids = [];
		$tfids = [];
		$texts = [];
		$firstIns = true;
		$firstDel = true;

		//
		//	Carga de datos
		//

		// tabla - selección
		$opNum = 8;
		$opName = "Selección de elementos de la tabla";

		$sql_qry = "SELECT te_id AS id, te_name FROM sa_table_column WHERE t_id=\"$tId\"";
		if ( \snkeng\core\engine\mysql::execQuery($sql_qry) )
		{
			while ( $datos = \snkeng\core\engine\mysql::$result->fetch_array() )
			{
				$tIds[] = $datos['id'];
			}
			$opResult = "OK";
		} else {
			$opResult = "Error";
		}
		$response['d'].= sprintf($string, $opNum, $opName, $opResult);

		// autonavs - selección
		$opNum = 9;
		$opName = "Selección de elementos de autonav";

		$sql_qry = "SELECT te_id AS id, antl.antl_id AS antlid
					FROM sa_autonav_link_telem AS anel
						INNER JOIN sa_autonav_link_table_obj AS antl ON anel.antl_id=antl.antl_id
					WHERE antl.t_id=\"$tId\"";
		if ( \snkeng\core\engine\mysql::execQuery($sql_qry) )
		{
			while ( $datos = \snkeng\core\engine\mysql::$result->fetch_array() )
			{
				$antlid = $datos['antlid'];
				$anids[$antlid][] = $datos['id'];
			}
			$opResult = "OK";
		} else {
			if ( empty(\snkeng\core\engine\mysql::error) )
			{
				$opResult = "EMPTY";
			} else {
				$opResult = "ERROR";
				$response['d'].= (\snkeng\core\engine\mysql::createAdvancedError('NO fue posible leer información', 'full'));
			}
		}
		$response['d'].= sprintf($string, $opNum, $opName, $opResult);

		// dintable - selección
		$opNum = 10;
		$opName = "Selección de elementos de dintable";

		$sql_qry = "SELECT te_id AS id, dttl.dttl_id AS dttlid
					FROM sa_dintab_link_telem AS dtel
						INNER JOIN sa_dintab_link_table_obj AS dttl ON dtel.dttl_id=dttl.dttl_id
					WHERE dttl.t_id=\"$tId\"";
		if ( \snkeng\core\engine\mysql::execQuery($sql_qry) )
		{
			while ( $datos = \snkeng\core\engine\mysql::$result->fetch_array() )
			{
				$dttlid = $datos['dttlid'];
				$dtids[$dttlid][] = $datos['id'];
			}
			$opResult = "OK";
		} else {
			if ( empty(\snkeng\core\engine\mysql::error) )
			{
				$opResult = "EMPTY";
			} else {
				$opResult = "ERROR";
				$response['d'].= (\snkeng\core\engine\mysql::createAdvancedError('NO fue posible leer información', 'full'));
			}
		}
		$response['d'].= sprintf($string, $opNum, $opName, $opResult);

		// tableForm - selección
		$opNum = 11;
		$opName = "Selección de elementos de formularios";

		$sql_qry = "SELECT tfl.te_id AS id, tf.tf_id AS tfid
					FROM sa_form_link_telem AS tfl
						INNER JOIN sa_form_obj AS tf ON tfl.tf_id=tf.tf_id
					WHERE tf.t_id=\"$tId\"";
		if ( \snkeng\core\engine\mysql::execQuery($sql_qry) )
		{
			while ( $datos = \snkeng\core\engine\mysql::$result->fetch_array() )
			{
				$tfid = $datos['tfid'];
				$tfids[$tfid][] = $datos['id'];
			}
			$opResult = "OK";
		} else {
			if ( empty(\snkeng\core\engine\mysql::error) )
			{
				$opResult = "EMPTY";
			} else {
				$opResult = "ERROR";
				$response['d'].= (\snkeng\core\engine\mysql::createAdvancedError('NO fue posible leer información', 'full'));
			}
		}
		$response['d'].= sprintf($string, $opNum, $opName, $opResult);


		//
		//	Inicio de operaciones
		//

		// Operaciones autonavs
		$opNum = 12;
		$opName = "Creación actualización autonavs";
		foreach ( $anids AS $antlid=>$anids )
		{
			// Agregar
			$tempArray = array_diff($tIds, $anids);
			foreach ( $tempArray AS $id )
			{
				if ( $firstIns )
				{
					$firstIns = false;
				} else {
					$texts[0].= ",\n";
				}
				$texts[0].= "($antlid, $id)";
			}

			// Borrar
			$tempArray = array_diff($anids, $tIds);
			foreach ( $tempArray AS $id )
			{
				if ( $firstDel )
				{
					$firstDel = false;
				} else {
					$texts[1].= " OR ";
				}
				$texts[1].= "te_id=$id";
			}
		}
		$opResult = "-";
		$response['d'].= sprintf($string, $opNum, $opName, $opResult);
		
		// Operaciones dintables
		$opNum = 13;
		$opName = "Creación actualización dintables";
		$firstIns = true;
		$firstDel = true;
		foreach ( $dtids AS $dttlid=>$dtids )
		{
			// Agregar
			$tempArray = array_diff($tIds, $dtids);
			foreach ( $tempArray AS $id )
			{
				if ( $firstIns )
				{
					$firstIns = false;
				} else {
					$texts[2].= ",\n";
				}
				$texts[2].= "($dttlid, $id)";
			}

			// Borrar
			$tempArray = array_diff($dtids, $tIds);
			foreach ( $tempArray AS $id )
			{
				if ( $firstDel )
				{
					$firstDel = false;
				} else {
					$texts[3].= " OR ";
				}
				$texts[3].= "te_id=$id";
			}
		}
		$opResult = "-";
		$response['d'].= sprintf($string, $opNum, $opName, $opResult);

		// Operaciones formularios
		$opNum = 14;
		$opName = "Creación actualización formularios";
		$firstIns = true;
		$firstDel = true;
		foreach ( $tfids AS $tftlid=>$tfids )
		{
			// Agregar
			$tempArray = array_diff($tIds, $tfids);
			foreach ( $tempArray AS $id )
			{
				if ( $firstIns )
				{
					$firstIns = false;
				} else {
					$texts[4].= ",\n";
				}
				$texts[4].= "($tftlid, $id)";
			}

			// Borrar
			$tempArray = array_diff($tfids, $tIds);
			foreach ( $tempArray AS $id )
			{
				if ( $firstDel )
				{
					$firstDel = false;
				} else {
					$texts[5].= " OR ";
				}
				$texts[5].= "te_id=$id";
			}
		}
		$opResult = "-";
		$response['d'].= sprintf($string, $opNum, $opName, $opResult);

		//
		// Generación query final
		//

		// Revisión de no estar vació
		$subQuery = "";
		if ( !empty($texts[0]) ) { $subQuery.= "INSERT INTO sa_autonav_link_telem (antl_id, te_id) VALUES\n".$texts[0].";\n"; }
		if ( !empty($texts[1]) ) { $subQuery.= "DELETE FROM sa_autonav_link_telem WHERE ".$texts[1].";\n"; }
		if ( !empty($texts[2]) ) { $subQuery.= "INSERT INTO sa_dintab_link_telem (dttl_id, te_id) VALUES\n".$texts[2].";\n"; }
		if ( !empty($texts[3]) ) { $subQuery.= "DELETE FROM sa_dintab_link_telem WHERE ".$texts[3].";\n"; }
		if ( !empty($texts[4]) ) { $subQuery.= "INSERT INTO sa_form_link_telem (tf_id, te_id) VALUES\n".$texts[4].";\n"; }
		if ( !empty($texts[5]) ) { $subQuery.= "DELETE FROM sa_form_link_telem WHERE ".$texts[5].";\n"; }

		// Actualizar elementos
		$opNum = 15;
		$opName = "Query de actualización";
		$debug = "<br />\n<br />\n$subQuery<br />\n<br />\n";
		if ( !empty($subQuery) && \snkeng\core\engine\mysql::submitMultiQuery($subQuery) )
		{
			$opResult = "OK";
		} else {
			if ( !\snkeng\core\engine\mysql::isError )
			{
				$opResult = "EMPTY";
			} else {
				$opResult = "ERROR";
				\snkeng\core\engine\mysql::killIfError("No se realizaron operaciones de actualización en engine.", "full");
			}
		}
		$response['d'].= sprintf($string, $opNum, $opName, $opResult);
		*/

$response['d'].=  <<<HTML
<tr><td colspan="3">Resultado</td></tr>
<tr><td colspan="3"><div class="pre">{$upd_qry}</div></td></tr>
</tbody>
</table>\n
HTML;
	}
	// END: updateOps
	//

	// }
}
