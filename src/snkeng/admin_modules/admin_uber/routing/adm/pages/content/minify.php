<?php
// Pre-check
\snkeng\core\engine\nav::cacheCheckFile(__FILE__);
\snkeng\core\engine\nav::cacheFinalCheck();

//
$page['head']['title'] = "Minificar";

//
\snkeng\core\engine\nav::pageFileModuleAdd('admin_modules', 'admin_uber', '/pages/uber-adm-pages-content-minify.mjs');

//
$page['body'] .= <<<HTML
<div class="pageSubTitle">Minificar</div>

<uber-adm-pages-content-minify class="grid" data-ajaxurl="{$params['page']['ajax']}">
	<div class="gr_sz02">
		<button data-action="minify" class="btn blue wide"><svg class="icon inline"><use xlink:href="#fa-file-zip-o"/></svg> Minificar</button>
		<label><input id="env_minify_purge" type="checkbox" value="1" /><span>Reconstruir</span></label>
	</div>
	<div class="gr_sz10">
		<div id="env_minify" class="container_simple"></div>
	</div>
</uber-adm-pages-content-minify>
HTML;
//
