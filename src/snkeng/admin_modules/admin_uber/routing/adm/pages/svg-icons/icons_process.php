<?php
// Pre-check
\snkeng\core\engine\nav::cacheCheckFile(__FILE__);
\snkeng\core\engine\nav::cacheFinalCheck();

//
$page['head']['title'] = "Ícono procesar";

//
\snkeng\core\engine\nav::pageFileModuleAdd('admin_modules', 'admin_uber', '/pages/uber-adm-pages-content-icons-process.mjs');

//
$page['body'] .= <<<HTML
<div class="pageSubTitle">Iconos procesar</div>

<uber-adm-pages-content-icons-process class="grid" data-ajaxurl="{$params['page']['ajax']}">
	<div class="gr_sz02 verticalListObjects">
		<button data-action="faProc" class="btn blue wide"><svg class="icon inline"><use xlink:href="#fa-fort-awesome"/></svg> FA</button>
		<button data-action="svgMerge" class="btn blue wide"><svg class="icon inline"><use xlink:href="#fa-file"/></svg> SVG</button>
	</div>
	<div class="gr_sz10">
		<div id="env_minify" class="container_simple"></div>
	</div>
</uber-adm-pages-content-icons-process>
HTML;
//
