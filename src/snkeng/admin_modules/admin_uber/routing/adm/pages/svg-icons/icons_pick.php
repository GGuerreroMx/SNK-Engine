<?php

// ADD JS module
\snkeng\core\engine\nav::pageFileModuleAdd('admin_modules', 'admin_uber', '/pages/uber-adm-pages-content-icons-pick.css');
\snkeng\core\engine\nav::pageFileModuleAdd('admin_modules', 'admin_uber', '/pages/uber-adm-pages-content-icons-pick.mjs');

$svg_icons_folders = $_SERVER['DOCUMENT_ROOT'] . '/snkeng/core/res/se_icons/';


//
$page['head']['title'] = "Ícono elegir";

//
$jsonFile = $_SERVER['DOCUMENT_ROOT'] . '/snkeng/core/res/se_icons/se-icons.json';
// Get ICONS
$iconData = json_decode(file_get_contents($jsonFile), true);
// Saved icons
$saveFile = $_SERVER['DOCUMENT_ROOT'] . '/se_files/site_generated/svg-icons/final/fe-icons.txt';

// Check if exists
if ( file_exists($saveFile) ) {
	$saveData = explode(',', file_get_contents($saveFile));

	//
	foreach ( $saveData as $iconID ) {
		if ( !empty($iconData[$iconID]) ) {
			$iconData[$iconID]['sel'] = " sel";
		}
	}

}

$iconTemplate = <<<HTML
<div class="iconSelector">
<svg xmlns="http://www.w3.org/2000/svg" version="1.1" viewBox="" ></svg>
<div class="content"></div>
</div>\n
HTML;

//
$json_list = '';
//
foreach ( $iconData as $idName => $data ) {
	//
	$data['sel'] = ( isset($data['sel']) ) ? 'true' : 'false';

	//
	$json_list.= <<<HTML
<div class="iconSelector" aria-selected="{$data['sel']}" data-id="{$idName}">
<svg xmlns="http://www.w3.org/2000/svg" version="1.1" viewBox="{$data['vb'][0]},{$data['vb'][1]},{$data['vb'][2]},{$data['vb'][3]}">{$data['svg']}</svg>
<div class="content">{$idName}</div>
</div>\n
HTML;
	//
}

//
$page['body'].= <<<HTML
<div class="pageSubTitle">Iconos selección</div>

<uber-adm-pages-content-icons-pick class="grid" data-apiurl="{$params['page']['ajax']}">

	<div class="gr_sz10">
		<div id="iconList" data-status="0">{$json_list}</div>
	</div>

	<div class="gr_sz02">
		<button class="btn blue wide" data-iconpick-action="upload"><svg class="icon inline"><use xlink:href="#fa-file"/></svg> Seleccionar</button>
		<output data-type="upload"></output>
		
		<h3>Mostrar</h3>
		<div class="verticalListObjects">
			<button class="btn blue wide" data-iconpick-action="filter" data-status="0">Todos</button>
			<button class="btn blue wide" data-iconpick-action="filter" data-status="1">Seleccionados</button>
			<button class="btn blue wide" data-iconpick-action="filter" data-status="2">No seleccionados</button>
		</div>
	</div>

</uber-adm-pages-content-icons-pick>
HTML;
//
