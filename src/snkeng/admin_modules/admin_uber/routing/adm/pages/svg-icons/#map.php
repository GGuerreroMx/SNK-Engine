<?php
//
$params['page']['main'].= '/svg-icons';
$params['page']['ajax'].= '/svg-icons';

// Root
\snkeng\core\admin\navBuild::navBreadcrumbAdd($params['page']['main'], 'Contenido');
\snkeng\core\admin\navBuild::navLevelSet([
	[$params['page']['main'].'/icons_add', 'Iconos Agregar'],
	[$params['page']['main'].'/icons_process', 'Iconos Process'],
	[$params['page']['main'].'/icons_pick', 'Iconos Seleccionar'],
	[$params['page']['main'].'/minify', 'Minify'],
]);

//
switch ( \snkeng\core\engine\nav::next() ) {
	//
	case 'groups':
		//
		$params['page']['main'].= '/' . \snkeng\core\engine\nav::current();
		$params['page']['ajax'].= '/' . \snkeng\core\engine\nav::current();

 		// Check if it has a file to check or show all
		if ( \snkeng\core\engine\nav::next(false) ) {
			require __DIR__ . '/groups_[name]_pick.php';
		} else {
			require __DIR__ . '/groups.php';
		}
		break;

	//
	case 'group':
		$params['page']['main'].= '/' . \snkeng\core\engine\nav::current();
		$params['page']['ajax'].= '/' . \snkeng\core\engine\nav::current();

		// Check if file exists


		require __DIR__ . '/group_edit.php';
		break;

	//
	case '':
	case null:
		require __DIR__ . '/#index.php';
		break;
	//
	default:
		\snkeng\core\engine\nav::invalidPage();
		break;
}

// Nav get
$nav = \snkeng\core\admin\navBuild::navCreate();

// Estructura
$page['body'] = <<<HTML
<div class="pageTitle">SVG Icons</div>

{$nav}

{$page['body']}\n
HTML;
//
