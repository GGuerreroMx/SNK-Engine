<?php
// Pre-check
\snkeng\core\engine\nav::cacheCheckFile(__FILE__);
\snkeng\core\engine\nav::cacheFinalCheck();

//
$page['head']['title'] = "Ícono agregar";

//
\snkeng\core\engine\nav::pageFileModuleAdd('admin_modules', 'admin_uber', '/pages/uber-adm-pages-content-icons-add.mjs');

//
$page['body'] = <<<HTML
<style>
#testIcon { width:400px; height:400px; background-color:#EEE; }
</style>

<div class="pageSubTitle">Iconos Agregar</div>

<uber-adm-pages-content-icons-add class="grid">
	<div class="gr_sz03">
		<form id="svgAdder" class="tabLessForm" method="post" action="{$params['page']['ajax']}/icons/add" is="se-async-form">
			<input type="text" class="elem" name="name" placeholder="Nombre" title="Nombre" required />
			<table class="elem grid">
				<tr><td>XI</td><td>YI</td><td>XP</td><td>YP</td></tr>
				<tr>
					<td><input type="number" name="xi" value="0" placeholder="XI" title="XI" required /></td>
					<td><input type="number" name="yi" value="0" placeholder="YI" title="YI" required /></td>
					<td><input type="number" name="xp" value="1000" placeholder="XP" title="XP" required /></td>
					<td><input type="number" name="yp" value="1000" placeholder="YP" title="YP" required /></td>
				</tr>
			</table>
			<textarea class="elem" name="svg" required></textarea>
			<div class="elem radio">
				<label><input type="radio" name="saveTo" required value="gen" /> General</label>
				<label><input type="radio" name="saveTo" required value="page" /> Página</label>
			</div>
			<button class="elem btn blue wide" type="submit"><svg class="icon inline mr"><use xlink:href="#fa-plus"/></svg>Agregar</button>
			<div class="elem" se-elem="response"></div>
		</form>
	</div>

	<div class="gr_sz09">
		<svg id="testIcon"></svg>
	</div>
</uber-adm-pages-content-icons-add>
HTML;
//
