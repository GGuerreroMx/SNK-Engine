<?php
$groupFileName = \snkeng\core\engine\nav::next();

// Check if file exists
$realFileName = $_SERVER['DOCUMENT_ROOT'] . "/se_files/site_generated/svg-icons/groups/{$groupFileName}.json";

if ( !file_exists($realFileName) ) {
	\snkeng\core\engine\nav::killWithError("file does not exists '{$groupFileName}'.");
}


//
$params['page']['ajax'] .= "/single";

// ADD JS module
\snkeng\core\engine\nav::pageFileModuleAdd('admin_modules', 'admin_uber', '/pages/uber-adm-pages-svg-icons-group-pick.css');
\snkeng\core\engine\nav::pageFileModuleAdd('admin_modules', 'admin_uber', '/pages/uber-adm-pages-svg-icons-group-pick.mjs');

//
$availablePacks = glob($_SERVER['DOCUMENT_ROOT'] . '/se_files/site_generated/svg-icons/libraries/*.svg');

// Get libraries
$libraryList = '';
foreach ( $availablePacks as $fileLocation ) {
	$cFileName = pathinfo($fileLocation)['filename'];
	//
	$libraryList .= <<<HTML
<option>{$cFileName}</option>
HTML;
}

if ( empty($libraryList) ) {
	\snkeng\core\engine\nav::killWithError("No available libraries?", "Location should be: /se_files/site_generated/svg-icons/libraries/");
}

// Get selected item list

$currentSelectedItems = file_get_contents($realFileName);

//
$page['head']['title'] = "SVG Pick for {$groupFileName}";

//
$page['body'] .= <<<HTML
<div class="pageSubTitle">Pick icons for "{$groupFileName}"</div>

<div>First select the libraries, then the icons.</div>

<uber-adm-pages-content-icons-pick data-apiurl="{$params['page']['ajax']}" data-groupname="{$groupFileName}">
	<script type="application/json" data-element="fileData">{$currentSelectedItems}</script>
	<div class="svgContainer"></div>

	<div class="grid">
		<div class="gr_sz10">
			
			<template>
				<div class="iconSelector" aria-selected="!isSelected;" data-id="!symbolId;">
					<svg xmlns="http://www.w3.org/2000/svg" version="1.1" viewBox="!symbolViewBox;" >!symbolContents;</svg>
					<div class="content">!symbolId;</div>
				</div>
			</template>
			<div id="iconList" data-status="0"></div>
		</div>
	
		<div class="gr_sz02">
			<button class="btn blue wide" data-iconpick-action="upload"><svg class="icon inline"><use xlink:href="#fa-save"/></svg> Save</button>
			<output data-type="upload"></output>
			
			<h3>Pick Library</h3>
			<select>
				<option> -- </option>
				{$libraryList}
			</select>
			
			<h3>Mostrar</h3>
			<div class="verticalListObjects">
				<button class="btn blue wide" data-iconpick-action="filter" data-status="0">Todos</button>
				<button class="btn blue wide" data-iconpick-action="filter" data-status="1">Seleccionados</button>
				<button class="btn blue wide" data-iconpick-action="filter" data-status="2">No seleccionados</button>
			</div>
		</div>
	</div>

</uber-adm-pages-content-icons-pick>
HTML;
//
