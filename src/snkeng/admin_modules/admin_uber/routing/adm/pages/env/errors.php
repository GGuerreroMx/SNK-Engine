<?php
// Pre-check
\snkeng\core\engine\nav::cacheCheckFile(__FILE__);
\snkeng\core\engine\nav::cacheFinalCheck();

//
$page['head']['title'] = "SQL Errors";

//
$params['page']['ajax'] = $params['page']['ajax'].'/errors';

// Estructura
$an_struct = <<<HTML
<tr data-element="row" data-dyntab-onclick="element_op" data-action="readSingle" data-objid="!id;" data-objtitle="!title;">
	<td>!id;</td>
	<td>!date;</td>
	<td>!type;</td>
	<td>!code;</td>
	<td>
		<button class="btn small" data-dyntab-onclick="element_op" data-action="del" title="Borrar"><svg class="icon inline"><use xlink:href="#fa-trash-o" /></svg></button>
	</td>
</tr>
HTML;
//
$exData = [
	'js_url' => $params['page']['ajax'].'/read_all',
	
	'printType' => 'table',
	'tableWide' => true,
	'tableHead' => [
		['name' => 'ID'],
		['name' => 'Fecha'],
		['name' => 'Tipo'],
		['name' => 'Clave'],
		['name' => 'Acciones'],
	],
	'settings' => ['nav' => true],
	'actions' => <<<JSON
{
	"deleteAll":
	{
		"action":"function",
		"ajax-action":"none",
		"menu":{"title":"Borrar todos", "icon":"fa-trash-o"},
		"ajax-elem":"none",
		"function":async function(context, contentElement) {
			try {
				//
				const response = await fetch(
					"{$params["page"]["ajax"]}/del_all",
					{
						method:'DELETE'
					}
				);

				//
				if ( !response.ok ) { return; }
				
				contentElement.innerHTML = '';
			} catch (e) {
				console.error("error", e);
			}
		}
	},
	"readSingle":
	{
		"action":"function",
		"ajax-action":"none",
		"ajax-elem":"obj",
		"function": async function(cRow, cObj, defData, cRowId, cRowTitle) {
			//
			try {
				const cTarget = document.getElementById('env_error');
				//
				const response = await fetch("{$params["page"]["ajax"]}/read_single?eId=" + cRowId);
				if ( !response.ok ) {
					//
					cTarget.innerText("no fue posible cargar la información.");
					return;
				}

				//
				const result = await response.json();
				//
				cTarget.innerText = result.d;
			} catch (e) {
				console.error("error", e);
				cTarget.innerText = "no fue posible cargar la 2.";
			}
		}
	},
	"del":{"action":"del", "ajax-elem":"obj", "del_url":"{$params["page"]["ajax"]}/del_single"}
}
JSON
];
// debugVariable($an_qry, '', true);
$tableStructure = \snkeng\core\general\dynamicTable::createElement($an_struct, $exData);

// Page
$page['body'].= <<<HTML
<div class="pageSubTitle">Errores</div>
<div class="grid">
<div class="gr_sz04">{$tableStructure}</div>
<div class="gr_sz08">
	<h4>Información</h4>
	<div id="env_error" class="container_simple"></div>
</div>
</div>
HTML;
//