<?php
// Pre-check
\snkeng\core\engine\nav::cacheCheckFile(__FILE__);
\snkeng\core\engine\nav::cacheFinalCheck();

//
$page['head']['title'] = "Env Variables";

// Estructura
$an_struct = <<<HTML
<form data-element="row" data-action="upd" data-objid="!id;" data-objtitle="!title;">
	<span>!id;</span>
	<span><input type="number" name="type" value="!type;"/></span>
	<span><input type="text" name="title" value="!title;" pattern="[A-z0-9]{3,}" /></span>
	<span><input type="text" name="value" value="!value;"/></span>
	<span class="actions">
		<button class="btn small" type="submit" title="Guardar"><svg class="icon inline"><use xlink:href="#fa-save" /></svg></button>
		<button class="btn small" data-dyntab-onclick="element_op" data-action="del" type="button" title="Borrar"><svg class="icon inline"><use xlink:href="#fa-trash-o" /></svg></button>
	</span>
</form>
HTML;
//
$exData = [
	'js_id' => 'EnvVars',
	'js_url' => $params['page']['ajax'].'/envvar/',
	'tableWide' => true,
	'printType' => 'tableMod',
	'tableHead' => [
		['name' => 'ID'],
		['name' => 'Tipo'],
		['name' => 'Nombre'],
		['name' => 'Valor'],
		['name' => 'Acciones'],
	],
	'settings' => ['nav' => true],
	'actions' => <<<JSON
{
	"add":
	{
		"ajax-action":"add",
		"ajax-elem":"none",
		"menu":{"title":"Agregar", "icon":"fa-plus"},
		"print":false,
		"action":"form",
		"form":{
			"title":"Nuevo",
			"save_url":"{$params["page"]["ajax"]}/envvar/",
			"actName":"Agregar",
			"elems": {
				"title": {
					"field_type":"input", "data_type":"text", "required":true,
					"info_name":"Nombre", "info_desc":"",
					"properties": {
						"type": "text",
						"minlength":0,
						"maxlength":255
					}
				},
				"value": {
					"field_type":"input", "data_type":"text", "required":true,
					"info_name":"Valor", "info_desc":"",
					"properties": {
						"type": "text",
						"minlength":0,
						"maxlength":255
					}
				},
				"type": {
					"field_type":"input", "data_type":"number", "required":true,
					"info_name":"Tipo", "info_desc":"",
					"properties": {
						"default_value": 0,
						"type": "number",
						"min": 1,
						"max": 10
					}
				}
			}
		}
	},
	"upd":
	{
		"action":"direct",
		"ajax-action":"upd",
		"save_url":"{$params["page"]["ajax"]}/envvar/{id}"
	},
	"del":{"action":"del", "ajax-elem":"obj", "del_url":"{$params["page"]["ajax"]}/envvar/{id}"}
}
JSON
];
// debugVariable($an_qry, '', true);
$tableStructure = \snkeng\core\general\dynamicTable::createElement($an_struct, $exData);

// Page
$page['body'].= <<<HTML
<div class="pageSubTitle">Env var</div>

{$tableStructure}
HTML;
//
