<?php
//
$params['page']['main'].= '/' . \snkeng\core\engine\nav::current();
$params['page']['ajax'].= '/' . \snkeng\core\engine\nav::current();

// Root
\snkeng\core\admin\navBuild::navBreadcrumbAdd($params['page']['main'], 'Env');
\snkeng\core\admin\navBuild::navLevelSet([
	[$params['page']['main'].'/envvar', 'Variables'],
	[$params['page']['main'].'/errors', 'Errores'],
	[$params['page']['main'].'/apache_errors', 'Apache errores'],
]);

//
switch ( \snkeng\core\engine\nav::next() ) {
	//
	case 'envvar':
		\snkeng\core\engine\nav::navPathAdd(['main', 'ajax']);
		require __DIR__ . '/envvar.php';
		break;

	//
	case 'errors':
		require __DIR__ . '/errors.php';
		break;

	//
	case 'apache_errors':
		require __DIR__ . '/apache_errors.php';
		break;

	//
	case '':
	case null:
		require __DIR__ . '/#index.php';
		break;

	//
	default:
		\snkeng\core\engine\nav::invalidPage();
		break;
}

// Nav get
$nav = \snkeng\core\admin\navBuild::navCreate();

// Estructura
$page['body'] = <<<HTML
<div class="pageTitle">Environment</div>

{$nav}

{$page['body']}\n
HTML;
//
