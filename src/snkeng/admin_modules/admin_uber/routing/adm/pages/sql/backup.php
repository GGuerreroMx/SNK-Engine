<?php
// Pre-check
\snkeng\core\engine\nav::cacheCheckFile(__FILE__);
\snkeng\core\engine\nav::cacheFinalCheck();

//
$page['head']['title'] = "Backup";

//
\snkeng\core\engine\nav::pageFileModuleAdd('admin_modules', 'admin_uber', '/pages/uber-adm-pages-sql-backup.mjs');

//
$page['body'].= <<<HTML
<div class="pageSubTitle">Backup</div>

<uber-adm-pages-sql-backup class="grid">
	<div class="gr_sz03 gr_ps03">
		<h3>Crear</h3>
		<div class="verticalListObjects">
			<a class="btn blue wide" href="{$params['page']['ajax']}/backup/download_all"><svg class="icon inline"><use xlink:href="#fa-download"/></svg> Todo</a>
			<a class="btn blue wide" href="{$params['page']['ajax']}/backup/download_struct"><svg class="icon inline"><use xlink:href="#fa-download"/></svg> Estructura</a>
		</div>
	</div>
	<div class="gr_sz03">
		<h3>Subir</h3>
		<form class="verticalListObjects" method="post" action="{$params['page']['ajax']}/backup/upload" is="se-async-form">
			<input type="file" name="sql_file" accept=".txt,.sql" />
			<button type="submit" class="btn blue wide"><svg class="icon inline"><use xlink:href="#fa-upload"/></svg>Subir</button>
			<output></output>
		</form>
	</div>
</uber-adm-pages-sql-backup>
HTML;
//
