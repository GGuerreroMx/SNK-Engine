<?php
// Pre-check
\snkeng\core\engine\nav::cacheCheckFile(__FILE__);
\snkeng\core\engine\nav::cacheFinalCheck();

//
$page['head']['title'] = "SQL - Direct Query";

//
\snkeng\core\engine\nav::pageFileModuleAdd('admin_modules', 'admin_uber', '/pages/uber-adm-pages-sql-direct-query.mjs');
\snkeng\core\engine\nav::pageFileModuleAdd('admin_modules', 'admin_uber', '/pages/uber-adm-pages-sql-direct-query.css');

//
$page['body'].= <<<HTML
<div class="pageSubTitle">Query</div>

<uber-adm-pages-sql-direct-query>

	<form method="post" action="{$params['page']['ajax']}/direct" is="se-async-form">
		<textarea name="query" rows="15" class="codeEditor"></textarea>
		<button type="submit" class="btn blue wide">Ejecutar</button>
		<output></output>
		<h3>Resultado</h3>
		<output se-elem="query"></output>
	</form>

</uber-adm-pages-sql-direct-query>
HTML;
//
