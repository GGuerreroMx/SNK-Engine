<?php
// Pre-check
\snkeng\core\engine\nav::cacheCheckFile(__FILE__);
\snkeng\core\engine\nav::cacheFinalCheck();

//
$page['head']['title'] = "SQL - Estructura";

//
\snkeng\core\engine\nav::pageFileModuleAdd('admin_modules', 'admin_uber', '/pages/uber-adm-pages-sql-structure.mjs');

//
$page['body'].= <<<HTML
<div class="pageSubTitle">Estructura</div>
<uber-adm-pages-sql-structure class="grid">
	<div class="gr_sz03">
		<div>
			<h3>Crear</h3>
			<a class="btn blue wide" href="{$params['page']['ajax']}/structure/download"><svg class="icon inline"><use xlink:href="#fa-download"/></svg> Descargar</a>
		</div>
		<div>
			<h3>Subir</h3>
			<form class="verticalListObjects" method="post" action="{$params['page']['ajax']}/structure/upload" is="se-async-form">
				<input type="file" name="sql_file" accept=".txt" />
				<button type="submit" class="btn blue wide"><svg class="icon inline"><use xlink:href="#fa-upload"/></svg>Subir</button>
			</form>
		</div>
	</div>
	<div class="gr_sz09">
		<output style="  white-space: pre-wrap; word-wrap: break-word;" se-elem="response"></output>
	</div>
</uber-adm-pages-sql-structure>
HTML;
//
