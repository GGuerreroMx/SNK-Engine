<?php
// Pre-check
\snkeng\core\engine\nav::cacheCheckFile(__FILE__);
\snkeng\core\engine\nav::cacheFinalCheck();

//
$page['head']['title'] = "Scripts";

//
\snkeng\core\engine\nav::pageFileModuleAdd('admin_modules', 'admin_uber', '/pages/uber-adm-pages-content-icons-add.mjs');


//
$folder = $_SERVER['DOCUMENT_ROOT'] . '/zz_patches';
$files = '';
if ( is_dir($folder) ) {
	foreach ( glob($folder."/*.php") as $filename ) {
		$title = basename($filename, '.php');
		$files.= "<option value='{$title}'>{$title}</option>";
	}
}

// Page
$page['body'].= <<<HTML
<style>
#sadm_patch_result { font-family:Consolas,monospace; font-size:1.2rem; white-space:pre-line; }
</style>

<div class="pageSubTitle">Smart Patches</div>

<div class="grid">
	<div class="gr_sz03">
	<form id="sadm_patch_form" enctype="multipart/form-data" action="{$params['page']['ajax']}/patches/exec" method="post" class="se_form">
		<div class="separator required"><label>
			<div class="cont"><span class="title">Patch</span><span class="desc"></span></div>
			<select name="file"><option value=""> - - </option>{$files}</select>
		</label></div>
		<div class="separator required"><label><input type="checkbox" value="1" name="exec">Ejectuar</label></div>
		<button type="submit">Process</button><span se-elem="response"></span></form>
	</div>
	<div class="gr_sz09">
		<div id="sadm_patch_result"></div>
	</div>
</div>
HTML;
//


$page['mt'].= <<<JS
$('#sadm_patch_form').se_plugin('simpleForm', {
	onRequest:function() {
		$('#sadm_patch_result').se_empty();
	},
	onSuccess:function(msg) {
		$('#sadm_patch_result').se_text(msg.d);
	}
});
JS;
