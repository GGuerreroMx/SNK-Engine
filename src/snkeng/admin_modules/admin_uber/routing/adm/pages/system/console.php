<?php
// Pre-check
\snkeng\core\engine\nav::cacheCheckFile(__FILE__);
\snkeng\core\engine\nav::cacheFinalCheck();

//
$page['head']['title'] = "Consola";

//
\snkeng\core\engine\nav::pageFileModuleAdd('admin_modules', 'admin_uber', '/pages/uber-adm-pages-system-console.mjs');
\snkeng\core\engine\nav::pageFileModuleAdd('admin_modules', 'admin_uber', '/pages/uber-adm-pages-system-console.css');

//
$params['page']['ajax'].= '/console';

//
$page['body'] = <<<HTML
<div class="pageTitle">Consola</div>

<uber-adm-pages-system-console class="grid">
	<div class="gr_sz06 gr_ps03">
		<div id="adm_console">
			<div class="content"></div>
			<form id="adm_console_form" method="post" action="{$params['page']['ajax']}" is="se-async-form">
				<input type="text" name="command" />
			</form>
		</div>
	</div>
</uber-adm-pages-system-console>
HTML;
//