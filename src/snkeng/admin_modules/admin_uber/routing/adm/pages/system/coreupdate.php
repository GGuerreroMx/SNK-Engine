<?php
// Pre-check
\snkeng\core\engine\nav::cacheCheckFile(__FILE__);
\snkeng\core\engine\nav::cacheFinalCheck();

//
$page['head']['title'] = "Actualización del core";

//
\snkeng\core\engine\nav::pageFileModuleAdd('admin_modules', 'admin_uber', '/pages/uber-adm-pages-content-icons-add.mjs');

//
$page['body'].= <<<HTML
<div class="pageSubTitle">Core update todo - remove?</div>

<div class="grid" se-plugin="uber_admin_adm_system_page_core_file_update" data-url="{$params['page']['ajax']}">
	<div class="gr_sz04 gr_ps02">
		<h3>Subir</h3>
		<div>Seleccionar el archivo con los elementos.</div>
		<form class="se_form" se-plugin="simpleForm" action="{$params['page']['ajax']}/coreupdate">
			<label class="separator required">
				<div class="cont"><span class="title">Archivo</span><span class="desc"></span></div>
				<input type="file" name="file" accept="application/zip" />
			</label>
			<button type="submit" class="btn blue wide"><svg class="icon inline mr"><use xlink:href="#fa-upload"/></svg>Subir</button>
			<output se-elem="response"></output>
		</form>
	</div>
	<div class="gr_sz04">
		<h3>Descargar (BETA)</h3>
		<div>
			<button class="btn blue wide" se-act="download"><svg class="icon inline mr"><use xlink:href="#fa-download" /></svg>Descargar</button>
			<output se-elem="download"></output>
		</div>
	</div>
</div>
HTML;
//
