<?php

//
$params['page']['nav'] = '';


if ( !is_numeric(\snkeng\core\engine\nav::next(false)) ) {
	switch (\snkeng\core\engine\nav::next()) {
		case null:
			require __DIR__ . '/#index.php';
			break;
	}
}
else {
	//
	$params['vars']['tId'] = intval(\snkeng\core\engine\nav::next());
	\snkeng\core\engine\nav::navPathAdd(['main', 'ajax']);

	//
	$urlGet = \snkeng\core\engine\nav::navPathGet('main');

	//
	$params['page']['nav'] = <<<HTML
<div class="nav4">
	<div class="normal">
		<a se-nav="app_content" href="{$urlGet}/columns-basic/">Columns General</a>
		<a se-nav="app_content" href="{$urlGet}/columns-properties/">Columns Properties</a>
		<a se-nav="app_content" href="{$urlGet}/columns-sql/">Columns SQL</a>
		<a se-nav="app_content" href="{$urlGet}/indexes/">Indexes</a>
		<a se-nav="app_content" href="{$urlGet}/foreign-keys">Foreign Keys</a>
		<a se-nav="app_content" href="{$urlGet}/relationship/">Relaciones</a>
	</div>
	<div class="normal">
		<a se-nav="app_content" href="{$urlGet}/queries/">Queries</a>
		<a se-nav="app_content" href="{$urlGet}/content/">Contenido</a>
		<a se-nav="app_content" href="{$urlGet}/update/">Actualizar</a>
	</div>
</div>
HTML;
	//

	//
	switch ( \snkeng\core\engine\nav::next() ) {
		//
		case 'columns-basic':
			\snkeng\core\engine\nav::navPathAdd('ajax', 'columns');

			require __DIR__ . '/[id]_columns-basic.php';
			break;
		//
		case 'columns-properties':
			\snkeng\core\engine\nav::navPathAdd('ajax', 'columns');

			require __DIR__ . '/[id]_columns-properties.php';
			break;
		//
		case 'columns-sql':
			\snkeng\core\engine\nav::navPathAdd('ajax', 'columns');

			require __DIR__ . '/[id]_columns-sql.php';
			break;
		//
		case 'indexes':
			// Indexes
			\snkeng\core\engine\nav::navPathAdd(['main', 'ajax']);

			//
			require __DIR__ . '/[id]_indexes.php';
			break;
		//
		case 'foreign-keys':
			// Foreign keys
			\snkeng\core\engine\nav::navPathAdd(['main', 'ajax']);

			//
			require __DIR__ . '/[id]_foreign-keys.php';
			break;
		//
		case 'queries':
			require __DIR__ . '/[id]_queries.php';
			break;
		//
		case 'content':
			require __DIR__ . '/[id]_content.php';
			break;
		//
		case 'update':
			require __DIR__ . '/[id]_update.php';
			break;
		//
		case '':
		case null:
			require __DIR__ . '/[id]_#index.php';
			break;
		//
		default:
			\snkeng\core\engine\nav::invalidPage();
			break;
	}
	//
}

//
$page['body'] = <<<HTML
<div class="pageTitle">Tablas</div>

{$params['page']['nav']}

{$page['body']}\n
HTML;
//
