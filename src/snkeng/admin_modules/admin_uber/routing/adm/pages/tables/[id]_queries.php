<?php
// Pre-check
\snkeng\core\engine\nav::cacheCheckFile(__FILE__);
\snkeng\core\engine\nav::cacheFinalCheck();

// Información de la tabla
$sql_qry = "SELECT t_dbname, t_dbnick FROM sa_table_object WHERE t_id={$params['vars']['tId']};";
$tableData = \snkeng\core\engine\mysql::singleRowAssoc($sql_qry);

// Procesar elementos
$sql_qry = <<<SQL
SELECT
te_id, t_id, te_dt_add, te_dt_mod,
te_name, te_desc, te_pos,
te_db_name, te_db_type, te_db_typedesc, te_db_notnull, te_db_unsigned, te_db_autoinc, te_db_prikey, te_db_default, te_db_fulltext,
te_d_nick, te_d_optional,
te_d_special, te_d_parenttable,
te_f_dtype, te_f_minl, te_f_maxl, te_f_maxli
FROM sa_table_column
WHERE t_id={$params['vars']['tId']}
ORDER BY te_pos ASC, te_id ASC;
SQL;

// Hacer el query
if ( \snkeng\core\engine\mysql::execQuery($sql_qry) ) {
	$qry_select_aux1 = "";
	$qry_select_aux2 = "";
	$qry_insert_aux1 = "";
	$qry_insert_aux2 = "";
	$qry_update_aux1 = "";
	$qry_update_aux2 = "";
	$first = true;
	$predictedPos = 0;
	//
	$tableEls = "";
	$tableId = '';
	$tableDtAdd = '';
	$tableDtMod = '';
	$eng_post_1 = '';
	$eng_post_2 = '';

	$eng_js_1 = '';
	$eng_js_2 = '';

	//
	$eng_query_where_array = '';
	$eng_query_order_array = '';

	//
	while ( $datos = \snkeng\core\engine\mysql::$result->fetch_assoc() ) {
		//
		$eng_post_2_validation = [];
		$eng_js_2_validation = [];
		$validationText = '';

		//
		$isRequired = ($datos['te_db_notnull']) ? 'true' : 'false';

		//
		$eng_query_where_stype = 'str';
		// Operaciones especiales
		if ( $datos['te_d_special'] === '2' ) {
			$qry_select_aux2 = "{$tableData['t_dbnick']}.{$datos['te_db_name']}='val_{$datos['te_db_name']}'";
			$qry_update_aux2 = "{$datos['te_db_name']}='val_{$datos['te_db_name']}'";
		}

		// Operaciones Generales
		$curPos = intval($datos['te_pos']);
		if ( $first ) {
			$predictedPos = $curPos + 1;
		} else {
			$qry_select_aux1 .= ',';
			$qry_insert_aux1 .= ',';
			$qry_insert_aux2 .= ',';
			$qry_update_aux1 .= ',';
			//
			$eng_query_where_array .= ",\n\t\t";
			$eng_query_order_array .= ",\n\t\t";
			//
			if ( $predictedPos === $curPos ) {
				$qry_select_aux1 .= ' ';
				$qry_insert_aux1 .= ' ';
				$qry_insert_aux2 .= ' ';
				$qry_update_aux1 .= ' ';
				$predictedPos++;
			} else {
				$qry_select_aux1 .= "\n\t\t";
				$qry_insert_aux1 .= "\n\t";
				$qry_insert_aux2 .= "\n\t";
				$qry_update_aux1 .= "\n\t";
				$predictedPos = $curPos + 1;
			}
		}
		$qry_select_aux1 .= "{$tableData['t_dbnick']}.{$datos['te_db_name']}";
		//
		if ( !empty($datos['te_d_nick']) ) {
			$qry_select_aux1 .= " AS {$datos['te_d_nick']}";
		}
		$qry_insert_aux1 .= "{$datos['te_db_name']}";
		$qry_insert_aux2 .= "'{\$data_array['{$datos['te_d_nick']}']}'";
		$qry_update_aux1 .= "{$datos['te_db_name']}='{\$data_array['{$datos['te_d_nick']}']}'";


		$skipAsElements = false;
		$dataType = 'str';
		$engPostProcess = ', \'process\'=>false, \'validate\'=>';
		$hasPostValidate = 'true';
		$inputType = 'ERROR';
		$inputExtra = '';
		$hasMin = '';
		$hasMax = '';
		$hasFilter = '';
		if ( in_array($datos['te_db_type'], ['TINYINT', 'SMALLINT', 'MEDIUMINT', 'INT', 'BIGINT']) ) {
			$dataType = 'int';
			$inputType = 'input';
			$eng_query_where_stype = 'int';

			//
			$minValue = 0;
			$maxValue = 0;

			if ( $datos['te_db_unsigned'] ) {
				switch ( $datos['te_db_type'] ) {
					case 'TINYINT':
						$maxValue = 255;
						break;
					case 'SMALLINT':
						$maxValue = 65535;
						break;
					case 'MEDIUMINT':
						$maxValue = 16777215;
						break;
					case 'INT':
						$maxValue = 4294967295;
						break;
					case 'BIGINT':
						$maxValue = 18446744073709551616;
						break;
				}
			} else {
				switch ( $datos['te_db_type'] ) {
					case 'TINYINT':
						$minValue = -128;
						$maxValue = 127;
						break;
					case 'SMALLINT':
						$minValue = -32768;
						$maxValue = 32767;
						break;
					case 'MEDIUMINT':
						$minValue = -8388608;
						$maxValue = 8388607;
						break;
					case 'INT':
						$minValue = -2147483648;
						$maxValue = 2147483647;
						break;
					case 'BIGINT':
						$minValue = -9223372036854775808;
						$maxValue = 9223372036854775807;
						break;
				}
			}

			//
			$eng_post_2_validation[] = "'min' => {$minValue}";
			$eng_post_2_validation[] = "'max' => {$maxValue}";
			$eng_js_2_validation[] = "\"type\": \"number\"";
			$eng_js_2_validation[] = "\"min\":{$minValue}";
			$eng_js_2_validation[] = "\"max\":{$maxValue}";
		} elseif ( in_array($datos['te_db_type'], ['FLOAT', 'DOUBLE', 'DECIMAL']) ) {
			//
			$dataType = 'float';

			//
			$inputType = 'input';
			$eng_js_2_validation[] = '"type": "number"';
			$eng_js_2_validation[] = '"step" => "0.01"';
		} elseif ( in_array($datos['te_db_type'], ['BOOLEAN']) ) {
			$dataType = 'bool';
			$inputType = 'input';
			$eng_js_2_validation[] = '"type": "checkbox"';
			$eng_js_2_validation[] = '"value": 1';


			$engPostProcess = '';
			$hasPostValidate = '';
		} elseif ( in_array($datos['te_db_type'], ['DATETIME']) ) {
			//
			$dataType = 'datetime';

			//
			$inputType = 'input';
			$eng_js_2_validation[] = '"type": "datetime-local"';

			//
			$eng_query_where_stype = 'date';
		} elseif ( in_array($datos['te_db_type'], ['DATE']) ) {
			//
			$dataType = 'date';

			//
			$inputType = 'input';
			$eng_js_2_validation[] = '"type": "date"';

			//
			$eng_query_where_stype = 'date';
		} elseif ( in_array($datos['te_db_type'], ['JSON']) ) {
			$inputType = 'textarea';
		} elseif ( in_array($datos['te_db_type'], ['TINYTEXT', 'TEXT', 'MEDIUMTEXT', 'LONGTEXT']) ) {
			//
			$minLength = 0;
			$maxLength = 0;

			//
			$dataType = "text";
			$inputType = 'textarea';

			//
			switch ( $datos['te_db_type'] ) {
				case 'TINYTEXT':
					$maxLength = 255;
					break;
				case 'TEXT':
					$maxLength = 32767;
					break;
				case 'MEDIUMTEXT':
					$maxLength = 65535;
					break;
				case 'LONGTEXT':
					$maxLength = 4294967295;
					break;
			}

			//
			$eng_post_2_validation[] = "'minlength' => {$minLength}";
			$eng_post_2_validation[] = "'maxlength' => {$maxLength}";
			$eng_js_2_validation[] = "\"minlength\":{$minLength}";
			$eng_js_2_validation[] = "\"maxlength\":{$maxLength}";
		} elseif ( in_array($datos['te_db_type'], ['VARCHAR']) ) {
			//
			$minLength = 0;
			$maxLength = 255;

			//
			$dataType = "text";
			$inputType = 'input';

			// Min
			if ( $datos['te_f_minl'] !== '0' ) {
				$minLength = intval($datos['te_f_minl']);
			}

			// Max
			if ( $datos['te_f_maxl'] !== '0' ) {
				$maxLength = intval($datos['te_f_maxl']);
			}

			//
			if ( !empty($datos['te_f_dtype']) ) {
				$hasFilter = ", 'filter'=>'{$datos['te_f_dtype']}'";
				$inputExtra .= ", 'regexp':{$datos['te_f_dtype']}";
			}

			// Filter
			switch ($datos['te_f_dtype']) {
				//
				case 'hexcolor':
					$eng_js_2_validation[] = '"type": "color"';
					break;
				//
				case 'email':
					$eng_js_2_validation[] = '"type": "email"';
					break;
				//
				case 'url':
					$eng_js_2_validation[] = '"type": "url"';
					break;
				//
				case 'file_name':
					$eng_js_2_validation[] = '"type": "file"';

					$minLength = null;
					$maxLength = null;
					break;
				//
				case null:
					$eng_js_2_validation[] = '"type": "text"';
					break;
				//
				default:
					$eng_post_2_validation[] = '"filter" => "' . $datos['te_f_dtype'] . '"';
					$eng_js_2_validation[] = '"type": "text"';
					$eng_js_2_validation[] = '"regexp": "' . $datos['te_f_dtype'] . '"';
					break;
			}

			//
			if ( $minLength !== null ) {
				$eng_post_2_validation[] = "\"minlength\" => {$minLength}";
				$eng_js_2_validation[] = "\"minlength\":{$minLength}";
			}
			if ( $maxLength !== null ) {
				$eng_post_2_validation[] = "\"maxlength\" => {$maxLength}";
				$eng_js_2_validation[] = "\"maxlength\":{$maxLength}";
			}
		} else {
			$skipAsElements = true;
		}

		//
		if ( $datos['te_d_special'] === '1' ) {
			$inputType = 'hidden';
		} elseif ( $datos['te_d_special'] === '2' ) {
			$tableId = $datos['te_db_name'];
		} elseif ( in_array($datos['te_d_special'], ['3', '4']) || $datos['te_db_type'] === 'TIMESTAMP' ) {
			$skipAsElements = true;
			switch ( $datos['te_d_special'] ) {
				case '3':
					$tableDtAdd = ",\n'dtAdd' => '{$datos['te_db_name']}'";
					break;
				case '4':
					$tableDtMod = ",\n'dtMod' => '{$datos['te_db_name']}'";
					break;
			}
		} else {
			$tableEls .= "\t'{$datos['te_db_name']}' => '{$datos['te_d_nick']}',\n";
		}


		if ( !$skipAsElements ) {
			// Engine
			$eng_post_1 .= <<<TEXT
'{$datos['te_d_nick']}' => ['name'=>'{$datos['te_name']}', 'type'=>'{$dataType}'{$hasMin}{$hasMax}{$hasFilter}{$engPostProcess}{$hasPostValidate}],\n
TEXT;
			// Engine
			$validationText = implode(",\n\t\t", $eng_post_2_validation);
			$eng_post_2 .= <<<TEXT
'{$datos['te_d_nick']}' => [
	'name' => '{$datos['te_name']}',
	'type' => '{$dataType}',
	'required' => {$isRequired},
	'validation' => [
		{$validationText}
	]
],\n
TEXT;
			$eng_js_1 .= <<<TEXT
\t{"ftype":"{$inputType}", "vname":"{$datos["te_d_nick"]}", "fname":"{$datos["te_name"]}", "fdesc":"", "dtype":"{$dataType}"{$inputExtra}, "required":{$isRequired}},\n
TEXT;
			//
			$validationText = implode(",\n\t\t\t", $eng_js_2_validation);
			$eng_js_2 .= <<<TEXT
	"{$datos["te_d_nick"]}": {
		"field_type":"{$inputType}", "data_type":"{$dataType}"{$inputExtra}, "required":{$isRequired},
		"info_name":"{$datos["te_name"]}", "info_desc":"",
		"attributes": {
			{$validationText}
		}
	},\n
TEXT;
			//
		}

		//
		$eng_query_where_array .= <<<TEXT
'{$datos['te_d_nick']}' => ['name'=>'{$datos['te_name']}', 'db'=>'{$tableData['t_dbnick']}.{$datos['te_db_name']}', 'vtype'=>'{$eng_query_where_stype}', 'stype'=>'eq']
TEXT;
		$eng_query_order_array .= <<<TEXT
'{$datos['te_d_nick']}' => ['name'=>'{$datos['te_name']}', 'db'=>'{$tableData['t_dbnick']}.{$datos['te_db_name']}']
TEXT;

		//
		$first = false;
	}

	//
	$qry_select = "SELECT\n\t{$qry_select_aux1}\nFROM {$tableData['t_dbname']} AS {$tableData['t_dbnick']}\nWHERE {$qry_select_aux2}\nLIMIT 1;";
	$qry_insert = "INSERT INTO {$tableData['t_dbname']} (\n\t{$qry_insert_aux1}\n) VALUES (\n\t{$qry_insert_aux2}\n);";
	$qry_update = "UPDATE {$tableData['t_dbname']} SET\n\t{$qry_update_aux1}\nWHERE {$qry_update_aux2}\nLIMIT 1;";
	// Engine
	$eng_javascript = <<<TEXT
"elems":[
{$eng_js_1}]

"elems":{
{$eng_js_2}}
TEXT;
	//
	$eng_table = <<<TEXT
'elems' => [
{$tableEls}],
'tName' => '{$tableData['t_dbname']}',
'tId' => ['nm' => 'id', 'db' => '{$tableId}']{$tableDtAdd}{$tableDtMod}
TEXT;
	//
	$eng_query = <<<TEXT
'{$tableData['t_dbnick']}' => [
	'sel' => "
		{$qry_select_aux1}
	",
	'from' => '{$tableData['t_dbname']} AS {$tableData['t_dbnick']}',
	'lim' => 20,
	'where' => [
		{$eng_query_where_array}
	],
	'order' => [
		{$eng_query_order_array}
	],
	'default_order' => []
],
TEXT;
	//

} else {
	\snkeng\core\engine\mysql::killIfError('No se pudo obtener los elementos de la tabla.', '');
}

// Imprimir
$page['body'] .= <<<HTML
<div class="pageSubTitle">Tablas</div>

<div class="">
	<div>
	<h3>Queries</h3>
	<h4>Select</h4>
	<pre class="sa_tab_qry">{$qry_select}</pre>
	<h4>Insert</h4>
	<pre class="sa_tab_qry">{$qry_insert}</pre>
	<h4>Update</h4>
	<pre class="sa_tab_qry">{$qry_update}</pre>
	</div>
	<div>
	<h3>Engine</h3>
	<h4>Post parsing</h4>
	<pre class="sa_tab_qry">{$eng_post_1}</pre>
	<h4>Post parsing 2</h4>
	<pre class="sa_tab_qry">{$eng_post_2}</pre>
	<h4>Table data</h4>
	<pre class="sa_tab_qry">{$eng_table}</pre>
	<h4>Query</h4>
	<pre class="sa_tab_qry">{$eng_query}</pre>
	</div>
	<div>
	<h3>Front End</h3>
	<h4>Forms</h4>
	<pre class="sa_tab_qry">{$eng_javascript}</pre>
	</div>
</div>
HTML;
