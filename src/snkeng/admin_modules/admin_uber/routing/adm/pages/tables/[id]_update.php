<?php
// Pre-check
\snkeng\core\engine\nav::cacheCheckFile(__FILE__);
\snkeng\core\engine\nav::cacheFinalCheck();

//
$ajaxUrl = \snkeng\core\engine\nav::navPathGet('ajax');


//
\snkeng\core\engine\nav::pageFileModuleAdd('admin_modules', 'admin_uber', '/pages/uber-adm-pages-tables-update.mjs');

//
$page['head']['title'].= 'Actualizar';

//
$page['body'].= <<<HTML
<div class="pageSubTitle">Actualizar</div>
<uber-adm-pages-tables-update class="grid" data-ajaxurl="{$ajaxUrl}/update/">
	<div class="gr_sz03">
		<h2>Operaciones</h2>
		<button data-action="update" class="btn blue wide">Actualizar</button>
		<output data-element="response"></output>
	</div>
	<div class="gr_sz09">
		<h2>Resultados</h2>
		<div>
			<output data-element="query"></output>
		</div>
	</div>
</uber-adm-pages-tables-update>
HTML;
//
