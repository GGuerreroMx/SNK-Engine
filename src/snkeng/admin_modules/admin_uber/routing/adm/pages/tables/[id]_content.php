<?php
// Pre-check
\snkeng\core\engine\nav::cacheCheckFile(__FILE__);
\snkeng\core\engine\nav::cacheFinalCheck();

//
\snkeng\core\engine\nav::pageFileModuleAdd('admin_modules', 'admin_uber', '/pages/uber-adm-pages-tables-content.css');
\snkeng\core\engine\nav::pageFileModuleAdd('admin_modules', 'admin_uber', '/pages/uber-adm-pages-tables-content.mjs');


$sql_qry = '';

// Información de la tabla
$sql_qry = "SELECT t_dbname, t_dbnick FROM sa_table_object WHERE t_id={$params['vars']['tId']};";
$tableData = \snkeng\core\engine\mysql::singleRowAssoc(
	$sql_qry,
	[],
	[
		'errorKey' => '',
		'errorDesc' => '',
	]
);

//
$tabElemsGeneral = <<<HTML
<tr data-name="!te_db_name;" data-dbtype="!te_db_type;" data-props='{"title":"!te_name;","dbName":"!te_db_name;","special":!te_d_special;,"type":"!te_db_type;","typedesc":"!te_db_typedesc;","notnull":!te_db_notnull;,"unsigned":!te_db_unsigned;}'>
	<td>!te_pos;</td>
	<td>!te_name;</td>
	<td>!te_db_name;</td>
	<td>
		<input type="checkbox" name="select" !checked; !readOnly; />
	</td>
	<td><input name="update" /></td>
	<td><input name="where_value" /></td>
	<td>
		<select name="where_type">
			<option value="default">Default</option>
			<option value="like">Like</option>
			<option value="inInt">IN (int)</option>
		</select>
	</td>
	<td><input name="order_priority" type="number" min="0" max="100" /></td>
	<td>
		<select name="order_type">
			<option value="">--</option>
			<option value="ASC">ASC</option>
			<option value="DESC">DESC</option>
		</select>
	</td>
</tr>\n
HTML;



//
$tabElemsSelect = <<<HTML
<label><input type="checkbox" !checked; !readOnly;
data-props='{"title":"!te_name;","dbName":"!te_db_name;","special":!te_d_special;,"type":"!te_db_type;","typedesc":"!te_db_typedesc;","notnull":!te_db_notnull;,"unsigned":!te_db_unsigned;}'
/>[!te_pos;] !te_name;</label>
HTML;
//
$tabElemsUpdate = <<<HTML
<tr data-name="!te_db_name;">
	<td>!te_pos;</td>
	<td>!te_name;</td>
	<td>!te_db_name;</td>
	<td><input name="value" /></td>
	</tr>
HTML;
//
$tabElemsWhere = <<<HTML
	<tr data-name="!te_db_name;">
	<td>!te_pos;</td>
	<td>!te_name;</td>
	<td>!te_db_name;</td>
	<td><input name="value" /></td>
	<td>
		<select name="type">
			<option value="default">Default</option>
			<option value="like">Like</option>
			<option value="inInt">IN (int)</option>
		</select>
	</td>
</tr>
HTML;
$tabElemsOrder = <<<HTML
<tr data-name="!te_db_name;">
<td>!te_pos;</td>
<td>!te_name;</td>
<td>!te_db_name;</td>
<td><input name="order" type="number" min="0" max="100" /></td>
<td>
	<select name="sorting">
		<option value="">--</option>
		<option value="ASC">ASC</option>
		<option value="DESC">DESC</option>
	</select>
</td>
</tr>
HTML;

$qry_select = "";
$table_id = "";

// Elements options
$sql_qry = <<<SQL
SELECT
	te_id, LPAD(te_pos, 3, '0') AS te_pos,
	te_name, te_db_name, te_d_nick,
	te_db_type, te_db_typedesc, te_db_notnull, te_db_unsigned,
	te_d_optional, te_d_special,
	te_f_dtype, te_f_minl, te_f_maxl
FROM sa_table_column
WHERE t_id={$params['vars']['tId']}
ORDER BY te_pos ASC, te_id ASC;
SQL;

// Query
// $elemListSelect = \snkeng\core\engine\mysql::printSimpleQuery($sql_qry, $tabElemsSelect);
$elemListGeneral = '';
$idName = '';
if ( \snkeng\core\engine\mysql::execQuery($sql_qry) ) {
	while ( $datos = \snkeng\core\engine\mysql::$result->fetch_assoc() ) {
		// Add els
		$datos['checked'] = '';
		$datos['readOnly'] = '';

		if ( $datos['te_d_special'] === '2' )
		{
			$table_id = $datos['te_db_name'];
			$qry_select.= $datos['te_db_name'];
			$idName = $datos['te_db_name'];
			$datos['checked'] = 'checked';
			$datos['readOnly'] = 'onclick="return false;"';
		}
		//
		$elemListGeneral.= stringPopulate($tabElemsGeneral, $datos);
	}
}
\snkeng\core\engine\mysql::killIfError('uAdminTabContentSelectElements', 'No fue posible leer los elementos.');

//
$page['body'].= <<<HTML
<div class="pageSubTitle">Tablas</div>
<uber-adm-pages-tables-content id="se_tableData" data-tablename="{$tableData['t_dbname']}" data-tableid="{$table_id}" data-ajaxurl="{$params['page']['ajax']}/content">
	<h3>Contenido</h3>
	<div id="tableContainer">
		<table id="tableMod" class="se_table wide alternate border sticky-header" se-elem="elemListGeneral">
			<thead>
				<tr>
					<th colspan="3"></th>
					<th>Select</th>
					<th>Update</th>
					<th colspan="2">Where</th>
					<th colspan="2">Order</th>
				</tr>
				<tr class="titles">
					<th>#</th><th>Name</th><th>DB Name</th>
					<th>Select</th>
					<th>New Value</th>
					<th>Filter</th><th>Type</th>
					<th>Order</th><th>Sorting</th>
				</tr>
				<tr class="filters">
					<th></th><th><input type="text" /></th><th><input type="text" /></th>
					<th></th>
					<th></th>
					<th></th><th></th>
					<th></th><th></th>
				</tr>
			</thead>
			<tbody style="max-height:500px; overflow:auto;">
{$elemListGeneral}
			</tbody>
		</table>
	</div>
	
	<div class="grid gOMB">
		<div class="gr_sz06">
			<h2>Select</h2>
			<form id="sa_form_query" se-elem="selectQueryForm">
				<label><span>SELECT</span><textarea name="elems" required readonly></textarea></label>
				<label><span>FROM</span> <input type="text" name="from" value="{$tableData['t_dbname']}" readonly /></label>
				<label><span>WHERE</span><textarea name="where"></textarea></label>
				<label><span>ORDER BY</span><textarea name="order"></textarea></label>
				<label><span>LIMIT</span><input type="number" name="limit" value="10" min="0" max="1000" /></label>
				<button type="submit" class="btn blue big">Select Query</button>
			</form>
		</div>
		<div class="gr_sz06">
			<h2>Update</h2>
			<form id="sa_form_query" se-elem="updateQueryForm">
				<label><span>UPDATE</span><input type="text" name="from" value="{$tableData['t_dbname']}" readonly /></label>
				<label><span>SET</span><textarea name="elems" required readonly></textarea></label>
				<label><span>WHERE</span><textarea name="where"></textarea></label>
				<label><span>ORDER BY</span><textarea name="order"></textarea></label>
				<label><span>LIMIT</span><input type="number" name="limit" value="1000" min="0" max="1000" /></label>
				<button type="submit" class="btn blue big">Update Query</button>
			</form>
		</div>
	</div>

	<h3>Content</h3>
	<div id="sa_query_table" class="se_table alternate selectable border wide">
		<div class="thead"><div class="titles" se-elem="elemTitles"></div></div>
		<div class="tbody" se-elem="elemContent"></div>
	</div>
</uber-adm-pages-tables-content>
HTML;
//
