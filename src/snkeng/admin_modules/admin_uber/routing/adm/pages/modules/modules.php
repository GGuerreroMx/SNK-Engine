<?php
// Pre-check
\snkeng\core\engine\nav::cacheCheckFile(__FILE__);
\snkeng\core\engine\nav::cacheFinalCheck();

//
$ajaxUrl = \snkeng\core\engine\nav::navPathGet('ajax');


// Estructura
$an_struct = <<<HTML
<tr data-element="row" data-objid="!id;" data-objtitle="!title;">
	<td>!id;</td>
	<td>!title;</td>
	<td>!ver;</td>
	<td class="actions">
		<button class="btn small" data-dyntab-onclick="element_op" data-action="edit" title="Editar"><svg class="icon inline"><use xlink:href="#fa-pencil" /></svg></button>
		<a class="btn small" href="{$ajaxUrl}/!id;/download/"><svg class="icon inline"><use xlink:href="#fa-download"/></svg></a>
		<button class="btn small" data-dyntab-onclick="element_op" data-action="del" title="Borrar"><svg class="icon inline"><use xlink:href="#fa-trash-o" /></svg></button>
	</td>
</tr>
HTML;

//
$exData = [
	'js_url' => $ajaxUrl . '/',
	'printType' => 'table',
	'tableWide' => true,
	'tableHead' => [
		['name' => 'ID', 'filter' => 1, 'fName' => 'id'],
		['name' => 'Título', 'filter' => 1, 'fName' => 'title'],
		['name' => 'Versión'],
		['name' => 'Acciones'],
	],
	'settings' => ['nav' => true],
	'actions' => <<<JSON
{
	"add":
	{
		"ajax-action":"add",
		"ajax-elem":"none",
		"menu":{"title":"Agregar", "icon":"fa-plus"},
		"print":false,
		"action":"form",
		"form":{
			"title":"Nuevo módulo",
			"save_url":"{$ajaxUrl}/",
			"actName":"Agregar",
			"elems":{
				"title": {
					"field_type": "input",
					"data_type": "string",
					"required": true,
					"info_name": "Nombre",
					"info_description": "",
					"attributes": {
						"minlength": 3,
						"maxlength": 30,
						"data-regexp": "simpleTitle"
					}
				},
				"ver": {
					"field_type": "input",
					"data_type": "string",
					"required": true,
					"info_name": "Versión",
					"info_description": "",
					"attributes": {
						"minlength": 3,
						"maxlength": 10,
						"data-regexp": "simpleTitle"
					}
				}
			}
		}
	},
	"edit":
	{
		"ajax-action":"edit",
		"ajax-elem":"obj",
		"ajax-update":true,
		"action":"form",
		"form":{
			"title":"Editar módulo",
			"load_url":"{$ajaxUrl}/{id}",
			"save_url":"{$ajaxUrl}/{id}",
			"actName":"Actualizar",
			"elems":{
				"title": {
					"field_type": "input",
					"data_type": "string",
					"default_value": "",
					"required": true,
					"info_name": "Nombre",
					"info_description": "",
					"attributes": {
						"minlength": 3,
						"maxlength": 30,
						"data-regexp": "simpleTitle"
					}
				},
				"ver": {
					"field_type": "input",
					"data_type": "string",
					"default_value": "",
					"required": true,
					"info_name": "Versión",
					"info_description": "",
					"attributes": {
						"minlength": 3,
						"maxlength": 10,
						"data-regexp": "simpleTitle"
					}
				},
				"content": {
					"field_type": "textarea",
					"data_type": "str",
					"default_value": "",
					"required": false,
					"info_name": "Descripción",
					"info_description": "",
					"attributes": {
						"maxlength": 255
					}
				}
			}
		}
	},
	"del":{
		"action":"del",
		"ajax-elem":"obj",
		"target_url":"{$ajaxUrl}/{id}"
	}
}
JSON
];

//
$tableStructure = \snkeng\core\general\dynamicTable::createElement($an_struct, $exData);

// Page
$page['body'] = <<<HTML
<div class="pageSubTitle">Modulos</div>

{$tableStructure}
HTML;
//
