<?php
//
$params['page']['main'].= '/' . \snkeng\core\engine\nav::current();
$params['page']['ajax'].= '/' . \snkeng\core\engine\nav::current();

// Root
\snkeng\core\admin\navBuild::navBreadcrumbAdd($params['page']['main'], 'External');
\snkeng\core\admin\navBuild::navLevelSet([
	[$params['page']['main'].'/sync', 'Sincronizar'],
	[$params['page']['main'].'/sql', 'SQL'],
	[$params['page']['main'].'/install', 'Instalación'],
]);

//
switch ( \snkeng\core\engine\nav::next() ) {
	//
	case 'sync':
		require __DIR__ . '/sync.php';
		break;
	//
	case 'sql':
		require __DIR__ . '/sql.php';
		break;
	//
	case 'install':
		require __DIR__ . '/install.php';
		break;
	//
	case '':
	case null:
		require __DIR__ . '/#index.php';
		break;
	//
	default:
		\snkeng\core\engine\nav::invalidPage();
		break;
}

// Nav get
$nav = \snkeng\core\admin\navBuild::navCreate();

// Estructura
$page['body'] = <<<HTML
<div class="pageTitle">External</div>

{$nav}

{$page['body']}\n
HTML;
//