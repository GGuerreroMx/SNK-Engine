<?php
// Pre-check
\snkeng\core\engine\nav::cacheCheckFile(__FILE__);
\snkeng\core\engine\nav::cacheFinalCheck();

//
$page['head']['title'] = "Install";

//
\snkeng\core\engine\nav::pageFileModuleAdd('admin_modules', 'admin_uber', '/pages/uber-adm-pages-content-icons-add.mjs');

//
$opts = '';
$struct = <<<HTML
<label class="radio"><input type="radio" name="site" value="%s" required><span class="title">%s - %s</span></label>\n
HTML;
//
$serverParams = ( require($_SERVER['DOCUMENT_ROOT'] . '/snkeng/site_core/specs/s_server_config.php') );
foreach ( $serverParams['locations'] as $loc => $params['vars'] ) {
	if ( isset($params['vars']['local']) && !$params['vars']['local'] ) {
		$serverUrl = ( isset($params['vars']['secure']) && !$params['vars']['secure'] ) ? 'https://' : 'http://';
		$serverUrl.= $loc;

		$type = ( $params['vars']['test'] ) ? 'Test' : 'Live';
		$opts .= sprintf($struct, $serverUrl, $type, $serverUrl);
	}
}

//
$page['body'].= <<<HTML
<div class="grid">
<div class="gr_sz04 gr_ps04">
	<h2>Instalación remota (FTP INIT)</h2>
	<form action="{$params['page']['ajax']}/install" method="post" class="se_form" se-plugin="simpleForm">
		<div class="separator required"><label>
			<span class="title">Destino</span><span class="desc"> </span>
			<div>
			{$opts}
			</div>
		</label></div>
		<div class="separator"><label>
			<span class="title">URL</span><span class="desc"></span>
			<input type="text" name="server" placeholder="ftp://ftp.example.com">
		</label></div>
		<div class="separator"><label>
			<span class="title">Raíz</span><span class="desc"></span>
			<input type="text" name="root" placeholder="/example">
		</label></div>
		<div class="separator"><label>
			<span class="title">Usuario</span><span class="desc"></span>
			<input type="text" name="user">
		</label></div>
		<div class="separator"><label>
			<span class="title">Contraseña</span><span class="desc"></span>
			<input type="text" name="pass">
		</label></div>
		<div class="elem">
			<button type="submit" class="btn big blue"><svg class="icon inline mr"><use xlink:href="#fa-upload"/></svg>Instalar</button>
			<output se-elem="response"></output>
		</div>
	</form>
</div>
</div>
HTML;
