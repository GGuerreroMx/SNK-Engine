<?php

//

\snkeng\core\engine\login::kill_loginLevel('uadmin');

//
switch ( \snkeng\core\engine\nav::next() )
{
	//
	case 'modules':
		require __DIR__ . '/modules/#map.php';
		break;

	//
	case 'tables':
		require __DIR__ . '/tables/#map.php';
		break;

	//
	case 'env':
		require __DIR__ . '/env/#map.php';
		break;

	//
	case 'content':
		require __DIR__ . '/content/#map.php';
		break;


	//
	case 'svg-icons':
		require __DIR__ . '/svg-icons/#map.php';
		break;

	//
	case 'external':
		require __DIR__ . '/external/#map.php';
		break;

	//
	case 'sql':
		require __DIR__ . '/sql/#map.php';
		break;

	//
	case 'system':
		require __DIR__ . '/system/#map.php';
		break;

	//
	default:
		\snkeng\core\engine\nav::invalidPage();
		break;
}
//
