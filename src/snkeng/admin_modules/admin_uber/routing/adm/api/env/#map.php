<?php

//
switch ( \snkeng\core\engine\nav::next() )
{
	//
	case 'envvar':
		require __DIR__ . '/envvar.php';
		break;

	//
	case 'errors':
		require __DIR__ . '/error.php';
		break;

	//
	case 'notifications':
		require __DIR__ . '/notifications.php';
		break;

	//
	case 'apache_errors':
		require __DIR__ . '/apache_errors.php';
		break;

	//
	default:
		\snkeng\core\engine\nav::invalidPage();
		break;
}
//
