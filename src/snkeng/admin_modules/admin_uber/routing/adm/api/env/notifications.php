<?php

//
switch ( \snkeng\core\engine\nav::next() )
{
	//
	case 'read_single':
		$eId = intval($_POST['eId']);
		if ( !$eId ) {
			\snkeng\core\engine\nav::killWithError('ID no encontrado', '');
		}

		$sql_qry = "SELECT elog_content AS content FROM sb_errorlog WHERE elog_id={$eId};";
		$response['d'] = \snkeng\core\engine\mysql::singleValue($sql_qry);
		break;
	//
	case 'read_all':
		// Preparar estructura
		\snkeng\core\general\asyncDataJson::printJson(
			[
				'sel' => "elog_id AS id, elog_dtadd AS date, elog_type AS type, elog_key AS code",
				'from' => 'sb_errorlog',
				'lim' => 50,
				'where' => [
					'type' => ['name' => 'Tipo', 'db' => 'elog_type', 'vtype' => 'int', 'stype' => 'eq', 'set'=> 2]
				],
				'order' => [
					'id' => ['Name' => 'ID', 'db' => 'elog_id']
				],
				'default_order' => [
					['id', 'ASC']
				]
			]
		);
		break;
	//
	case 'del_all':
		$sql_qry = "TRUNCATE TABLE sb_errorlog;";
		\snkeng\core\engine\mysql::submitQuery(
			$sql_qry,
			[
				'errorKey' => 'sadmin-sqlops-truncateErr',
				'errorDesc' => 'No pudo ser borrado el log.'
			]
		);
		break;
	//
	default:
		\snkeng\core\engine\nav::invalidPage();
		break;
}
//
