<?php

//
if ( !is_numeric(\snkeng\core\engine\nav::next(false)) ) {
	switch ( \snkeng\core\engine\nav::next() ) {
		// Agregar
		case 'add':
			\snkeng\core\general\saveData::sql_complex_rowAdd(
				$response,
				[
					'targetId' => ['name' => 'Nombre', 'type' => 'str', 'lMin' => 0, 'lMax' => 60, 'filter' => 'simpleTitle', 'process' => false, 'validate' => true],
				],
				[
					'elems' => [
						'tfk_name' => 'name',
						'orig_t_id' => 'origId',
						'target_t_id' => 'targetId'
					],
					'tName' => 'sa_table_column',
					'tId' => ['nm' => 'id', 'db' => 'te_id']
				],
				[
					'setData' => [
						'tId' => $params['vars']['tableId']
					],
					'opsData' => function(&$params) {
					}
				]
			);
			break;
		// Agregar
		case 'readAll':
			// Preparar estructura
			\snkeng\core\general\asyncDataJson::printJson(
				[
					'sel' => "tfk_id AS id,
						tfk_dt_add AS dtAdd, tfk_dt_mod AS dtMod,
						tfk_name AS fkName,
						orig_t_id, target_t_id,
						tfk_colums, tfk_on_update, tfk_on_delete,
						sto.t_name AS tableName",
					'from' => 'sa_table_foreign_key
					INNER JOIN sa_table_object AS sto ON sto.t_id=target_t_id',
					'lim' => 50,
					'where' => [
						'tId' => ['name' => 'ID Tabla', 'db' => 'orig_t_id', 'vtype' => 'int', 'stype' => 'eq', 'set' => $params['vars']['tableId']],
					]
				]
			);
			break;
		//
		default:
			\snkeng\core\engine\nav::invalidPage();
			break;
	}
} else {
	//
	$params['vars']['tableIndexId'] = intval(\snkeng\core\engine\nav::next());

	//
	switch ( \snkeng\core\engine\nav::next() ) {

		//
		default:
			\snkeng\core\engine\nav::invalidPage();
			break;
	}
}
