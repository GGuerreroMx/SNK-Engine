<?php

//
if ( !is_numeric(\snkeng\core\engine\nav::next(false)) ) {
	switch (\snkeng\core\engine\nav::next()) {
		case null:
			require __DIR__ . '/#collection.php';
			break;
		case 'table_multi':
			require __DIR__ . '/table_multi.php';
			break;
	}
}
else {
	//
	$params['vars']['tableId'] = intval(\snkeng\core\engine\nav::next());

	//
	switch ( \snkeng\core\engine\nav::next() ) {
		//
		case 'content':
			require __DIR__ . '/[id]_content.php';
			break;

		//
		case 'columns':
			require __DIR__ . '/[id]_columns.php';
			break;

		//
		case 'indexes':
			require __DIR__ . '/[id]_indexes.php';
			break;

		//
		case 'foreign-keys':
			require __DIR__ . '/[id]_foreign-keys.php';
			break;

		//
		case 'update':
			require __DIR__ . '/[id]_update.php';
			break;

		//
		default:
			require __DIR__ . '/[id]_#attributes.php';
			break;
	}
}
//
