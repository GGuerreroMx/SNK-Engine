<?php
$new_qry = '';
//
$elems = explode(',', $_POST['elems']);
if ( empty($elems) ) {
	\snkeng\core\engine\nav::killWithError('No hay tablas seleccionadas.', '');
}

//
switch ( \snkeng\core\engine\nav::next() ) {
	//
	case 'update';
		$alpha = new \snkeng\admin_modules\admin_uber\snkEng_update();
		$alpha->updateTables($response, $elems);
		break;
	//
	case 'truncate':
		$sql_qry = "SELECT t_dbname FROM sa_table_object WHERE t_id IN ({$_POST['elems']});";
		if ( \snkeng\core\engine\mysql::execQuery($sql_qry) ) {
			while ( $datos = \snkeng\core\engine\mysql::$result->fetch_array() ) {
				$new_qry .= "TRUNCATE {$datos['t_dbname']};\n";
			}
		}
		else {
			\snkeng\core\engine\mysql::killIfError('No fue posible leer las tablas del sistema', 'snkAdmAdvTableOp01');
		}
		//
		/*
		$response['debug']['qry'] = $new_qry;
		if ( !\snkeng\core\engine\mysql::submitMultiQuery($new_qry) ){
			$response['s']['t'] = 0;
			$response['s']['d'] = 'No fue posible realizar la operación.';
			$response['s']['ex'] = \snkeng\core\engine\mysql::createAdvancedError('', '');
		}
		*/
		break;
	//
	case 'drop':
		$sql_qry = "SELECT t_dbname FROM sa_table_object WHERE t_id IN ({$_POST['elems']});";
		if ( \snkeng\core\engine\mysql::execQuery($sql_qry) ) {
			while ( $datos = \snkeng\core\engine\mysql::$result->fetch_array() ) {
				$new_qry .= "DROP TABLE IF EXISTS {$datos['t_dbname']};\n";
			}
			$new_qry .= 'DELETE FROM sb_ghost_table_object WHERE t_id IN (' . $_POST['elems'] . ");\n" . 'DELETE FROM sb_ghost_table_column WHERE gt_id IN (' . $_POST['elems'] . ");\n";
		}
		else {
			\snkeng\core\engine\mysql::killIfError('No fue posible leer las tablas del sistema', 'snkAdmAdvTableOp02');
		}
		//
		/*
		$response['debug']['qry'] = $new_qry;
		if ( !\snkeng\core\engine\mysql::submitMultiQuery($new_qry) ) {
			$response['s']['t'] = 0;
			$response['s']['d'] = 'No fue posible realizar la operación.';
			$response['s']['ex'] = \snkeng\core\engine\mysql::createAdvancedError('', '');
		}
		*/
		break;
	//
	case 'system':
		$sql_qry = "SELECT t_dbname FROM sa_table_object WHERE t_id IN ({$_POST['elems']});";
		if ( \snkeng\core\engine\mysql::execQuery($sql_qry) ) {
			while ( $datos = \snkeng\core\engine\mysql::$result->fetch_array() ) {
				$new_qry .= "DROP TABLE IF EXISTS {$datos['t_dbname']};\n";
			}
			$new_qry .= <<<SQL
DELETE
sa_table_object, sa_table_column,
sb_ghost_table_object, sb_ghost_table_column
FROM sa_table_object
LEFT OUTER JOIN sa_table_column		ON sa_table_object.t_id=sa_table_column.t_id
LEFT OUTER JOIN sb_ghost_table_object		ON sa_table_object.t_id=sb_ghost_table_object.t_id
LEFT OUTER JOIN sb_ghost_table_column		ON sa_table_object.t_id=sb_ghost_table_column.gt_id
WHERE sa_table_object.t_id IN ({$_POST['elems']});\n
SQL;
		}
		else {
			\snkeng\core\engine\mysql::killIfError('No fue posible leer las tablas del sistema', 'snkAdmAdvTableOp03');
		}
		/*
		//
		$response['debug']['qry'] = $new_qry;
		if ( !\snkeng\core\engine\mysql::submitMultiQuery($new_qry) ) {
			$response['s']['t'] = 0;
			$response['s']['d'] = 'No fue posible realizar la operación.';
			$response['s']['ex'] = \snkeng\core\engine\mysql::createAdvancedError('', '');
		}*/
		break;
	//
	case 'system2':
		$sql_qry = "SELECT t_dbname FROM sa_table_object WHERE t_id IN ({$_POST['elems']});";
		if ( \snkeng\core\engine\mysql::execQuery($sql_qry) ) {
			while ( $datos = \snkeng\core\engine\mysql::$result->fetch_array() ) {
				$new_qry .= "DROP TABLE IF EXISTS {$datos['t_dbname']};\n";
			}
			$new_qry .= <<<SQL
DELETE
sa_table_object, sa_table_column,
sb_ghost_table_object, sb_ghost_table_column
FROM sa_table_object
LEFT OUTER JOIN sa_table_column		ON sa_table_object.t_id=sa_table_column.t_id
LEFT OUTER JOIN sb_ghost_table_object		ON sa_table_object.t_id=sb_ghost_table_object.t_id
LEFT OUTER JOIN sb_ghost_table_column		ON sa_table_object.t_id=sb_ghost_table_column.gt_id
WHERE sa_table_object.t_id IN ({$_POST['elems']});\n
SQL;
		}
		else {
			\snkeng\core\engine\mysql::killIfError(
				'No fue posible leer las tablas del sistema', 'snkAdmAdvTableOp03'
			);
		}
		/*
		//
		$response['debug']['qry'] = $new_qry;
		if ( !\snkeng\core\engine\mysql::submitMultiQuery($new_qry) ) {
			$response['s']['t'] = 0;
			$response['s']['d'] = 'No fue posible realizar la operación.';
			$response['s']['ex'] = \snkeng\core\engine\mysql::createAdvancedError('', '');
		}*/
		break;
	//
	default:
		\snkeng\core\engine\nav::invalidPage();
		break;
}

//
$response['d'] = [];
$response['d']['qry'] = "SQL:\n" . $new_qry;