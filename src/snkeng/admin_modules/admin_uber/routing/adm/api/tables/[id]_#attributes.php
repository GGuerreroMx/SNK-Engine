<?php
//
switch ( \snkeng\core\engine\nav::next() ) {
	//
	case '':
	case null:
		$tableStructure = [
			'elems' => [
				't_name' => 'title',
				'mod_id' => 'modId',
				't_dbname' => 'dbName',
				't_dbnick' => 'dbNick',
				't_dbtype' => 'dbType',
			],
			'tName' => 'sa_table_object',
			'tId' => ['nm' => 'id', 'db' => 't_id', 'value' => $params['vars']['tableId']]
		];

		//
		switch ( $_SERVER['REQUEST_METHOD'] ) {
			//
			case 'GET':
				$response['d'] = \snkeng\core\engine\restfulOps::resourceReadSingle(
					$tableStructure
				);
				break;

			//
			case 'POST':
				$response['d'] = \snkeng\core\engine\restfulOps::resourceUpdateSingle(
					[
						'title' =>
							[
								'name' => 'Nombre',
								'type' => 'str',
								'required' => false,
								'validation' =>
									[
										'minlength' => 0,
										'maxlength' => 75,
										'filter' => 'simpleTitle',
									],
							],
						'modId' =>
							[
								'name' => 'ID Modulo',
								'type' => 'int',
								'required' => false,
								'validation' =>
									[
										'min' => 0,
										'max' => 255,
										'not_null' => true,
									],
							],
						'dbName' =>
							[
								'name' => 'Nombre DB',
								'type' => 'str',
								'required' => false,
								'validation' =>
									[
										'minlength' => 0,
										'maxlength' => 75,
										'filter' => 'urlTitles',
									],
							],
						'dbNick' =>
							[
								'name' => 'Nick DB',
								'type' => 'str',
								'required' => false,
								'validation' =>
									[
										'minlength' => 0,
										'maxlength' => 10,
										'filter' => 'urlTitles',
									],
							],
						'dbType' =>
							[
								'name' => 'Tipo DB',
								'type' => 'str',
								'required' => false,
								'validation' =>
									[
										'minlength' => 0,
										'maxlength' => 10,
										'filter' => 'urlTitles',
									],
							]
					],
					$tableStructure
				);
				break;

			//
			case 'DELETE':
				\snkeng\core\engine\restfulOps::resourceDeleteSingle($tableStructure);
				break;

			//
			default:
				\snkeng\core\engine\nav::invalidMethod();
				break;
		}
		break;

	//
	default:
		\snkeng\core\engine\nav::invalidPage();
		break;
}
//

