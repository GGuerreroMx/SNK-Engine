<?php

// Start Server Side Event Behaviour
\snkeng\core\engine\serverSideEvent::startServerSideEvent();

// Get site properties
$serverParams = (require($_SERVER['DOCUMENT_ROOT'] . '/snkeng/site_core/specs/s_server_config.php'));

$currentServer = $serverParams['locations'][$_GET['site']];

// Validate if site is in list (must be, but avoid user manipulation)
if ( empty($currentServer) ) {
	\snkeng\core\engine\serverSideEvent::sendMessage("No se encontró el servidor actual. Recibido: {$_GET['site']}");

	// Stop
	\snkeng\core\engine\serverSideEvent::sendMessage("STOP", "stop");
	exit();
}

//
$rebuild = $_GET['rebuild'] ?? false;

//
switch ( $_GET['op'] ) {
	// Actualizar archivos
	case 'files_update':
		//
		\snkeng\core\engine\serverSideEvent::sendMessage("File update initialization.");

		//
		\snkeng\core\engine\serverSideEvent::sendMessage("Processing bundles: ");
		\snkeng\admin_modules\admin_uber\minify_bundles::exec($response, $rebuild);

		//
		\snkeng\core\engine\serverSideEvent::sendMessage("Processing modules: ");
		\snkeng\admin_modules\admin_uber\minify_modules::exec($response, $rebuild);

		//
		\snkeng\core\engine\serverSideEvent::sendMessage("Uploading files");
		\snkeng\core\engine\rsync::syncToServer($currentServer['server_ssh_access'], $currentServer['server_main_folder']);

		\snkeng\core\engine\serverSideEvent::sendMessage("Files uploaded!");
		break;

	// Reinstalar archivos
	case 'files_reinstall':
		//
		\snkeng\core\engine\serverSideEvent::sendMessage("Substitution initialization.");

		//
		\snkeng\core\engine\serverSideEvent::sendMessage("Processing bundles");
		\snkeng\admin_modules\admin_uber\minify_bundles::exec($response, $rebuild);

		//
		\snkeng\core\engine\serverSideEvent::sendMessage("Processing modules");
		\snkeng\admin_modules\admin_uber\minify_modules::exec($response, $rebuild);

		//
		\snkeng\core\engine\serverSideEvent::sendMessage("Uploading files");
		$engUpdater->files_systemReinstall($response, $_GET['site']);
		break;

	// SQL Structure
	case 'sql_structure':
		$engUpdater->sql_structureCreate($response, $_GET['site']);
		break;

	// SQL ALL
	case 'sql_all':
		if ( $_GET['security'] === 'replace' ) {
			$engUpdater->sql_allUpdate($response, $_GET['site']);
		} else {
			\snkeng\core\engine\nav::killWithError('Pregunta de seguridad no aprobada', '');
		}
		break;

	// Files Uploads
	case 'files_uploads':
		if ( $_GET['security'] === 'replace' ) {
			$engUpdater->files_uploads($response, $_GET['site']);
		} else {
			\snkeng\core\engine\nav::killWithError('Pregunta de seguridad no aprobada', '');
		}
		break;

	//
	default:
		\snkeng\core\engine\nav::invalidPage();
		break;
}

// Stop
\snkeng\core\engine\serverSideEvent::sendMessage("STOP", "stop");
exit();
//

