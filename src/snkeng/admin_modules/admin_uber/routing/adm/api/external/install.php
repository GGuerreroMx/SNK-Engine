<?php

//
$rData = \snkeng\core\general\saveData::postParsing(
	$response,
	[
		'site' => ['name' => 'Site',   'type' => 'str','validate' => true],
		'server' => ['name' => 'URL',   'type' => 'str', 'validate' => true],
		'root' => ['name' => 'Root',   'type' => 'str', 'validate' => false],
		'user' => ['name' => 'User',   'type' => 'str','validate' => true],
		'pass' => ['name' => 'Pass',   'type' => 'str', 'validate' => true],
		'passive' => ['name' => 'Passive',   'type' => 'bool'],
	]
);

//
$engUpdater = new \snkeng\core\engine\fileUpd();
$engUpdater->first_install($response, $rData);
//
