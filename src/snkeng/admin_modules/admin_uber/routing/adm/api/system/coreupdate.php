<?php

//
switch ( \snkeng\core\engine\nav::next() ) {
	case 'fileUpload':
		// Validar
		$rData = \snkeng\core\general\saveData::postParsing($response,
			[
				'file' => ['name' => 'Archivo', 'type' => 'file', 'fType'=>['zip'], 'mSize'=>10000, 'validate' => true],
			]
		);

		//
		$engUpdater = new \snkeng\core\engine\fileUpd();
		$engUpdater->file_core_update($response, $_FILES['file']['tmp_name']);
		break;

	//
	case 'internetDownload':
		$tempFile = $_SERVER['DOCUMENT_ROOT'] . '/se_files/user_upload/temp/core.zip';

		// Validar
		/*
		//
		$http_query_data = [
			'url' => 'https://snako.cora.snako.dev/server/snkeng/versions/core/download',
			'auth' => [
				'user' => 'abc',
				'pass' => 'def'
			]
		];



		// Authorization
		$hash = base64_encode($http_query_data['auth']['user'] . ":" . $http_query_data['auth']['pass']);

		$headers = [
			"Authorization: Basic $hash"
		];

		//
		$fp = fopen($tempFile, 'w+');

		$options = array(
			CURLOPT_RETURNTRANSFER => true,         // return web page
			CURLOPT_HEADER         => false,        // don't return headers
			CURLOPT_FOLLOWLOCATION => true,         // follow redirects
			CURLOPT_ENCODING       => "",           // handle all encodings
			CURLOPT_USERAGENT      => "snkeng",     // who am i
			CURLOPT_AUTOREFERER    => true,         // set referer on redirect
			CURLOPT_CONNECTTIMEOUT => 120,          // timeout on connect
			CURLOPT_TIMEOUT        => 0,          // timeout on response
			CURLOPT_MAXREDIRS      => 1,           // stop after 1 redirects
			CURLOPT_HTTPHEADER      => $headers,
			CURLOPT_FILE            => $fp,
			CURLOPT_SSL_VERIFYHOST => 0,            // don't verify ssl
			CURLOPT_SSL_VERIFYPEER => false,        //
			CURLOPT_VERBOSE        => 1                //
		);

		$ch      = curl_init($http_query_data['url']);
		curl_setopt_array($ch, $options);
		$content = curl_exec($ch);
		$err     = curl_errno($ch);
		$errmsg  = curl_error($ch) ;
		$header  = curl_getinfo($ch);
		curl_close($ch);
		fclose($fp);
		*/
		//
		$engUpdater = new \snkeng\core\engine\fileUpd();
		$engUpdater->file_core_update($response, $tempFile);

		// delete downloaded file
		// unlink($tempFile);
		break;

	//
	default:
		\snkeng\core\engine\nav::invalidPage();
		break;
}
//
