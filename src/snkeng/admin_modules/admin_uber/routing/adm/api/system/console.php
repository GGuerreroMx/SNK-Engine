<?php
// Commands
$commands = explode(' ', $_POST['command']);

function se_adm_getTables() {
		$tables = [];
	$sql_qry = "SHOW TABLES;";
	if ( \snkeng\core\engine\mysql::execQuery($sql_qry) ) {
		while ( $data = \snkeng\core\engine\mysql::$result->fetch_array() ) {
			$table = $data[0];
			if ( substr( $table, 0, 3 ) !== "sa_" && substr( $table, 0, 3 ) !== "sb_" ) {
				$tables[] = $table;
			}
		}
	} else {
		\snkeng\core\engine\mysql::killIfError('', '');
	}
	return $tables;
}

// Commands
switch ( array_shift($commands) )
{
	// System
	case 'system':
		switch ( array_shift($commands) ) {
			//
			case 'tables':
				// Revisar SQL
				$tables = se_adm_getTables();

				// Revisar ghost
				$sql_qry = "SELECT gt_dbname FROM sb_ghost_table_object;";
				if ( \snkeng\core\engine\mysql::execQuery($sql_qry) ) {
					while ( $data = \snkeng\core\engine\mysql::$result->fetch_array() ) {
						$gTables[] = $data['gt_dbname'];
					}
				} else {
					\snkeng\core\engine\mysql::killIfError('', '');
				}

				// Revisar SQL
				$rTables = se_adm_getTables();

				$grDiff = array_diff($gTables, $rTables);
				$rgDiff = array_diff($rTables, $gTables);
				$dSql = '';
				foreach ( $rgDiff as $i ) {
					$dSql.= "DROP TABLE IF EXISTS `{$i}`;\n";
				}
				$response['d'] = $dSql;
				break;

			// Actualizar archivos
			case 'resetFull':
				// Revisar SQL
				$tables = se_adm_getTables();
				//
				foreach ( $tables as $table ) {
					$response['d'].= "DROP TABLE IF EXISTS {$table};\n";
				}

				$response['d'].= <<<SQL
TRUNCATE sa_modules;
TRUNCATE sa_table_object;
TRUNCATE sa_table_column;
TRUNCATE sb_ghost_table_object;
TRUNCATE sb_ghost_table_column;
TRUNCATE sb_users_logins;
TRUNCATE sb_users_login_log;
TRUNCATE sb_errorlog;
TRUNCATE sb_notifications;
DELETE FROM sb_users_mail WHERE am_id>1;
ALTER TABLE sb_users_mail AUTO_INCREMENT=2;
DELETE FROM sb_objects_obj WHERE a_id>1;
ALTER TABLE sb_objects_obj AUTO_INCREMENT=2;
SQL;
				break;

			//
			case 'resetPartial':
				// Revisar SQL
				$tables = se_adm_getTables();
				//
				foreach ( $tables as $table ) {
					$response['d'].= "DROP TABLE IF EXISTS {$table};\n";
				}

				$response['d'].= <<<SQL
TRUNCATE sb_ghost_table_object;
TRUNCATE sb_ghost_table_column;
TRUNCATE sb_users_logins;
TRUNCATE sb_users_login_log;
TRUNCATE sb_errorlog;
TRUNCATE sb_notifications;
DELETE FROM sb_users_mail WHERE am_id>1;
ALTER TABLE sb_users_mail AUTO_INCREMENT=2;
DELETE FROM sb_objects_obj WHERE a_id>1;
ALTER TABLE sb_objects_obj AUTO_INCREMENT=2;
SQL;
				break;

			//
			case 'resetContent':
				// Revisar SQL
				$tables = se_adm_getTables();
				//
				foreach ( $tables as $table ) {
					$response['d'].= "TRUNCATE {$table};\n";
				}

				$response['d'].= <<<SQL
TRUNCATE sb_errorlog;
TRUNCATE sb_notifications;
TRUNCATE sb_users_logins;
TRUNCATE sb_users_login_log;
TRUNCATE sb_users_socnet;
TRUNCATE sb_objects_properties;
DELETE FROM sb_users_mail WHERE am_id>1;
ALTER TABLE sb_users_mail AUTO_INCREMENT=2;
DELETE FROM sb_objects_obj WHERE a_id>1;
ALTER TABLE sb_objects_obj AUTO_INCREMENT=2;
SQL;
				break;

			//
			case 'help':
				$response['d'] = "Available commands:\n - tables\n - resetFull\n - resetPartial\n - resetContent";
				break;

			//
			default:
				$response['d'] = 'Type system help for mor information.';
				break;
		}
		break;

	// Help
	case 'help':
		$response['d'] = "Available commands:\n - system";
		break;

	//
	default:
		$response['d'] = "Not valid operation, type help for available commands.";
		break;
}
//
