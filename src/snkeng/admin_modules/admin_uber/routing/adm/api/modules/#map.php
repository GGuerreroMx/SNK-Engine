<?php
//
if ( !is_numeric(\snkeng\core\engine\nav::next(false)) ) {
	switch ( \snkeng\core\engine\nav::next() ) {
		//
		case '':
		case null:
			switch ( $_SERVER['REQUEST_METHOD'] ) {
				//
				case 'GET':
					// Preparar estructura
					\snkeng\core\general\asyncDataJson::printJson(
						[
							'sel' => "mod_id AS id, mod_name AS title, mod_ver AS ver",
							'from' => 'sa_modules',
							'lim' => 20,
							'where' => [
								'id' => ['name' => 'ID', 'db' => 'mod_id', 'vtype' => 'int', 'stype' => 'eq'],
								'title' => ['name' => 'Title', 'db' => 'mod_name', 'vtype' => 'str', 'stype' => 'like']
							],
							'order' => [
								'name' => ['Name' => 'Nombre', 'db' => 'mod_name', 'set' => 'ASC']
							],
							'order_def' => [
								['name', 'ASC']
							]
						]
					);
					break;

				//
				case 'POST':
					$response['d'] = \snkeng\core\engine\restfulOps::resourceAddSingle(
						[
							'title' =>
								[
									'name' => 'Nombre',
									'type' => 'str',
									'required' => true,
									'validation' =>
										[
											'minlength' => 0,
											'maxlength' => 30,
											'filter' => 'simpleTitle',
										],
								],
							'ver' =>
								[
									'name' => 'Versión',
									'type' => 'str',
									'required' => false,
									'validation' =>
										[
											'minlength' => 0,
											'maxlength' => 10,
											'filter' => 'simpleTitle',
										],
								],
						],
						[
							'elems' => [
								'sv_var' => 'title',
								'sv_value' => 'value',
								'sv_type' => 'type',
							],
							'tName' => 'sa_sitevars',
							'tId' => ['nm' => 'id', 'db' => 'sv_id']
						]
					);
					break;

				//
				default:
					\snkeng\core\engine\nav::invalidMethod();
					break;
			}
			break;

		//
		default:
			\snkeng\core\engine\nav::invalidPage();
			break;
	}
}
else {
	$params['vars']['tableId'] = intval(\snkeng\core\engine\nav::next());

	//
	switch ( \snkeng\core\engine\nav::next() ) {
		//
		case '':
		case null:
			$tableStructure = [
				'elems' => [
					'mod_name' => 'title',
					'mod_ver' => 'ver',
					'mod_desc' => 'content'
				],
				'tName' => 'sa_modules',
				'tId' => ['nm' => 'id', 'db' => 'mod_id', 'value' => $params['vars']['tableId']]
			];

			switch ( $_SERVER['REQUEST_METHOD'] ) {
				//
				case 'GET':
					$response['d'] = \snkeng\core\engine\restfulOps::resourceReadSingle(
						$tableStructure
					);
					break;

				//
				case 'POST':
					$response['d'] = \snkeng\core\engine\restfulOps::resourceUpdateSingle(
						[
							'title' =>
								[
									'name' => 'Nombre',
									'type' => 'str',
									'required' => true,
									'validation' =>
										[
											'minlength' => 0,
											'maxlength' => 30,
											'filter' => 'simpleTitle',
										],
								],
							'ver' =>
								[
									'name' => 'Versión',
									'type' => 'str',
									'required' => false,
									'validation' =>
										[
											'minlength' => 0,
											'maxlength' => 10,
											'filter' => 'simpleTitle',
										],
								],
							'content' =>
								[
									'name' => 'Descripción',
									'type' => 'str',
									'required' => false,
									'validation' =>
										[
											'minlength' => 0,
											'maxlength' => 255,
											'filter' => '',
										],
								],
						],
						$tableStructure
					);
					break;

				//
				case 'DELETE':
					\snkeng\core\engine\restfulOps::resourceDeleteSingle($tableStructure);
					break;

				//
				default:
					\snkeng\core\engine\nav::invalidMethod();
					break;
			}
			break;

		//
		case 'download':
			$alpha = new \snkeng\admin_modules\admin_uber\module_manager();
			$alpha->moduleExtract($params['vars']['tableId'], true);
			break;

		//
		default:
			\snkeng\core\engine\nav::invalidPage();
			break;
	}
}
