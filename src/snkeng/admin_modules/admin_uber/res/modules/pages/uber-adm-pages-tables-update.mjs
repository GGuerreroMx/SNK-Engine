import * as formActions from '/snkeng/core/res/modules/library/forms.mjs';

//
customElements.define('uber-adm-pages-tables-update', class extends HTMLElement {
	structureForm = this.querySelector('form');

	//
	constructor() {
		super();
	}

	//
	connectedCallback() {
		this.addEventListener('click', this.onClickCallBacks.bind(this));
	}

	//
	onClickCallBacks(e) {
		let cBtn = e.target.closest('button[data-action]');
		if ( !cBtn ) {
			return;
		}

		//
		let operation = 'onClick_' + cBtn.dataset['action'];

		//
		if ( typeof this[operation] !== 'function' ) {
			console.error("operación no definida.", operation, cBtn);
			return;
		}

		//
		this[operation](e, cBtn);
	}

	//
	onClick_update(e, cBtn) {
		//
		const submitButton = this.querySelector('button[data-action="update"]'),
			outputQuery = this.querySelector('output[data-element="query"]'),
			outputResponse = this.querySelector('output[data-element="response"]')

		//
		formActions.postRequest(
			this.dataset['ajaxurl'],
			null,
			{
				response:outputResponse,
				onRequest:() => {
					submitButton.disabled = true;
				},
				onComplete:() => {
					submitButton.disabled = false;
				},
				onSuccess:(msg) => {
					outputQuery.innerHTML = msg.d;
				},
				onFail:(msg) => {
					outputQuery.innerHTML = msg.title;
				},
				onError:(xhr) => {
					outputQuery.innerHTML = "<pre>" + xhr.response + "</pre>";
				}
			}
		)
	}
});