import * as formActions from '/snkeng/core/res/modules/library/forms.mjs';

//
customElements.define('uber-adm-pages-sql-cleanup-ops', class extends HTMLElement {
	//
	constructor() {
		super();
	}

	//
	connectedCallback() {
		this.apiUrl = this.dataset['apiurl'];

		// Bindings
		this.addEventListener('click', this.onClickCallBacks.bind(this));
	}

	//
	onClickCallBacks(e) {
		let cBtn = e.target.closest('button[data-iconpick-action]');
		if ( !cBtn ) {
			return;
		}

		//
		let operation = 'onClick_' + cBtn.dataset['iconpickAction'];

		//
		if ( typeof this[operation] !== 'function' ) {
			console.error("operación no definida.", operation, cBtn);
			return;
		}

		//
		this[operation](e, cBtn);
	}

	//
	onClick_clearGTables(e, cBtn) {
		formActions.postRequest(
			this.apiUrl + '/gtable_clean',
			{},
			{
				onSuccess:function(msg) {
					$('#sql_output').se_html(msg.d);
				},
				onFail:function(xhr) {
					$('#sql_output').se_html(xhr.response);
				}
			}
		);
	}

	//
	onClick_showTables(e, cBtn) {
		formActions.postRequest(
			this.apiUrl + '/table_check',
			{},
			{
				onSuccess:function(msg) {
					$('#sql_output').se_html(msg.d);
				},
				onFail:function(xhr) {
					$('#sql_output').se_html(xhr.response);
				}
			}
		);
	}
});