import * as formActions from '/snkeng/core/res/modules/library/forms.mjs';

//
customElements.define('uber-adm-pages-external-sync', class extends HTMLElement {
	updateForm = this.querySelector('form[data-element="update"]');
	updateList = this.querySelector("#opList");

	//
	constructor() {
		super();
	}

	//
	connectedCallback() {
		this.updateForm.se_on('submit', this.syncRequest.bind(this));

	}

	syncRequest(e) {
		e.preventDefault();

		// Delete all messages
		this.updateList.innerHTML = '';

		console.log("start event source");

		//
		const urlParams = formActions.convertFormToURLParams(this.updateForm);


		const apiUrl = this.dataset['apiurl'];

		const evtSource = new EventSource(
			apiUrl + "/update/?" + urlParams,
			{
				withCredentials: true,
			}
		);

		evtSource.onmessage = (event) => {
			this.syncAddMessage(`${event.data}`);
		};

		evtSource.addEventListener("error", (event) => {
			this.syncAddMessage(`${event.data}`, 'error');
		});

		evtSource.addEventListener("results", (event) => {
			let displayText = "";
			//
			switch ( typeof event.data ) {
				//
				case "string":
					displayText = event.data;
					break;
				//
				case "object":
					const cJson = JSON.parse(event.data);
					console.error("caso no programado", cJson);
					displayText = "Programar como sacar el json XD";
					break;
			}

			this.syncAddMessage(`${event.data}`, 'result');
		});

		//
		evtSource.addEventListener("stop", (event) => {
			console.log("SSE CLOSED");
			evtSource.close();
		});
	}

	syncAddMessage(message, modifier) {
		//
		const newElement = document.createElement("li");
		if ( modifier ) {
			newElement.classList.add(modifier);
		}
		newElement.textContent = message;

		//
		this.updateList.appendChild(newElement);

		this.updateList.scrollTop = this.updateList.scrollHeight;
	}
});