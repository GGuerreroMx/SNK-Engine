import * as formActions from '/snkeng/core/res/modules/library/forms.mjs';

//
customElements.define('uber-adm-pages-content-minify', class extends HTMLElement {
	//
	constructor() {
		super();
	}

	//
	connectedCallback() {
		this.ajaxUrl = this.dataset['ajaxurl'];

		//
		this.addEventListener('click', this.onClickCallBacks.bind(this));
	}

	//
	onClickCallBacks(e) {
		let cBtn = e.target.closest('button[data-action]');
		if ( !cBtn ) {
			return;
		}

		//
		let operation = 'onClick_' + cBtn.dataset['action'];

		//
		if ( typeof this[operation] !== 'function' ) {
			console.error("operación no definida.", operation, cBtn);
			return;
		}

		//
		this[operation](e, cBtn);
	}

	//
	onClick_minify(e, cEl) {
		//
		cEl.se_text("Cargando...");

		//
		formActions.postRequest(
			this.ajaxUrl + '/minify',
			{
				purge:( $('#env_minify_purge').checked ) ? 1 : 0
			},
			{
				onSuccess:function(msg){
					cEl.innerText = msg.d.ops;
					console.log(msg.d.file);
				},
				onFail:function(msg){
					cEl.innerText = msg.title;
				}
			}
		);
	}
});