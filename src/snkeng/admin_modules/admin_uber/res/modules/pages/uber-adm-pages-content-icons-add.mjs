import * as simpleForm from '/snkeng/core/res/modules/components-simple/se-async-form.mjs';

//
customElements.define('uber-adm-pages-content-icons-add', class extends HTMLElement {
	structureForm = this.querySelector('form');

	//
	constructor() {
		super();
	}

	//
	connectedCallback() {
		//
		this.structureForm.se_on('change', ['textarea', 'input'], function(ev) {
			let cForm = ev.target.se_closest('form'),
				svg = this.getElementById('testIcon'),
				xi = cForm.se_formElVal('xi'),
				yi = cForm.se_formElVal('yi'),
				xp = cForm.se_formElVal('xp'),
				yp = cForm.se_formElVal('yp'),
				svgData = cForm.se_formElVal('svg');
			//
			svg.setAttribute('viewBox', '' + xi + ',' + yi + ',' + xp + ',' + yp);
			svg.innerHTML = svgData;
		});

		// Set form callback to add function
		window.customElements.whenDefined('se-async-form').then(() => {
			this.structureForm.dispatchEvent(
				new CustomEvent("setCallBacks", {
					detail: {
						onSuccess: (msg) => {
							this.querySelector('output[se-elem="response"]').innerText = msg.d;
						}
					},
				}),
			);
		});
	}
});