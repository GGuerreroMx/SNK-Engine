import * as formActions from '/snkeng/core/res/modules/library/forms.mjs';

//
customElements.define('uber-adm-pages-content-icons-pick', class extends HTMLElement {
	//
	constructor() {
		super();
	}

	//
	connectedCallback() {
		this.apiUrl = this.dataset['apiurl'];

		// Bindings
		this.se_on('click', '.iconSelector', this.iconSelect.bind(this));
		this.addEventListener('click', this.onClickCallBacks.bind(this));
	}

	//
	onClickCallBacks(e) {
		let cBtn = e.target.closest('button[data-iconpick-action]');
		if ( !cBtn ) {
			return;
		}

		//
		let operation = 'onClick_' + cBtn.dataset['iconpickAction'];

		//
		if ( typeof this[operation] !== 'function' ) {
			console.error("operación no definida.", operation, cBtn);
			return;
		}

		//
		this[operation](e, cBtn);
	}

	//
	onClick_upload(e, cBtn) {
		let iconList = this.querySelectorAll('div.iconSelector[aria-selected="true"]'),
			length = iconList.length,
			elList = '';

		//
		if ( length === 0 ) {
			alert("No hay iconos seleccionados.");
			return;
		}

		//
		let first = true;
		for ( let i = 0; i < length; i++ ) {
			elList += (first) ? '' : ',';
			elList += iconList[i].dataset['id'];
			first = false;
		}

		//
		formActions.postRequest(
			this.apiUrl + '/select',
			{'list': elList},
			{
				response: this.querySelector('output[data-type="upload"]')
			}
		);
	}


	//
	iconSelect(ev, cEl) {
		cEl.setAttribute('aria-selected', ( cEl.getAttribute('aria-selected') === 'true' ) ? 'false' : 'true');
	}

	//
	onClick_filter(e, cBtn) {
		$('#iconList').dataset['status'] = cBtn.dataset['status'];
	}
});