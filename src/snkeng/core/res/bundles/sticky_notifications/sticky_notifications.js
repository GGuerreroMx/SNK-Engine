
//
se.addon.sticky = function(options) {
	// Default settings
	let defaults = {
			'message':'',
			'onClick':null,
			'onCreate':null,
			'speed': 'fast',	 // animations: fast, slow, or integer
			'duplicates': true,  // true or false
			'closeTime': 10000,  // integer or false
			'position':'bottom-right', // top-left, top-right, bottom-left, or bottom-right,
			'data':null
		},
		uniqID = 'sticky-'+se.uniqueId(),
		container = $('#se-sticky'),
		content = '',
		sticky = null,
		timer = null,
		// INIT
		settings = se.object.merge(defaults, options),
		// Funciones
		closeTime = (elem, time) => {
			timer = setTimeout(() => { fadeOut(elem); }, time);
		},
		fadeOut = (elem) => {
			let transEvent = transitionEndEventName();
			elem.style.transition = 'opacity 0.5s';
			elem.style.opacity = 0;
			if ( transEvent ) {
				elem.addEventListener(transEvent, (e) => {
					elem.se_remove();
				});
			} else {
				elem.se_remove();
			}
		},
		closeElem = (e, btn) => {
			e.stopPropagation();
			let elem = btn.se_closest('.sticky');
			fadeOut(elem);
		},
		checkClear = () => {

		};

	// Contenedor
	if ( !container ) {
		container = document.body.se_prepend('<div id="se-sticky" class="sticky-queue ' + settings.position + '"></div>');
	}

	// Creación
	content+= '<div class="sticky border-' + settings.position + '" id="' + uniqID + '">';
	content+= '<div class="sticky-close" title="Close"><svg class="icon inline"><use xlink:href="#fa-remove"></use></div>';
	content+= '<div class="sticky-note">' + settings.message + '</div>';
	content+= '</div>';
	container.se_prepend(content);
	//
	sticky = $('#'+uniqID);
	sticky.transition = 'opacity 0.5s';
	sticky.opacity = 1;
	// EVENTOS
	// closeTime
	if ( settings.closeTime ) {
		closeTime(sticky, settings.closeTime);
	}
	// Btn
	sticky.se_on('click', '.sticky-close', closeElem);

	// Callback data
	let response = {
		'id': uniqID,
		'position': settings.position,
		'data': settings.data
	}
	// Callback function?
	if ( typeof settings.onCreate === 'function' ) {
		settings.onCreate(response);
	}
	// Callback function?
	if ( typeof settings.onClick === 'function' ) {
		sticky.se_on('click', '.sticky-note', () => {
			settings.onClick(response);
		});
	}
	else {
		return (response);
	}
};
//
