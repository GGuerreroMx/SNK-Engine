'use_strict';
//
se.addon.webSocketCon = function(cOptions) {
	let pDefaults = {
			'url':null,
			'autoStart':false,
			'reconnect':true,
			'reconnectMaxTime':16000, // time in milliseconds
			'onConnect':null,
			'onDisconnect':null,
			'onError':null,
			'onMessage':null
		},
		pSettings = se.object.merge(pDefaults, cOptions),
		//
		wsSocket,
		debugging = false,
		//
		status = {
			connected:false,
			askClose:false,
			reconnectTime:0,
			reconnectAttempts:0
		};

	//
	function init() {
		//
		if ( pSettings.autoStart ) {
			ws_connect();
		}
	}

	//
	function ws_connect() {
		if ( !pSettings.url ) {
			ws_log('error', 'Error: url not defined', pSettings);
			return;
		}

		//
		try {
			//
			wsSocket = new WebSocket(pSettings.url);
			wsSocket.onopen = ws_open;
			wsSocket.onmessage = ws_messageIncomming;
			wsSocket.onclose = ws_close;
			wsSocket.onerror = ws_error;
		}
		catch(ex) {
			console.error("No connection to server", ex);
		}
	}

	//
	function ws_messageIncomming(msg) {
		let msgData;
		if ( msg.data.length === 0 ) {
			console.warn("incomming message empty");
		}
		//
		try {
			msgData = JSON.parse(msg.data);
		}
		catch ( err ) {
			console.log(err, msg);
			return;
		}

		//
		ws_log('log', 'Message IN', msgData);

		if ( typeof pSettings.onMessage === "function" ) {
			pSettings.onMessage(msgData);
		}
	}

	//
	function ws_messageSend(cMsg) {
		ws_log('log', 'Message OUT', cMsg);
		//
		wsSocket.send(JSON.stringify(cMsg));
	}

	//
	function ws_open(data) {
		ws_log('log', 'CONNECT:', data);
		//
		status.reconnectAttempts = 0;
		status.connected = true;

		//
		if ( typeof pSettings.onConnect === "function" ) {
			pSettings.onConnect(data);
		}
	}

	//
	function ws_close(e) {
		status.connected = false;
		if ( typeof pSettings.onDisconnect === "function" ) {
			pSettings.onDisconnect(e);
		}

		//
		if ( !e.wasClean ) {
			ws_log('error', 'CONNECTION INTERRUPTED', e);
		}

		//
		if ( !e.wasClean && pSettings.reconnect ) {
			let cTime = ( ( status.reconnectAttempts * 3 ) + 2 ) * 1000;
			cTime = ( cTime > pSettings.reconnectMaxTime ) ? pSettings.reconnectMaxTime : cTime;

			//
			ws_log('log', 'RECONNECT ATTEMPT', status.reconnectAttempts, cTime);
			console.log('RECONNECT IN: %dms', cTime);

			//
			status.reconnectAttempts++;

			//
			setTimeout(ws_connect, cTime);
		}
	}

	//
	function ws_error(data) {
		ws_log('error', 'HARD ERROR', data);
		status.connected = false;
		if ( typeof pSettings.onError === "function" ) {
			pSettings.onError(data);
		}
	}

	//
	function ws_forceClose() {
		pSettings.reconnect = false;
		wsSocket.onclose = null;
		wsSocket.close(1000);
	}

	//
	function ws_log(type, title) {
		if ( !debugging && type !== 'error') {
			return;
		}

		let cTitle = 'WS - ' + title,
			argList = [];

		for ( let i = 2; i < arguments.length; i++ ) {
			argList.push(arguments[i]);
		}

		//
		switch (type) {
			//
			case 'log':
				console.log(cTitle, argList);
				break;
			//
			case 'error':
				console.error(cTitle, argList);
				break;
			//
			default:
				console.error('WS - INCORRECT DATA')
				break;
		}

	}

	//
	init();
	return {
		'connect':ws_connect,
		'close':ws_forceClose,
		'msgSend':ws_messageSend,
	};
}
//
