"use strict";
//
se.plugin.ajaxContentSimple = function(pElement, options) {
	// Float
	let defaults = {
			jsonUrl:null,
			act_defaults:null,
			lim:null,
			cSort:null,
			//
			eIni: 0,
			eEnd: 0,
			eCur: 0,
			eLim: 20,
			eTot: 0,
			pCur: 0,
			pTot: 0,
			//
			scrollCount: 3,
			reload: false,
			isNew: false,
			scroll_load:true,
			loadFirst:true,
			//
			as_table: false,
			forms_grid:true,
			urlFilter:true,
			//
			f_id:'',
			f_type:'static',
			//
			container:null,
			actions:null,
			preProcessFunc:null
		},
		pSettings = se.object.merge(defaults, options),
		//
		pStatus = { 'loading': false, 'scrollCount': 0, 'empty': false, 'isNew':false, 'asTable':false, 'fragmet':'div'},
		advFilterBtnClose, advFilterBtnOpen, advFilterModal,
		actBar,
		filter,
		sortContent,
		cSel,
		loadMoreBtn;

	// Structure
	const content = pElement.querySelector('[se-ajaxelem="content"]'),
		response = pElement.querySelector('[se-ajaxelem="response"]'),
		templateBody = pElement.querySelector('[se-ajaxelem="template"]').se_html(),
		navBar = pElement.querySelector('[se-elem="navBar"]'),
		advFilter = pElement.querySelector('[se-elem="adv_filter"]'),

		// formBar,
		pFilter = pElement.querySelector('[se-elem="ajax_filter"]')
		;


	//<editor-fold desc="Initialization">
	function init() {
		// INITIALIZE
		pElement.se_classAdd("se_ajaxSimple");

		// Barra de navegación
		if ( navBar ) {
			//
			pStatus.asTable = true;
			// Existe la barra
			navBar.se_on('click', '[se-nav-act]', navBarOps);
			navBar.se_on('change', 'input', navBarOps);
		} else {
			// No hay barra, usar loadMore
			loadMoreBtn = pElement.se_site_coreend('<div se-ajaxelem="loadMore" class="btn_loadMore">Mostrar más</div>');
			loadMoreBtn.se_hide();
			// Funciones
			loadMoreBtn.se_on('click', (e) => {
				e.preventDefault();
				pStatus.scrollCount = 0;
				loadMore(false);
			});

			// Autoload
			if ( pSettings.eTot >= pSettings.eLim ) {
				loadMoreBtn.se_show();
				//
				if ( pSettings.scroll_load ) {
					se.windowEvent(pElement, 'scroll', scrollLoad);
				}
			}
		}

		//
		loadMore(true);
	}
	//</editor-fold>

	//<editor-fold desc="Operaciones internas">

	// scrollLoad
	function scrollLoad(ev) {
		let rect = loadMoreBtn.se_offset(),
			wintop = window.scrollY,
			winheight = window.outerHeight,
			loadMorePos = rect.y;
		if ( ((winheight + wintop) > loadMorePos) && !pStatus.empty && !pStatus.loading && pStatus.scrollCount < pSettings.scrollCount ) {
			pStatus.scrollCount++;
			loadMore(false);
		}
	}

	// Cargado
	function loadMore(vClear) {
		let fCount = 0;
		// Response
		if ( response ) {
			se.struct.notifSimple(response, 'notification', '<span>Cargando...  <svg class="icon inline spin"><use xlink:href="#fa-spinner"></use></svg></span>');
		}

		if ( pStatus.asTable ) {
			// Reinicia siempre
			content.se_html(pSettings.container.sprintf('<svg class="icon inline spin"><use xlink:href="#fa-spinner fa-spin" /></svg>'));
			if ( vClear ) {
				pSettings.pCur = 0;
				pSettings.isNew = true;
			}
		}
		else {
			// Técnica unlimited scroll
			if ( vClear ) {
				// Condición de reinicio
				content.se_html(pSettings.container.sprintf('<svg class="icon inline spin">< xlink:href="#fa-spinner fa-spin" /></svg>'));
				pSettings.pCur = 0;
				fCount = 1;
				pStatus.empty = false;
				pSettings.isNew = true;
				if ( pStatus.asTable ) {

				}
			} else {
				pSettings.pCur++;
			}
		}

		//
		pStatus.loading = true;
		let queryData = se.object.merge(
			pSettings.act_defaults,
			{
				sort:pSettings.cSort,
				lim: pSettings.lim,
				pCur:pSettings.pCur,
				fCount:fCount,
				isNew:pSettings.isNew
			}
		);


		//
		se.ajax.json(
			pSettings.jsonUrl + '?' + se.object.serialize(queryData),
			null,
			{
				onComplete:() => {
					response.se_html('');
					pStatus.loading = false;
					pSettings.isNew = false;
				},
				onSuccess:(msgs) => {
					// Borrar datos
					if ( pStatus.asTable || vClear ) {
						content.se_empty();
						pSettings.eCur = 0;
					}

					//
					if ( msgs.p.eCur !== 0 ) {
						// Agregar todos
						addMultiple(msgs.d);

						// Tabla con navegación
						if ( navBar ) {
							navBar.querySelector('span[data-count="eIni"]').se_text(msgs.p.eIni);
							navBar.querySelector('span[data-count="eEnd"]').se_text(msgs.p.eEnd);
							navBar.querySelector('input[data-count="pCur"]').value = pSettings.pCur + 1;
							//
							if ( msgs.p.isNew ) {
								navBar.querySelector('span[data-count="eTot"]').se_text(msgs.p.eTot);
								navBar.querySelector('span[data-count="pTot"]').se_text(msgs.p.pTot);
								pSettings.pTot = msgs.p.pTot;
							}
						}
						// Checar fin
						if ( loadMoreBtn ) {
							if ( msgs.p.eCur < msgs.p.eLim ) {
								loadMoreBtn.se_hide();
								pStatus.empty = true;
							} else {
								loadMoreBtn.se_show();
								pStatus.empty = false;
							}
						}
					} else {
						pStatus.empty = true;
						if ( pSettings.eCur === 0 ) {
							content.se_html(pSettings.container.sprintf('No hay resultados'));
						}
						if ( loadMoreBtn ) {
							loadMoreBtn.se_hide();
						}
					}
				}
			}
		);
	}
	//</editor-fold>

	//<editor-fold desc="Operaciones externas">

	//
	function createElement(data) {
		if ( typeof pSettings.preProcessFunc === 'function' ) {
			data = pSettings.preProcessFunc(data);
		}
		return se.struct.stringPopulate(templateBody, data);
	}

	//
	function addMultiple(msg, dirTop) {
		let tCont = document.createElement(pStatus.fragmet),
			direction = ( dirTop ) ? 'afterbegin' : 'beforeend';

		// Cargar datos
		for ( let i = 0, len = msg.length; i < len; i++) {
			pSettings.eCur++;
			tCont.insertAdjacentHTML(direction, createElement(msg[i]));
			// addElement(msgs.d[i]);
		}
		// Areglar pElementos (valores)
		se.element.fixValues(tCont);
		// Pasar
		while ( tCont.childNodes.length > 0 ) {
			if ( dirTop) {
				content.insertBefore(tCont.childNodes[0], content.children[0]);
			} else {
				content.appendChild(tCont.childNodes[0]);
			}
		}
	}

	//
	function addElement(msg, dirTop) {
		let direction = ( dirTop ) ? 'afterbegin' : 'beforeend',
			cEl;
		if ( pSettings.eCur === 0 ) { content.se_empty(); }
		content.insertAdjacentHTML(direction, createElement(msg));
		pSettings.eCur++;
		cEl = ( dirTop ) ? content.firstElementChild : content.lastElementChild;
		se.element.fixValues(cEl);
	}

	//
	function updateElement(cEl, msg) {
		let nEl = cEl.se_replaceWith(createElement(msg));
		se.element.fixValues(nEl);
		return nEl;
	}

	//
	function elementExists(id) {
		let oEl = content.querySelector('[data-objid="'+id+'"]');
		// console.log(element);
		return oEl;
	}

	//
	function updElement(msg) {
		let cData = [],
			cElemsL = pSettings.elems.length;
		//
		for ( let y = 0; y < cElemsL; y++ ) {
			cData.push(msg.d[pSettings.elems[y]]);
		}

		// Buscar pElemento original
		let oEl = content.querySelector('[data-objid="'+msg.d.id+'"]');
		if ( !oEl ) {
			console.warn("UPDELEMENT FAIL, not exists", msg.d.id);
			return;
		}

		//
		let nEl = cSel.se_replaceWith(se.struct.stringPopulate(pSettings.struct, cData));
		se.element.fixValues(nEL);
	}

	//
	function removeElement(msgs) {
		cSel.se_remove();
	}

	// Refresh
	function reload() {
		loadMore(true);
	}

	//</editor-fold>

	//
	init();

	//
	return {
		addMultiple:addMultiple,
		addElement:addElement,
		updElement:updElement,
		elementExists:elementExists,
		elementUpd:updateElement,
		removeElement:removeElement,
		reload:reload
	};
};
//
