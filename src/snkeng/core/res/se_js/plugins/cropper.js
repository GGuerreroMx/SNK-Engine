"use strict";
se.plugin.cropper = function(plugElem, options) {
	let defaults = {
			ratio:'1:1',
			min:'40x40',
			background:'#ccc;',
			onNewSel:null,
			onNewImg:null,
			autoLoad:null
		},
		cSettings = se.object.merge(defaults, options),
	//
		image = null,
		ctx = plugElem.getContext('2d'),
		eSel = {
			x:0,
			y:0,
			w:0,
			h:0,
			w_2:0,
			h_2:0,
			px:0,
			py:0,
			sx:0,
			sy:0,
			cornerSelCubeSizeHalf:5,
			cornerSelCubeSizeFull:10,
			midSelCubeSizeHalf:3,
			midSelCubeSizeFull:6,
			bHow:[false, false, false, false],
			iCSize:[this.cornerSelCubeSizeFull, this.cornerSelCubeSizeFull, this.cornerSelCubeSizeFull, this.cornerSelCubeSizeFull],
			bDrag:[false, false, false, false],
			bDragAny:false,
			bDragAll:false,
			selecting:false,
			modding:false,
			exists:false
		},
		sz = {
			set:{
				min:{ w:0, h:0 },
				ratio:{ w:0, h:0 }
			},
			real:{
				min:{ w:0, h:0 },
				ratio:{ w:0, h:0 }
			}
		},
		imgData = { scale:1, cw:0, ch:0, iw:0, ih:0 },
		imgCropData = { x:0, y:0, w:0, h:0, valid:false };
	//
	// Funciones internas;
	function init() {
		sizeSetMinText(cSettings.min);
		sizeSetRatioTxt(cSettings.ratio);
		// Eventos
		plugElem.se_on({
			'mousemove':mouseMove,
			'mousedown':mouseDown,
			'mouseup':mouseUp
		});
		//
		if ( cSettings.autoLoad == null || cSettings.autoLoad === '' ) {
			cSettings.autoLoad = plugElem.se_data('autoload');
		}

		// Permitir drop
		if ( cSettings.autoLoad != null && cSettings.autoLoad !== '' ) {
			setImageBySrc(cSettings.autoLoad);
		}
	}

	//
	function selectorDraw() {
		// Image (display based on criteria)
		let cx1 = eSel.x,
			cx2 = eSel.x + eSel.w,
			cw = eSel.w,
			cy1 = eSel.y,
			cy2 = eSel.y + eSel.h,
			ch = eSel.h;
		// draw part of original image
		if ( cx1 >= 0 && cy1 >= 0 && cx2 <= imgData.cw && cy2 <= imgData.ch ) {
			imgCropData.x = Math.floor(cx1 * imgData.scale),
			imgCropData.w = Math.floor(cw * imgData.scale),
			imgCropData.y = Math.floor(cy1 * imgData.scale),
			imgCropData.h = Math.floor(ch * imgData.scale),
			imgCropData.valid = true;
			ctx.drawImage(image,
				imgCropData.x, imgCropData.y,
				imgCropData.w - 1, imgCropData.h - 1,
				eSel.x, eSel.y,
				eSel.w - 1, eSel.h - 1);
		} else {
			eSel.exists = false;
			imgCropData.valid = false;
		}
		// guides
		ctx.globalAlpha = 0.8;
		ctx.strokeStyle = '#FFF';
		ctx.lineWidth = 1;
		let hThird = Math.floor(eSel.w / 3),
			vThird = Math.floor(eSel.h / 3),
			hCenter = eSel.x + eSel.w_2,
			vCenter = eSel.y + eSel.h_2,
			cLength = 10;
		// Center
		// X
		ctx.beginPath();
		ctx.moveTo(hCenter - cLength, vCenter);
		ctx.lineTo(hCenter + cLength, vCenter);
		ctx.stroke();
		// Y
		ctx.beginPath();
		ctx.moveTo(hCenter, vCenter - cLength);
		ctx.lineTo(hCenter, vCenter + cLength);
		ctx.stroke();
		//
		ctx.setLineDash([4, 2]);
		//
		ctx.beginPath();
		ctx.moveTo(eSel.x, eSel.y + vThird);
		ctx.lineTo(eSel.x + eSel.w, eSel.y + vThird);
		ctx.stroke();
		//
		ctx.beginPath();
		ctx.moveTo(eSel.x, eSel.y + (vThird * 2) );
		ctx.lineTo(eSel.x + eSel.w, eSel.y +  (vThird * 2) );
		ctx.stroke();
		//
		ctx.beginPath();
		ctx.moveTo(eSel.x + hThird, eSel.y);
		ctx.lineTo(eSel.x + hThird, eSel.y + eSel.h);
		ctx.stroke();
		//
		ctx.beginPath();
		ctx.moveTo(eSel.x + ( hThird * 2 ), eSel.y);
		ctx.lineTo(eSel.x + ( hThird * 2 ), eSel.y + eSel.h);
		ctx.stroke();
		// Main rectangle
		ctx.setLineDash([]);
		ctx.strokeStyle = '#000';
		ctx.lineWidth = 2;
		ctx.strokeRect(eSel.x, eSel.y, eSel.w, eSel.h);
		//
		// draw resize cubes
		ctx.fillStyle = '#fff';
		ctx.fillRect(eSel.x - eSel.cornerSelCubeSizeHalf, eSel.y - eSel.cornerSelCubeSizeHalf, eSel.cornerSelCubeSizeFull, eSel.cornerSelCubeSizeFull);
		ctx.fillRect(eSel.x + eSel.w - eSel.cornerSelCubeSizeHalf, eSel.y - eSel.cornerSelCubeSizeHalf, eSel.cornerSelCubeSizeFull, eSel.cornerSelCubeSizeFull);
		ctx.fillRect(eSel.x + eSel.w - eSel.cornerSelCubeSizeHalf, eSel.y + eSel.h - eSel.cornerSelCubeSizeHalf, eSel.cornerSelCubeSizeFull, eSel.cornerSelCubeSizeFull);
		ctx.fillRect(eSel.x - eSel.cornerSelCubeSizeHalf, eSel.y + eSel.h - eSel.cornerSelCubeSizeHalf, eSel.cornerSelCubeSizeFull, eSel.cornerSelCubeSizeFull);
		//
		ctx.fillRect(hCenter - eSel.midSelCubeSizeHalf, eSel.y  - eSel.midSelCubeSizeHalf, eSel.midSelCubeSizeFull, eSel.midSelCubeSizeFull);
		ctx.fillRect(eSel.x + eSel.w - eSel.midSelCubeSizeHalf, vCenter - eSel.midSelCubeSizeHalf, eSel.midSelCubeSizeFull, eSel.midSelCubeSizeFull);
		ctx.fillRect(hCenter - eSel.midSelCubeSizeHalf, eSel.y + eSel.h - eSel.midSelCubeSizeHalf, eSel.midSelCubeSizeFull, eSel.midSelCubeSizeFull);
		ctx.fillRect(eSel.x - eSel.midSelCubeSizeHalf, vCenter - eSel.midSelCubeSizeHalf, eSel.midSelCubeSizeFull, eSel.midSelCubeSizeFull);
		//
		ctx.globalAlpha = 1;
		croperReport();
	}

	//
	function reDraw() {
		//
		ctx.clearRect(0, 0, ctx.canvas.width, ctx.canvas.height);
		ctx.fillStyle = '#E5E5E5';
		ctx.fillRect(0, 0, ctx.canvas.width, ctx.canvas.height);
		ctx.drawImage(image, 0, 0, imgData.cw, imgData.ch);
		//
		if ( eSel.exists ) {
			// and make it darker
			ctx.fillStyle = 'rgba(0, 0, 0, 0.5)';
			ctx.fillRect(0, 0, ctx.canvas.width, ctx.canvas.height);
			// draw selection
			selectorDraw();
		}
	}

	//
	function selectorUpdate(x,y,w,h){
		w = ( typeof w !== 'undefined' ) ? w : eSel.w;
		h = ( typeof h !== 'undefined' ) ? h : eSel.h;
		let x2 = x + w, y2 = y + h;
		if ( x >= 0 && y >= 0 && x2 <= imgData.cw && y2 <= imgData.ch ) {
			eSel.x = x;
			eSel.y = y;
			eSel.w = w;
			eSel.h = h;
			eSel.w_2 = Math.floor(eSel.w / 2);
			eSel.h_2 = Math.floor(eSel.h / 2);
			eSel.exists = true;
			return true;
		} else {
			return false;
		}
	}

	//
	function sizeFix(w, h) {
		// Size
		if ( w < sz.real.min.w ) {
			w = sz.real.min.w;
		}
		if ( h < sz.real.min.h ) {
			h = sz.real.min.h;
		}
		// Ratio
		if ( sz.real.ratio !== null && ( h !== 0 || w !== 0) ) {
			if ( h < w ) {
				h = Math.round(w * sz.real.ratio.h / sz.real.ratio.w);
			} else {
				w = Math.round(h * sz.real.ratio.w / sz.real.ratio.h);
			}
		}
		return {w:w, h:h};
	}

	//
	function sizeSetRatioTxt(ratioTxt) {
		let pos = ratioTxt.indexOf(':');
		if ( pos !== -1 ) {
			sz.set.ratio = {
				w:parseInt(ratioTxt.substring(0, pos)),
				h:parseInt(ratioTxt.substring(pos + 1))
			};
		} else {
			sz.set.ratio = null;
		}
		sizeFixRatio();
	}

	//
	function sizeFixRatio(){
		// sz.real.ratio.w = Math.ceil(sz.set.ratio.w / imgData.scale);
		// sz.real.ratio.h = Math.ceil(sz.set.ratio.h / imgData.scale);
		if ( sz.set.ratio ) {
			sz.real.ratio = {
				w:sz.set.ratio.w,
				h:sz.set.ratio.h
			};
		} else {
			sz.real.ratio = null;
		}
	}

	//
	function sizeSetMinText(minTxt) {
		let pos = minTxt.indexOf('x');
		if ( pos !== -1 ) {
			sz.set.min = {
				w:parseInt(minTxt.substring(0, pos)),
				h:parseInt(minTxt.substring(pos + 1))
			};
		} else {
			sz.set.min = { w:10, h:10 };
		}
		sizeFixMin();
	}

	//
	function sizeFixMin() {
		sz.real.min.w = Math.ceil(sz.set.min.w / imgData.scale);
		sz.real.min.h = Math.ceil(sz.set.min.h / imgData.scale);
	}

	//
	function croperReset() {
		eSel.x = 0;
		eSel.y = 0;
		eSel.w = 0;
		eSel.h = 0;
		eSel.w_2 = 0;
		eSel.h_2 = 0;
		eSel.exists = false;
		//
		imgCropData.x = 0;
		imgCropData.y = 0;
		imgCropData.w = 0;
		imgCropData.h = 0;
		imgCropData.valid = false;
		croperReport();
		reDraw();
	}

	//
	function croperReport(){
		// callBack
		if ( eSel.modding && typeof cSettings.onNewSel === 'function' ) {
			cSettings.onNewSel(imgCropData);
		}
	}

	// Eventos
	function mouseMove(event) {
		let canvasOffset = plugElem.se_offset(),
			iMouseX = Math.floor(event.pageX - canvasOffset.x),
			iMouseY = Math.floor(event.pageY - canvasOffset.y),
			x, y, w, h, sf, i;
		if ( eSel.exists ) {
			// Drag selector
			if ( eSel.bDragAll ) {
				// Moviendo elemento
				let x1 = iMouseX - eSel.px,
					y1 = iMouseY - eSel.py,
					x2 = x1 + eSel.w,
					y2 = y1 + eSel.h;
				//
				if ( x1 > 0 && x2 < imgData.cw ) {
					// within bounds
					eSel.x = iMouseX - eSel.px;
				} else if ( x1 < 0 ) {
					eSel.x = 0;
					eSel.px = iMouseX;
				} else if ( x2 > imgData.cw ) {
					eSel.x = imgData.cw - eSel.w;
				}
				//
				if ( y1 > 0 && y2 < imgData.ch ) {
					eSel.y = iMouseY - eSel.py;
				} else if ( y1 < 0 ) {
					eSel.y = 0;
					eSel.py = iMouseY;
				} else if ( y2 > imgData.ch ) {
					eSel.y = imgData.ch - eSel.h;
				}
				plugElem.style.cursor = 'grabbing';
			}
			// Selecting
			else if ( eSel.selecting ) {
				// nueva selecciòn
				w = Math.abs(iMouseX - eSel.sx);
				h = Math.abs(iMouseY - eSel.sy);
				sf = sizeFix(w,h);
				if ( iMouseX > eSel.sx ) {
					x = eSel.sx;
				} else {
					if ( w < sf.w ) {
						x = eSel.sx - sf.w;
					} else {
						x = iMouseX;
					}
				}
				//
				if ( iMouseY > eSel.sy ) {
					y = eSel.sy;
				} else {
					if ( h < sf.h ) {
						y = eSel.sy - sf.h;
					} else {
						y = iMouseY;
					}
				}
				selectorUpdate(x, y, sf.w, sf.h);
			}
			// Resizing
			else if ( eSel.bDragAny ) {
				let iFW = 0, iFH = 0, iFX, iFY;
				// nueva selecciòn
				w = Math.abs(iMouseX - eSel.sx);
				h = Math.abs(iMouseY - eSel.sy);
				sf = sizeFix(w, h);
				// Corner selectors
				if ( eSel.bDrag[0] ) {
					// Sólo operar si esta del lado superior
					if ( iMouseX < eSel.sx && iMouseY < eSel.sy ) {
						if ( w < sf.w ) {
							x = eSel.sx - sf.w;
						} else {
							x = iMouseX;
						}
						if ( h < sf.h ) {
							y = eSel.sy - sf.h;
						} else {
							y = iMouseY;
						}
					}
				} else if ( eSel.bDrag[1] ) {
					// Sólo operar si esta del lado superior
					if ( iMouseX > eSel.sx && iMouseY < eSel.sy ) {
						x = eSel.sx;
						if ( h < sf.h ) {
							y = eSel.sy - sf.h;
						} else {
							y = iMouseY;
						}
					}
				} else if ( eSel.bDrag[2] ) {
					// Sólo operar si esta del lado superior
					if ( iMouseX > eSel.sx && iMouseY > eSel.sy ) {
						x = eSel.sx;
						y = eSel.sy;
					}
				} else if ( eSel.bDrag[3] ) {
					// Sólo operar si esta del lado superior
					if ( iMouseX < eSel.sx && iMouseY > eSel.sy ) {
						if ( w < sf.w ) {
							x = eSel.sx - sf.w;
						} else {
							x = iMouseX;
						}
						y = eSel.sy;
					}
				}
				// Mid Selectors
				else if ( eSel.bDrag[4] ) {
					if ( iMouseY < eSel.sy ) {
						x = eSel.sx;
						if ( h < sf.h ) {
							y = eSel.sy - sf.h;
						} else {
							y = iMouseY;
						}
					}
				} else if ( eSel.bDrag[5] ) {
					y = eSel.sy;
					if ( iMouseX > eSel.sx ) {
						x = eSel.sx;
					}
				} else if ( eSel.bDrag[6] ) {
					x = eSel.sx;
					if ( iMouseY > eSel.sy ) {
						y = eSel.sy;
					}
				} else if ( eSel.bDrag[7] ) {
					y = eSel.sy;
					if ( iMouseX < eSel.sx ) {
						if ( w < sf.w ) {
							x = eSel.sx - sf.w;
						} else {
							x = iMouseX;
						}
					}
				}
				selectorUpdate(x, y, sf.w, sf.h);
			}
			// No action
			else {
				// Reset resize
				for ( i = 0; i < 8; i++ ) {
					eSel.bHow[i] = false;
					// eSel.iCSize[i] = eSel.cornerSelCubeSizeFull;
				}
				// Grab
				if (
					iMouseX > eSel.x + eSel.cornerSelCubeSizeHalf &&
					iMouseX < eSel.x + eSel.w - eSel.cornerSelCubeSizeHalf &&
					iMouseY > eSel.y + eSel.cornerSelCubeSizeHalf &&
					iMouseY < eSel.y + eSel.h - eSel.cornerSelCubeSizeHalf
				) {
					plugElem.style.cursor = 'grab';
				}
				// Diagonals
				else if (
					iMouseX > eSel.x - eSel.cornerSelCubeSizeHalf &&
					iMouseX < eSel.x + eSel.cornerSelCubeSizeHalf &&
					iMouseY > eSel.y - eSel.cornerSelCubeSizeHalf &&
					iMouseY < eSel.y + eSel.cornerSelCubeSizeHalf
				) {
					eSel.bHow[0] = true;
					// eSel.cornerSelCubeSizeHalf = eSel.cornerSelCubeSizeHalf;
					plugElem.style.cursor = 'nw-resize';
				} else if (
					iMouseX > eSel.x + eSel.w - eSel.cornerSelCubeSizeHalf &&
					iMouseX < eSel.x + eSel.w + eSel.cornerSelCubeSizeHalf &&
					iMouseY > eSel.y - eSel.cornerSelCubeSizeHalf &&
					iMouseY < eSel.y + eSel.cornerSelCubeSizeHalf
				) {
					eSel.bHow[1] = true;
					// eSel.cornerSelCubeSizeHalf = eSel.cornerSelCubeSizeHalf;
					plugElem.style.cursor = 'ne-resize';
				} else if (
					iMouseX > eSel.x + eSel.w - eSel.cornerSelCubeSizeHalf &&
					iMouseX < eSel.x + eSel.w + eSel.cornerSelCubeSizeHalf &&
					iMouseY > eSel.y + eSel.h - eSel.cornerSelCubeSizeHalf &&
					iMouseY < eSel.y + eSel.h + eSel.cornerSelCubeSizeHalf
				) {
					eSel.bHow[2] = true;
					// eSel.cornerSelCubeSizeHalf = eSel.cornerSelCubeSizeHalf;
					plugElem.style.cursor = 'se-resize';
				} else if (
					iMouseX > eSel.x - eSel.cornerSelCubeSizeHalf &&
					iMouseX < eSel.x + eSel.cornerSelCubeSizeHalf &&
					iMouseY > eSel.y + eSel.h - eSel.cornerSelCubeSizeHalf &&
					iMouseY < eSel.y + eSel.h + eSel.cornerSelCubeSizeHalf
				) {
					eSel.bHow[3] = true;
					// eSel.cornerSelCubeSizeHalf = eSel.cornerSelCubeSizeHalf;
					plugElem.style.cursor = 'sw-resize';
				}
				// horizontals
				else if (
					iMouseX > eSel.x + eSel.w_2 - eSel.midSelCubeSizeHalf &&
					iMouseX < eSel.x + eSel.w_2 + eSel.midSelCubeSizeHalf &&
					iMouseY > eSel.y - eSel.midSelCubeSizeHalf &&
					iMouseY < eSel.y + eSel.midSelCubeSizeHalf
				) {
					eSel.bHow[4] = true;
					// eSel.cornerSelCubeSizeHalf = eSel.cornerSelCubeSizeHalf;
					plugElem.style.cursor = 'n-resize';
				} else if (
					iMouseX > eSel.x + eSel.w - eSel.midSelCubeSizeHalf &&
					iMouseX < eSel.x + eSel.w + eSel.midSelCubeSizeHalf &&
					iMouseY > eSel.y + eSel.h_2 - eSel.midSelCubeSizeHalf &&
					iMouseY < eSel.y + eSel.h_2 + eSel.midSelCubeSizeHalf
				) {
					eSel.bHow[5] = true;
					// eSel.cornerSelCubeSizeHalf = eSel.cornerSelCubeSizeHalf;
					plugElem.style.cursor = 'e-resize';
				} else if (
					iMouseX > eSel.x + eSel.w_2 - eSel.midSelCubeSizeHalf &&
					iMouseX < eSel.x + eSel.w_2 + eSel.midSelCubeSizeHalf &&
					iMouseY > eSel.y + eSel.h - eSel.midSelCubeSizeHalf &&
					iMouseY < eSel.y + eSel.h + eSel.midSelCubeSizeHalf
				) {
					eSel.bHow[6] = true;
					// eSel.cornerSelCubeSizeHalf = eSel.cornerSelCubeSizeHalf;
					plugElem.style.cursor = 's-resize';
				} else if (
					iMouseX > eSel.x - eSel.midSelCubeSizeHalf &&
					iMouseX < eSel.x + eSel.midSelCubeSizeHalf &&
					iMouseY > eSel.y + eSel.h_2 - eSel.midSelCubeSizeHalf &&
					iMouseY < eSel.y + eSel.h_2 + eSel.midSelCubeSizeHalf
				) {
					eSel.bHow[7] = true;
					// eSel.cornerSelCubeSizeHalf = eSel.cornerSelCubeSizeHalf;
					plugElem.style.cursor = 'w-resize';
				}
				// Out of image
				else {
					plugElem.style.cursor = 'crosshair';
				}
			}
			//
			reDraw();
		}
	}

	//
	function mouseDown(event) {
		let canvasOffset = plugElem.se_offset(),
			iMouseX = Math.floor(event.pageX - canvasOffset.x),
			iMouseY = Math.floor(event.pageY - canvasOffset.y);
		//
		eSel.exists = true;
		eSel.modding = true;
		//
		eSel.px = iMouseX - eSel.x;
		eSel.py = iMouseY - eSel.y;
		eSel.sx = iMouseX;
		eSel.sy = iMouseY;
		// Testing
		// console.log(eSel);
		// Corner Drag
		if ( eSel.bHow[0] ) {
			eSel.px = iMouseX - eSel.x;
			eSel.py = iMouseY - eSel.y;
			eSel.sx = eSel.x + eSel.w;
			eSel.sy = eSel.y + eSel.h;
		} else if ( eSel.bHow[1] ) {
			eSel.px = iMouseX - eSel.x - eSel.w;
			eSel.py = iMouseY - eSel.y;
			eSel.sx = eSel.x;
			eSel.sy = eSel.y + eSel.h;
		} else if ( eSel.bHow[2] ) {
			eSel.px = iMouseX - eSel.x - eSel.w;
			eSel.py = iMouseY - eSel.y - eSel.h;
			eSel.sx = eSel.x;
			eSel.sy = eSel.y;
		} else if ( eSel.bHow[3] ) {
			eSel.px = iMouseX - eSel.x;
			eSel.py = iMouseY - eSel.y - eSel.h;
			eSel.sx = eSel.x + eSel.w;
			eSel.sy = eSel.y;
		}
		// Mid Drag
		else if ( eSel.bHow[4] ) {
			eSel.sx = eSel.x;
			eSel.sy = eSel.y + eSel.h;
		} else if ( eSel.bHow[5] ) {
			eSel.sx = eSel.x;
			eSel.sy = eSel.y;
		} else if ( eSel.bHow[6] ) {
			eSel.sx = eSel.x;
			eSel.sy = eSel.y;
		} else if ( eSel.bHow[7] ) {
			eSel.sx = eSel.x + eSel.w;
			eSel.sy = eSel.y;
		}
		// Center Drag
		else if (
			iMouseX > eSel.x + eSel.cornerSelCubeSizeHalf &&
			iMouseX < eSel.x + eSel.w - eSel.cornerSelCubeSizeHalf &&
			iMouseY > eSel.y + eSel.cornerSelCubeSizeHalf &&
			iMouseY < eSel.y + eSel.h - eSel.cornerSelCubeSizeHalf
		) {
			eSel.bDragAll = true;
		} else {
			// Determinar si esta dentro de la imágen...
			if ( iMouseX > 0 && iMouseX < imgData.cw - sz.real.min.w && iMouseY > 0 && iMouseY < imgData.ch - sz.real.min.h) {
				// Punto inicial
				eSel.x = iMouseX;
				eSel.y = iMouseY;
				eSel.w = sz.real.min.w;
				eSel.h = sz.real.min.h;
				eSel.w_2 = Math.floor(eSel.w / 2);
				eSel.h_2 = Math.floor(eSel.h / 2);
				eSel.selecting = true;
			}
		}

		for ( let i = 0; i < 8; i++ ) {
			if ( eSel.bHow[i] ) {
				eSel.bDrag[i] = true;
				eSel.bDragAny = true;
			}
		}
	}

	//
	function mouseUp(event) {
		let canvasOffset = plugElem.se_offset(),
			iMouseX = Math.floor(event.pageX - canvasOffset.x),
			iMouseY = Math.floor(event.pageY - canvasOffset.y);
		//
		if ( eSel.selecting && eSel.x === iMouseX && eSel.y === iMouseY) {
			eSel.exists = false;
			reDraw();
		}

		eSel.modding = false;
		eSel.bDragAll = false;
		eSel.bDragAny = false;
		eSel.selecting = false;
		for ( let i = 0; i < 8; i++ ) {
			eSel.bDrag[i] = false;
		}
		eSel.px = 0;
		eSel.py = 0;
	}

	// Imagen
	function canvasAdjust() {
		// Determinar si ha cambiado de tamaño
		let elProps = window.getComputedStyle(plugElem, null),
			nW = elProps.getPropertyValue("width"),
			nH = elProps.getPropertyValue("height");
		//
		nW = nW.substring(0, nW.length - 2);
		nH = nH.substring(0, nH.length - 2);
		if ( nW !== plugElem.width || nH !== plugElem.height ) {
			plugElem.width = nW;
			plugElem.height = nH;
		}
	}

	//
	function imageSet(cImg) {
		image = cImg;
		canvasReDraw();
	}

	//
	function canvasReDraw() {
		canvasAdjust();
		// Procesar
		let iw = image.naturalWidth,
			ih = image.naturalHeight,
			cw = plugElem.width,
			ch = plugElem.height,
			xScale = iw / cw,
			yScale = ih / ch;
		if ( xScale > 1 && yScale > 1 ) {
			if ( xScale > yScale ) {
				imgData.cw = Math.floor(iw / xScale);
				imgData.ch = Math.floor(ih / xScale);
				imgData.scale = xScale;
			} else {
				imgData.cw = Math.floor(iw / yScale);
				imgData.ch = Math.floor(ih / yScale);
				imgData.scale = yScale;
			}
		} else if ( xScale > 1 ) {
			imgData.cw = Math.floor(iw / xScale);
			imgData.ch = Math.floor(ih / xScale);
			imgData.scale = xScale;
		} else if ( yScale > 1 ) {
			imgData.cw = Math.floor(iw / yScale);
			imgData.ch = Math.floor(ih / yScale);
			imgData.scale = yScale;
		} else {
			imgData.cw = iw;
			imgData.ch = ih;
			imgData.scale = 1;
		}
		imgData.iw = iw;
		imgData.ih = ih;
		if ( typeof cSettings.onNewImg === 'function' ) {
			cSettings.onNewImg(imgData);
		}
		// Fixes
		sizeFixMin();
		sizeFixRatio();
		// Dibujar
		croperReset();
	}

	//
	function imageCrop(iniX, iniY, iniW, iniH) {
		let tCanvas = document.createElement('canvas'),
			tCtx = tCanvas.getContext("2d");
		tCanvas.width = imgCropData.w;
		tCanvas.height = imgCropData.h;
		tCtx.drawImage(image, iniX, iniY, iniW, iniH, 0, 0, iniW, iniH);
		let dataUrl = tCanvas.toDataURL("image/jpeg");
		setImageBySrc(dataUrl);
	}

	//
	function imageCropResize(iniX, iniY, iniW, iniH, endW, endH) {
		let tCanvas = document.createElement('canvas'),
			tCtx = tCanvas.getContext("2d");
		tCanvas.width = endW;
		tCanvas.height = endH;
		tCtx.drawImage(image, iniX, iniY, iniW, iniH, 0, 0, endW, endH);
		let dataUrl = tCanvas.toDataURL("image/jpeg");
		setImageBySrc(dataUrl);
	}

	//
	function imageResize(endW, endH, mode) {
		let endW2, endH2;
		mode = parseInt(mode);
		switch ( mode ) {
			case 1:
				endW2 = endW;
				endH2 = endH;
				break;
		}
		let tCanvas = document.createElement('canvas'),
			tCtx = tCanvas.getContext("2d");
		tCanvas.width = endW2;
		tCanvas.height = endH2;
		tCtx.drawImage(image, 0, 0, imgData.iw, imgData.ih, 0, 0, endW2, endH2);
		let dataUrl = tCanvas.toDataURL("image/jpeg");
		setImageBySrc(dataUrl);
	}

	// Externas
	function setImageByFile(file) {
		let reader = new FileReader();
		reader.crossOrigin = "Anonymous";
		reader.onload = (event) => {
			setImageBySrc(event.target.result);
		};
		reader.readAsDataURL(file);
	}

	//
	function setImageBySrc(src) {
		//
		let image = new Image;
		image.crossOrigin = "Anonymous";
		image.onload = () => {
			imageSet(image);
		};
		image.src = src;
	}

	//
	function getImage(callback, options) {
		let tCanvas = document.createElement('canvas'),
			tCtx = tCanvas.getContext("2d"),
			defaults = {
				limit:false,
				mW:1920,
				mH:1920,
				format:'webp',
				quality:0.95
			},
			settings = se.object.merge(defaults, options),
			iW, iH;

		// Adjustments
		if ( settings.limit ) {
			let xCoeff = imgData.iw / settings.mW,
				yCoeff = imgData.ih / settings.mH;
			if ( xCoeff > 1 && yCoeff > 1 ) {
				if ( xCoeff > yCoeff ) {
					iW = settings.mW;
					iH = imgData.ih / xCoeff;
				} else {
					iH = settings.mH;
					iW = imgData.iw / yCoeff;
				}
			} else if ( xCoeff > 1 && yCoeff <= 1 ) {
				iW = settings.mW;
				iH = imgData.ih / xCoeff;
			} else if ( xCoeff <= 1 && yCoeff > 1 ) {
				iH = settings.mH;
				iW = imgData.iw / yCoeff;
			} else {
				iW = imgData.iw;
				iH = imgData.ih;
			}
			console.log("orig:", iW, iH);
			iW = Math.floor(iW);
			iH = Math.floor(iH);
			console.log("new:", iW, iH);
		} else {
			iW = imgData.iw;
			iH = imgData.ih;
		}
		//
		tCanvas.width = iW;
		tCanvas.height = iH;
		tCtx.drawImage(image, 0, 0, imgData.iw, imgData.ih, 0, 0, iW, iH);
		//
		switch ( settings.format ) {
			//
			case 'jpg':
				tCanvas.toBlob(callback, "image/jpeg", settings.quality);
				break;
			//
			case 'webp':
				console.log("get webp format image");
				function saveCB(cImage) {
					if ( cImage.type !== 'image/webp' ) {
						console.error("WEBP NOT SUPPORTED, USING FALLBACK");
						tCanvas.toBlob(callback, "image/jpeg", 0.85);
					} else {
						callback(cImage)
					}
				}

				//
				tCanvas.toBlob(saveCB, "image/webp", settings.quality);
				break;
			//
			default:
				console.error("format not supported", settings.format);
				break;
		}
		//
	}

	//
	function imageCropExt() {
		//
		if ( imgCropData.valid ) {
			imageCrop(imgCropData.x, imgCropData.y, imgCropData.w, imgCropData.h);
		} else {
			console.log("crop no vàlido");
		}
	}

	//
	function imageCropResizeExt(endX, endY) {
		//
		if ( imgCropData.valid ) {
			imageCropResize(imgCropData.x, imgCropData.y, imgCropData.w, imgCropData.h, endX, endY);
		} else {
			console.log("crop no vàlido");
		}
	}

	//
	function imageResizeExt(endX, endY, mode) {
		//
		imageResize(endX, endY, mode);
	}

	//
	function setLimits(min, ratio) {
		sizeSetMinText(min);
		sizeSetRatioTxt(ratio);
		croperReset();
	}

	//
	function setFinalSize(x, y) {
		x = parseInt(x);
		y = parseInt(y);
		sz.set.min = { w:x, h:y };
		sz.set.ratio = { w:x, h:y };
		sizeFixMin();
		sizeFixRatio();
		croperReset();
	}

	//
	function setCrop(x,y,w,h,change) {
		console.log("ini", x,y,w,h, change);
		let valid = true;
		x = Math.ceil(x / imgData.scale);
		y = Math.ceil(y / imgData.scale);
		w = Math.ceil(w / imgData.scale);
		h = Math.ceil(h / imgData.scale);
		console.log("ini", x, y, w, h, change);
		if ( sz.real.ratio !== null && ( change === 'w' || change === 'h' ) ) {
			if ( change === 'w' ) {
				h = Math.round(w * sz.real.ratio.h / sz.real.ratio.w);
			} else {
				w = Math.round(h * sz.real.ratio.w / sz.real.ratio.h);
			}
		}
		if ( w >= sz.real.min.w && h >= sz.real.min.h ) {
			if ( selectorUpdate(x, y, w, h) ) {
				reDraw();
				return imgCropData;
			} else { console.log("fail"); }
		}
	}

	//
	function resize(x, y, method) {
		imageResize(x, y, method);
	}

	//
	function rotate(degrees) {
		ctx.clearRect(0, 0, plugElem.width, plugElem.height);
		// save the unrotated context of the canvas so we can restore it later
		// the alternative is to untranslate & unrotate after drawing
		ctx.save();

		// move to the center of the canvas
		ctx.translate(plugElem.width / 2, plugElem.height / 2);

		// rotate the canvas to the specified degrees
		ctx.rotate(degrees * Math.PI / 180);

		// draw the image
		// since the context is rotated, the image will be rotated also
		ctx.drawImage(image ,-image.width / 2, -image.width / 2);

		// we’re done with the rotating so restore the unrotated context
		ctx.restore();
	}

	//
	function flip(direction) {
		if ( direction === 'horizontal' ) {
			ctx.translate(plugElem.width, 0);
			ctx.scale(-1, 1);
		} else {
			ctx.translate(0, plugElem.height);
			ctx.scale(1, -1);
		}
		ctx.drawImage(image, 0, 0);
	}

	// Inicializar
	init();

	return {
		setImageByFile:setImageByFile,
		setImageBySrc:setImageBySrc,
		getImage:getImage,
		imageCrop:imageCropExt,
		imageCropResize:imageCropResizeExt,
		imageResize:imageResizeExt,
		setLimits:setLimits,
		setFinalSize:setFinalSize,
		setCrop:setCrop,
		resize:resize,
		rotate:rotate,
		flip:flip,
		reDraw:reDraw,
		canvasReDraw:canvasReDraw
	};
};