"use strict";
se.plugin.scrollNav = function (element, options) {
	var defaults = {
			target:'',
			lvlIni:1,
			lvlEnd:4
		},
		plugin = this;
	//
	plugin.settings = {};
	// Funciones internas;
	var init = function () {
		var menuEls = [], elTitle, menuContent = '', menuTarget;
		plugin.settings = se.object.merge(defaults, options);
		if ( !('target' in plugin.settings) ) {
			console.log('scrollNav error: target not defined.');
			return false;
		}
		menuTarget = $('#'+plugin.settings.target);
		if ( !menuTarget ) {
			console.log('scrollNav error: target not present.');
			return false;
		}
		// ID Definition
		elTitle = element.id || 'sn_' + se.uniqueId();
		//
		getTitles();
			/*
		// Process titles
		menuEls = cLevelAdd(element, elTitle, 2, menuEls);
		// Create
		if ( menuEls ) {
			menuContent = levelPrint(menuEls, menuContent);
		}
		//
		menuTarget.se_html(menuContent);
		console.log(menuEls, menuContent);
		*/
	},
	getTitles = function(cId) {
		var elements = $("h1, h2, h3, h4, h5, h6"),
			cEl,
			i,
			len = elements.length,
			menu = '',
			cId = '';
		for ( i=0; i < len; i++ ) {
			cEl = elements[i];
			cId = ( cEl.id ) ? cEl.id : 'sn_'+i;
			cEl.id = cId;
			menu+= '<a href="#'+cId+'">'+cEl.se_text()+"</a>\n";
			console.log("pass el", cEl);
		}
		$('#'+plugin.settings.target).se_html(menu);
	},
	cLevelAdd = function(parent, parentId, lvl, menuEls) {
		var elems = parent.querySelectorAll('h'+lvl),
			nLvl = lvl + 1;
		elems.se_each(function(index, cEl) {
			var cId, cMEls;
			if ( cEl.id ) {
				cId = cEl.id;
			} else {
				cId = parentId + '_'+index;
				cEl.id = cId;
			}
			// Inception
			cMEls = cLevelAdd(cEl, cId, nLvl, []);
			// Save
			menuEls[index] = {
				title:cEl.se_text(),
				id:cId,
				lvl:lvl,
				elems:cMEls
			};
		});
		return menuEls;
	},
	levelPrint = function(menuEl, str) {
		var struct = '';
		menuEl.se_each(function(index, cEl) {
			// console.log(cEl);
			str+= '<a href="#'+cEl.id+'">'+cEl.title+"</a>\n";
		});
		return str;
	};
	plugin.resize = function(x, y, method) {

	};
	// Iniciar
	init();
};