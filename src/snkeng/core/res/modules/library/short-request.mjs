//
export async function postRequest(targetUrl, postData, configuration) {
	const defaults = {
		method: 'POST',
		onRequest: null,
		onComplete: null,
		onSuccess: null,
		onFail: null,
		onError: null,
	};
	//
	let settings = Object.assign(defaults, configuration);

	// Actual parameters
	let fetchParams = {
		method:settings.method,
		headers:{}
	};

	//
	if ( postData instanceof FormData ) {
		fetchParams.body = postData;
	} else if ( typeof postData === 'object' && postData !== null ) {
		fetchParams.headers["Content-type"] = "application/json";
		fetchParams.body = JSON.stringify(postData);
	} else {
		console.error("Type of post data not supported", typeof postData, postData);
		return;
	}

	// Probably reset parameters
	if ( typeof settings.onRequest === 'function' ) {
		settings.onRequest();
	}

	//
	try {
		let fResponse = await fetch(
			targetUrl,
			fetchParams
		);

		//
		if ( typeof settings.onComplete === 'function' ) {
			settings.onComplete();
		}

		//
		let msg = await fResponse.json();


		if ( !fResponse.ok ) {
			//
			if ( typeof settings.onFail === 'function' ) {
				settings.onFail(msg);
			}
		} else {
			if ( typeof settings.onSuccess === 'function' ) {
				settings.onSuccess(msg);
			}
		}
	} catch ( e ) {
		console.error("Could not fetch.", e);
		if ( typeof settings.onError === 'function' ) {
			settings.onError(e);
		}
	}

	// Probably reset parameters
	if ( typeof settings.onComplete === 'function' ) {
		settings.onComplete();
	}
}