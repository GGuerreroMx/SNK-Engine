export function	secondsToTime(seconds) {
	// we will use this function to convert seconds in normal time format
	let secs = Math.abs(seconds),
		hr = Math.floor(secs / 3600),
		min = Math.floor((secs - (hr * 3600)) / 60),
		sec = Math.floor(secs - (hr * 3600) - (min * 60)),
		time = '';
	if ( hr !== 0 ) {
		if ( hr < 10 ) {
			time += '0';
		}
		time += hr + ':';
	}
	if ( min !== 0 ) {
		if ( min < 10 ) {
			time += '0';
		}
		time += min + ':';
	} else {
		time += '00:';
	}
	if ( sec < 10 ) {
		time += '0';
	}
	time += sec;
	if ( seconds < 0 ) {
		time = '-' + time;
	}
	return time;
}
export function	timeToSecs(text) {
	let time,
		seconds = 0,
		negative;
	text = text.trim();
	negative = ( text.charAt(0) === '-' );
	time = text.split(':');
	switch ( time.length ) {
		case 1:
			seconds = intVal(time[0]);
			break;
		case 2:
			seconds = intVal(time[0]) * 60 + intVal(time[1]);
			break;
		case 3:
			seconds = intVal(time[0]) * 3600 + intVal(time[1]) * 60 + intVal(time[2]);
			break;
	}
	if ( negative ) { seconds = -seconds; }
	return seconds;
}
export function	timePad(txt) {
	let pad = "00";
	txt = "" + txt;
	return pad.substring(0, pad.length - txt.length) + txt;
}
export function	toHHMMSS(sec_numb, hoursHide) {
	let hours = Math.floor(sec_numb / 3600),
		minutes = Math.floor((sec_numb - (hours * 3600)) / 60),
		seconds = sec_numb - (hours * 3600) - (minutes * 60);
	if ( hours < 10 ) {
		hours = "0" + hours;
	}
	if ( minutes < 10 ) {
		minutes = "0" + minutes;
	}
	if ( seconds < 10 ) {
		seconds = "0" + seconds;
	}
	if ( hoursHide ) {
		return minutes + ':' + seconds;
	} else {
		return hours + ':' + minutes + ':' + seconds;
	}
}
export function	stringToDateObject(dateString) {
	// MySQL String Date a DateObj
	let reggie = /(\d{4})-(\d{2})-(\d{2}) (\d{2}):(\d{2}):(\d{2})/,
		dateArray = reggie.exec(dateString),
		dateObject = new Date(
			(+dateArray[1]),
			(+dateArray[2]) - 1, // Careful, month starts at 0!
			(+dateArray[3]),
			(+dateArray[4]),
			(+dateArray[5]),
			(+dateArray[6])
		);
	return dateObject;
}
export function	dateObjectToString(dateObject) {
	let dtMonth = dateObject.getMonth() + 1;
	return dateObject.getFullYear().toString() + '-' +
		timePad(dtMonth.toString()) + '-' +
		timePad(dateObject.getDate().toString()) + ' ' +
		timePad(dateObject.getHours().toString()) + ':' +
		timePad(dateObject.getMinutes().toString()) + ':' +
		timePad(dateObject.getSeconds().toString());
}
