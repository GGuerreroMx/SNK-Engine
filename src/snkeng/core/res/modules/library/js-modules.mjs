export async function inlineImport(domElement) {
	// Validate input
	if ( !(domElement instanceof HTMLElement) || domElement.tagName.toLowerCase() !== 'script' ) {
		console.error("Invalid inline import, not script.");
		return;
	}

	let isDebug = domElement.dataset['debug'];

	let jsCode = domElement.textContent;

	if ( isDebug ) {
		jsCode = jsCode.replaceAll('/snkeng/', window.location.origin + '/snkeng/');
	} else {
		// Replace common used local called functions.
		const arrReplace = [
			['/snkeng/site_core/res/modules/', '/res/modules/site_core/'],
			['/snkeng/core/res/modules/', '/res/modules/core/'],
			['/snkeng/admin_core/res/modules/', '/res/modules/admin_core/'],
		];
		for ( let i = 0; i < arrReplace.length; i++ ) {
			let cArr = arrReplace[i];
			jsCode = jsCode.replaceAll(cArr[0], window.location.origin + cArr[1]);
		}
	}

	// Actual conversion to a blob and import
	const blob = new Blob([jsCode], {
		type: 'text/javascript'
	});
	const blobURL = URL.createObjectURL(blob);
	return await import(blobURL);
}