import './../components-simple/se-async-form.mjs'
import * as domManipulation from './../library/dom-manipulation.mjs'
import * as siteCore from './../library/site-core.mjs'

let regexpsimple = {
	'decimal':'[-+]?[0-9]+(\\.[0-9]+)?',
	'number':'[0-9]',
	'letter':'[A-z]',
	'names':'[A-z\\u00C0-\\u00ff ]',
	'password':'[A-z0-9.,_\\-\\!\\¡\\?\\¿]',
	'urlnames':'[A-z0-9._\\-]',
	'simpleTitle':'[A-z\\u00C0-\\u00ff0-9\\-., _¡!¿?]',
	'simpleText':'[A-z\\u00C0-\\u00ff0-9\\.\\,\\¡\\!\\¿\\?\\(\\)\\[\\]\\-\\_\\s\\"\\\'\\%\\&\\$\\#\\\]',
	'urlTitle':'[A-z0-9\\-]',
	'urlSimple':'[A-z0-9/\\&\\_\\.\\\\-\\?]',
	'urlPath':'[A-z0-9-_/]+',
	'datetime-local':'[0-2][0-9]{3}\\-[0-1][0-9]\\-[0-3][0-9] [0-5][0-9]\\:[0-5][0-9]\\:[0-5][0-9]',
	'time':'[0-9]{1,2}:[0-9]{1,2}',
	'color':'#[a-fA-F0-9]{6}',
	'email':'[A-z0-9_\\-\\.]+[@][A-z0-9_-]+([.][A-z0-9_-]+)*[.][A-z]{2,4}'
};

//
export function createForm(formData) {
	let defaults = {
			'method':'POST',
			'defaults':null,
			'load_url':'',
			'load_data':null,
			'save_url':'',
			'save_data':null,
			'actName':'Guardar',
			'class':'se_form',
			'enctype':'multipart/form-data',
			'is': 'se-async-form',
			'elems':[],
			'cData':[]
		},
		settings = Object.assign(defaults, formData);


	//
	let cFormObj = {
		type:'form',
		attr:{
			'class': settings.class,
			'method': settings.method,
			'action': settings.save_url,
			'enctype': settings.enctype,
			'is': settings.is,
			'data-loadurl': settings.load_url
		},
		content:[]
	};

	// Form elements to transform
	if ( settings.elems !== null && typeof settings.elems === 'object' ) {
		for ( let cElemName in settings.elems ) {
			let cElem = settings.elems[cElemName];

			// Hidden elements, are just appended
			if ( cElem.ftype === 'hidden' ) {
				cFormObj.content.push(
					{
						type: 'input',
						attr: {
							type: 'hidden',
							name: cElemName,
							value: cElem.attributes?.default_value ?? null
						}
					}
				);
			} else {
				//
				let dataBox = {
					type: 'div',
					attr: {
						class: 'cont'
					},
					content: [
						{
							type: 'span',
							attr: {
								class: 'title'
							},
							content: cElem.info_name
						}
					]
				};

				//
				if ( 'fdesc' in cElem ) {
					dataBox.content.push({
						type: 'span',
						attr: {
							class: 'desc'
						},
						content: cElem.info_description
					});
				}

				//
				let attrList = {
					name: cElemName,
					type: cElem.attributes?.type ?? 'text',
					value: cElem.attributes?.default_value ?? null,
					required: ( cElem.required ),
					placeholder: cElem.attributes?.placeholder ?? null,
					'data-type': cElem.attributes?.data_type ?? null,
					'data-regex': cElem.attributes?.['data-regex'] ?? null,
					regex: cElem.attributes?.regex ?? null,
					pattern: cElem.attributes?.pattern ?? null,
					minlength: cElem.attributes?.minlength ?? null,
					maxlength: cElem.attributes?.maxlength ?? null,
					min: cElem.attributes?.min ?? null,
					max: cElem.attributes?.max ?? null,
					step: cElem.attributes?.step ?? null,
					is: cElem.attributes?.is ?? null,
					accept: cElem.attributes?.accept ?? null,
					cols: cElem.attributes?.cols ?? null,
					rows: cElem.attributes?.rows ?? null,
				};

				//
				switch ( cElem.field_type ) {
					// Group of input type with text with no special operations
					case 'input':
						//
						switch ( attrList.type ) {
							//
							case 'text':
								if ( attrList['data-regexp'] && !attrList.regex ) {
									attrList.regex = regexpsimple[attrList['data-regexp']];
								}
								if ( attrList.regex && !attrList.pattern ) {
									attrList.pattern = regexpsimple[attrList.regex] + "*";
								}
								break;
							//
							case 'money':
							case 'moneyInt':
								attrList.type = 'number';
								attrList.min = attrList.min || '0.00';
								attrList.max = attrList.max || '100000.00';
								attrList.step = attrList.step || '0.01';
								break;
							//
							case 'date':
							case 'time':
							case 'datetime-local':
								attrList.is = 'calendarInput';
								break;
							//
							case 'checkbox':
								attrList.value = cElem.attributes?.value ?? 1;
								break;
							//
							case 'color':
								attrList.regex = attrList.regex || '#[a-fA-F0-9]{6}';
								attrList.pattern = attrList.pattern || '#FFFFFFF';
								break;
							//
							case 'url':
								attrList.pattern = attrList.pattern || regexpsimple.url;
								break;
							//
							case 'email':
								attrList.pattern = attrList.pattern || regexpsimple.email;
								break;
						}

						//
						cFormObj.content.push(
							{
								type: 'label',
								attr: {
									class: 'separator',
									'data-required': (attrList.requiered) ? '1' : '0',
								},
								content: [
									dataBox,
									{
										type: 'input',
										attr: attrList
									}
								]
							}
						);
						break;

					//
					case 'textarea':
					case 'textareaesp':
						//
						cFormObj.content.push(
							{
								type: 'label',
								attr: {
									class: 'separator',
									'data-required': (attrList.requiered) ? '1' : '0',
								},
								content: [
									dataBox,
									{
										type: 'textarea',
										attr: attrList,
										content: attrList.default_value
									}
								]
							}
						);
						break;

					//
					case 'checkbox':
						//
						cFormObj.content.push(
							{
								type: 'label',
								attr:
								{
									class: 'checkbox',
								},
								content: [
									{
										type: 'input',
										attr: attrList
									},
									dataBox,
								]
							}
						);
						break;

					//
					case 'radio':
						//
						let cRadioEls = [];
						for ( let cVal in cElem.attributes.opts ) {
							//
							cRadioEls.push({
								type: 'label',
								content: [
									{
										type: 'input',
										attr: {
											type: 'radio',
											name: attrList.name,
											value: cVal,
											required: (cElem.requiered) ? 'required' : null,
											selected: (cVal === cElem.default_value) ? 'checked' : null
										},
										content: cElem.opts[cVal]
									}
								]
							});
						}

						//
						cFormObj.content.push(
							{
								type: 'div',
								attr: {
									class: 'cont radioList',
									'data-required': (cElem.requiered) ? '1' : '0',
								},
								content: [
									dataBox,
									{
										type: 'div',
										attr: {
											class: '',
										},
										content: cRadioEls
									}
								]
							}
						);
						break;

					//
					case 'list':
					case 'menu':
						//
						let cListEls = [];
						for ( const cIndex in cElem.attributes?.options ) {
							//
							cListEls.push({
								type: 'option',
								attr: {
									value: cIndex,
									selected: (cIndex === cElem.default_value) ? 'selected' : null
								},
								content: cElem.attributes.options[cIndex]
							});
						}

						//
						delete attrList.type;

						//
						cFormObj.content.push(
							{
								type: 'label',
								attr: {
									class: 'cont radioList',
									'data-required': (cElem.requiered) ? '1' : '0',
								},
								content: [
									dataBox,
									{
										type: 'select',
										attr: attrList,
										content: cListEls
									}
								]
							}
						);
						break;

					//
					default:
						console.log("Form type not defined", cElem);
						//
						cFormObj.content.push(
							{
								type: 'div',
								attr: {
									class: 'error',
								},
								content: 'No proper content'
							}
						);
						break;
				}
			}
		}
	}
	// Element in object form (name of element is separated and more options are taken in sub sections) 
	else if ( typeof settings.elems === 'object' ) {

	} else {
		console.error("Elements not correctly defined.");
		return;
	}
	//
	cFormObj.content.push(
		{
			type: 'button',
			attr: {
				type: 'submit',
			},
			content: settings.actName
		}
	);
	cFormObj.content.push(
		{
			type: 'output',
			attr: {
				'se-elem': 'response'
			}
		}
	);

	return domManipulation.buildFromObject(cFormObj);
}

export class Modal {
	element = null;
	constructor(options) {
		//
		let defaults = {
			title: null,
			closable: true,
			content: null,
			footer: null,
			method:null,
			fullScreen:false
		};
		this.options = Object.assign(defaults, options);

		//
		this.close = this.forceClose;

		//
		if ( this.element == null ) {
			this.element = document.createElement('dialog');
			this.element.classList.add('asWindow');
			if ( this.options.fullScreen ) {
				this.element.classList.add('fullScreen');
			}
			this.element.innerHTML = `
				<div class="wHead"><div class="title">${this.options.title}</div></div>
				<div class="wBody"></div>
				<div class="wFoot"></div>
            `;
			document.body.prepend(this.element);

			this.element.showModal();
		}

		// Add closing option (for window type)
		if ( this.options.closable ) {
			this.element.querySelector('.wHead').insertAdjacentHTML('beforeend', `<button data-modal-onclick="close" class="close"><svg class="icon inline"><use xlink:href="#fa-remove" /></svg></button>`)
		}

		this.elBody = this.element.querySelector('.wBody')
		this.elFooter = this.element.querySelector('.wFoot')

		//
		if ( this.options.content ) {
			this.elBody.innerHTML = this.options.content;
		}
		//
		if ( this.options.footer ) {
			this.elFooter.innerHTML = this.options.footer;
		}
		//
		this._eventHandlers();
	}

	_eventHandlers() {
		this.element.querySelectorAll('[data-modal-onclick="close"]').forEach(element => {
			element.onclick = event => {
				event.preventDefault();
				this.element.close();
			};
		});

		//
		this.element.addEventListener("close", (event) => {
			this.element.remove();
		});
	}

	//
	forceClose() {
		this.element.close();
		this.element.remove();
	}

	//
	static async page(pageUrl, fullScreen = false, onLoad = null) {
		// Request page
		const fResponse = await fetch(pageUrl);

		// Validate content type
		const contentType = fResponse.headers.get('content-type');
		if ( !contentType || !contentType.includes('application/json') ) {
			throw new TypeError("Response is not valid.");
		}

		if ( !fResponse.ok ) {
			const message = `An error has occured: ${fResponse.status}`;
			throw new Error(message);
		}

		const pageData = await fResponse.json();

		// Setup modal
		let modal = await new Modal({
			title: "Seleccionar imagen",
			content: pageData.d.body,
			fullScreen: fullScreen
		});

		//
		siteCore.fileLoadAsync(pageData.d, null, () => {
			if ( typeof onLoad === 'function' ) {
				onLoad();
			}
		});

		console.warn("is modal", modal);

		//
		return modal;
	}

	static form(formProperties, onSuccess, onCancel) {
		let formHTML = createForm(formProperties);

		// Setup modal
		let modal = new Modal({
			content: formHTML,
			title: formProperties.title,
			footer: '<button class="btn wide red" data-modal-onclick="close"><svg class="icon inline mr"><use xlink:href="#fa-remove" /></svg>Cancelar</button>'
		});

		//
		if ( typeof onSuccess === 'function' ) {
			modal.element.querySelector('form[is="se-async-form"]').dispatchEvent(
				new CustomEvent("setCallBacks", {
					detail: {
						onSuccess: onSuccess,
						onCancel: onCancel
					}
				}),
			);
		}
	}

	static confirm(value, success, cancel) {
		let modal = new Modal({
			content: value,
			header: '',
			footer: '<button class="success">OK</button><button class="cancel alt">Cancel</button>'
		});
		modal.footerElement.querySelector('.success').onclick = event => {
			event.preventDefault();
			if ( success ) success();
			modal.close();
		};
		modal.footerElement.querySelector('.cancel').onclick = event => {
			event.preventDefault();
			if ( cancel ) cancel();
			modal.close();
		};
		modal.open();
	}

	static alert(value, success) {
		let modal = new Modal({content: value, header: '', footer: '<button class="success">OK</button>'});
		modal.footerElement.querySelector('.success').onclick = event => {
			event.preventDefault();
			if ( success ) success();
			modal.close();
		};
		modal.open();
	}

	static prompt(value, def, success, cancel) {
		let modal = new Modal({
			header: '',
			footer: '<button class="success">OK</button><button class="cancel alt">Cancel</button>'
		});
		modal.content = value + `<div class="prompt-input"><input type="text" value="${def}" placeholder="Enter your text..."></div>`;
		modal.footerElement.querySelector('.success').onclick = event => {
			event.preventDefault();
			if ( success ) success(modal.contentElement.querySelector('.prompt-input input').value);
			modal.close();
		};
		modal.footerElement.querySelector('.cancel').onclick = event => {
			event.preventDefault();
			if ( cancel ) cancel(modal.contentElement.querySelector('.prompt-input input').value);
			modal.close();
		};
		modal.open();
	}

}