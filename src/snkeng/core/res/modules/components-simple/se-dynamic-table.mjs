/*
Dynamic table option, merges other usage
*/
import * as formCreator from './../library/form-creator.mjs'
import * as formOps from './../library/forms.mjs'
import * as domManipulator from './../library/dom-manipulation.mjs'
import * as jsModules from './../library/js-modules.mjs'

//
customElements.define('se-dynamic-table', class extends HTMLElement {
	// Elements
	el_filter_advanced = this.querySelector('div[data-element="filterAdvanced"]');
	el_top_navigation = this.querySelector('div[data-element="topNavigation"]');
	el_filter_basic = this.querySelector('[data-element="filter_basic"]');
	el_async_content = this.querySelector('se-async-content');
	navigationProps = {
		eIni: 0,
		eEnd: 0,
		eCur: 0,
		eLim: 20,
		eTot: 0,
		pCur: 0,
		pTot: 0,
	};
	settings = {
		navigation:{
			lim:25,
			cPage:1,
			tPage:1,
		},
		actions:null,
		defaults: {}
	};
	extraActions = null;
	extraFunctions = null;


	//
	constructor() {
		super();
	}

	//
	async connectedCallback() {
		//<editor-fold desc="Read variables from de json">
		let settingEl = this.querySelector(':scope > script[se-elem="defaults"]');
		if ( !settingEl ) {
			console.error("Cant load dynamic table properties.");
			return;
		}

		let tDefaults;
		try {
			tDefaults = Function('"use strict";return (' + settingEl.innerHTML + ')')();
		} catch(e) {
			console.error("INVALID DEFAULT VARIABLES:", e);
			console.error(settingEl.innerHTML);
			return;
		}


		if ( typeof tDefaults === 'object' ) {
			this.extraActions = {...tDefaults.actions};
		} else {
			console.error("Incorrect data from default load.", tDefaults);
			return;
		}
		//</editor-fold>

		//<editor-fold desc="Build menu elements">
		let menuBtns = '';
		//
		for ( const [action, cAct] of Object.entries(this.extraActions) ) {
			// Botones
			if ( 'menu' in cAct ) {
				let btnTxt = '<button class="btn" title="' + name + '" data-action="' + action + '">';
				btnTxt += ( typeof cAct.menu.icon !== 'undefined' && cAct.menu.icon !== '' ) ? '<svg class="icon inline mr"><use xlink:href="#' + cAct.menu.icon + '"></use></svg>' : '';
				btnTxt += cAct.menu.title + '</button>';
				//
				menuBtns += btnTxt;
			}
		}

		//
		if ( menuBtns !== '' ) {
			this.insertAdjacentHTML('afterbegin', '<div data-elem="actBar" class="menu">' + menuBtns + '</div>');
			// Definition
			let actBar = this.querySelector('[data-elem="actBar"]');
			actBar.se_on('click', '[data-action]', (e, elem) => {
				this.generalButtonSolver(e, elem, 'menu');
			});
		}
		//</editor-fold>

		// Bindings
		this.addEventListener('click', this.onClickCallBacks.bind(this));
		if ( this.el_filter_basic ) {
			this.el_filter_basic.addEventListener('change', this.updateFilter.bind(this));
		}

		//
		this.el_top_navigation.querySelector('input[data-count="pCur"]').addEventListener('change', this.selectPage.bind(this));

		//
		window.customElements.whenDefined('se-async-content').then(() => {
			this.el_async_content.dispatchEvent(
				new CustomEvent("navPropCB", {
					detail: {
						cbFunction: this.navigationCallBack.bind(this)
					},
				}),
			);
		});

		//
		// Inputs
		this.el_async_content.se_on('change', ['input', 'select'], (e, cObj) => {
			let cElem = cObj.closest('[data-element="row"]');
			cElem.classList.add('mod');
		});
		// Forms
		this.el_async_content.se_on('submit', 'form[data-action]', this.inlineFormAction.bind(this));

		// The following is for side importing extra javascript operations related to the table.
		const cJSModule = this.querySelector('* > script[type="module"]');

		//
		if ( cJSModule ) {
			this.extraFunctions = await jsModules.inlineImport(cJSModule);
		}

	}

	//
	updateFilter(e) {
		let cEl = e.target,
			cName = cEl.name,
			cValue = cEl.value;

		//
		this.el_async_content.dispatchEvent(
			new CustomEvent("filterMod", {
				detail: {
					type:'filter',
					name: cName,
					value: cValue
				}
			}),
		);
	}

	//
	onClickCallBacks(e) {
		let cBtn = e.target.closest('[data-dyntab-onclick]');
		if ( !cBtn ) { return; }

		//
		let operation = 'onClick_' + cBtn.dataset['dyntabOnclick'];

		//
		if ( typeof this[operation] !== 'function' ) {
			console.error("operación no definida.", operation, cBtn);
			return;
		}

		//
		this[operation](e, cBtn);
	}

	//
	onClick_navigation(e, cBtn) {
		this.el_async_content.dispatchEvent(
			new CustomEvent("externalOperation", {
				detail: { operation:cBtn.dataset['action'] },
			}),
		);
	}

	//
	onClick_element_op(e, cBtn) {
		// event.preventDefault();
		//
		this.generalButtonSolver(e, cBtn, 'obj');
	}

	//
	navigationCallBack(cData) {
		// Update elements
		domManipulator.updateChildren(this.el_top_navigation, {
			'span[data-count="eIni"]' : ['text', cData.eIni],
			'span[data-count="eEnd"]' : ['text', cData.eEnd],
			'span[data-count="eTot"]' : ['text', cData.eTot],
			'meta[data-count="eCur"]' : ['attr', 'content', cData.eCur],
			'span[data-count="pTot"]' : ['text', cData.pTot],
			'input[data-count="pCur"]' : [
				['attr', 'value', cData.pCur + 1],
				['attr', 'max', cData.pTot],
			]
		});
	}

	//
	async generalButtonSolver(e, cBtn, origin) {
		//
		let action = cBtn.dataset['action'],
			shortElem = this.extraActions[action],
			targetRow,
			objId;
		//
		if ( typeof shortElem === 'undefined' ) {
			console.error("DynTable: Extra action operation not defined:'%s'.", action);
			console.warn(this.extraActions);
			return;
		}

		// Get object properties
		if ( origin === 'obj' ) {
			targetRow = cBtn.closest('[data-element="row"]');
			objId = targetRow.dataset['objid'];
		}

		//
		if ( shortElem['ajax-elem'] !== 'obj' || ( shortElem['ajax-elem'] === 'obj' && origin === 'obj' ) ) {
			switch ( shortElem.action ) {
				// Formulario
				case 'form':
					let cForm = shortElem.form,
						formLoadUrl = ( 'load_url' in cForm ) ? cForm.load_url : null,
						formLoadData = ( 'load_data' in cForm ) ? cForm.load_data : null,
						formSaveUrl = ( 'save_url' in cForm ) ? cForm.save_url : null,
						formSaveData = ( 'save_data' in cForm ) ? cForm.save_data : null,
						actName = ( 'actName' in cForm ) ? cForm.actName : null,
						onSuccess = null;

					// Determine element actions by type
					if ( shortElem['ajax-elem'] && origin === 'obj' && shortElem['ajax-action'] === 'edit' ) {
						let loadDataId = null,
							saveDataId = null;

						// update urls if needed or add to post information (old method)
						if ( formLoadUrl && formLoadUrl.includes('{id}') ) {
							formLoadUrl = formLoadUrl.replace('{id}', objId);
						} else {
							loadDataId = {'id': objId};
						}
						//
						if ( formSaveUrl && formSaveUrl.includes('{id}') ) {
							formSaveUrl = formSaveUrl.replace('{id}', objId);
						} else {
							saveDataId = {'id': objId};
						}

						//
						formLoadData = Object.assign(this.settings.defaults, loadDataId, formLoadData);
						formSaveData = Object.assign(this.settings.defaults, saveDataId, formSaveData);

						//
						let skipElemUpdate = ( !('ajax-update' in shortElem) || ( 'ajax-update' in shortElem && !shortElem['ajax-update'] ) );

						//
						if ( !skipElemUpdate ) {
							onSuccess = (e, msg) => {
								this.rowUpdate(targetRow, msg);
							};
						}
					} else if ( shortElem['ajax-action'] === 'add' ) {
						formSaveData = Object.assign(this.settings.defaults, formSaveData);
						onSuccess = (msg) => {
							this.rowAdd(msg.d);
						};
					} else if ( shortElem['ajax-action'] === 'addMany' ) {
						console.log("button form add");
						formSaveData = Object.assign(this.settings.defaults, formSaveData);
						onSuccess = (msg) => {
							this.rowAdd(msg.d);
						};
					} else if( shortElem['ajax-action'] === 'function' ) {
						//
						onSuccess = cForm.onSuccess;
					} else if( shortElem['ajax-action'] === 'editMany' ) {
						let cSelection = this.el_async_content.querySelectorAll('input:checked');
						if ( cSelection.length !== 0 ) {
							let targets = [];
							cSelection.se_each((id, cEl) => {
								targets.push(cEl.se_closest('[se-ajaxelem="obj"]').se_data('objid'));
							});
							//
							formSaveData = Object.assign(this.settings.defaults, formSaveData, { 'idElems':targets });
							console.log("LKAJSDLFKJASDF", this.settings.defaults, formSaveData, {'idElems':targets}, formSaveData);
							onSuccess = (msg) => {
								reload();
							};
						} else {
							alert("No se detectan elementos seleccionados.");
							return;
						}
					}

					//
					formCreator.Modal.form(
						{
							title:cForm.title,
							method:cForm.method ?? 'post',
							load_url:formLoadUrl,
							load_data:formLoadData,
							save_url:formSaveUrl,
							save_data:formSaveData,
							elems:cForm.elems,
							actName:actName,
						},
						onSuccess
					);
					break;

				//
				case 'actionWindow':
					//
					let cWindow = shortElem.actionWindow,
						actionWinParams = {
							title:( 'title' in cWindow ) ? cWindow.title : 'Encabezado',
							description:( 'description' in cWindow ) ? cWindow.description : '',
							defaults:Object.assign(this.settings.defaults, { 'id':objId }),
							buttons:( 'buttons' in cWindow ) ? cWindow.buttons : []
						};

					// Llamado
					se.addon.windowAction(actionWinParams);
					break;

				// Función
				case 'function':
				case 'functionModule':
					//
					let selected;
					let cFunc = null;

					//
					if ( shortElem.action === 'function' ) {
						if ( typeof shortElem.function !== 'function' ) {
							console.error("Function is not defined.");
							return;
						}

						cFunc = shortElem.function;
					} else {
						if ( typeof this.extraFunctions[shortElem.function] !== 'function' ) {
							console.error("Function is not defined.", this.extraFunctions);
							return;
						}

						cFunc = this.extraFunctions[shortElem.function];
					}

					// Determine target
					switch ( shortElem['ajax-elem'] ) {
						//
						case 'obj':
							cFunc(targetRow, this, this.settings.defaults, targetRow.dataset['objid'], targetRow.dataset['objtitle']);
							break;
						//
						case 'sel':
							selected = this.el_async_content.querySelectorAll('input:checked');
							if ( selected.length !== 0 ) {
								let targets = [];
								selected.se_each(function(id, cEl) {
									targets.push(cEl.se_closest('[data-element="row"]'));
								});
								cFunc(targets, this.settings.defaults);
							} else {
								console.log("No se detectan elementos seleccionados.");
							}
							break;
						//
						case 'mod':
							selected = this.el_async_content.querySelectorAll('input:checked');
							if ( selected.length !== 0 ) {
								cFunc(selected, this.settings.defaults);
							} else {
								console.log("No se detectan pElementos modificados.");
							}
							break;
						//
						case 'none':
							cFunc(this, this.el_async_content, this.settings.defaults);
							break;
						//
						default:
							console.log('shortElem.ajax-elem no definido', shortElem['ajax-elem']);
							break;
					}
					break;

				// Guardar todos
				case 'saveAll':
					// Seleccionar objetos
					let selects = this.el_async_content.querySelectorAll('form.mod');
					if ( selects.length === 0 ) {
						console.warn("No available updated elements to save.");
						return;
					}

					// Convert each row into a json object
					let cData = {};
					//
					for ( let cEl of selects ) {
						cData[cEl.dataset['objid']] = formOps.serializeFormToJSON(cEl);
					}

					//
					this.saveAll(
						shortElem.save_url,
						cData,
						() => {
							// loadMore(true);
							selects.forEach((cEl)=> cEl.classList.remove('mod'));
						},
						(msg) => {
							alert(msg.title);
						},
					);
					break;

				// Borrar
				case 'del':
					if ( !confirm('¿Desea borrar el pElemento "' + targetRow.dataset['objtitle'] + '" de la base de datos?') ) {
						console.warn('Deletion canceled');
						return;
					}

					let deleteUrl = shortElem.del_url;

					//
					if ( deleteUrl.includes('{id}') ) {
						deleteUrl = deleteUrl.replace('{id}', objId);
					} else {
						const searchParams = +new URLSearchParams({
							id: objId
						});

						deleteUrl += '?' + searchParams;
					}

					//
					try {
						//
						const response = await fetch(
							deleteUrl,
							{
								'method': "DELETE"
							}
						);

						//
						if ( !response.ok ) {
							//
							console.error("Could not delete.");
							return;
						}

						//
						targetRow.remove();
					} catch (e) {
						console.error("error", e);
					}
					break;

				//
				default:
					console.log('Función no definida', shortElem.action, object);
					break;
			}
		}
		else {
			// content.addEventListener('click', modeUpdate);
		}
	}

	//
	async inlineFormAction(e, cForm) {
		e.preventDefault();

		let cActionName = cForm.dataset['action'];

		if ( !this.extraActions.hasOwnProperty(cActionName) ) {
			console.error("warn, form action not found");
			return;
		}

		let cAction = this.extraActions[cActionName],
			targetUrl = cAction.save_url,
			formData = new FormData(cForm),
			objId = parseInt(cForm.dataset['objid']);

		if ( targetUrl && targetUrl.includes('{id}') ) {
			targetUrl = targetUrl.replace('{id}', objId);
		} else {
			formData.append('id', objId);
		}

		//
		await fetch(targetUrl, {method:'post', body: formData});
	}

	//
	async saveAll(url, data, onSuccess, onFail) {
		let response = await fetch(url,
			{
				method:'PUT',
				headers: {
					"Content-Type": "application/json",
				},
				body: JSON.stringify(data)
			}
		);


		let msg;

		try {
			msg = await response.json();
		} catch ( e ) {
			if ( typeof onFail === 'function' ) {
				onFail();
			}

			console.error("error", e);
			return;
		}


		//
		if ( typeof onSuccess === 'function' ) {
			onSuccess(msg);
		}
	}

	// Select page
	selectPage(e) {
		this.el_async_content.dispatchEvent(
			new CustomEvent("externalOperation", {
				detail: {
					operation:'cpage',
					pageNumber:e.target.value,
				},
			}),
		);
	}

	//
	rowUpdate(targetRow, data) {
		this.el_async_content.dispatchEvent(
			new CustomEvent("elementUpdate", {
				detail: {
					target:targetRow,
					content:data
				},
			}),
		);
	}

	//
	rowAdd(data) {
		this.el_async_content.dispatchEvent(
			new CustomEvent("elementAdd", {
				detail: {
					position:'afterbegin',
					content:data
				},
			}),
		);
	}
});