import * as forms from '../library/forms.mjs';

/*
 * Form
 */
customElements.define(
	'se-async-form',
	class extends HTMLFormElement {
		//
		formCallBacks = {
			onComplete: null,
			onSuccess: null,
			onFail: null,
			onError: null,
		};

		//
		constructor() {
			super();

			// Get definitions
			this.objSubmitBtn = this.querySelector('[type="submit"]');
			this.objSubmitActionName = ( this.objSubmitBtn && this.objSubmitBtn.tagName.toLowerCase() === 'button' ) ? this.objSubmitBtn.innerHTML : '';
			this.objOutput = this.querySelector('output');

			//
			this.submitCB = null;
		}

		//
		connectedCallback() {
			// Check for initial values
			if ( this.dataset['loadurl'] ) {
				this.formLoad();
			}

			//
			this.addEventListener('setCallBacks', this.setCallBacks.bind(this));

			// Bindings
			this.addEventListener('submit', this.formSubmit.bind(this));
			this.addEventListener('change', this.formChanged.bind(this));
		}

		//
		setCallBacks(e) {
			//
			const defaults = {
				onComplete: null,
				onSuccess: null,
				onFail: null,
				onError: null,
			};

			// Set callback
			this.formCallBacks = Object.assign(defaults, e.detail);
		}

		//
		formChanged(e) {
			//
			if ( this.checkValidity() ) {
				this.submitBtnUpdate('fa-circle-o', 'pulsate');
			} else {
				this.submitBtnUpdate('fa-warning');
			}
		}

		//
		submitBtnUpdate(exData, iconMod, disable = null) {
			if ( !this.objSubmitBtn ) { return; }

			if ( typeof disable !== null ) {
				this.objSubmitBtn.disable = disable;
			}


			if ( this.objSubmitActionName !== '' ) {
				iconMod = ( typeof iconMod === 'string' ) ? iconMod : '';
				this.objSubmitBtn.innerHTML = '<svg class="icon inline mr '+iconMod+'"><use xlink:href="#'+exData+ '" /></svg>' + this.objSubmitActionName;
			}
		}


		//
		async formLoad() {
			//
			let fResponse = await fetch(this.dataset.loadurl);
			let msg = await fResponse.json();

			//
			for ( let key in msg.d ) {
				if ( !msg.d.hasOwnProperty(key) ) {
					continue;
				}
				// type of content in key
				if ( typeof msg.d[key] === 'object' ) {
					// Is an array with multiple objects
					this.flattenNameAndAssign(key, msg.d[key]);
				} else {
					this.assignValue(key, msg.d[key]);
				}
			}
		}

		//
		flattenNameAndAssign(name, result) {
			//
			for ( let cSubName in result ) {
				let cName = name + "[" + cSubName + "]";
				//
				if ( typeof result[cSubName] === 'object' ) {
					this.flattenNameAndAssign(cName, result[cSubName]);
				} else {
					this.assignValue(cName, result[cSubName]);
				}
			}
		}

		assignValue(elName, elValue) {
			//
			let cEl = this.elements.namedItem(elName);
			//
			if ( !cEl ) {
				console.log("Form El Val, undefined element:", elName);
				return;
			}

			if ( cEl.type && cEl.type.toLowerCase() === 'checkbox' ) {
				cEl.checked = (elValue === cEl.value);
			} else {
				cEl.value = elValue;
			}
		}

		//
		async formSubmit(e) {
			e.preventDefault();


			// Ending operation functions
			let endingRequest = (notifData) => {
				//
				this.notificationBuild(notifData);

				//
				if ( typeof this.formCallBacks.onComplete === 'function' ) {
					this.formCallBacks.onComplete.onFail();
				}

				//
				this.submitBtnUpdate(notifData.icon, null, false);
			}

			//
			let fResponse,
				notificationData = {
					title: '',
					content: '',
					style: 'notification ',
					icon: '',
					autoClear: null
				}


			//
			this.submitBtnUpdate('fa-spinner', 'spin', true);
			//
			if ( typeof this.formCallBacks.onRequest === 'function' ) {
				this.formCallBacks.onRequest();
			}

			//
			try {
				// Actual fetch
				fResponse = await fetch(
					this.action,
					{
						method: this.method || 'post',
						body: new FormData(this),
					}
				);
			} catch (e) {
				console.error("Could not fetch", e);

				//
				if ( typeof this.formCallBacks.onError === 'function' ) {
					this.formCallBacks.onError();
				}

				//
				notificationData.title = "Could not fetch";
				notificationData.content = e;
				notificationData.style = "notification error";
				notificationData.icon = "fa-alert";

				//
				endingRequest(notificationData)

				return;
			}
			let msg;

			try {
				msg = await fResponse.json();
			} catch ( error ) {
				if ( error instanceof SyntaxError ) {
					// Unexpected token < in JSON
					console.error("There was a SyntaxError.\n", error);
				} else {
					console.error("There was an error.\n", error);
				}

				//
				if ( typeof this.formCallBacks.onError === 'function' ) {
					this.formCallBacks.onError();
				}

				//
				notificationData.title = "Parsing error";
				notificationData.content = error;
				notificationData.style = "notification error";
				notificationData.icon = "fa-alert";

				//
				endingRequest(notificationData)

				return;
			}

			//
			if ( !fResponse.ok ) {
				// Successful request.
				notificationData.title = ('title' in msg) ? 'ERROR: ' + msg.title : 'ERROR: Caso no definido';
				notificationData.content = ('description' in msg) ? msg.description : ('timestamp' in msg) ? msg.timestamp : '';
				notificationData.style = 'notification error';
				notificationData.icon = 'fa-remove';

				//
				if ( typeof this.formCallBacks.onFail === 'function' ) {
					this.formCallBacks.onFail(msg);
				}

				//
				endingRequest(notificationData)

				return;
			}

			// Successful request.
			notificationData.title = ( 'title' in msg && msg.title !== '' ) ? msg.title : 'Enviado';
			notificationData.content = ( 'description' in msg && msg.description !== '' ) ? msg.description : ('timestamp' in msg) ? msg.timestamp : '';
			notificationData.style = 'notification success';
			notificationData.icon = 'fa-check';
			notificationData.autoClear = 30000;

			//
			if ( typeof this.formCallBacks.onSuccess === 'function' ) {
				this.formCallBacks.onSuccess(msg);
			}

			endingRequest(notificationData);
		}

		notificationBuild(cOptions) {
			if ( !this.objOutput ) { return; }

			//
			let defaults = {
					icon:null,
					iconMod:null,
					title:null,
					content:'',
					contentHTML:false,
					style:null,
					autoClear:null
				},
				settings = {...defaults, ...cOptions},
				//
				html = '',
				clearInterval;

			//
			if ( settings.icon ) {
				html+= '<div class="icon">';
				if ( settings.icon.indexOf('.') === -1 )
				{
					settings.iconMod = ( typeof settings.iconMod === 'string' ) ? settings.iconMod : '';
					html+= '<svg class="' + settings.iconMod + '"><use xlink:href="#' + settings.icon + '" /></svg>';
				} else {
					console.log("NOTIFICACION, IMAGEN NO PROGRAMADA");
				}
				html+= '</div>';
			}
			// Contenido
			let htmlContent = ( settings.contentHTML ) ? ' html' : '';
			html+= '<div class="text"><div class="title">' + settings.title  + '</div><div class="content'+ htmlContent +'">' + settings.content + '</div></div>';

			// Apply
			this.objOutput.className = settings.style;
			this.objOutput.innerHTML = html;

			//
			if ( typeof settings.autoClear === 'number' && settings.autoClear !== 0 ) {
				clearInterval = setTimeout(() => {
					this.objOutput.se_empty();
				}, settings.autoClear);
			} else {
				clearTimeout(clearInterval);
			}
		}
	},
	{extends: 'form'}
);