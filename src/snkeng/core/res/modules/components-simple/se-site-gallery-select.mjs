import * as formOps from './../library/forms.mjs';
import * as simpleTab from './se-simple-tab.mjs';
import * as simpleForm from './se-async-form.mjs';
import * as asyncContent from './se-async-content.mjs';

//
customElements.define('se-site-gallery-select', class extends HTMLElement {
	// Upload object
	el_uploadForm = this.querySelector('form[se-elem="uploadImages"]');
	el_uploadInput = this.el_uploadForm.querySelector('input[name="files[]"]');
	el_uploadTemplate = this.el_uploadForm.querySelector('template');
	el_uploadContent = this.el_uploadForm.querySelector('div[se-elem="content"]');
	// Actual list
	el_photoObj = this.querySelector('div[se-elem="photos"]');
	el_photoTemplate = this.el_photoObj.querySelector('template');
	el_photoList = this.el_photoObj.querySelector('div[se-elem="content"]');
	//
	constructor() {
		super();
	}

	//
	connectedCallback() {
		// Bindings
		this.addEventListener('click', this.onClickCallBacks.bind(this));
		this.el_photoList.se_on('change', ['input', 'textarea'], upd_content);
		//Check File API support
		if ( window.File && window.FileList && window.FileReader ) {
			this.el_uploadInput.se_on("change", images_preview);
		}
	}

	//
	onClickCallBacks(e) {
		let cBtn = e.target.closest('button[data-dyntab-onclick]');
		if ( !cBtn ) { return; }

		//
		let operation = 'onClick_' + cBtn.dataset['dyntabOnclick'];

		//
		if ( typeof this[operation] !== 'function' ) {
			console.error("operación no definida.", operation, cBtn);
			return;
		}

		//
		this[operation](e, cBtn);
	}

	//

});





'use strict';
// Galería seleccionar imagenes
se.plugin.adm_gallerySelect = function(plugElem, plugOptions) {
	// Binds
	let pDefaults = {
			callback:null
		},
		pSettings = se.object.merge(pDefaults, plugOptions),
		//
		ajaxUrl = plugElem.se_data('url'),
		postData = {
			galId:plugElem.se_data('galid')
		};


	//
	function init() {

		//
		this.el_uploadForm.se_plugin('simpleForm', {
			save_url:ajaxUrl + '/gallery/upload',
			onSuccess:images_upload
		});
	}

	//
	function images_upload(msg) {
		this.el_uploadContent.innerHTML = '';
		this.el_photoList.se_site_coreend(se.struct.stringPopulateMany(this.el_photoTemplate.se_html(), msg.d));
	}

	//
	function images_preview(event) {
		this.el_uploadContent.innerHTML = '';
		//
		let files = event.target.files; //FileList object
		for ( let i = 0; i < files.length; i++ ) {
			let file = files[i];
			//Only pics
			if ( !file.type.match('image') ) { continue; }

			let picReader = new FileReader();
			picReader.addEventListener("load",
				(event) => {
					let picFile = event.target;
					this.el_uploadContent.se_site_coreend(se.struct.stringPopulate(this.el_uploadTemplate.se_html(), {'result':picFile.result}));
				}
			);
			//Read the image
			picReader.readAsDataURL(file);
		}
	}

	//
	function upd_content(e, cEl) {
		let imgEl = cEl.se_closest('div.photo'),
			imgData = {
				imgId:imgEl.se_data('id'),
				title:imgEl.querySelector('input[name="title"]').value,
				description:imgEl.querySelector('textarea[name="description"]').value
			};


		// AJAX
		se.ajax.json(ajaxUrl + '/gallery/upd', se.object.merge(postData, imgData), {
			onSuccess:(msg) => {
				// imgEl.se_replaceWith(se.struct.stringPopulate(this.el_photoTemplate.se_html(), imgData));
				console.log("content updated");
			}
		});
	}

	//
	function btn_actions(e, cEl) {
		//
		switch ( cEl.se_attr('se-act') ) {
			//
			case 'add':
				se.addon.floatWin({
					iniUrl:'/api/module_adm/core/site/images/win_images_sel',
					fullScreen:true,
					id:'admin_photoSel',
					onLoad:() => {
						$('#photoNav').adm_imageEditor.setup({
							onSelect:img_callBack
						});
					}
				});
				break;

			//
			case 'del':
				if ( confirm('¿Eliminar foto de la galería? (No se remueve del servidor)') ) {
					let imgEl = cEl.se_closest('div.photo');
					se.ajax.json(ajaxUrl + '/gallery/del', se.object.merge(postData, {id: imgEl.se_data('id')}), {
						onSuccess: (msg) => {
							imgEl.se_remove();
						}
					});
				}
				break;
			//
			default:
				console.log("Acción no definida.", cEl);
				break;
		}
	}

	//
	function img_callBack(imgData) {
		let imgDataFinal = {
			imgId:imgData.id,
			fName:imgData.fname,
			title:imgData.title,
			description:imgData.desc
		};
		//
		console.log("IMAGE SELECTED DATA", imgData, imgDataFinal);

		// AJAX
		se.ajax.json(ajaxUrl+'/gallery/add', se.object.merge(postData, imgDataFinal), {
			onSuccess:(msg) => {
				this.el_photoList.se_site_coreend(se.struct.stringPopulate(this.el_photoTemplate.se_html(), imgDataFinal));
			}
		});
	}

	// Expose
	init();
	return {};
};