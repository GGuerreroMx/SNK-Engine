<?php

// Default no cache
\snkeng\core\engine\nav::cacheSetType('');
\snkeng\core\engine\nav::cacheSetTime(5);

//
\snkeng\core\engine\nav::$async = true;
$_ENV['SE_ASYNC'] = true;

//
$response = [
	'title' => '',
	'description' => '',
	'timestamp' => ''
];
//

//
switch ( \snkeng\core\engine\nav::next() )
{
	//
	case 'module_adm':
		//
		require $_SERVER['DOCUMENT_ROOT'] . '/snkeng/core/php/load/admin.php';

		// No user
		if ( !\snkeng\core\engine\login::check_loginLevel('admin') ) {
			se_killWithError('No permits', 'Insuficient level.', 401);
		}

		//
		switch ( \snkeng\core\engine\nav::next() )
		{
			//
			case 'core':
			case 'site':
			case 'user':
				$method = \snkeng\core\engine\nav::current();
				$appData['name'] = \snkeng\core\engine\nav::next();

				//
				switch ( $method ) {
					case 'core':
						$appData['dir'] = $_SERVER['DOCUMENT_ROOT'] . '/snkeng/admin_modules/'.$appData['name'];
						break;
					case 'site':
						$appData['dir'] = $_SERVER['DOCUMENT_ROOT'] . '/snkeng/site_modules/'.$appData['name'];
						break;
					case 'user':
						$appData['dir'] = $_SERVER['DOCUMENT_ROOT'] . '/snkeng/admin_modules/user';
						break;
				}

				//
				if ( !is_dir($appData['dir']) ) {
					\snkeng\core\engine\nav::invalidPage();
				}

				//
				require $appData['dir']. '/routing/adm/api/#map.php';
				break;

			//
			default:
				\snkeng\core\engine\nav::invalidPage();
				break;
		}
		break;

	//
	case 'module_usr':
		$apiName = \snkeng\core\engine\nav::next();
		if ( empty($apiName) ) { \snkeng\core\engine\nav::invalidPage('NO API NAME'); }

		// Api Name
		$dir = $_SERVER['DOCUMENT_ROOT'] . '/snkeng/site_modules/'.$apiName;
		$propLoad = $dir.'/params.php';
		$ajaxLoad = $dir.'/routing/usr/api/#map.php';

		//
		if ( !file_exists($dir) ) { se_killWithError("Module Load Error - Not found. ({$apiName})"); }
		// Revisar archivo inicial
		if ( !file_exists($ajaxLoad) ) { se_killWithError("Module Load Error - Initial file not found. (({$apiName}))"); }

		//
		seLoad_appParams($apiName);
		require $ajaxLoad;
		break;

	//
	case 'user':
		//
		$siteVars['page']['async'] = true;
		header("Cache-Control: no-cache, must-revalidate");

		//
		require __DIR__ . '/ajax_user/#map.php';
		break;

	// Not a system default option, read site (error there)
	default:
		require $_SERVER['DOCUMENT_ROOT'] . '/snkeng/site_core/routing/api.php';
		break;
}
//

//
\snkeng\core\engine\nav::printJSON($response);
