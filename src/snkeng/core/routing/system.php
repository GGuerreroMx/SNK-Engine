<?php

//
switch ( \snkeng\core\engine\nav::next() ) {
	//
	case 'sw':
		switch ( \snkeng\core\engine\nav::next() ) {
			//
			case 'cache':
				$jsonFile = $_SERVER['DOCUMENT_ROOT'] . '/se_files/user_upload/min_files/files.json';
				$files = json_decode(file_get_contents($jsonFile), true);
				$isUpdate = intval($_GET['update']);

				//
				$data = [];

				foreach ( $files as $cFile ) {
					if ( $isUpdate === 0 || $isUpdate < $cFile['utime'] ) {
						$data[] = $cFile['cName'];
					}
				}

				printJSONData([
					'files' => $data,
					's' => [
						't' => 1,
						'time' => time()
					]
				]);
				break;
			//
			case 'template':
				switch ( \snkeng\core\engine\nav::next() ) {
					case 'usr':
						break;
					case 'adm':
						break;
					default:
						\snkeng\core\engine\nav::invalidPage();
						break;
				}
				break;
			//
			default:
				\snkeng\core\engine\nav::invalidPage();
				break;
		}
		break;

	//
	default:
		\snkeng\core\engine\nav::invalidPage();
		break;
}
//
