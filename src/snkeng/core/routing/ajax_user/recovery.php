<?php
//

switch ( \snkeng\core\engine\nav::next() ) {
	//
	case 'create':
		$pData = [
			'eMail' => ['name' => 'Correo electrónico', 'type' => 'email', 'lMax' => 70, 'validate' => true],
			'type' => ['name' => 'Tipo', 'type' => 'email', 'lMax' => 5, 'validate' => false],
		];
		$rData = \snkeng\core\general\saveData::postParsing($response, $pData);

		// Revisar anti bot
		$localCaptcha = new \snkeng\core\general\captcha();
		if ( !$localCaptcha->verify() ) {
			se_killWithError("Captcha no aprobado.");
		}

		// Revisar que la base de datos tenga el correo
		$sql_qry = <<<SQL
SELECT
	am.am_id AS id, am.am_mail AS eMail,
	soo.a_obj_name AS usrName
FROM sb_users_mail AS am
INNER JOIN sb_objects_obj soo ON am.a_id = soo.a_id
WHERE am_mail='{$rData['eMail']}'
LIMIT 1;
SQL;
		$mailData = \snkeng\core\engine\mysql::singleRowAssoc($sql_qry, [
			'int' => ['id']
		]);

		// Quietly fail
		if ( empty($mailData) ) {
			// Fecha y sacar
			$response['s']['date'] = date('h:i:s A');
			printJSONData($response);
		}

		// Fecha y sacar
		$recoveryType = ( $rData['type'] === 'adm' ) ? 'adm_recov' : 'usr_recov';


		//
		$linkData = \snkeng\core\user\extlink::linkCreate('core', $recoveryType, $mailData['id'], 7, true);

		$response['debug']['linkData'] = $linkData;

		//
		// Enviar correo
		$to = "{$mailData['usrName']} <{$mailData['eMail']}>";
		$title = 'Recuperar acceso a '.$siteVars['site']['name'];
		//
		$message = <<<HTML
<p>Se ha solicitado la recuperación de contraseña en: {$siteVars['site']['name']}.</p>
<p>Si usted no ha solicitado recuperación de contraseña, haga caso omiso a este correo.</p>
<p>Para cambiar la contraseña haga click en el siguiente vínculo.</p>
<p><a href="{$linkData['url']}">RECUPERAR CONTRASEÑA</a></p>
HTML;
		//

		//
		$mail = \snkeng\core\general\mail::sendHTML($to, $title, $message, '/snkeng/site_core/templates/mail.php');
		$response['debug']['mail'] = $mail['debug']['mail'];
		break;

	//
	case 'validate':
		$pData = [
			'key' => ['name' => 'Clave', 'type' => 'str', 'lMax' => 100, 'validate' => true],
			'uPass1' => ['name' => 'Contraseña 1', 'type' => 'str', 'lMin' => 8, 'lMax' => 32, 'filter' => 'password', 'validate' => true],
			'uPass2' => ['name' => 'Contraseña 2', 'type' => 'str', 'lMin' => 8, 'lMax' => 32, 'filter' => 'password', 'validate' => true],
		];
		$rData = \snkeng\core\general\saveData::postParsing($response, $pData);

		//
		$linkData = \snkeng\core\user\extlink::linkValidate( $rData['key'] );
		if ( empty($linkData) || $linkData['app'] !== 'core' || !( $linkData['act'] === 'usr_recov' || $linkData['act'] === 'adm_recov' ) ) {
			se_killWithError('Clave no válida.');
		}

		if ( $linkData['status']['expired'] || !$linkData['status']['valid'] ) {
			se_killWithError('Clave vencida.');
		}

		//
		if ( $rData['uPass1'] !== $rData['uPass2'] ) {
			se_killWithError('Las contraseñas no coinciden.');
		}

		//
		\snkeng\core\user\management::passRecoveryUpdate($linkData['id'], $rData['uPass1']);

		//
		\snkeng\core\user\extlink::linkExpire( $linkData['id'] );
		break;

	//
	default:
		\snkeng\core\engine\nav::invalidPage();
		break;
}
//
