<?php
//
switch ( \snkeng\core\engine\nav::next() ) {
	//
	case 'request':
		// Local captcha mode
		$localCaptcha = new \snkeng\core\general\captcha();
		$localCaptcha->request();
		break;
	//
	default:
		\snkeng\core\engine\nav::invalidPage();
		break;
}
//
