<?php
// User data

$userData = \snkeng\core\engine\login::getUserData();

//
switch ( \snkeng\core\engine\nav::next() ) {
	//
	case 'names':
		$rData = \snkeng\core\general\saveData::postParsing(
			$response,
			[
				'fName' => ['name' => 'Nombre','type' => 'str', 'lMin' => 3, 'lMax' => 50, 'filter' => 'names', 'validate' => true],
				'lName' => ['name' => 'Apellido', 'type' => 'str', 'lMin' => 3, 'lMax' => 50, 'filter' => 'names', 'validate' => true],
			]
		);

		$fullName = $rData['fName'] . ' ' . $rData['lName'];

		//
		$sql_qry = <<<SQL
UPDATE sb_objects_obj SET a_fname='{$rData['fName']}', a_lname='{$rData['lName']}', a_obj_name='{$fullName}' WHERE a_id='{$userData['id']}';
SQL;
		\snkeng\core\engine\mysql::submitQuery($sql_qry,
			[
				'errorKey' => '',
				'errorDesc' => ''
			]
		);
		break;

	//
	case 'password':
		$rData = \snkeng\core\general\saveData::postParsing($response,
			[
				'pass_orig' => ['name' => 'Pw Old', 'type' => 'str', 'lMin' => 4, 'lMax' => 32, 'filter' => 'password', 'validate' => true],
				'pass_new1' => ['name' => 'PW New 1', 'type' => 'str', 'lMin' => 8, 'lMax' => 32, 'filter' => 'password', 'validate' => true],
				'pass_new2' => ['name' => 'PW New 2', 'type' => 'str', 'lMin' => 8, 'lMax' => 32, 'filter' => 'password', 'validate' => true],
			]
		);

		//
		if ( $rData['pass_new1'] !== $rData['pass_new2'] ) { se_killWithError("Las nuevas contraseñas no son iguales."); }

		//
		\snkeng\core\user\management::changePassword($rData['pass_orig'], $rData['pass_new1'], $userData['id']);
		break;

	//
	case 'image':
		switch ( \snkeng\core\engine\nav::next() ) {
			//
			case 'upload':
				\snkeng\core\socnet\object_picture::picture_upload($response, $userData['id']);
				break;

			//
			case 'crop':
				\snkeng\core\socnet\object_picture::picture_crop($response, $userData['id']);
				break;

			//
			default:
				\snkeng\core\engine\nav::invalidPage();
				break;
		}
		break;

	//
	case 'email':
		if ( !is_numeric(\snkeng\core\engine\nav::next(false)) ) {
			require __DIR__ . '/settings_email_general.php';
		}
		else {
			$params['vars']['emailId'] = intval(\snkeng\core\engine\nav::next());

			if ( $params['vars']['emailId'] === 0 ) { \snkeng\core\engine\nav::killWithError('ID no válido.'); }

			//
			$mailInfo = \snkeng\core\user\management::mailStatus($params['vars']['emailId']);

			if ( $mailInfo['usrId'] !== $userData['id'] ) {
				\snkeng\core\engine\nav::invalidPage('El correo no corresponde al usuario', 401);
			}

			require __DIR__ . '/settings_email_properties.php';
		}
		break;

	//
	case 'socnet':
		switch ( \snkeng\core\engine\nav::next() ) {
			// Picture
			case 'link':
				\snkeng\core\socnet\object_picture::picture_crop($response, $userData['id']);
				break;

			//
			case 'unlink':
				break;

			//
			default:
				\snkeng\core\engine\nav::invalidPage();
				break;
		}
		break;

	//
	default:
		\snkeng\core\engine\nav::invalidPage();
		break;
}
//
