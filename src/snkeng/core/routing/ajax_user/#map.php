<?php
// /api/user

//

switch ( \snkeng\core\engine\nav::next() ) {
	//
	case 'log':
		require __DIR__ . '/log.php';
		break;

	//
	case 'register':
		\snkeng\core\engine\login::loginExceded();

		//
		require __DIR__ . '/register.php';
		break;

	//
	case 'recovery':
		\snkeng\core\engine\login::loginExceded();

		//
		require __DIR__ . '/recovery.php';
		break;

	//
	case 'notifications':
		//
		\snkeng\core\engine\login::loginRequired();

		//
		require __DIR__ . '/notifications.php';
		break;

	//
	case 'captcha':
		//
		require __DIR__ . '/captcha.php';
		break;

	//
	case 'settings':
		//
		\snkeng\core\engine\login::loginRequired();

		//
		require __DIR__ . '/settings.php';
		break;

	//
	default:
		\snkeng\core\engine\nav::invalidPage();
		break;
}
//
