<?php
//
switch ( \snkeng\core\engine\nav::next() ) {
	//
	case 'delete':
		\snkeng\core\user\management::mailRemove($params['vars']['emailId'], $userData['id']);
		break;

	//
	case 'primary':
		\snkeng\core\user\management::mailSetPrimary($params['vars']['emailId'], $userData['id']);
		break;

	//
	case 'validate':
		//
		$mailInfo = \snkeng\core\user\management::mailStatus($params['vars']['emailId']);
		break;

	//
	default:
		\snkeng\core\engine\nav::invalidPage();
		break;
}
//
