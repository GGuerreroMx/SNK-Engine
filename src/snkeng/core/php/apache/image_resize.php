<?php
// Fix document root
$_SERVER['DOCUMENT_ROOT'] = substr($_SERVER['DOCUMENT_ROOT'], 0, -12);

// Load responses
use se_core\php\apache\responses;

require __DIR__ . '/responses.php';

//
require $_SERVER['DOCUMENT_ROOT'] . '/snkeng/core/class/engine/environment.php';

function getCenteringDisplacement(int $sizeMax, int $sizeImg) : int {
	return round( ($sizeImg - $sizeMax) / 2 );
}

//
function getTargetImageSize(int $targetWidth, int $targetHeight, string | null $targetAspectRatio, int $originalWidth, int $originalHeight) : array {
	$result = [
		'width' => $targetWidth,
		'height' => $targetHeight,
		'aspect_ratio' => 1
	];

	if ( $targetAspectRatio ) {
		$tempRatio = explode(",", $targetAspectRatio, 2);
		$result['aspect_ratio'] = intval($tempRatio[0]) / intval($tempRatio[1]);
	} else {
		$result['aspect_ratio'] = $originalWidth / $originalHeight;
	}


	//
	if ( $targetWidth ) {
		$result['height'] = round($targetWidth / $result['aspect_ratio']);
	} elseif ($targetHeight) {
		$result['width'] = round($targetHeight * $result['aspect_ratio']);
	}


	return $result;
}

// Load environment variables
\snkeng\core\engine\environment::loadEnv('.');

// Bool convert debug status.
$_ENV['SE_DEBUG'] = ($_ENV['SE_DEBUG'] === 'true');

//
// $_ENV['SE_DEBUG'] = true;
$debug = false;
$rebuildImage = false;
$fileFormatChange = false;

//
$fName = $_GET['name'];
$fType = $_GET['type'];
$fParams = $_GET['params'];
$fDir = $_GET['dir'];
$mode = $_GET['mode'];

//
if ( substr($fDir, 0, 12) === 'site_content' ) {
	$subDir = substr($fDir, 13);

	//
	$cFolders = explode('/', $subDir, 2);

	// Begin building target folder
	$img_orig_folder = "/snkeng";
	$img_new_folder = "/se_files/site_processed/img";

	//
	switch ( $cFolders[0] ) {
		case 'site_core':
		case 'admin_core':
			$subFolder = (!empty($cFolders[1])) ? $cFolders[1] . '/' : '';

			$img_orig_folder .= "/{$cFolders[0]}/res/img/{$subFolder}";
			$img_new_folder .= "/{$cFolders[0]}/{$subFolder}";
			break;
		case 'site_modules':
		case 'admin_modules':
			$cFolders2 = explode('/', $cFolders[1], 2);

			$subFolder = (!empty($cFolders2[1])) ? $cFolders2[1] . '/' : '';


			$img_orig_folder .= "/{$cFolders[0]}/{$cFolders2[0]}/res/img/{$subFolder}";
			$img_new_folder .= "/{$cFolders[0]}/$cFolders2[0]/{$subFolder}";
			break;
		default:
			die("IMAGE ERROR: Invalid starting folder '{$cFolders[0]}'.");
			break;
	}

}
else {
	$img_orig_folder = "/se_files/user_upload/img/{$fDir}/";
	$img_new_folder = "/se_files/user_processed/img/{$fDir}/";
}

//
if ( $debug ) {
	ini_set('display_errors', '1');
	ini_set('display_startup_errors', '1');
	error_reporting(E_ALL);

	header('Content-Type: text/plain; charset=UTF-8');
	echo <<<TEXT
NAME:\t\t{$fName}
TYPE:\t\t{$fType}
PARAMS:\t\t{$fParams}
DIR PARAM:\t{$fDir}
DIR ORIG:\t{$img_orig_folder}
DIR TARGET:\t{$img_new_folder}
----\n
TEXT;
	//
}

// Pass original file?
if ( $fParams === 'original' ) {
	$originalFileName = $_SERVER['DOCUMENT_ROOT'] . $img_orig_folder . $fName . '.' . $fType;
	if ( !file_exists($originalFileName) ) {
		http_response_code(404);
		header("Content-Type: image/svg+xml; charset=UTF-8");
		echo file_get_contents($_SERVER['DOCUMENT_ROOT'] . '/snkeng/core/res/se_img/empty.svg');
		exit;
	}

	responses::fileSend($originalFileName, $fType);
}

// Valid file
if ( !in_array($fType, ['jpg', 'jpeg', 'gif', 'png', 'webp', 'avif']) ) {
	responses::op_killWithError("Invalid file type.");
}

// tipo empty, especial para no mostrar error.
if ( $fName === 'empty' ) {
	//
	$fileName = $_SERVER['DOCUMENT_ROOT'] . '/se_files/user_upload/imgr/empty.' . $fType;
	if ( file_exists($fileName) ) {
		responses::fileSend($fileName, $fType);
	}
	else {
		//
		$new_image = imagecreatetruecolor(1, 1);
		switch ( $fType ) {
			//
			case 'avif':
				if ( !(imagetypes() & IMG_AVIF) ) {
					die("image file not supported");
				}
				imageavif($new_image, $fileName);
				break;
			//
			case 'webp':
				imagewebp($new_image, $fileName);
				break;
			//
			case 'jpg':
				imagejpeg($new_image, $fileName, 1);
				break;
			//
			case 'png':
				imagegif($new_image, $fileName);
				break;
			//
			case 'gif':
				imagepng($new_image, $fileName);
				break;
		}
		//
		responses::fileSend($fileName, $fType);
	}
	exit;
}

//
$reqFileName = $_SERVER['DOCUMENT_ROOT'] . $img_new_folder . $fName . '.' . $fParams . '.' . $fType;
$realUrl = 'https://' . $_SERVER['HTTP_HOST'] . $img_new_folder . $fName . '.' . $fParams . '.' . $fType;

// Avoid recreating if not requested for it
if ( file_exists($reqFileName) && !$rebuildImage ) {
	//
	if ( !$debug ) {
		responses::fileSend($reqFileName, $fType);
	}
	else {
		$file_length = filesize($reqFileName);
		$last_modified_time = filemtime($reqFileName);
		$last_modified_time_txt = date('Y-m-d H:m:s', $last_modified_time);
		$last_modified_time_gmt = responses::timeToGMT($last_modified_time);

		//
		echo <<<TXT
IMAGE EXISTS (SENDING):
Modificado (unix): {$last_modified_time}
Modificado (normal): {$last_modified_time_txt}
Modificado (gmt): {$last_modified_time_gmt}
Archivo final: {$reqFileName}
URL Final: {$realUrl}
Tamaño: {$file_length}
TXT;
		exit;
	}
}

// Check if image is being processed
$tempFileMarker = $reqFileName . '.temp';
if ( file_exists($tempFileMarker) ) {
	if ( $debug ) {
		echo "FILE SEEMS TO BE PROCESSING.... ";
	}

	//
	if ( time() - filemtime($tempFileMarker) < 60 ) {
		if ( $debug ) {
			echo "Seems fresh, skipping.";
		}

		http_response_code(503);
		header('Retry-After: 300');
		exit;
	}

	if ( $debug ) {
		echo "Not fresh, attempt again.";
	}

	// Long time has passed, remove current file
	unlink($tempFileMarker);
}

// If not test server, require same referer else ignore (avoid simple attacks)
if ( !$_ENV['SE_DEBUG'] && !$debug ) {
	if ( !isset($_SERVER['HTTP_REFERER']) ) {
		responses::op_killWithError('Invalid page. (R)', '', 500);
	}

	//
	$parse = parse_url($_SERVER['HTTP_REFERER']);

	echo "{$parse['host']} - {$_SERVER['SERVER_NAME']}";

	//
	if ( $parse['host'] !== $_SERVER['SERVER_NAME'] ) {
		responses::op_killWithError('Invalid page. (S)', '', 500);
	}
}

// Notify current file status
if ( $debug ) {
	if ( $rebuildImage ) {
		echo "IGNORING NEW FILE EXIST. GENERATING NEW.\n";
	}
	else {
		echo "OPTIMIZED FILE NOT FOUND: ('{$reqFileName}').\n";
	}
}

//
// ATTEMPT TO CREATE NEW FILE
//

// Find original file (that can have a different file type)
$origFileStruct = $_SERVER['DOCUMENT_ROOT'] . $img_orig_folder . $fName;

if ( ($results = glob($_SERVER['DOCUMENT_ROOT'] . $img_orig_folder . $fName . '.{jpeg,jpg,webp,avif,png}', GLOB_BRACE)) ) {
	$fileFormatChange = true;
	$origFileName = $results[0];
	if ( $debug ) {
		// print_r($results);
		echo "BASE FILE FOUND: '{$origFileName}'.\n";
		echo "URL: 'https://{$_SERVER['HTTP_HOST']}.{$img_orig_folder}{$fName}'.\n";
	}
}
else {
	if ( $debug ) {
		echo "ORIGINAL IMAGE NOT FOUND '{$origFileStruct}'.\nWILL SEND DEFAULT.\n";
		exit;
	}
	http_response_code(404);
	header("Content-Type: image/svg+xml; charset=UTF-8");
	echo file_get_contents($_SERVER['DOCUMENT_ROOT'] . '/snkeng/core/res/se_img/empty.svg');
	exit;
}


// Stop connection notify not available
ignore_user_abort(true);
http_response_code(503);
header('Retry-After: 180');
header("Connection: close");
//
flush();

// Do create?
set_time_limit(120);

//
$fParams = str_replace(['-', '_'], ['&', '='], $fParams);
parse_str($fParams, $fParamList);

// Load image
$origImgSize = getimagesize($origFileName);

//
if ( !file_exists($_SERVER['DOCUMENT_ROOT'] . $img_new_folder) ) {
	if ( !mkdir($_SERVER['DOCUMENT_ROOT'] . $img_new_folder, 0774, true) ) {
		responses::op_killWithError('System Error, Folder doesn\'t exists.', "Folder: {$img_new_folder}");
	}
}

// Load imagick
$cImage = new \Imagick($origFileName);


$imgParams = [
	'm' => (isset($fParamList['m'])) ? $fParamList['m'] : '',
	'r' => (isset($fParamList['r'])) ? $fParamList['r'] : '',
	'c' => (isset($fParamList['c'])) ? $fParamList['c'] : "000",
	'w' => (isset($fParamList['w'])) ? intval($fParamList['w']) : 0,
	'h' => (isset($fParamList['h'])) ? intval($fParamList['h']) : 0,
	'q' => (isset($fParamList['q'])) ? intval($fParamList['q']) : 0,
];

if ( $debug ) {
	echo "PARAMS:\n";
	print_r($imgParams);
	echo "---\n";
	echo "CREATING IMAGE?\n";
}

function validSize($size)
{
	if ( $size < 30 ) {
		return false;
	}
	if ( $size < 50 ) {
		if ( $size % 5 !== 0 ) {
			return false;
		}
	}
	elseif ( $size < 400 ) {
		if ( $size % 10 !== 0 ) {
			return false;
		}
	}
	elseif ( $size < 800 ) {
		if ( $size % 20 !== 0 ) {
			return false;
		}
	}
	elseif ( $size < 1000 ) {
		if ( $size % 50 !== 0 ) {
			return false;
		}
	}
	elseif ( $size <= 2000 ) {
		if ( $size % 100 !== 0 ) {
			return false;
		}
	}
	else {
		return false;
	}

	return true;
}


//
function invalidSize(int $targetWidth, int $targetHeight) : void {
	if ( $targetWidth ) {
		if ( !validSize($targetWidth) ) {
			if ( $GLOBALS['debug'] ) {
				echo "Not valid width ({$imgParams['w']}).\n";
			}

			killInvalidImage();
		}
	} elseif ( $targetHeight ) {
		if ( !validSize($targetHeight) ) {
			if ( $GLOBALS['debug'] ) {
				echo "Not valid width ({$imgParams['w']}).\n";
			}

			killInvalidImage();
		}
	}
}

function killInvalidImage() : void {
	if ( !$GLOBALS['debug'] ) {
		header("HTTP/1.1 404 Not Found", true, 404);
		header("Content-Type: image/svg+xml; charset=UTF-8");
		echo file_get_contents($_SERVER['DOCUMENT_ROOT'] . '/snkeng/core/res/se_img/empty.svg');
	}
	exit();
}

// Check validity of target sizes (only invalid exits early)
invalidSize($imgParams['w'], $imgParams['h']);

//
$originalSizes = [
	'width' => $cImage->getImageWidth(),
	'height' => $cImage->getImageHeight(),
	'aspect_ratio' => $cImage->getImageWidth() / $cImage->getImageHeight(),
];

//
$targetSizes = getTargetImageSize($imgParams['w'], $imgParams['h'], $imgParams['r'], $originalSizes['width'], $originalSizes['height']);

//
if ( $debug ) {
	echo <<<TEXT
\n ---
PRE OP PROPERTIES
PROP   | ORIG | TARGET
WIDTH  | {$originalSizes['width']} | {$targetSizes['width']} |
HEIGHT | {$originalSizes['height']} | {$targetSizes['height']} |
ASPECT | {$originalSizes['aspect_ratio']} | {$targetSizes['aspect_ratio']} |
---\n
TEXT;

}

//
switch ( $imgParams['m'] ) {
	//
	case 'smart':
	// Crop the image in order to have the same image, but cutting anything extra if it has not the same aspect ratio
	case 'fit':
		//
		if ( $debug ) {
			echo <<<TEXT
\n ---
FIT IMAGE
Target Size: {$targetSizes['width']}x{$targetSizes['height']}\n
TEXT;

		}
		if ( $targetSizes['aspect_ratio'] > $originalSizes['aspect_ratio'] ) {
			// target image is wider, resize to width with extra height, crop height

			$height_original = round($targetSizes['width'] / $originalSizes['aspect_ratio']);

			// Mid resize
			$cImage->resizeImage($targetSizes['width'], $height_original, imagick::FILTER_QUADRATIC, 1);

			// Crop result
			$y = getCenteringDisplacement($targetSizes['height'], $height_original);
			$cImage->cropImage($targetSizes['width'], $targetSizes['height'], 0, $y);

			//
			if ( $debug ) {
				echo <<<TEXT
WIDER TARGET, ADJUSTING WIDTH CROP
RESIZE IS: {$targetSizes['width']}x{$height_original}
CROP Y displacement: $y\n
TEXT;
			}
		} else if ( $targetSizes['aspect_ratio'] < $originalSizes['aspect_ratio'] ) {
			// Image taller, get the proper height, crop the width
			$width_original = round($targetSizes['height'] * $originalSizes['aspect_ratio']);

			$cImage->resizeImage($width_original, $targetSizes['height'], imagick::FILTER_QUADRATIC, 1);

			// Should crop result
			$x = getCenteringDisplacement($targetSizes['height'], $width_original);
			$cImage->cropImage($targetSizes['width'], $targetSizes['height'], $x, 0);

			//
			if ( $debug ) {
				echo <<<TEXT
WIDE IMAGE
RESIZE IS: {$width_original}x{$targetSizes['height']}
CROP X displacement: $x\n
TEXT;
			}
		} else {
			// Same aspect ratio, simple crop
			$cImage->cropThumbnailImage($targetSizes['w'], $targetSizes['h']);
		}

		//
		if ( $debug ) {
			echo <<<TEXT
\n---\n
TEXT;

		}
		break;

	// Simple crop to maximum dimensions (excellent for thumbnail)
	case 'crop':
		$cImage->cropThumbnailImage($imgParams['w'], $imgParams['h']);
		break;
	//
	default:
		//
		if ( $debug ) {
			echo <<<TEXT
\n ---
NORMAL RESIZE
Target Size: {$imgParams['w']}x{$imgParams['h']}
Original Size: {$originalSizes['width']}x{$originalSizes['height']}
---\n
TEXT;

		}
		$cImage->adaptiveResizeImage($targetSizes['width'], $targetSizes['height'], true);
		break;
}

// Create blocking file
file_put_contents($tempFileMarker, "");

// Save file according to parameters
switch ( $fType ) {
	//
	case 'jpeg':
	case 'jpg':
		$imgParams['q'] = ($imgParams['q']) ?: 70;
		$cImage->setImageCompressionQuality($imgParams['q']);
		$cImage->setImageFormat('jpeg');
		break;
	//
	case 'webp':
		$imgParams['q'] = ($imgParams['q']) ?: 60;
		$cImage->setImageCompressionQuality($imgParams['q']);
		$cImage->setImageFormat('webp');
		break;
	//
	case 'avif':
		$imgParams['q'] = ($imgParams['q']) ?: 50;
		if ( !\Imagick::queryformats('AVIF*') ) {
			// Avif not supported can redirect?
			$webpLoc = str_replace('.avif', '.webp', $_SERVER['REQUEST_URI']);
			header("Location: " . $webpLoc);
			exit;
		}
		$cImage->setCompressionQuality($imgParams['q']);
		$cImage->setImageFormat('avif');
		break;
	//
	default:
		break;
}

// Save image
$cImage->writeImage($reqFileName);

// Imprimir debug
if ( $debug ) {
	echo "IMAGEN CREADA, PERO MODO DEBUG ON\n";
	echo "Calidad:{$imgParams['q']}\n";
	echo "END FILE:{$reqFileName}\n";
	echo "URL: {$realUrl}\n";
}

// Current version no longer serves the image so first load is always a 503 http status code
// responses::fileSend($reqFileName, $fType);

// Remove temp file
unlink($tempFileMarker);

//
exit;
