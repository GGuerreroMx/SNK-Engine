<?php
header("Cache-Control: no-cache, must-revalidate");

// $url = 'http://www.searchenginepeople.com/wp-content/uploads/2012/04/php-twitter1.jpg';
$url = urldecode($_GET['url']);
//
if ( empty($url) ) {
	die('URL not defined');
}
//
if ( !filter_var($url, FILTER_VALIDATE_URL)) {
	//
	header('Content-type: text/plain; charset=utf-8');
	echo "NOT URL.\n{$url}";
}

//
$imginfo = getimagesize($url);

//
if ( in_array($imginfo['mime'], ['image/jpg', 'image/jpeg', 'image/gif', 'image/png', 'image/webp', 'image/avif']) ) {
	header("Content-type: {$imginfo['mime']}");
	readfile($url);
} elseif ( !$imginfo ) {
	//
	header('Content-type: text/plain; charset=utf-8');
	echo "NOT AN IMAGE.\n{$url}";
} else {
	header('Content-type: text/plain; charset=utf-8');
	echo "IMAGE NOT VALID: '{$imginfo['mime']}'.\n";
}

exit();
