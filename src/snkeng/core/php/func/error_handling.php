<?php

function ERR_HTML_OUTPUT(array $errData) : void {

	// Set HTML and status code
	header('Content-Type: text/html; charset=utf-8', true, 500);
	header('Pragma: no-cache');


	echo <<<HTML
<html>
<head>
<title>ERROR</title>
</head>
<body>
<h1>ERROR</h1>
HTML;

	//
	if ( empty($errData['trace']) ) {
		//
		echo <<<HTML
<h1>INFORMATION</h1>
{$errData['description']}
HTML;
		//
	} else {
		echo <<<HTML
<h2>INFORMATION</h2>
{$errData['description']}
<h2>LOCATION</h2>
{$errData['location']}
<h2>TRACE</h2>
<pre>
{$errData['trace']}
</pre>
HTML;
	}

	//
	echo <<<HTML
</body>
</html>
HTML;
	exit();
}

function ERR_JSON_OUTPUT(array $errData) : void {
	$_SERVER["HTTP_USER_AGENT"] = (!empty($_SERVER["HTTP_USER_AGENT"])) ? $_SERVER["HTTP_USER_AGENT"] : '';
	if ( preg_match("/msie\s(\d+).(\d+)/", strtolower($_SERVER["HTTP_USER_AGENT"]), $arr) ) {
		header('Content-type: text/html; charset=utf-8', true, 500);
	} else {
		header('Content-type: application/json; charset=utf-8', true, 500);
	}

	//
	$errData['timestamp'] = date("Y-m-d H:i:s");

	//
	echo(json_encode($errData, JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES | JSON_PRETTY_PRINT));
	exit();
}



// Custom definitions for error handling.
function ERR_HANDLER($errno, $errstr, $errfile, $errline) : bool
{

	// Only E_USER_* error should be processed here
	if ( !(error_reporting() & $errno) ) {
		// This error code is not included in error_reporting, so let it fall
		// through to the standard PHP error handler
		return false;
	}

	//
	$errstr = htmlspecialchars($errstr);

	//
	$errData = [
		'title' => "ERROR: ",
		'description' => "[{$errno}] {$errstr}",
		'location' => "{$errfile}' @ {$errline}",
		'trace' => ''
	];

	//
	switch ( $errno ) {
		case E_USER_ERROR:
			$errData['title'].= 'USER ERROR';
			break;

		case E_USER_WARNING:
			$errData['title'].= 'USER WARNING';
			break;

		case E_USER_NOTICE:
			$errData['title'].= 'USER NOTICE';
			break;

		default:
			$errData['title'].= 'NOT SET';
			break;
	}

	// Log error in system
	error_log("ERR: {$errData['description']}. LOC: {$errData['location']}.");

	//
	if ( $_ENV['SE_DEBUG'] ) {
		$errData['trace'] = (new Exception)->getTraceAsString();
	}

	//
	if ( !empty($_ENV['SE_ASYNC']) && $_ENV['SE_ASYNC'] ) {
		ERR_JSON_OUTPUT($errData);
	} else {
		ERR_HTML_OUTPUT($errData);
	}

	return true;
}

function EXC_HANDLER(Throwable $exception) {
	//
	$errData = [
		'title' => "ERROR",
		'description' => $exception->getMessage(),
		'location' => $exception->getFile() . ' @ ' . $exception->getLine(),
		'trace' => null
	];

	//
	if ( $_ENV['SE_DEBUG'] ) {
		//
		$errData['trace'] = $exception->getTraceAsString();
	}

	// Log error in system
	error_log("EXC: {$errData['description']}. LOC: {$errData['location']}.");

	//
	if ( !empty($_ENV['SE_ASYNC']) && $_ENV['SE_ASYNC'] ) {
		ERR_JSON_OUTPUT($errData);
	} else {
		ERR_HTML_OUTPUT($errData);
	}
}

//
set_error_handler("ERR_HANDLER");
set_exception_handler("EXC_HANDLER");