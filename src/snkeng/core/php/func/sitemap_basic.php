<?php

//
$siteMap = <<<XML
<?xml version='1.0' encoding='UTF-8'?>
<urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">\n
XML;
//
$urlStr = <<<XML
<url><loc>{$siteVars['server']['url']}!url;</loc><lastmod>!dtMod;</lastmod></url>\n
XML;


// primero imprimir el site (default)
$sql_qry = <<<SQL
SELECT
art_url AS url,
DATE(GREATEST(art_dt_mod, art_dt_pub)) AS dtMod
FROM sc_site_articles
WHERE art_status_published=1 AND art_status_indexable=1 AND art_dt_pub<NOW();
SQL;
$siteMap.= \snkeng\core\engine\mysql::printSimpleQuery($sql_qry, $urlStr);


// Search other main apps for posible
$appDir = $_SERVER['DOCUMENT_ROOT']."/snkeng/site_modules/";
$cdir = scandir($appDir);
foreach ( $cdir as $key => $value ) {
	if ( !in_array($value, [".", ".."]) ) {
		$tempDir = $appDir . DIRECTORY_SEPARATOR . $value;
		$plausibleFile = $tempDir . DIRECTORY_SEPARATOR . "map" . DIRECTORY_SEPARATOR . "sitemap.php";
		if ( is_dir($tempDir) && file_exists($plausibleFile) ) {
			$file = (require($plausibleFile));
			$siteMap.= \snkeng\core\engine\mysql::printSimpleQuery($file['sql'], $urlStr);
		}
	}
}

// Fin
$siteMap.= "</urlset>";

// Enviar información
if ( strstr($_SERVER['HTTP_USER_AGENT'], 'W3C_Validator') !== false || strstr($_SERVER['HTTP_ACCEPT_ENCODING'], 'gzip') === false || $_ENV['SE_DEBUG'] ) {
	// do not compress
	header('Content-type: text/xml; charset=UTF-8');
}else{
	// can send GZIP compressed data
	header('content-type: application/x-gzip');
	header('Content-Disposition: attachment; filename="sitemap.gz"');
	$siteMap = gzencode($siteMap, 9);
}
// Imprimir contenido
echo $siteMap;
exit();