<?php

/**
 * CURL wrapper intended for simplification of CURL operations
 *
 * @param string $url URL that curl request will be made to
 * @param array $params Setup parameters for CURL, must actually read documentation beforehadn
 * @param bool $debug Anex configuration to response for debugging purposes
 * @return array
 */
function se_curl($url, $params, $debug = false) {
	//
	if ( !function_exists('curl_init') ) {
		se_killWithError('ERROR SISTEMA: CURL no definido.');
	}

	// Defaults
	$params['headers'] = ( isset($params['headers']) ) ? $params['headers'] : [];
	$params['getParams'] = ( isset($params['getParams']) ) ? $params['getParams'] : [];
	$params['postData'] = ( isset($params['postData']) ) ? $params['postData'] : [];
	$params['curl_params'] = ( isset($params['curl_params']) ) ? $params['curl_params'] : [];
	$params['method'] = ( isset($params['method']) ) ? $params['method'] : 'GET';
	$params['return'] = ( isset($params['return']) ) ? $params['return'] : false;

	// CURL PARAMS DEFAULT
	$params['curl_params'][CURLOPT_CONNECTTIMEOUT] = ( !empty($params['curl_params'][CURLOPT_CONNECTTIMEOUT]) ) ? $params['curl_params'][CURLOPT_CONNECTTIMEOUT] : 10;
	$params['curl_params'][CURLOPT_TIMEOUT] = ( !empty($params['curl_params'][CURLOPT_TIMEOUT]) ) ? $params['curl_params'][CURLOPT_TIMEOUT] : 30;

	// Response structure
	$response = [
		'info' => '',
		'content' => '',
		'error' => [
			'number' => 0,
			'text' => ''
		]
	];

	// Get data
	if ( $params['getParams'] ) {
		$url.= '?' . http_build_query($params['getParams']);
	}

	// URL
	$params['curl_params'][CURLOPT_URL] = $url;

	//
	switch ( $params['method'] ) {
		//
		case 'GET':
			break;
		//
		case 'POST':
			$params['headers'][] = 'Content-Type: application/x-www-form-urlencoded';
			//
			$params['curl_params'][CURLOPT_POST] = TRUE;
			$params['curl_params'][CURLOPT_POSTFIELDS] = http_build_query($params['postData']);
			break;
		//
		case 'JSON':
			$jsonData = json_encode($params['postData']);
			$params['headers'][] = 'Content-Type: application/json';
			$params['headers'][] = 'Content-Length: ' . strlen($jsonData) ;
			//
			$params['curl_params'][CURLOPT_POST] = TRUE;
			$params['curl_params'][CURLOPT_POSTFIELDS] = $jsonData;
			break;
		//
		default:
			die("CURL - Método no programado. ". $params['method']);
			break;
	}

	//
	if ( $params['return'] ) {
		$params['curl_params'][CURLINFO_HEADER_OUT] = TRUE;
		$params['curl_params'][CURLOPT_RETURNTRANSFER] = TRUE;
	}

	// Final headers
	$params['curl_params'][CURLOPT_HTTPHEADER] = $params['headers'];

	// CURL ACTUAL OPERATION

	//
	$ch = curl_init();
	curl_setopt_array($ch, $params['curl_params']);

	// Comprobar el código de estado HTTP
	if ( curl_errno($ch) ) {
		$response['error']['number'] = curl_errno($ch);
		$response['error']['text'] = curl_error($ch);
	}

	//
	$response['info'] = curl_getinfo($ch);

	//
	if ( $params['return'] ) {
		$response['content'] = curl_exec($ch);
	} else {
		curl_exec($ch);
	}

	//
	curl_close ($ch);

	//
	if ( $params['return'] === 'json' ) {
		$response['content'] = json_decode($response['content'], true);
	}

	//
	if ( $debug ) {
		$response['debug'] = $params;
	}

	//
	return $response;
}