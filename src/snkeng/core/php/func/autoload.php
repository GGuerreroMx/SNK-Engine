<?php
//
spl_autoload_register(function ($className) {
	$file = '';

	// If not starts with snkeng, skip
	if ( !substr($className, 0, 6) === "snkeng" ) {
		return;
	}

	//
	$classNameMod = substr($className, 7);

	//
	$classPaths = explode('\\', $classNameMod);

	//
	$mainDirectory = '';
	$secondaryDirectory = '';
	
	//
	$reqClass = array_shift($classPaths);
	switch ( $reqClass ) {
		//
		case 'site_core':
			$mainDirectory = 'site_core';
			break;
		//
		case 'admin_core':
			$mainDirectory = 'admin_core';
			break;
		//
		case 'core':
			$mainDirectory = 'core';
			break;
		//
		case 'admin_modules':
			$mainDirectory = 'admin_modules' . DIRECTORY_SEPARATOR . array_shift($classPaths);
			break;
		//
		case 'site_modules':
			$mainDirectory = 'site_modules' . DIRECTORY_SEPARATOR . array_shift($classPaths);
			break;
		//
		default:
			echo "AUTOLOAD ERROR: Invalid internal 'snkeng' class requested ({$reqClass}). Asked for: {$className}.";
			exit;
			break;
	}

	//
	$loadFile = $_SERVER['DOCUMENT_ROOT'] . "/snkeng/{$mainDirectory}/class/" . implode(DIRECTORY_SEPARATOR, $classPaths) . ".php";

	if ( is_readable($loadFile) ) {
		require_once $loadFile;
	} else {
		$cTrace = print_r(debug_backtrace(), true);
		se_killWithError('Class file not present.', "CLASS: {$className}.\nCLASS MOD: {$classNameMod}.\nFILE: {$loadFile}\nTRACE:\n{$cTrace}");
	}
});

// PHP composer
require $_SERVER['DOCUMENT_ROOT'] . '/vendor/autoload.php';
