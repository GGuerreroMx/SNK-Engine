<?php

// Regresar un error
function se_returnError($error, $errorAdvanced = '') {
	global $response;
	$response['s'] = [
		't' => 0,
		'e' => $error,
		'ex' => $errorAdvanced,
		'date' => date('h:i:s A'),
		'time' => time()
	];
	printJSONData($response);
}

//
$forceDebug = false; // Peligrosidad 1000

//
if ( false ) {
	var_dump($_SERVER);
	die("mofo die");
}

//
error_reporting(E_ERROR | E_WARNING | E_PARSE);

// Funciones básicas (session, siteVars, server validation, lang, mysql)
require $_SERVER['DOCUMENT_ROOT'] . '/snkeng/core/php/load/head.php';
//
$response = [
	's' => [
		't' => 1,
		'e' => '',
		'ex' => '',
	]
];

//<editor-fold desc="Passing">

// Filtrado
if ( $_SERVER['HTTP_USER_AGENT'] !== "MSIE 3.0" ) {
	se_returnError('E-Exp');
}

//
if ( $forceDebug ) {
	error_reporting(E_ALL);
	ini_set('display_errors', TRUE);
	ini_set('display_startup_errors', TRUE);
}

// Variables

//
if ( empty($_POST['passA']) ) {
	se_returnError('E-NP', "No data.\n".print_r($_POST, true));
}

//
$_POST['passA'] = preg_replace("/[^A-z0-9@.#_-]/", "", $_POST['passA'] );


//
if ( $_POST['passA'] !== $_ENV['SE_SYNC_CODE'] ) {
	$error = ( $forceDebug ) ? "PW-A (ENG): '{$_ENV['SE_SYNC_CODE']}' - '{$_POST['passA']}' - '{$passA}'." : '';
	se_returnError('E-WP1', $error);
}

//</editor-fold>

// Operacion

// Soportar SQL de verdad
\snkeng\core\engine\mysql::changeConnection( $_ENV['MYSQL_ADM_USR'], $_ENV['MYSQL_ADM_PWD'] );

//
switch ( $_POST['op'] )
{
	// Actualizar archivos
	case 'files':
		switch ( $_POST['act'] ) {
			//
			case 'read':
				$response['d']['date'] = se_siteVar('engUpdate');
				break;
			//
			case 'upload':
				if ( $_FILES['file']['error'] ) {
					switch ( intval($_FILES['file']['error']) ) {
						//
						case 1:
							se_returnError("Error al subir el archivo", "El tamaño del archivo excede el máximo permitido por el servidor.");
							break;
						//
						case 2:
							se_returnError("Error al subir el archivo","El tamaño del archivo excede el máximo permitido por el formulario.");
							break;
						//
						case 3:
							se_returnError("Error al subir el archivo","Archivo subido parcialmente, error en la comunicación.");
							break;
						//
						case 4:
							se_returnError("Error al subir el archivo","No se subió el archivo.");
							break;
						//
						case 6:
							se_returnError("Error al subir el archivo","No hay carpeta para subir los archivos temporalmente.");
							break;
						//
						case 7:
							se_returnError("Error al subir el archivo","No se tienen los permisos en el SO para guardar los archivos subidos.");
							break;
						//
						case 8:
							se_returnError("Error al subir el archivo","Desconocido.");
							break;
					}
				}

				// debugVariable($_FILES, 'ARCHIVOS');

				// Revisar archivo
				if ( empty($_FILES['file']['tmp_name']) ) {
					se_returnError("Error al subir el archivo. (no se encuentra en el sistema");
				}

				// Ejectuar
				$engUpdater = new \snkeng\core\engine\fileUpd();
				$engUpdater->files_updateExec($response, $_FILES['file']['tmp_name'], $_POST['date']);
				break;
			//
			case 'add':
				// debugVariable($_FILES, 'ARCHIVOS');
				// Revisar archivo
				$engUpdater = new \snkeng\core\engine\fileUpd();
				$engUpdater->files_addExec($response, $_FILES['file']['tmp_name']);
				break;
			//
			default:
				$response['s']['t'] = 0;
				$response['s']['e'] = "FILES - NO ACT";
				break;
		}
		break;
	//
	case 'sql_exec':
		$sql_qry = $_POST['sql'];
		if ( get_magic_quotes_gpc() ) { $sql_qry = stripslashes($sql_qry); }
		// Revisar archivo
		$engUpdater = new \snkeng\core\engine\fileUpd();
		$engUpdater->sql_direct($response, $sql_qry);
		break;
	//
	case 'sql_file':
		// Revisar archivo
		$engUpdater = new \snkeng\core\engine\fileUpd();
		$engUpdater->sql_file($response, $_FILES['file']['tmp_name']);
		break;

	// Error
	default:
		$response['s']['t'] = 0;
		$response['s']['e'] = "E-WROP";
		break;
}


// Imprimir resultados
if ( $_SERVER['HTTP_USER_AGENT'] === "MSIE 3.0" || $forceDebug )
{
	printJSONData($response);
} else {
	http_response_code(404);
	echo("404 - NOT FOUND");
}

//
exit();
