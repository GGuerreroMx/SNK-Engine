<?php
// Basic library (spl_autoload_register and basic debugging)
require $_SERVER['DOCUMENT_ROOT'] . '/snkeng/core/php/func/error_handling.php';
require $_SERVER['DOCUMENT_ROOT'] . '/snkeng/core/php/func/autoload.php';
require $_SERVER['DOCUMENT_ROOT'] . '/snkeng/core/php/func/basic_library.php';

// Load environment varibles
\snkeng\core\engine\environment::loadEnv('.');

// Bool convert debug status.
$_ENV['SE_DEBUG'] = ( $_ENV['SE_DEBUG'] === 'true' );


// Default site variables load
$siteVars = ( require $_SERVER['DOCUMENT_ROOT'] . '/snkeng/site_core/specs/s_sitevars.php' );

// Server specifications
$serverParams = ( require $_SERVER['DOCUMENT_ROOT'] . '/snkeng/site_core/specs/s_server_config.php' );

// Server validation: live, test, redirect or fail
$dbData = [];
foreach ( $serverParams['locations'] as $loc => $params ) {
	if ( $_SERVER['SERVER_NAME'] === $loc ) {
		// Redirección automática
		if ( isset($params['redirect']) && !empty($params['redirect']) ) {
			header('HTTP/1.1 301 Moved Permanently');
			header('Location: '.$params['redirect'].$_SERVER['REQUEST_URI']);
			exit();
		}
		break;
	}
}

// Page navigation load
\snkeng\core\engine\nav::init();


// Language setup / override
$newLang = '';
if ( isset($_GET['lang']) ) {
	// Validate valid language
	if ( in_array($_GET['lang'], $siteVars['site']['langs']) ) {
		$_SESSION['lang'] = $_GET['lang'];
	}
}
// Update lang if required
if ( isset($_SESSION['lang']) ) {
	$siteVars['site']['lang'] = $_SESSION['lang'];
}

// Definir servidor seguro (bug en caso de no tener SSL)
$siteVars['server']['url'] = ( !empty($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on' ) ? 'https://'.$_SERVER["SERVER_NAME"] : 'http://'.$_SERVER["SERVER_NAME"];

// Conectar con base de datos y definir región
\snkeng\core\engine\mysql::firstConnect( $_ENV['MYSQL_SERVER'], $_ENV['MYSQL_DB_NAME'], $_ENV['MYSQL_USR_USR'], $_ENV['MYSQL_USR_PWD'] );
if ( !isset($siteVars['site']['lang']) || $siteVars['site']['lang'] !== 'en_US' ) {
	\snkeng\core\engine\mysql::submitQuery('SET lc_time_names="es_MX";');
}

// Liberar memoria
unset($serverParams);