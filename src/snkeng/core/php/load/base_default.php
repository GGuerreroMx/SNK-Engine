<?php
//
function page_build_print() {
	global $siteVars, $page;
	//
	$cPage = [
		'titleFull' => '',
		'headFiles' => '',
		'headJS' => '',
		'extraHead' => '',
		'extFiles' => [],
		'extFilesTxt' => '',
		'og' => [
			'image'=>'',
			'type'=>'',
			'twType'=>'',
			'extra'=>'',
		]
	];
	// debugVariable($page['files']);

	//<editor-fold desc="SETUP">
	$cPage['extraHead'] .= $page['head']['extraHead'];

	// Parámetros
	if ( $_ENV['SE_DEBUG'] ) {
		// Evitar robots
		$cPage['extraHead'] .= '<META NAME="ROBOTS" CONTENT="NOINDEX, NOFOLLOW">'."\n";
	} else {
		// Google Analytics v3 (sunsetting)
		if ( !empty($_ENV['GOOGLE_ANALYTICS_V3']) ) {
			$cPage['headJS'].= <<<JS
	var _gaq = _gaq || [];
	_gaq.push(['_setAccount', '{$_ENV['GOOGLE_ANALYTICS_V3']}'], ['_setDomainName', '{$siteVars['site']['domain']}'], ['_trackPageview']);\n
JS;
			//
			$page['files']['ext'][] = '//www.google-analytics.com/ga.js';
		}

		// Google Tag (also related to google analytics v4)
		if ( !empty($_ENV['GOOGLE_TAG_V1']) ) {
			$cPage['headJS'].= <<<JS
	window.dataLayer = window.dataLayer || [];
	function gtag(){dataLayer.push(arguments);}
	gtag('js', new Date());
	gtag('config', '{$_ENV['GOOGLE_TAG_V1']}');\n
JS;
			//
			$cPage['headFiles'].= "<script async defer src=\"https://www.googletagmanager.com/gtag/js?id={$_ENV['GOOGLE_TAG_V1']}\"></script>\n";
		}
	}
	//</editor-fold>


	//<editor-fold desc="File management">

	// Vanilla CSS files
	foreach ( $page['files']['css'] as $cssLoc ) {
		$cPage['headFiles'].= "<link type=\"text/css\" rel=\"stylesheet\" href=\"{$cssLoc}\" />\n";
	}
	// JS modules
	foreach ( $page['files']['mjs'] as $mjsLoc ) {
		$cPage['headFiles'].= "<script type=\"module\" src=\"{$mjsLoc}\"></script>\n";
	}
	// JS files (allways defered)
	foreach ( $page['files']['js'] as $jsLoc ) {
		$cPage['headFiles'].= "<script defer src=\"{$jsLoc}\"></script>\n";
	}
	// Add to load after first load
	foreach ( $page['files']['svg'] as $file ) {
		$cPage['extFiles'][] = $file;
	}
	// External files (loaded after core functionality)
	foreach ( $page['files']['ext'] as $file ) {
		$cPage['extFiles'][] = $file;
	}

	// External files merge (async download)
	$cPage['extFilesTxt'] = implode(";", $cPage['extFiles']);

	//</editor-fold>


	//<editor-fold desc="Basics">

	// OG Defaults
	$cPage['titleFull'] = siteTitle($siteVars['site']['name'], $page['head']['title']);
	$page['head']['title'] = ( !empty($page['head']['title']) ) ? $page['head']['title'] : $siteVars['site']['name'];

	//
	$page['head']['og']['img'] = ( isset($page['head']['og']['img']) ) ? $page['head']['og']['img']: $siteVars['server']['url'].$siteVars['site']['img'];
	$page['head']['og']['type'] = ( isset($page['head']['og']['type']) ) ? $page['head']['og']['type'] : 'article';
	$page['head']['og']['twType'] = ( isset($page['head']['og']['twType']) ) ? $page['head']['og']['twType'] : 'summary_large_image';

	// Extra OG
	if ( count($page['head']['og']['data']) !== 0 ) {
		foreach ( $page['head']['og']['data'] as $ogd ) {
			$propName = ( $ogd[0] === 'twitter' ) ? 'name' : 'property';
			$cPage['og']['extra'] .= "<meta data-pageOnly='1' {$propName}='{$ogd[1]}' content='{$ogd[2]}'>\n";
		}
	}

	// Extra Link
	if ( isset($page['head']['extra']) && count($page['head']['extra']) !== 0 ) {
		foreach ( $page['head']['extra'] as $hf ) {
			$cPage['og']['extra'] .= "<{$hf['type']} data-pageOnly='1' {$hf['content']} />\n";
		}
	}

	//</editor-fold>

	//<editor-fold desc="Return HTML">

	$canonicalUrl = $siteVars['server']['url'].$page['head']['url'];

	return <<<HTML
<!doctype html>
<html lang="{$siteVars['site']['lang']}">\n
<head prefix="og: http://ogp.me/ns#">
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width,initial-scale=1">
<!-- DESCRIPTION -->
<title>{$cPage['titleFull']}</title>
<meta name="keywords" content="{$page['head']['metaWord']}">
<meta name="description" content="{$page['head']['metaDesc']}">
<link rel="canonical" href="{$canonicalUrl}">
<meta property="og:site_name" content="{$siteVars['site']['name']}">
<meta property="og:title" content="{$page['head']['title']}">
<meta property="og:url" content="{$canonicalUrl}">
<meta property="og:image" content="{$page['head']['og']['img']}">
<meta property="og:description" content="{$page['head']['metaDesc']}">
<meta property="og:type" content="{$page['head']['og']['type']}">
<meta name="twitter:card" content="{$page['head']['og']['twType']}">
<!-- META -->
<meta name="se:files" content="{$cPage['extFilesTxt']}">
<meta name="se:properties" data-name="{$siteVars['site']['name']}" data-url="{$siteVars['server']['url']}" data-image="{$siteVars['site']['img']}" >
{$cPage['og']['extra']}<!-- FILES -->
{$cPage['headFiles']}<!-- TEMPLATE -->
{$cPage['extraHead']}<!-- HEAD JS -->
<!-- MacFix lack of custom elements -->
<script>
(self.chrome||self.netscape)||document.write('<script src="https://unpkg.com/@ungap/custom-elements/es.js" />');
</script>
<script>{$cPage['headJS']}</script>
</head>\n
<body data-userlogin="0">
<!-- INI:Content -->
<div id="se_template_root">\n{$page['body']}\n</div>
<!-- END:Content -->\n\n
<!-- INI:SCRIPT -->
<script id="jsMain">
// Javascript (global)
{$page['js']}
// Javascript postLibraries
function postLoad() {
{$page['mt']}
}
</script>
<script id="jsAjax"></script>
<!-- END:SCRIPT -->
</body>
</html>
HTML;
	//

	//</editor-fold>
}
