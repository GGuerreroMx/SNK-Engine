<?php
// Based on facebook local implementation with data from http://usefulangle.com/post/9/google-login-api-with-php-curl


//
namespace snkeng\core\external;
require_once $_SERVER['DOCUMENT_ROOT'] . '/snkeng/core/php/func/curl.php';

//
class sn_google_se
{
	// Variables
	private $appData = ['id' => '', 'secret' => ''];
	private $siteVars;
	private $userAccessToken;
	private $gpConnectUrl = '/api/user/log/in/socnet/gp';

	public function __construct()
	{
		global $siteVars;
		$this->siteVars =& $siteVars;
		if ( !empty($siteVars['sn']['gp']['app']) ) {
			$this->appData['id'] = $siteVars['sn']['gp']['app']['id'];
			$this->appData['secret'] = $siteVars['sn']['gp']['app']['secret'];
		} else {
			se_killWithError("SYSTEM ERROR, NO GOOGLE DATA IN THE PLATFORM");
		}
	}

	//
	public function getUser()
	{
		//
		$this->userAccessToken = null;

		//
		if ( isset($_SESSION) && isset($_SESSION['user']['gp']['token']) ) {
			// Sessión, validar
			$this->userAccessToken = $_SESSION['user']['gp']['token'];
		} else {
			// Revisar redirección
			$code = (isset($_GET['code'])) ? strval($_GET['code']) : '';
			$state = (isset($_GET['state'])) ? strval($_GET['state']) : '';
			$uniqueCode = md5(session_id());

			//
			if ( !empty($code) && $state === $uniqueCode ) {
				//
				$gp_response = se_curl(
					'https://www.googleapis.com/oauth2/v4/token',
					[
						'curl_params' => [
							CURLOPT_SSL_VERIFYPEER => false,
						],
						'method' => 'POST',
						'postData' => [
							'client_id' => $this->appData['id'],
							'client_secret' => $this->appData['secret'],
							'redirect_uri' => $this->siteVars['server']['url'] . $this->gpConnectUrl,
							'code' => $code,
							'grant_type' => 'authorization_code'
						],
						'return' => 'json'
					],
					true
				);


				//
				if ( isset($gp_response['content']['access_token']) ) {
					$this->userAccessToken = $gp_response['content']['access_token'];
					$_SESSION['user']['gp']['token'] = $this->userAccessToken;
				} else {
					debugVariable($gp_response, 'NO ACCESS TOKEN');
				}
			}
			//
		}

		//
		if ( !$this->userAccessToken ) {
			// No hay conexión, solicitar
			$qry = http_build_query([
				'client_id' => $this->appData['id'],
				'redirect_uri' => $this->siteVars['server']['url'] . $this->gpConnectUrl,
				'response_type' => 'code',
				'access_type' => 'online',
				'scope' => 'https://www.googleapis.com/auth/userinfo.profile',
				'state' => md5(session_id()),           // Not necesary probably not working... remove?
			]);
			header('Location: https://accounts.google.com/o/oauth2/v2/auth?' . $qry);
			exit();
		} else {
			return $this->getUserCoreData();
		}
	}

	//
	public function getUserCoreData()
	{
		//
		$coreData = se_curl(
			'https://www.googleapis.com/oauth2/v2/userinfo',
			[
				'getParams' => [
					'fields' => 'name,email,gender,id,picture'
				],
				'headers' => [
					'Authorization: Bearer ' . $this->userAccessToken
				],
				'curl_params' => [
					CURLOPT_SSL_VERIFYPEER => false,
				],
				'return' => 'json'
			]
		);

		// Modify user
		$userData = $coreData['content'];
		list($userData['first_name'], $userData['last_name']) = explode(' ', $userData['name'], 2);

		//
		return $userData;
	}

}
