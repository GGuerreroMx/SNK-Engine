<?php

//
namespace snkeng\core\external;
require_once $_SERVER['DOCUMENT_ROOT'] . '/snkeng/core/php/func/curl.php';

//
class sn_facebook_se
{
	// Variables
	private $appData = ['id' => '', 'secret' => ''];
	private $siteVars;
	private $appAccessToken;
	private $userAccessToken;
	private $fbConnectUrl = '/api/user/log/in/socnet/fb';

	private $graphVersion = 'v3.3';

	private $userId = 0;

	public function __construct()
	{
		global $siteVars;
		$this->siteVars =& $siteVars;
		if ( !empty($siteVars['sn']['fb']['app']) ) {
			$this->appData['id'] = $siteVars['sn']['fb']['app']['id'];
			$this->appData['secret'] = $siteVars['sn']['fb']['app']['secret'];
		} else {
			se_killWithError("NO FACEBOOK DATA");
		}
	}

	//
	// INIT: giveAppAccess
	//	Revisar por acceso a la aplicación
	public function giveAppAccess()
	{
		//
		if ( !empty($this->appAccessToken) ) {
			return;
		}

		//
		if ( empty($_SESSION['user']['fb']['accessToken']) ) {
			// Conectar a FB
			$fb_response = se_curl(
				"https://graph.facebook.com/oauth/access_token",
				[
					'getParams' => [
						'client_id' => $this->appData['id'],
						'client_secret' => $this->appData['secret'],
						'grant_type' => 'client_credentials'
					],
					'return' => true
				]
			);
//
			if ( $fb_response['error']['number'] ) {
				debugVarToSQL('FB_LOGIN_RESPONSE', $fb_response, true);
			}


			// Procesar
			$stParts = explode("=", $fb_response['content']);
			if ( $stParts[0] == "access_token" ) {
				$_SESSION['user']['fb']['accessToken'] = $stParts[1];
				$this->appAccessToken = $stParts[1];
			} else {
				die("ERROR (SISTEMA): FB auth.");
			}
		} else {
			$this->appAccessToken = $_SESSION['user']['fb']['accessToken'];
		}
	}
	// END: giveAppAccess
	//

	//
	// INIT: giveUserAccess
	//	Revisar por acceso a la aplicación
	public function giveUserAccess()
	{
		if ( !empty($this->userAccessToken) ) {
			return;
		}

		// Conectar a FB
		$token_url = "https://graph.facebook.com/oauth/access_token?";
		$token_url .= "client_id={$this->appData['id']}&client_secret={$this->appData['secret']}&grant_type=client_credentials";

		$this->userAccessToken = file_get_contents($token_url);
	}
	// END: giveUserAccess
	//

	private function graphQry($path, $params)
	{
		// Conectar a FB
		$fb_response = se_curl(
			'https://graph.facebook.com' . $path,
			[
				'getParams' => $params,
				'return' => true
			]
		);

		//
		if ( $fb_response['error']['number'] ) {
			debugVarToSQL('FB_GRAPHQL_RESPONSE_GRAPH_QRY_CURL_ERROR', $fb_response, true);
		}

		//
		$result = $fb_response['content'];

		//
		if ( substr($result, 0, 1) === '{' ) {
			$result = json_decode($result, true);

			//
			if ( isset($result['error']) ) {
				debugVarToSQL('FB_GRAPHQL_RESPONSE_GRAPH_QRY_FB_ERROR', $fb_response, true);
			}
		} else {
			parse_str($result, $result);
		}

		return $result;
	}

	//
	private function graphApiQuery($path, $params = [])
	{
		//
		$defaults = [
			'access_token' => $this->userAccessToken,
			'format' => 'json',
			'method' => 'get'
		];

		return $this->graphQry('/' . $this->graphVersion . $path, array_merge($defaults, $params));
	}

	//
	public function getUser($active)
	{
		$this->userAccessToken = null;


		//
		if ( isset($_SESSION) && isset($_SESSION['user']['fb']['token']) ) {
			// Sessión, validar
			$this->userAccessToken = $_SESSION['user']['fb']['token'];
		} else {
			// Revisar redirección
			$code = (isset($_GET['code'])) ? strval($_GET['code']) : '';
			$state = (isset($_GET['state'])) ? strval($_GET['state']) : '';
			$uniqueCode = md5(session_id());

			//
			if ( !empty($code) && $state === $uniqueCode ) {
				// Validar segundo paso
				$params = [
					'client_id' => $this->appData['id'],
					'client_secret' => $this->appData['secret'],
					'redirect_uri' => $this->siteVars['server']['url'] . $this->fbConnectUrl,
					'code' => $code
				];
				$result = $this->graphQry('/oauth/access_token', $params);

				//
				if ( isset($result['access_token']) ) {
					$this->userAccessToken = $result['access_token'];
					$_SESSION['user']['fb']['token'] = $result['access_token'];
					// header('Location: '.$this->siteVars['server']['url'].$this->fbConnectUrl);
					// exit();
				} else {
					debugVariable($result);
				}
			}
			//
		}

		//
		if ( !$this->userAccessToken && $active ) {
			// No hay conexión, solicitar
			$qry = http_build_query([
				'client_id' => $this->appData['id'],
				'redirect_uri' => $this->siteVars['server']['url'] . $this->fbConnectUrl,
				'response_type' => 'code',
				'state' => md5(session_id())
			]);
			header('Location: https://www.facebook.com/dialog/oauth?' . $qry);
			exit();
		} else {
			$userData = $this->graphApiQuery('/me');
			$this->userId = $userData['id'];
			return $userData;
		}
	}

	//
	public function getUserCoreData()
	{
		return $this->graphApiQuery("/{$this->userId}", ["fields" => "id,first_name,last_name,name,picture,short_name"]);
	}

	//
	// INIT: publishAction
	//	Realizar de forma automática las operaciones de los wallposts
	public function publishAction($id, $action, $objName, $objUrl)
	{
		$this->giveAppAccess();
		$url = "https://graph.facebook.com/{$id}/{$this->appData['namespace']}:{$action}?";
		$postData = [
			$objName => $objUrl,
			"access_token" => $this->appAccessToken
		];

		$result = $this->curlOpPost($url, $postData);
		return $result;
	}
	// END: publishAction
	// 


	//
	// INIT: curlOpPost
	//	Realizar operación por internet
	public function curlOpPost($url, $postData)
	{
		//
		// Conectar a FB
		$fb_response = se_curl(
			$url,
			[
				'curl_params' => [
					CURLOPT_RETURNTRANSFER => true,
					CURLOPT_FOLLOWLOCATION => false,
					CURLOPT_SSL_VERIFYPEER => false,
					CURLOPT_SSL_VERIFYHOST => 2,
				],
				'method' => 'post',
				'postData' => $postData,
				'return' => 'json'
			]
		);

		//
		if ( $fb_response['error']['number'] ) {
			debugVarToSQL('FB_GRAPHQL_RESPONSE_OP_POST', $fb_response, true);
		}

		// Debuging
		if ( isset($fb_response['content']['error']) ) {
			echo("<br /><br /><br /><br /><pre>");
			echo("url: {$url}\n");
			print_r($postData);
			echo("\n");
			print_r($fb_response['content']);
			echo("</pre><br /><br /><br />\n");
		}


		// Regresar resultado
		return $fb_response['content'];
	}
	// END: curlOpPost
	// 

	//
	// INIT: checkOps
	//	Realizar de forma automática las operaciones de los wallposts
	public function singleRequest($userId, $msg, $data, $mode)
	{
		$msg = urlencode($msg);
		if ( !empty($data) ) {
			switch ( $mode ) {
				case 'binary':
					break;
				case 'simple':
					break;
			}
		}
		$apprequest_url = "https://graph.facebook.com/{$userId}/apprequests?";
		$apprequest_url .= "message={$msg}&data={$data}";
		$apprequest_url .= "&access_token={$this->appAccessToken}&method=post";
		$result = file_get_contents($apprequest_url);
		return $result;
	}
	// END: checkOps
	// 

	//
	// INIT: checkCommentOps
	//	REvisar operaciones de los comentarios dentro del wallpost
	public function batchRequest()
	{
	}
	// END: checkCommentOps
	// 


	//
	// INIT: deleteAllRequests
	//	Borrar los request previamente enviados
	public function readAllRequests($userId)
	{
		$apprequest_url = "https://graph.facebook.com/{$userId}/apprequests?{$this->appAccessToken}";
		$result = file_get_contents($apprequest_url);
		return json_decode($result);
	}
	// END: deleteAllRequests
	// 

	//
	// INIT: deleteAllRequests
	//	Borrar los request previamente enviados
	public function deleteAllRequests($userId)
	{
		$result = "";
		$requests = $this->readAllRequests($userId);
		print_r($requests);
		if ( !empty($requests) ) {
			$batch = "[";
			$first = true;
			foreach ( $requests->data as $req ) {
				$id = $req->id;
				$batch .= ($first) ? "" : ",";
				$batch .= '{"method":"delete","relative_url":"' . $id . '"}';
				$first = false;
			}
			$batch .= "]";
			$apprequest_url = "https://graph.facebook.com/?batch={$batch}&{$this->appAccessToken}&method=post";
			$result = file_get_contents($apprequest_url);
		}
		return $result;
	}
	// END: deleteAllRequests
	// 

	// }
	//	funciones públicas
	//

	// ===========================================
	//	Agregar mensajes en la pared
	// ===========================================
	// {}
	// Agregar mensajes en la pared
	//

	public function array_implode($glue, $separator, $array)
	{
		if ( !is_array($array) ) return $array;
		$string = array();
		foreach ( $array as $key => $val ) {
			if ( is_array($val) ) $val = implode(',', $val);
			$string[] = "{$key}{$glue}{$val}";
		}
		return implode($separator, $string);
	}
}
