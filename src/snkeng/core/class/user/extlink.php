<?php

namespace snkeng\core\user;

/*
ExtLink
Creación y validación de vínculos
*/
class extlink
{
	//
	public static function linkCreate($appName, $appAction, $appId, $timeToAdd, $clearOld) {
		
		//
		if ( $clearOld ) {
			$sql_upd = "UPDATE sc_smartlinks SET sl_status=0 WHERE sl_app='{$appName}' AND sl_act_name='{$appAction}' AND sl_act_id='{$appId}' AND sl_status=1;";
			\snkeng\core\engine\mysql::submitQuery($sql_upd);
		}

		// Nuevos datos
		$slId = \snkeng\core\engine\mysql::getNextId('sc_smartlinks');
		$slKey = \se_randString(32);



		//
		$sql_ins = <<<SQL
INSERT sc_smartlinks (
	sl_id, sl_app, sl_act_name, sl_act_id, sl_dt_end, sl_key, sl_status
) VALUES (
   {$slId}, '{$appName}', '{$appAction}', '{$appId}', DATE_ADD(NOW(), INTERVAL {$timeToAdd} DAY), '{$slKey}', 1
);
SQL;
		//
		\snkeng\core\engine\mysql::submitQuery($sql_ins, [
			'errorKey' => '',
			'errorDesc' => ''
		]);

		//
		return [
			'slId' => $slId,
			'slKey' => $slKey,
			'url' => "/se_slink/{$slId}:{$slKey}"
		];
	}

	//
	public static function linkValidate($cString)
	{
		
		// Cleanup
		$cString = preg_replace('/[^A-z0-9\-_:]/', '', $cString);

		//
		list ($lId, $lKey) = explode(':', $cString, 2);
		$lId = intval($lId);

		//
		if ( empty($lId) ) {
			return false;
		}

		//
		$sql_qry = <<<SQL
SELECT
    sl_key AS iKey, sl_status AS status, (NOW() > sl_dt_end) AS expired,
    sl_app AS app, sl_act_name AS actName, sl_act_id AS actId
FROM sc_smartlinks
WHERE sl_id='{$lId}';
SQL;

		$slData = \snkeng\core\engine\mysql::singleRowAssoc($sql_qry,
			[
				'int' => ['actId'],
				'bool' => ['expired']
			],
			[
				'errorKey' => '',
				'errorDesc' => ''
			]
		);

		//
		if ( $slData['iKey'] !== $lKey ) {
			return false;
		}

		//
		return [
			'app' => $slData['app'],
			'act' => $slData['actName'],
			'id' => $slData['actId'],
			'status' => [
				'expired' => ( $slData['expired'] ),
				'valid' => ( $slData['status'] ),
			]
		];
	}

	//
	public static function linkExpire($id) {
		
		$sql_upd = "UPDATE sc_smartlinks SET sl_status=0 WHERE sl_id='{$id}';";
		\snkeng\core\engine\mysql::submitQuery($sql_upd);
	}
}
