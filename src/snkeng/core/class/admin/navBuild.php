<?php
//
namespace snkeng\core\admin;

/**
 * Class admin_build
 */
class navBuild {
	private static $navLevel = [];
	private static $breadCrumb = [];

	//
	public static function navBreadcrumbAdd(string $link, string $title) : void {
		self::$breadCrumb[] = [$link, $title];
	}

	//
	public static function navLevelSet(array $list) : void {
		self::$navLevel = $list;
	}

	//
	public static function navLevelClear() : void {
		self::$navLevel = [];
	}

	//
	public static function navCreate() : string {
		$hasContent = false;
		$nav = '';

		//
		if ( self::$breadCrumb ) {
			$nav.= "\t<div class='breadcrumbs menuContainer'>
\t\t<div class='menuIcon'><svg class='icon inline'><use xlink:href='#fa-navicon' /></svg></div>
\t\t<div class='menu'>\n";
			foreach ( self::$breadCrumb AS $level ) {
				$nav.= "\t\t\t<a se-nav='app_content' href='{$level[0]}/'>{$level[1]}</a>\n";
			}
			$nav.= "\t\t</div>\n\t</div>\n";
		}

		//
		if ( self::$navLevel ) {
			$nav.= "\t<div class='normal menuContainer'>
\t\t<div class='menuIcon'><svg class='icon inline'><use xlink:href='#fa-navicon' /></svg></div>
\t\t<div class='menu'>\n";
			foreach ( self::$navLevel AS $level ) {
				$nav.= "\t\t\t<a se-nav='app_content' href='{$level[0]}/'>{$level[1]}</a>\n";
			}
            $nav.= "\t\t</div>\n\t</div>\n";
		}

		//
		return <<<HTML
<div class="nav4">
{$nav}</div>
HTML;
	}
}
//
