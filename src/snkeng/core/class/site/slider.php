<?php
//
namespace snkeng\core\site;

//
class slider
{
	//
	// INI: printBanner
	//
	public static function printFrontBanner($linkTarget)
	{

		\snkeng\core\engine\nav::pageFileModuleAdd('core', '', '/components-simple/se-simple-slider.mjs');
		\snkeng\core\engine\nav::pageFileModuleAdd('core', '', '/components-simple/se-simple-slider.css');

		$html = '';

		// Obtener banner
		$sql_qry = <<<SQL
SELECT
	ban_id, ban_fname, ban_alt, ban_url
FROM sc_site_banners
WHERE
	ban_size=1 AND ban_active=1 AND ban_type=1
	AND ( ban_curclicks<ban_maxclicks OR ban_maxclicks=0 )
	AND ( ban_curprints<ban_maxprints OR ban_maxprints=0 )
	AND ( NOW()<ban_dtend OR ban_dtend IS NULL )
	AND ( NOW()>ban_dtini OR ban_dtini IS NULL )
ORDER BY ban_dtend ASC
LIMIT 6
SQL;
		if ( \snkeng\core\engine\mysql::execQuery($sql_qry) ) {
			$se_plugin = ( !empty($linkTarget) ) ? " se-nav='{$linkTarget}'" : '';

			//
			while ( $datos = \snkeng\core\engine\mysql::$result->fetch_array() ) {
				$html.= <<<HTML
<a{$se_plugin} href='{$datos['ban_url']}'>
<picture>
	<source type="image/webp" srcset="/res/image/banners/w_1200/{$datos['ban_fname']}.webp" /> 
	<img src='/res/image/banners/w_1200/{$datos['ban_fname']}.jpg' alt='{$datos['ban_alt']}' />
</picture>
</a>
HTML;

			}

			// Crear plugin
			$html = <<<HTML
<se-simple-slider>
<div class="container">
{$html}
</div>
</se-simple-slider>
HTML;
			//

		} else {
			// No hay banners
			$html = "<!--NB-->";
		}

		//
		return $html;
	}
	// END:printBanner
	//

}
//
