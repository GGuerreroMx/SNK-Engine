<?php

# SE - Markdown
# Is a merge of parsedown and extended parsedown with added functions for html
#
# Parsedown [http://parsedown.org] - By Emanuil Rusev [http://erusev.com]

//
namespace snkeng\core\site;

//
use snkeng\core\site\DOMElement;

class markdown
{
	# ~

	const version = '1.8.0-beta-7';
	//
	private $mode = '';
	private $imageSize = 12;

	# ~

	function text($text, $mode = 'html')
	{
		//
		$this->mode = $mode;

		//
		$Elements = $this->textElements($text);

		# convert to markup
		$markup = $this->elements($Elements);

		# trim line breaks
		$markup = trim($markup, "\n");

		# merge consecutive dl elements

		$markup = preg_replace('/<\/dl>\s+<dl>\s+/', '', $markup);

		# add footnotes

		if ( isset($this->DefinitionData['Footnote']) ) {
			$Element = $this->buildFootnoteElement();

			$markup .= "\n" . $this->element($Element);
		}

		return $markup;
	}

	protected function textElements($text)
	{
		# make sure no definitions are set
		$this->DefinitionData = [];

		# standardize line breaks
		$text = str_replace(["\r\n", "\r"], "\n", $text);

		# remove surrounding line breaks
		$text = trim($text, "\n");

		# split text into lines
		$lines = explode("\n", $text);

		# iterate through lines to identify blocks
		return $this->linesElements($lines);
	}

	//<editor-fold desc="Setters">

	function setBreaksEnabled($breaksEnabled)
	{
		$this->breaksEnabled = $breaksEnabled;

		return $this;
	}

	protected $breaksEnabled;

	function setMarkupEscaped($markupEscaped)
	{
		$this->markupEscaped = $markupEscaped;

		return $this;
	}

	protected $markupEscaped;

	function setUrlsLinked($urlsLinked)
	{
		$this->urlsLinked = $urlsLinked;

		return $this;
	}

	protected $urlsLinked = true;

	function setSafeMode($safeMode)
	{
		$this->safeMode = (bool)$safeMode;

		return $this;
	}

	protected $safeMode;

	function setStrictMode($strictMode)
	{
		$this->strictMode = (bool)$strictMode;

		return $this;
	}

	protected $strictMode;

	protected $safeLinksWhitelist = [
		'http://',
		'https://',
		'ftp://',
		'ftps://',
		'mailto:',
		'tel:',
		'data:image/png;base64,',
		'data:image/gif;base64,',
		'data:image/jpeg;base64,',
		'irc:',
		'ircs:',
		'git:',
		'ssh:',
		'news:',
		'steam:',
	];
	//</editor-fold>

	//<editor-fold desc="Lines">

	protected $BlockTypes = [
		'#' => ['Header'],
		'*' => ['Rule', 'List', 'Abbreviation'],
		'+' => ['List'],
		'-' => ['SetextHeader', 'Table', 'Rule', 'List'], // ,
		'0' => ['List'],
		'1' => ['List'],
		'2' => ['List'],
		'3' => ['List'],
		'4' => ['List'],
		'5' => ['List'],
		'6' => ['List'],
		'7' => ['List'],
		'8' => ['List'],
		'9' => ['List'],
		':' => ['Table', 'DefinitionList'],
		'<' => ['Comment', 'Markup'],
		'=' => ['SetextHeader'],
		'>' => ['ElementVideo', 'ElementImage', 'ElementImageDirect', 'ElementTable', 'ElementGallery', 'ElementNotification', 'ElementNote', 'ElementQuote', 'Quote'],
		'[' => ['Footnote', 'Reference'],
		'_' => ['Rule'],
		'`' => ['FencedCode'],
		'|' => ['Table'],
		'~' => ['FencedCode'],
	];

	# ~

	protected $unmarkedBlockTypes = [
		'Code',
	];

	//</editor-fold>

	// ==========

	//<editor-fold desc="Blocks">

	//<editor-fold desc="General">

	protected function lines(array $lines)
	{
		return $this->elements($this->linesElements($lines));
	}

	protected function linesElements(array $lines)
	{
		$Elements = [];
		$CurrentBlock = null;

		// Operation per line
		foreach ( $lines as $line ) {
			// Check last string for continuation
			if ( chop($line) === '' ) {
				// If in current block
				if ( isset($CurrentBlock) ) {
					$CurrentBlock['interrupted'] =
						(isset($CurrentBlock['interrupted'])
							? $CurrentBlock['interrupted'] + 1
							: 1
						);
				}

				continue;
			}

			// Check if there are tabs (ul/ol) maybe?
			while ( ($beforeTab = strstr($line, "\t", true)) !== false ) {
				$shortage = 4 - mb_strlen($beforeTab, 'utf-8') % 4;

				$line = $beforeTab
					. str_repeat(' ', $shortage)
					. substr($line, strlen($beforeTab) + 1);
			}

			// Get current indentation
			$indent = strspn($line, ' ');

			// Remove indent for string
			$text = ($indent > 0) ? substr($line, $indent) : $line;

			// Define current line composition
			$Line = ['body' => $line, 'indent' => $indent, 'text' => $text];

			// Check if current block type is continuable
			if ( isset($CurrentBlock['continuable']) ) {
				$Block = $this->{'block' . $CurrentBlock['type'] . 'Continue'}($Line, $CurrentBlock);

				//
				if ( isset($Block) ) {
					$CurrentBlock = $Block;
					continue;
				} else {
					if ( $this->isBlockCompletable($CurrentBlock['type']) ) {
						$CurrentBlock = $this->{'block' . $CurrentBlock['type'] . 'Complete'}($CurrentBlock);
					}
				}
			}

			// Get first character in row, check if it is a marker
			$marker = $text[0];

			// Add code to all possible definitions
			$blockTypes = $this->unmarkedBlockTypes;

			// Check if marker exist in definitions and add all possible cases
			if ( isset($this->BlockTypes[$marker]) ) {
				foreach ( $this->BlockTypes[$marker] as $blockType ) {
					$blockTypes[] = $blockType;
				}
			}

			# Loop between all current definitions and do operation?
			foreach ( $blockTypes as $blockType ) {
				$Block = $this->{'block' . $blockType}($Line, $CurrentBlock);

				// Check if accepted operation
				if ( isset($Block) ) {
					$Block['type'] = $blockType;

					if ( !isset($Block['identified']) ) {
						if ( isset($CurrentBlock) ) {
							$Elements[] = $this->extractElement($CurrentBlock);
						}

						$Block['identified'] = true;
					}

					if ( $this->isBlockContinuable($blockType) ) {
						$Block['continuable'] = true;
					}

					$CurrentBlock = $Block;

					// Exit loop as accepted type of block
					continue 2;
				}
			}

			# ~

			// Operación break para parrafo
			if ( isset($CurrentBlock) and $CurrentBlock['type'] === 'Paragraph' ) {
				$Block = $this->paragraphContinue($Line, $CurrentBlock);
			}

			// Definir bloque
			if ( isset($Block) ) {
				$CurrentBlock = $Block;
			} else {
				// Extraer elementos
				if ( isset($CurrentBlock) ) {
					$Elements[] = $this->extractElement($CurrentBlock);
				}

				$CurrentBlock = $this->paragraph($Line);

				$CurrentBlock['identified'] = true;
			}
		}

		// Close an ongoing special type
		if ( isset($CurrentBlock['continuable']) and $this->isBlockCompletable($CurrentBlock['type']) ) {
			$methodName = 'block' . $CurrentBlock['type'] . 'Complete';
			$CurrentBlock = $this->$methodName($CurrentBlock);
		}

		//
		if ( isset($CurrentBlock) ) {
			$Elements[] = $this->extractElement($CurrentBlock);
		}

		# ~

		return $Elements;
	}

	protected function extractElement(array $Component)
	{
		if ( !isset($Component['element']) ) {
			if ( isset($Component['markup']) ) {
				$Component['element'] = ['rawHtml' => $Component['markup']];
			} elseif ( isset($Component['hidden']) ) {
				$Component['element'] = [];
			}
		}

		return $Component['element'];
	}

	protected function isBlockContinuable($Type)
	{
		return method_exists($this, 'block' . $Type . 'Continue');
	}

	protected function isBlockCompletable($Type)
	{
		return method_exists($this, 'block' . $Type . 'Complete');
	}

	//</editor-fold>

	//<editor-fold desc="Block Abbreviation">

	//
	protected function blockAbbreviation($Line)
	{
		if ( preg_match('/^\*\[(.+?)\]:[ ]*(.+?)[ ]*$/', $Line['text'], $matches) ) {
			$this->DefinitionData['Abbreviation'][$matches[1]] = $matches[2];

			$Block = [
				'hidden' => true,
			];

			return $Block;
		}
	}

	//</editor-fold>

	//<editor-fold desc="Block Code">

	//
	protected function blockCode($Line, $Block = null)
	{
		if ( isset($Block) and $Block['type'] === 'Paragraph' and !isset($Block['interrupted']) ) {
			return null;
		}

		if ( $Line['indent'] >= 4 ) {
			$text = substr($Line['body'], 4);

			$Block = [
				'element' => [
					'name' => 'pre',
					'element' => [
						'name' => 'code',
						'text' => $text,
					],
				],
			];

			return $Block;
		}
	}

	//
	protected function blockCodeContinue($Line, $Block)
	{
		if ( $Line['indent'] >= 4 ) {
			if ( isset($Block['interrupted']) ) {
				$Block['element']['element']['text'] .= str_repeat("\n", $Block['interrupted']);

				unset($Block['interrupted']);
			}

			$Block['element']['element']['text'] .= "\n";

			$text = substr($Line['body'], 4);

			$Block['element']['element']['text'] .= $text;

			return $Block;
		}
	}

	//
	protected function blockCodeComplete($Block)
	{
		return $Block;
	}

	//</editor-fold>

	//<editor-fold desc="Block Comment">

	//
	protected function blockComment($Line)
	{
		if ( $this->markupEscaped or $this->safeMode ) {
			return null;
		}

		if ( strpos($Line['text'], '<!--') === 0 ) {
			$Block = [
				'element' => [
					'rawHtml' => $Line['body'],
					'autobreak' => true,
				],
			];

			if ( strpos($Line['text'], '-->') !== false ) {
				$Block['closed'] = true;
			}

			return $Block;
		}
	}

	//
	protected function blockCommentContinue($Line, array $Block)
	{
		if ( isset($Block['closed']) ) {
			return null;
		}

		$Block['element']['rawHtml'] .= "\n" . $Line['body'];

		if ( strpos($Line['text'], '-->') !== false ) {
			$Block['closed'] = true;
		}

		return $Block;
	}
	//</editor-fold>

	//<editor-fold desc="Block Definition List">

	//
	protected function blockDefinitionList($Line, $Block)
	{
		if ( !isset($Block) or $Block['type'] !== 'Paragraph' ) {
			return null;
		}

		$Element = [
			'name' => 'dl',
			'elements' => [],
		];

		$terms = explode("\n", $Block['element']['handler']['argument']);

		foreach ( $terms as $term ) {
			$Element['elements'] [] = [
				'name' => 'dt',
				'handler' => [
					'function' => 'lineElements',
					'argument' => $term,
					'destination' => 'elements'
				],
			];
		}

		$Block['element'] = $Element;

		$Block = $this->addDdElement($Line, $Block);

		return $Block;
	}

	//
	protected function blockDefinitionListContinue($Line, array $Block)
	{
		if ( $Line['text'][0] === ':' ) {
			$Block = $this->addDdElement($Line, $Block);

			return $Block;
		} else {
			if ( isset($Block['interrupted']) and $Line['indent'] === 0 ) {
				return null;
			}

			if ( isset($Block['interrupted']) ) {
				$Block['dd']['handler']['function'] = 'textElements';
				$Block['dd']['handler']['argument'] .= "\n\n";

				$Block['dd']['handler']['destination'] = 'elements';

				unset($Block['interrupted']);
			}

			$text = substr($Line['body'], min($Line['indent'], 4));

			$Block['dd']['handler']['argument'] .= "\n" . $text;

			return $Block;
		}
	}

	//</editor-fold>

	//<editor-fold desc="Block FootNote">

	//
	protected function blockFootnote($Line)
	{
		if ( preg_match('/^\[\^(.+?)\]:[ ]?(.*)$/', $Line['text'], $matches) ) {
			$Block = [
				'label' => $matches[1],
				'text' => $matches[2],
				'hidden' => true,
			];

			return $Block;
		}
	}

	protected function blockFootnoteContinue($Line, $Block)
	{
		if ( $Line['text'][0] === '[' and preg_match('/^\[\^(.+?)\]:/', $Line['text']) ) {
			return null;
		}

		if ( isset($Block['interrupted']) ) {
			if ( $Line['indent'] >= 4 ) {
				$Block['text'] .= "\n\n" . $Line['text'];

				return $Block;
			}
		} else {
			$Block['text'] .= "\n" . $Line['text'];

			return $Block;
		}
	}

	protected function blockFootnoteComplete($Block)
	{
		$this->DefinitionData['Footnote'][$Block['label']] = [
			'text' => $Block['text'],
			'count' => null,
			'number' => null,
		];

		return $Block;
	}
	//</editor-fold>

	//<editor-fold desc="Block Fenced Code">

	//
	protected function blockFencedCode($Line)
	{
		$marker = $Line['text'][0];

		$openerLength = strspn($Line['text'], $marker);

		if ( $openerLength < 3 ) {
			return null;
		}

		$infostring = trim(substr($Line['text'], $openerLength), "\t ");

		if ( strpos($infostring, '`') !== false ) {
			return null;
		}

		$Element = [
			'name' => 'code',
			'text' => '',
		];

		if ( $infostring !== '' ) {
			/**
			 * https://www.w3.org/TR/2011/WD-html5-20110525/elements.html#classes
			 * Every HTML element may have a class attribute specified.
			 * The attribute, if specified, must have a value that is a set
			 * of space-separated tokens representing the various classes
			 * that the element belongs to.
			 * [...]
			 * The space characters, for the purposes of this specification,
			 * are U+0020 SPACE, U+0009 CHARACTER TABULATION (tab),
			 * U+000A LINE FEED (LF), U+000C FORM FEED (FF), and
			 * U+000D CARRIAGE RETURN (CR).
			 */
			$language = substr($infostring, 0, strcspn($infostring, " \t\n\f\r"));

			$Element['attributes'] = ['class' => "language-$language"];
		}

		$Block = [
			'char' => $marker,
			'openerLength' => $openerLength,
			'element' => [
				'name' => 'pre',
				'element' => $Element,
			],
		];

		return $Block;
	}

	//
	protected function blockFencedCodeContinue($Line, $Block)
	{
		if ( isset($Block['complete']) ) {
			return null;
		}

		if ( isset($Block['interrupted']) ) {
			$Block['element']['element']['text'] .= str_repeat("\n", $Block['interrupted']);

			unset($Block['interrupted']);
		}

		if ( ($len = strspn($Line['text'], $Block['char'])) >= $Block['openerLength']
			and chop(substr($Line['text'], $len), ' ') === ''
		) {
			$Block['element']['element']['text'] = substr($Block['element']['element']['text'], 1);

			$Block['complete'] = true;

			return $Block;
		}

		$Block['element']['element']['text'] .= "\n" . $Line['body'];

		return $Block;
	}

	//
	protected function blockFencedCodeComplete($Block)
	{
		return $Block;
	}

	//</editor-fold>

	//<editor-fold desc="Block Header ">

	//
	protected function blockHeader($Line)
	{
		$level = strspn($Line['text'], '#');

		if ( $level > 6 ) {
			return null;
		}

		$text = trim($Line['text'], '#');

		if ( $this->strictMode and isset($text[0]) and $text[0] !== ' ' ) {
			return null;
		}

		$text = trim($text, ' ');

		$Block = [
			'element' => [
				'name' => 'h' . $level,
				'handler' => [
					'function' => 'lineElements',
					'argument' => $text,
					'destination' => 'elements',
				]
			],
		];

		# Extended
		if ( $Block !== null && preg_match('/[ #]*{(' . $this->regexAttribute . '+)}[ ]*$/', $Block['element']['handler']['argument'], $matches, PREG_OFFSET_CAPTURE) ) {
			$attributeString = $matches[1][0];

			$Block['element']['attributes'] = $this->parseAttributeData($attributeString);

			$Block['element']['handler']['argument'] = substr($Block['element']['handler']['argument'], 0, $matches[0][1]);
		}


		return $Block;
	}

	//</editor-fold>

	//<editor-fold desc="Block List">

	//
	protected function blockList($Line, array $CurrentBlock = null)
	{
		[$name, $pattern] = $Line['text'][0] <= '-' ? ['ul', '[*+-]'] : ['ol', '[0-9]{1,9}+[.\)]'];

		// Detect type of pattern
		if ( preg_match('/^(' . $pattern . '([ ]++|$))(.*+)/', $Line['text'], $matches) ) {
			$contentIndent = strlen($matches[2]);

			if ( $contentIndent >= 5 ) {
				$contentIndent -= 1;
				$matches[1] = substr($matches[1], 0, -$contentIndent);
				$matches[3] = str_repeat(' ', $contentIndent) . $matches[3];
			} elseif ( $contentIndent === 0 ) {
				$matches[1] .= ' ';
			}

			$markerWithoutWhitespace = strstr($matches[1], ' ', true);

			$Block = [
				'indent' => $Line['indent'],
				'pattern' => $pattern,
				'data' => [
					'type' => $name,
					'marker' => $matches[1],
					'markerType' => ($name === 'ul' ? $markerWithoutWhitespace : substr($markerWithoutWhitespace, -1)),
				],
				'element' => [
					'name' => $name,
					'elements' => [],
				],
			];
			$Block['data']['markerTypeRegex'] = preg_quote($Block['data']['markerType'], '/');

			if ( $name === 'ol' ) {
				$listStart = ltrim(strstr($matches[1], $Block['data']['markerType'], true), '0') ? : '0';

				if ( $listStart !== '1' ) {
					if (
						isset($CurrentBlock)
						and $CurrentBlock['type'] === 'Paragraph'
						and !isset($CurrentBlock['interrupted'])
					) {
						return null;
					}

					$Block['element']['attributes'] = ['start' => $listStart];
				}
			}

			$Block['li'] = [
				'name' => 'li',
				'handler' => [
					'function' => 'li',
					'argument' => !empty($matches[3]) ? [$matches[3]] : [],
					'destination' => 'elements'
				]
			];

			$Block['element']['elements'] [] = &$Block['li'];

			return $Block;
		}
	}

	//
	protected function blockListContinue($Line, array $Block)
	{
		if ( isset($Block['interrupted']) and empty($Block['li']['handler']['argument']) ) {
			return null;
		}

		$requiredIndent = ($Block['indent'] + strlen($Block['data']['marker']));

		// Check if children of element
		if ( $Line['indent'] < $requiredIndent
			and
			(
				(
					$Block['data']['type'] === 'ol'
					and preg_match('/^[0-9]++' . $Block['data']['markerTypeRegex'] . '(?:[ ]++(.*)|$)/', $Line['text'], $matches)
				) or (
					$Block['data']['type'] === 'ul'
					and preg_match('/^' . $Block['data']['markerTypeRegex'] . '(?:[ ]++(.*)|$)/', $Line['text'], $matches)
				)
			)
		) {
			if ( isset($Block['interrupted']) ) {
				$Block['li']['handler']['argument'] [] = '';

				$Block['loose'] = true;

				unset($Block['interrupted']);
			}

			unset($Block['li']);

			$text = isset($matches[1]) ? $matches[1] : '';

			$Block['indent'] = $Line['indent'];

			$Block['li'] = [
				'name' => 'li',
				'handler' => [
					'function' => 'li',
					'argument' => [$text],
					'destination' => 'elements'
				]
			];

			$Block['element']['elements'] [] = &$Block['li'];

			return $Block;
		} elseif ( $Line['indent'] < $requiredIndent and $this->blockList($Line) ) {
			return null;
		}

		if ( $Line['text'][0] === '[' and $this->blockReference($Line) ) {
			return $Block;
		}

		if ( $Line['indent'] >= $requiredIndent ) {
			if ( isset($Block['interrupted']) ) {
				$Block['li']['handler']['argument'] [] = '';

				$Block['loose'] = true;

				unset($Block['interrupted']);
			}

			$text = substr($Line['body'], $requiredIndent);

			$Block['li']['handler']['argument'] [] = $text;

			return $Block;
		}

		if ( !isset($Block['interrupted']) ) {
			$text = preg_replace('/^[ ]{0,' . $requiredIndent . '}+/', '', $Line['body']);

			$Block['li']['handler']['argument'] [] = $text;

			return $Block;
		}
	}

	//
	protected function blockListComplete(array $Block)
	{
		if ( isset($Block['loose']) ) {
			foreach ( $Block['element']['elements'] as &$li ) {
				if ( end($li['handler']['argument']) !== '' ) {
					$li['handler']['argument'] [] = '';
				}
			}
		}

		return $Block;
	}

	//</editor-fold>

	//<editor-fold desc="Block Quote">

	//
	protected function blockQuote($Line)
	{
		if ( preg_match('/^>[ ]?+(.*+)/', $Line['text'], $matches) ) {
			$Block = [
				'element' => [
					'name' => 'blockquote',
					'handler' => [
						'function' => 'linesElements',
						'argument' => (array)$matches[1],
						'destination' => 'elements',
					]
				],
			];

			return $Block;
		}
	}

	//
	protected function blockQuoteContinue($Line, array $Block)
	{
		if ( isset($Block['interrupted']) ) {
			return null;
		}

		if ( $Line['text'][0] === '>' and preg_match('/^>[ ]?+(.*+)/', $Line['text'], $matches) ) {
			$Block['element']['handler']['argument'] [] = $matches[1];

			return $Block;
		}

		if ( !isset($Block['interrupted']) ) {
			$Block['element']['handler']['argument'] [] = $Line['text'];

			return $Block;
		}

		return null;
	}

	//</editor-fold>

	//<editor-fold desc="Block SNK Engine Custom">

	//<editor-fold desc="Element Video">

	//
	protected function blockElementVideo($Line)
	{
		if ( preg_match('/^>\[!VIDEO][ ]?+\(([^)]*)\)/', $Line['text'], $matches) ) {
			// debugVariable((array)$matches);

			//
			switch ( $this->mode ) {
				case 'html':
					// Conceptual block
					$Block = [
						'element' => [
							'name' => 'div',
							'attributes' => [
								'class' => 'videoWrapper',
							],
							'element' => [
								'name' => 'iframe',
								'text' => ' ',
								'attributes' => [
									'src' => null,
									'allow' => 'fullscreen'
								],
							],
						],
					];


					if ( strpos($matches[1], 'youtube.com/watch') !== false ) {
						//
						$vidId = strstr($matches[1], 'v=');
						$vidId = substr($vidId, 2);
						if ( strpos($vidId, '&') !== false ) {
							$vidId = strstr($vidId, '&', true);
						}
						//
						$Block['element']['element']['attributes']['src'] = 'https://www.youtube.com/embed/' . $vidId . '?rel=0';
					} elseif ( strpos($matches[1], 'youtu.be/') !== false ) {
						$vidId = strstr($matches[1], '.be/');
						$vidId = substr($vidId, 4);
						//
						$Block['element']['element']['attributes']['src'] = 'https://www.youtube.com/embed/' . $vidId . '?rel=0';
					} elseif ( strpos($matches[1], 'vimeo.com') !== false ) {
						$vidId = strstr($matches[1], '.com/');
						$vidId = substr($vidId, 4);
						//
						$Block['element']['element']['attributes']['src'] = 'http://player.vimeo.com/video/' . $vidId;
					} else {
						$Block = [
							'element' => [
								'name' => 'div',
								'text' => 'video element not valid',
								'attributes' => [
									'class' => 'error',
								],
							],
						];
					}
					break;

				//
				case 'amp':
					// Conceptual block
					$Block = [
						'element' => [
							'name' => 'amp-youtube',
							'text' => ' ',
							'attributes' => [
								'data-videoid' => '',
								'layout' => 'responsive',
								'width' => 480,
								'height' => 270
							],
						],
					];

					//
					if ( strpos($matches[1], 'youtube.com/watch') !== false ) {
						//
						$vidId = strstr($matches[1], 'v=');
						$vidId = substr($vidId, 2);
						if ( strpos($vidId, '&') !== false ) {
							$vidId = strstr($vidId, '&', true);
						}
						//
						$Block['element']['attributes']['data-videoid'] = $vidId;
					} elseif ( strpos($matches[1], 'youtu.be/') !== false ) {
						$vidId = strstr($matches[1], '.be/');
						$vidId = substr($vidId, 4);
						//
						$Block['element']['attributes']['data-videoid'] = $vidId;
					} elseif ( strpos($matches[1], 'vimeo.com') !== false ) {
						$Block = [
							'element' => [
								'name' => 'div',
								'text' => 'vimeo not supported',
								'attributes' => [
									'class' => 'error',
								],
							],
						];
					} else {
						$Block = [
							'element' => [
								'name' => 'div',
								'text' => 'video element not valid',
								'attributes' => [
									'class' => 'error',
								],
							],
						];
					}
					break;

				//
				default:
					$Block = [
						'element' => [
							'name' => 'div',
							'text' => 'invalid mode',
							'attributes' => [
								'class' => 'error',
							],
						],
					];
					break;
			}
			//
			return $Block;
		}

		// No operation
		return null;
	}

	//</editor-fold>

	//<editor-fold desc="Element Image Original">

	//
	private $figCaptionTxt = '';
	private $figCreditTxt = '';

	//
	protected function blockElementImage($Line)
	{
		// Reset figcaptionTxt
		$this->figCaptionTxt = '';
		$this->figCreditTxt = '';


		// Check
		if ( preg_match('/^>\[!IMAGE][ ]?+\(([^,]+), ([^)|,]+)(, ([^)|,]+))?(, ([^)|,]+))?(, ([^)|,]+))?\)/', $Line['text'], $matches) ) {
			// debugVariable((array)$matches);

			$imageData = explode('.', $matches[2], 2);
			$width = (isset($matches[4])) ? intval($matches[4]) : null;
			$height = (isset($matches[6])) ? intval($matches[6]) : null;
			$altText = (isset($matches[8])) ? $matches[8] : null;

			//
			$sizeList = ['360', '720'];
			$fileSizesAvif = '';
			$fileSizesWebP = '';
			$fileSizesJPG = '';
			$fileSizesDesc = "(max-width:768px) 100vw";

			//
			$maxSize = \ceil(1200 * ($this->imageSize / 12) / 100 ) * 100;

			//
			if ( $maxSize > 768 ) {
				$sizeList[] = $maxSize;
				//
				$value = \ceil($maxSize / 1200 * 100);
				if ( $value == 100 ) {
					$fileSizesDesc = '100vw';
				} else {
					$fileSizesDesc .= ", {$value}vw";
				}
			}

			//
			$first = true;
			foreach ( $sizeList as $cSize ) {
				//
				$fileSizesJPG .= ($first) ? '' : ', ';
				$fileSizesJPG .= "/res/image/{$matches[1]}/w_{$cSize}/$imageData[0].jpg {$cSize}w";
				//
				$fileSizesWebP .= ($first) ? '' : ', ';
				$fileSizesWebP .= "/res/image/{$matches[1]}/w_{$cSize}/$imageData[0].webp {$cSize}w";
				//
				$fileSizesAvif .= ($first) ? '' : ', ';
				$fileSizesAvif .= "/res/image/{$matches[1]}/w_{$cSize}/$imageData[0].avif {$cSize}w";
				//
				$first = false;
			}

			//
			$maxFile = "/res/image/{$matches[1]}/w_{$maxSize}/$imageData[0].jpg";

			//
			if ( $this->mode === 'html' ) {
				// Conceptual block
				$Block = [
					'element' => [
						'name' => 'figure',
						'elements' => [
							[
								'name' => 'picture',
								'elements' => [
									[
										'name' => 'source',
										'attributes' => [
											'type' => "image/avif",
											'srcset' => $fileSizesAvif,
											'sizes' => $fileSizesDesc,
										]
									],
									[
										'name' => 'source',
										'attributes' => [
											'type' => "image/webp",
											'srcset' => $fileSizesWebP,
											'sizes' => $fileSizesDesc,
										]
									],
									[
										'name' => 'img',
										'attributes' => [
											'src' => $maxFile,
											'srcset' => $fileSizesJPG,
											'sizes' => $fileSizesDesc,
											'width' => $width,
											'height' => $height,
											'loading' => 'lazy',
											'alt' => $altText,
										]
									]
								]
							]
						],
					],
				];
			} else {
				// Conceptual block
				$Block = [
					'element' => [
						'name' => 'figure',
						'elements' => [
							[
								'name' => 'amp-img',
								'text' => ' ',
								'element' => [
									'name' => 'amp-img',
									'text' => ' ',
									'attributes' => [
										'src' => "/res/image/{$matches[1]}/w_1200/$imageData[0].jpg",
										'srcset' => $fileSizesJPG,
										'width' => $width,
										'height' => $height,
										'layout' => "responsive",
										'fallback' => "fallback",
										'alt' => $altText,
									]
								],
								'attributes' => [
									'alt' => "",
									'src' => "/res/image/{$matches[1]}/w_1200/$imageData[0].webp",
									'srcset' => $fileSizesWebP,
									'width' => $width,
									'height' => $height,
									'layout' => "responsive",
								]
							]
						],
					],
				];
			}

			return $Block;
		}

		//
		return null;
	}


	//
	protected function blockElementImageContinue($Line, array $Block)
	{
		// debugVariable($Block, '', false);
		// debugVariable($Line, '', true);

		//
		if ( isset($Block['interrupted']) ) {
			return null;
		}

		// Fill comment
		if ( $Line['text'][0] === '>' and preg_match('/^>[ ]?C:+(.*+)/', $Line['text'], $matches) ) {
			// Current fig caption
			$this->figCreditTxt .= $matches[1];

			return $Block;
		} elseif ( $Line['text'][0] === '>' and preg_match('/^>[ ]?+(.*+)/', $Line['text'], $matches) ) {
			$this->figCaptionTxt .= " " . $matches[1];

			return $Block;
		}

		// Interruption?
		if ( !isset($Block['interrupted']) ) {
			return $Block;
		}

		return null;
	}

	//
	protected function blockElementImageComplete($Block)
	{
		$hasFigCaption = false;

		//
		$caption = null;
		if ( !empty($this->figCaptionTxt) ) {
			$hasFigCaption = true;
			$caption = [
				'name' => 'p',
				'text' => $this->figCaptionTxt
			];
		}

		$credit = null;
		if ( !empty($this->figCreditTxt) ) {
			$hasFigCaption = true;
			$credit = [
				'name' => 'p',
				'attributes' => [
					'class' => 'credit'
				],
				'text' => $this->figCreditTxt
			];
		}

		//
		if ( $hasFigCaption ) {
			$Block['element']['elements'][] = [
				'name' => 'figCaption',
				'elements' => [
					$caption,
					$credit
				]
			];
		}

		//
		return $Block;
	}

	//</editor-fold>

	//<editor-fold desc="Element Image Direct">

	//
	protected function blockElementImageDirect($Line)
	{
		// Check
		if ( preg_match('/^>\[!IMAGED][ ]?+\(([^,]+)(, ([^)|,]+))?(, ([^)|,]+))?(, ([^)|,]+))?\)/', $Line['text'], $matches) ) {
			// debugVariable((array)$matches);

			$fileName = $matches[1];
			$width = (isset($matches[3])) ? intval($matches[3]) : null;
			$height = (isset($matches[5])) ? intval($matches[5]) : null;
			$altText = (isset($matches[7])) ? $matches[7] : null;


			//
			if ( $this->mode === 'html' ) {
				// Conceptual block
				$Block = [
					'element' => [
						'name' => 'figure',
						'elements' => [
							[
								'name' => 'picture',
								'elements' => [
									[
										'name' => 'img',
										'attributes' => [
											'src' => $fileName,
											'width' => $width,
											'height' => $height,
											'loading' => 'lazy',
											'alt' => $altText,
										]
									]
								]
							]
						],
					],
				];
			} else {
				// Conceptual block
				$Block = [
					'element' => [
						'name' => 'figure',
						'elements' => [
							[
								'name' => 'amp-img',
								'text' => ' ',
								'attributes' => [
									'src' => $fileName,
									'width' => $width,
									'height' => $height,
									'loading' => 'lazy',
									'alt' => $altText,
								]
							]
						],
					],
				];
			}

			return $Block;
		}

		//
		return null;
	}

	//
	protected function blockElementImageDirectContinue($Line, array $Block)
	{
		// debugVariable($Block, '', false);
		// debugVariable($Line, '', true);

		//
		if ( isset($Block['interrupted']) ) {
			return null;
		}

		// Fill comment
		if ( $Line['text'][0] === '>' and preg_match('/^>[ ]?+(.*+)/', $Line['text'], $matches) ) {
			// Current fig caption
			$cElem = [
				'name' => 'figcaption',
				'text' => $matches[1]
			];

			// If multi line, extend content
			if ( $this->mode === 'html' ) {
				if ( isset($Block['element']['elements'][0]['elements'][1]['attributes']['alt']) ) {
					$Block['element']['elements'][0]['elements'][1]['attributes']['alt'] .= $cElem['text'];
				}
			} else {
				if ( isset($Block['ement']['elements'][0]['attributes']['alt']) ) {
					$Block['element']['elements'][0]['attributes']['alt'] .= $cElem['text'];
				}
			}

			// Add element?
			$Block['element']['elements'][] = $cElem;

			return $Block;
		}

		// Interruption?
		if ( !isset($Block['interrupted']) ) {
			// Current fig caption
			$cElem = [
				'name' => 'figCaption',
				'text' => $matches[1]
			];

			//
			if ( isset($Block['element']['element']['elements']) ) {
				$Block['element']['element']['elements'][] = $cElem;
			} else {
				$Block['element']['element'] = [
					'elements' => [
						$Block['element'], $cElem
					]
				];
			}

			return $Block;
		}

		return null;
	}

	//
	protected function blockElementImageDirectComplete($Block)
	{
		// debugVariable($Block, 'block end', true);
		return $Block;
	}

	//</editor-fold>

	//<editor-fold desc="Element Table">

	//
	private $elementTable = [];

	//
	protected function blockElementTable($Line)
	{
		// Check
		if ( preg_match('/^>\[!TABLE][ ]?+\(([^,]+)(, ([^)|,]+))?(, ([^)|,]+))?\)/', $Line['text'], $matches) ) {
			// debugVariable((array)$matches);

			$tableClass = (isset($matches[1])) ? $matches[1] : null;
			$figureTitle = (isset($matches[3])) ? trim($matches[3]) : null;
			$figureDesc = (isset($matches[5])) ? trim($matches[5]) : null;

			//
			$figCaptionTxt = '';

			if ( $figureTitle ) {
				$figCaptionTxt .= "**{$figureTitle}**";
			}

			if ( $figureDesc ) {
				$figCaptionTxt .= (empty($figCaptionTxt)) ? $figureDesc : " - {$figureDesc}";
			}

			// Conceptual block
			$Block = [
				'element' => [
					'name' => 'figure',
					'elements' => [
						[
							'name' => 'table',
							'attributes' => [
								'class' => $tableClass
							],
							'elements' => []
						]
					],
				],
			];

			//
			if ( $figCaptionTxt ) {
				$Block['element']['elements'][] = [
					'name' => 'figCaption',
					'handler' => [
						'function' => 'lineElements',
						'argument' => $figCaptionTxt,
						'destination' => 'elements',
					]
				];
			}

			//
			$this->elementTable = [
				'cols' => 0,
				'compnum' => 0,
				'comp' => [
					0 => []
				],
			];

			//
			return $Block;
		}

		//
		return null;
	}

	//
	protected function blockElementTableContinue($Line, array $Block)
	{
		// debugVariable($Block, '', false);
		// debugVariable($Line, '', true);

		//
		if ( isset($Block['interrupted']) ) {
			return null;
		}

		// Fill comment
		if ( $Line['text'][0] === '>' and preg_match('/^>[ ]*\|(.*+)/', $Line['text'], $matches) ) {
			//
			$cStr = trim(substr($matches[1], 0, strrpos($matches[1], '|')));

			//
			if ( $cStr === '----' ) {
				$this->elementTable['compnum']++;
				$this->elementTable['comp'][$this->elementTable['compnum']] = [];
			} else {
				$row = explode('|', $cStr);

				//
				if ( count($row) > $this->elementTable['cols'] ) {
					$this->elementTable['cols'] = count($row);
				}

				$this->elementTable['comp'][$this->elementTable['compnum']][] = $row;
			}

			//
			return $Block;
		}

		// Interruption?
		if ( !isset($Block['interrupted']) ) {
			// Current fig caption
			$cElem = [
				'name' => 'figCaption',
				'text' => $matches[1]
			];

			//
			if ( isset($Block['element']['element']['elements']) ) {
				$Block['element']['element']['elements'][] = $cElem;
			} else {
				$Block['element']['element'] = [
					'elements' => [
						$Block['element'], $cElem
					]
				];
			}

			return $Block;
		}

		return null;
	}

	//
	protected function blockElementTableComplete($Block)
	{
		// debugVariable($this->elementTable, '', false);

		//
		for ( $i = 0; $i < count($this->elementTable['comp']); $i++ ) {

			$elemName = ($this->elementTable['compnum'] > 0 && $i === 0) ? 'th' : 'td';
			$elRows = [];


			// Build rows
			foreach ( $this->elementTable['comp'][$i] as $cRows ) {
				$elCols = [];
				foreach ( $cRows as $cEl ) {
					$elCols[] = [
						'name' => $elemName,
						'handler' => [
							'function' => 'lineElements',
							'argument' => $cEl,
							'destination' => 'elements',
						]
					];
				}

				//
				$elRows[] = [
					'name' => 'tr',
					'elements' => $elCols
				];
			}

			$compName = '';
			switch ( $i ) {
				//
				case 0:
					if ( $this->elementTable['compnum'] > 0 ) {
						$compName = 'thead';
					} else {
						$compName = 'tbody';
					}
					break;
				//
				case 1:
					$compName = 'tbody';
					break;
				//
				default:
					$compName = 'tfoot';
					break;
			}

			//
			$Block['element']['elements'][0]['elements'][] = [
				'name' => $compName,
				'elements' => $elRows
			];
		}

		//
		// debugVariable($Block, 'block end', false);

		//
		return $Block;
	}

	//</editor-fold>

	//<editor-fold desc="Element Notification">

	//
	protected function blockElementNotification($Line)
	{
		// Check
		if ( preg_match('/^>\[!NOTIFICATION][ ]?+\(([^,]*), ([^[,|)]*)(, ([^)]*))?\)/', $Line['text'], $matches) ) {
			// debugVariable((array)$matches);
			$extraClass = (isset($matches[4])) ? " " . $matches[4] : '';

			// Conceptual block
			$Block = [
				'element' => [
					'name' => 'div',
					'attributes' => [
						'class' => "notification" . $extraClass
					],
					'elements' => [
						[
							'name' => 'svg',
							'attributes' => [
								'class' => "icon"
							],
							'element' => [
								'name' => 'use',
								'attributes' => [
									'xlink:href' => $matches[1]
								],
							]
						],
						[
							'name' => 'div',
							'attributes' => [
								'class' => "text"
							],
							'elements' => [
								[
									'name' => 'div',
									'text' => $matches[2],
									'attributes' => [
										'class' => "title"
									]
								],
								[
									'name' => 'div',
									'text' => '',
									'attributes' => [
										'class' => "content"
									],
									'handler' => [
										'function' => 'linesElements',
										'argument' => [],
										'destination' => 'elements',
									]
								]
							]
						]
					],
				],
			];

			return $Block;
		}

		//
		return null;
	}

	//
	protected function blockElementNotificationContinue($Line, array $Block)
	{
		// debugVariable($Block, '', false);
		// debugVariable($Line, '', true);

		//
		if ( isset($Block['interrupted']) ) {
			return null;
		}

		// Fill comment
		if ( $Line['text'][0] === '>' and preg_match('/^>[ ]?+(.*+)/', $Line['text'], $matches) ) {
			$Block['element']['elements'][1]['elements'][1]['handler']['argument'][] = $matches[1];

			return $Block;
		}

		// Interruption?
		if ( !isset($Block['interrupted']) ) {
			$Block['element']['elements'][1]['elements'][1]['handler']['argument'][] = $matches[1];

			return $Block;
		}

		return null;
	}

	//
	protected function blockElementNotificationComplete($Block)
	{
		// debugVariable($Block, 'block end', true);
		return $Block;
	}

	//</editor-fold>

	//<editor-fold desc="Element Note">

	//
	protected function blockElementNote($Line)
	{
		// Check
		if ( preg_match('/^>\[!NOTE][ ]?+\(([^[,|)]*)(, ([^)]*))?\)/', $Line['text'], $matches) ) {
			// debugVariable((array)$matches);
			$extraClass = (isset($matches[3])) ? " " . $matches[3] : '';

			// Conceptual block
			$Block = [
				'element' => [
					'name' => 'div',
					'attributes' => [
						'class' => "note" . $extraClass
					],
					'elements' => [
						[
							'name' => 'div',
							'text' => $matches[1],
							'attributes' => [
								'class' => "title"
							]
						],
						[
							'name' => 'div',
							'text' => '',
							'attributes' => [
								'class' => "content"
							],
							'handler' => [
								'function' => 'linesElements',
								'argument' => [],
								'destination' => 'elements',
							]
						]
					]
				],
			];

			return $Block;
		}

		//
		return null;
	}

	//
	protected function blockElementNoteContinue($Line, array $Block)
	{
		// debugVariable($Block, '', false);
		// debugVariable($Line, '', true);

		//
		if ( isset($Block['interrupted']) ) {
			return null;
		}

		// Fill comment
		if ( $Line['text'][0] === '>' and preg_match('/^>[ ]?+(.*+)/', $Line['text'], $matches) ) {
			$Block['element']['elements'][1]['handler']['argument'][] = $matches[1];

			return $Block;
		}

		// Interruption?
		if ( !isset($Block['interrupted']) ) {
			$Block['element']['elements'][1]['handler']['argument'][] = $matches[1];

			return $Block;
		}

		return null;
	}

	//
	protected function blockElementNoteComplete($Block)
	{
		// debugVariable($Block, 'block end', true);
		return $Block;
	}

	//</editor-fold>

	//<editor-fold desc="Element Quote">

	//
	protected function blockElementQuote($Line)
	{
		// Check
		if ( preg_match('/^>\[!QUOTE][ ]?+\(([^[,|)]*)(, ([^)|,]+))?(, ([^)|,]+))?\)/', $Line['text'], $matches) ) {
			// debugVariable((array)$matches);
			$source = $matches[1];
			$author = (isset($matches[3])) ? $matches[3] : '';
			$url = (isset($matches[5])) ? $matches[5] : '';

			// Conceptual block
			$Block = [
				'element' => [
					'name' => 'figure',
					'attributes' => [
						'class' => "quote"
					],
					'elements' => [
						[
							'name' => 'blockquote',
							'text' => '',
							'attributes' => [
								'class' => "content"
							],
							'handler' => [
								'function' => 'linesElements',
								'argument' => [],
								'destination' => 'elements',
							]
						],
						[
							'name' => 'figcaption',
							'elements' => [
								[
									'name' => 'span',
									'text' => $author,
									'attributes' => [
										'class' => "author"
									],
								],
								[
									'name' => 'cite',
									'text' => $source,
								],
							]
						],
					]
				],
			];

			return $Block;
		}

		//
		return null;
	}

	//
	protected function blockElementQuoteContinue($Line, array $Block)
	{
		// debugVariable($Block, '', false);
		// debugVariable($Line, '', true);

		//
		if ( isset($Block['interrupted']) ) {
			return null;
		}

		// Fill comment
		if ( $Line['text'][0] === '>' and preg_match('/^>[ ]?+(.*+)/', $Line['text'], $matches) ) {
			$Block['element']['elements'][0]['handler']['argument'][] = $matches[1];

			return $Block;
		}

		// Interruption?
		if ( !isset($Block['interrupted']) ) {
			$Block['element']['elements'][0]['handler']['argument'][] = $matches[1];

			return $Block;
		}

		return null;
	}

	//
	protected function blockElementQuoteComplete($Block)
	{
		// debugVariable($Block, 'block end', true);
		return $Block;
	}

	//</editor-fold>


	//<editor-fold desc="Element Gallery">

	//
	protected function blockElementGallery($Line)
	{
		if ( preg_match('/^>\[!GALLERY][ ]?+\(([^)]*)\)/', $Line['text'], $matches) ) {
			// debugVariable((array)$matches);


			// Conceptual block
			$Block = [
				'element' => [
					'name' => 'div',
					'attributes' => [
						'class' => 'videoWrapper',
					],
					'element' => [
						'name' => 'iframe',
						'attributes' => [
							'src' => null,
							'allow' => 'fullscreen'
						],
					],
				],
			];


			if ( strpos($matches[1], 'youtube.com/watch') !== false ) {
				//
				$vidId = strstr($matches[1], 'v=');
				$vidId = substr($vidId, 2);
				if ( strpos($vidId, '&') !== false ) {
					$vidId = strstr($vidId, '&', true);
				}
				//
				$Block['element']['element']['attributes']['src'] = 'https://www.youtube.com/embed/' . $vidId . '?rel=0';
			} elseif ( strpos($matches[1], 'youtu.be/') !== false ) {
				$vidId = strstr($matches[1], '.be/');
				$vidId = substr($vidId, 4);
				//
				$Block['element']['element']['attributes']['src'] = 'https://www.youtube.com/embed/' . $vidId . '?rel=0';
			} elseif ( strpos($matches[1], 'vimeo.com') !== false ) {
				$vidId = strstr($matches[1], '.com/');
				$vidId = substr($vidId, 4);
				//
				$Block['element']['element']['attributes']['src'] = 'http://player.vimeo.com/video/' . $vidId;
			} else {
				$Block = [
					'element' => [
						'name' => 'div',
						'text' => 'video element not valid',
						'attributes' => [
							'class' => 'error',
						],
					],
				];
			}

			return $Block;
		}

		return null;
	}

	//
	protected function blockElementGalleryContinue($Line, array $Block)
	{
		// debugVariable($Block, '', false);
		// debugVariable($Line, '', true);

		//
		if ( isset($Block['interrupted']) ) {
			return null;
		}

		// Fill comment
		if ( $Line['text'][0] === '>' and preg_match('/^>[ ]?+(.*+)/', $Line['text'], $matches) ) {
			// Current fig caption
			$cElem = [
				'name' => 'figCaption',
				'text' => $matches[1]
			];

			//
			if ( isset($Block['element']['element']['elements']) ) {
				$Block['element']['element']['elements'][] = $cElem;
			} else {
				$Block['element']['element'] = [
					'elements' => [
						$Block['element'], $cElem
					]
				];
			}

			//
			// debugVariable($Block, 'block 01', false);

			return $Block;
		}

		// Interruption?
		if ( !isset($Block['interrupted']) ) {
			// Current fig caption
			$cElem = [
				'name' => 'figCaption',
				'text' => $matches[1]
			];

			//
			if ( isset($Block['element']['element']['elements']) ) {
				$Block['element']['element']['elements'][] = $cElem;
			} else {
				$Block['element']['element'] = [
					'elements' => [
						$Block['element'], $cElem
					]
				];
			}

			return $Block;
		}

		return null;
	}

	//
	protected function blockElementGalleryComplete($Block)
	{
		// debugVariable($Block, 'block end', true);
		return $Block;
	}

	//</editor-fold>

	//</editor-fold>

	//<editor-fold desc="Block Rule">

	//
	protected function blockRule($Line)
	{
		$marker = $Line['text'][0];

		if ( substr_count($Line['text'], $marker) >= 3 and chop($Line['text'], " $marker") === '' ) {
			$Block = [
				'element' => [
					'name' => 'hr',
				],
			];

			return $Block;
		}

		return null;
	}

	//</editor-fold>

	//<editor-fold desc="Block SetextHeader">

	//
	protected function blockSetextHeader($Line, array $Block = null)
	{
		// Classic skip plus indent detection (should have no indent)
		if ( !isset($Block) or $Block['type'] !== 'Paragraph' or isset($Block['interrupted']) or $Line['indent'] > 0 ) {
			return null;
		}

		// Skip if any other character
		if ( $Line['indent'] < 4 and chop(chop($Line['text'], ' '), $Line['text'][0]) === '' ) {
			$Block['element']['name'] = $Line['text'][0] === '=' ? 'h1' : 'h2';
		}

		// Extended
		if ( $Block !== null && preg_match('/[ ]*{(' . $this->regexAttribute . '+)}[ ]*$/', $Block['element']['handler']['argument'], $matches, PREG_OFFSET_CAPTURE) ) {
			$attributeString = $matches[1][0];

			$Block['element']['attributes'] = $this->parseAttributeData($attributeString);

			$Block['element']['handler']['argument'] = substr($Block['element']['handler']['argument'], 0, $matches[0][1]);
		}

		return $Block;
	}

	//</editor-fold>

	//<editor-fold desc="Block Markup">

	// Extended version
	protected function blockMarkup($Line)
	{
		if ( $this->markupEscaped or $this->safeMode ) {
			return null;
		}

		if ( preg_match('/^<(\w[\w-]*)(?:[ ]*' . $this->regexHtmlAttribute . ')*[ ]*(\/)?>/', $Line['text'], $matches) ) {
			$element = strtolower($matches[1]);

			if ( in_array($element, $this->textLevelElements) ) {
				return null;
			}

			$Block = [
				'name' => $matches[1],
				'depth' => 0,
				'element' => [
					'rawHtml' => $Line['text'],
					'autobreak' => true,
				],
			];

			$length = strlen($matches[0]);
			$remainder = substr($Line['text'], $length);

			if ( trim($remainder) === '' ) {
				if ( isset($matches[2]) or in_array($matches[1], $this->voidElements) ) {
					$Block['closed'] = true;
					$Block['void'] = true;
				}
			} else {
				if ( isset($matches[2]) or in_array($matches[1], $this->voidElements) ) {
					return null;
				}
				if ( preg_match('/<\/' . $matches[1] . '>[ ]*$/i', $remainder) ) {
					$Block['closed'] = true;
				}
			}

			return $Block;
		}

		return null;
	}

	//
	protected function blockMarkupContinue($Line, array $Block)
	{
		if ( isset($Block['closed']) ) {
			return null;
		}

		if ( preg_match('/^<' . $Block['name'] . '(?:[ ]*' . $this->regexHtmlAttribute . ')*[ ]*>/i', $Line['text']) ) # open
		{
			$Block['depth']++;
		}

		if ( preg_match('/(.*?)<\/' . $Block['name'] . '>[ ]*$/i', $Line['text'], $matches) ) # close
		{
			if ( $Block['depth'] > 0 ) {
				$Block['depth']--;
			} else {
				$Block['closed'] = true;
			}
		}

		if ( isset($Block['interrupted']) ) {
			$Block['element']['rawHtml'] .= "\n";
			unset($Block['interrupted']);
		}

		$Block['element']['rawHtml'] .= "\n" . $Line['body'];

		return $Block;
	}

	//
	protected function blockMarkupComplete($Block)
	{
		if ( !isset($Block['void']) ) {
			$Block['element']['rawHtml'] = $this->processTag($Block['element']['rawHtml']);
		}

		return $Block;
	}

	//</editor-fold>

	//<editor-fold desc="Block Reference">

	//
	protected function blockReference($Line)
	{
		if ( strpos($Line['text'], ']') !== false
			and preg_match('/^\[(.+?)\]:[ ]*+<?(\S+?)>?(?:[ ]+["\'(](.+)["\')])?[ ]*+$/', $Line['text'], $matches)
		) {
			$id = strtolower($matches[1]);

			$Data = [
				'url' => $matches[2],
				'title' => isset($matches[3]) ? $matches[3] : null,
			];

			$this->DefinitionData['Reference'][$id] = $Data;

			$Block = [
				'element' => [],
			];

			return $Block;
		}
	}

	//</editor-fold>

	//<editor-fold desc="Block Table">

	//
	protected function blockTable($Line, array $Block = null)
	{
		if ( !isset($Block) or $Block['type'] !== 'Paragraph' or isset($Block['interrupted']) ) {
			return null;
		}

		if (
			strpos($Block['element']['handler']['argument'], '|') === false
			and strpos($Line['text'], '|') === false
			and strpos($Line['text'], ':') === false
			or strpos($Block['element']['handler']['argument'], "\n") !== false
		) {
			return null;
		}

		if ( chop($Line['text'], ' -:|') !== '' ) {
			return null;
		}

		$alignments = [];

		$divider = $Line['text'];

		$divider = trim($divider);
		$divider = trim($divider, '|');

		$dividerCells = explode('|', $divider);

		foreach ( $dividerCells as $dividerCell ) {
			$dividerCell = trim($dividerCell);

			if ( $dividerCell === '' ) {
				return null;
			}

			$alignment = null;

			if ( $dividerCell[0] === ':' ) {
				$alignment = 'left';
			}

			if ( substr($dividerCell, -1) === ':' ) {
				$alignment = $alignment === 'left' ? 'center' : 'right';
			}

			$alignments [] = $alignment;
		}

		# ~

		$HeaderElements = [];

		$header = $Block['element']['handler']['argument'];

		$header = trim($header);
		$header = trim($header, '|');

		$headerCells = explode('|', $header);

		if ( count($headerCells) !== count($alignments) ) {
			return null;
		}

		foreach ( $headerCells as $index => $headerCell ) {
			$headerCell = trim($headerCell);

			$HeaderElement = [
				'name' => 'th',
				'handler' => [
					'function' => 'lineElements',
					'argument' => $headerCell,
					'destination' => 'elements',
				]
			];

			if ( isset($alignments[$index]) ) {
				$alignment = $alignments[$index];

				$HeaderElement['attributes'] = [
					'style' => "text-align: $alignment;",
				];
			}

			$HeaderElements [] = $HeaderElement;
		}

		# ~

		$Block = [
			'alignments' => $alignments,
			'identified' => true,
			'element' => [
				'name' => 'table',
				'elements' => [],
			],
		];

		$Block['element']['elements'] [] = [
			'name' => 'thead',
		];

		$Block['element']['elements'] [] = [
			'name' => 'tbody',
			'elements' => [],
		];

		$Block['element']['elements'][0]['elements'] [] = [
			'name' => 'tr',
			'elements' => $HeaderElements,
		];

		return $Block;
	}

	//
	protected function blockTableContinue($Line, array $Block)
	{
		if ( isset($Block['interrupted']) ) {
			return null;
		}

		if ( count($Block['alignments']) === 1 or $Line['text'][0] === '|' or strpos($Line['text'], '|') ) {
			$Elements = [];

			$row = $Line['text'];

			$row = trim($row);
			$row = trim($row, '|');

			preg_match_all('/(?:(\\\\[|])|[^|`]|`[^`]++`|`)++/', $row, $matches);

			$cells = array_slice($matches[0], 0, count($Block['alignments']));

			foreach ( $cells as $index => $cell ) {
				$cell = trim($cell);

				$Element = [
					'name' => 'td',
					'handler' => [
						'function' => 'lineElements',
						'argument' => $cell,
						'destination' => 'elements',
					]
				];

				if ( isset($Block['alignments'][$index]) ) {
					$Element['attributes'] = [
						'style' => 'text-align: ' . $Block['alignments'][$index] . ';',
					];
				}

				$Elements [] = $Element;
			}

			$Element = [
				'name' => 'tr',
				'elements' => $Elements,
			];

			$Block['element']['elements'][1]['elements'] [] = $Element;

			return $Block;
		}
	}

	//</editor-fold>

	//<editor-fold desc="Block Paragraph">

	//
	protected function paragraph($Line)
	{
		return [
			'type' => 'Paragraph',
			'element' => [
				'name' => 'p',
				'handler' => [
					'function' => 'lineElements',
					'argument' => $Line['text'],
					'destination' => 'elements',
				],
			],
		];
	}

	//
	protected function paragraphContinue($Line, array $Block)
	{
		if ( isset($Block['interrupted']) ) {
			return null;
		}

		$Block['element']['handler']['argument'] .= "\n" . $Line['text'];

		return $Block;
	}

	//</editor-fold>

	//</editor-fold>

	// ==========

	//<editor-fold desc="Inline Elements">

	protected $InlineTypes = [
		'!' => ['Image'],
		'&' => ['SpecialCharacter'],
		'*' => ['Emphasis'],
		':' => ['Url'],
		'<' => ['UrlTag', 'EmailTag', 'Markup'],
		'[' => ['FootnoteMarker', 'Link'],
		'_' => ['Emphasis'],
		'`' => ['Code'],
		'~' => ['Strikethrough'],
		'\\' => ['EscapeSequence'],
	];

	# ~

	protected $inlineMarkerList = '!*_&[:<`~\\';

	#
	# ~
	#

	//<editor-fold desc="Core">

	//
	public function line($text, $nonNestables = [])
	{
		return $this->elements($this->lineElements($text, $nonNestables));
	}

	//
	protected function lineElements($text, $nonNestables = [])
	{
		# standardize line breaks
		$text = str_replace(["\r\n", "\r"], "\n", $text);

		$Elements = [];

		$nonNestables = (empty($nonNestables)
			? []
			: array_combine($nonNestables, $nonNestables)
		);

		# $excerpt is based on the first occurrence of a marker

		while ( $excerpt = strpbrk($text, $this->inlineMarkerList) ) {
			$marker = $excerpt[0];

			$markerPosition = strlen($text) - strlen($excerpt);

			$Excerpt = ['text' => $excerpt, 'context' => $text];

			foreach ( $this->InlineTypes[$marker] as $inlineType ) {
				# check to see if the current inline type is nestable in the current context

				if ( isset($nonNestables[$inlineType]) ) {
					continue;
				}

				$Inline = $this->{"inline$inlineType"}($Excerpt);

				if ( !isset($Inline) ) {
					continue;
				}

				# makes sure that the inline belongs to "our" marker

				if ( isset($Inline['position']) and $Inline['position'] > $markerPosition ) {
					continue;
				}

				# sets a default inline position

				if ( !isset($Inline['position']) ) {
					$Inline['position'] = $markerPosition;
				}

				# cause the new element to 'inherit' our non nestables


				$Inline['element']['nonNestables'] = isset($Inline['element']['nonNestables'])
					? array_merge($Inline['element']['nonNestables'], $nonNestables)
					: $nonNestables;

				# the text that comes before the inline
				$unmarkedText = substr($text, 0, $Inline['position']);

				# compile the unmarked text
				$InlineText = $this->inlineText($unmarkedText);
				$Elements[] = $InlineText['element'];

				# compile the inline
				$Elements[] = $this->extractElement($Inline);

				# remove the examined text
				$text = substr($text, $Inline['position'] + $Inline['extent']);

				continue 2;
			}

			# the marker does not belong to an inline

			$unmarkedText = substr($text, 0, $markerPosition + 1);

			$InlineText = $this->inlineText($unmarkedText);
			$Elements[] = $InlineText['element'];

			$text = substr($text, $markerPosition + 1);
		}

		$InlineText = $this->inlineText($text);
		$Elements[] = $InlineText['element'];

		foreach ( $Elements as &$Element ) {
			if ( !isset($Element['autobreak']) ) {
				$Element['autobreak'] = false;
			}
		}

		return $Elements;
	}

	//</editor-fold>

	#
	# ~
	#

	protected function inlineText($text)
	{
		$Inline = [
			'extent' => strlen($text),
			'element' => [],
		];

		$Inline['element']['elements'] = self::pregReplaceElements(
			$this->breaksEnabled ? '/[ ]*+\n/' : '/(?:[ ]*+\\\\|[ ]{2,}+)\n/',
			[
				['name' => 'br'],
				['text' => "\n"],
			],
			$text
		);

		if ( isset($this->DefinitionData['Abbreviation']) ) {
			foreach ( $this->DefinitionData['Abbreviation'] as $abbreviation => $meaning ) {
				$this->currentAbreviation = $abbreviation;
				$this->currentMeaning = $meaning;

				$Inline['element'] = $this->elementApplyRecursiveDepthFirst(
					[$this, 'insertAbreviation'],
					$Inline['element']
				);
			}
		}

		return $Inline;
	}

	protected function inlineCode($Excerpt)
	{
		$marker = $Excerpt['text'][0];

		if ( preg_match('/^([' . $marker . ']++)[ ]*+(.+?)[ ]*+(?<![' . $marker . '])\1(?!' . $marker . ')/s', $Excerpt['text'], $matches) ) {
			$text = $matches[2];
			$text = preg_replace('/[ ]*+\n/', ' ', $text);

			return [
				'extent' => strlen($matches[0]),
				'element' => [
					'name' => 'code',
					'text' => $text,
				],
			];
		}

		return null;
	}

	protected function inlineEmailTag($Excerpt)
	{
		$hostnameLabel = '[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?';

		$commonMarkEmail = '[a-zA-Z0-9.!#$%&\'*+\/=?^_`{|}~-]++@'
			. $hostnameLabel . '(?:\.' . $hostnameLabel . ')*';

		if (
			strpos($Excerpt['text'], '>') !== false
			&& preg_match("/^<((mailto:)?$commonMarkEmail)>/i", $Excerpt['text'], $matches)
		) {
			$url = $matches[1];

			if ( !isset($matches[2]) ) {
				$url = "mailto:$url";
			}

			return [
				'extent' => strlen($matches[0]),
				'element' => [
					'name' => 'a',
					'text' => $matches[1],
					'attributes' => [
						'href' => $url,
					],
				],
			];
		}

		return null;
	}

	protected function inlineEmphasis($Excerpt)
	{
		if ( !isset($Excerpt['text'][1]) ) {
			return null;
		}

		$marker = $Excerpt['text'][0];

		if ( $Excerpt['text'][1] === $marker and preg_match($this->StrongRegex[$marker], $Excerpt['text'], $matches) ) {
			$emphasis = 'strong';
		} elseif ( preg_match($this->EmRegex[$marker], $Excerpt['text'], $matches) ) {
			$emphasis = 'em';
		} else {
			return null;
		}

		return [
			'extent' => strlen($matches[0]),
			'element' => [
				'name' => $emphasis,
				'handler' => [
					'function' => 'lineElements',
					'argument' => $matches[1],
					'destination' => 'elements',
				]
			],
		];
	}

	//
	protected function inlineEscapeSequence($Excerpt)
	{
		if ( isset($Excerpt['text'][1]) and in_array($Excerpt['text'][1], $this->specialCharacters) ) {
			return [
				'element' => ['rawHtml' => $Excerpt['text'][1]],
				'extent' => 2,
			];
		}

		return null;
	}

	//
	protected function inlineImage($Excerpt)
	{
		if ( !isset($Excerpt['text'][1]) or $Excerpt['text'][1] !== '[' ) {
			return null;
		}

		$Excerpt['text'] = substr($Excerpt['text'], 1);

		$Link = $this->inlineLink($Excerpt);

		if ( $Link === null ) {
			return null;
		}

		$Inline = [
			'extent' => $Link['extent'] + 1,
			'element' => [
				'name' => 'img',
				'attributes' => [
					'src' => $Link['element']['attributes']['href'],
					'alt' => $Link['element']['handler']['argument'],
				],
				'autobreak' => true,
			],
		];

		$Inline['element']['attributes'] += $Link['element']['attributes'];

		unset($Inline['element']['attributes']['href']);

		return $Inline;
	}

	protected function inlineLink($Excerpt)
	{
		$Element = [
			'name' => 'a',
			'handler' => [
				'function' => 'lineElements',
				'argument' => null,
				'destination' => 'elements',
			],
			'nonNestables' => ['Url', 'Link'],
			'attributes' => [
				'href' => null,
				'title' => null,
			],
		];

		$extent = 0;

		$remainder = $Excerpt['text'];

		//
		if ( preg_match('/\[((?:[^][]++|(?R))*+)\]/', $remainder, $matches) ) {
			$Element['handler']['argument'] = $matches[1];

			$extent += strlen($matches[0]);

			$remainder = substr($remainder, $extent);
		} else {
			return null;
		}

		//
		if ( preg_match('/^[(]\s*+((?:[^ ()]++|[(][^ )]+[)])++)(?:[ ]+("[^"]*+"|\'[^\']*+\'))?\s*+[)]/', $remainder, $matches) ) {
			$Element['attributes']['href'] = $matches[1];

			// If internal links, add navigation element
			if ( $matches[1][0] === '/' || $matches[1][0] === '.' ) {
				$Element['attributes']['se-nav'] = 'se_middle';
			}

			if ( isset($matches[2]) ) {
				$Element['attributes']['title'] = substr($matches[2], 1, -1);
			}

			$extent += strlen($matches[0]);
		} else {
			if ( preg_match('/^\s*\[(.*?)\]/', $remainder, $matches) ) {
				se_killWithError("nadie nació para mi", $remainder);

				$definition = strlen($matches[1]) ? $matches[1] : $Element['handler']['argument'];
				$definition = strtolower($definition);

				$extent += strlen($matches[0]);
			} else {
				$definition = strtolower($Element['handler']['argument']);
			}

			if ( !isset($this->DefinitionData['Reference'][$definition]) ) {
				return null;
			}

			$Definition = $this->DefinitionData['Reference'][$definition];

			$Element['attributes']['href'] = $Definition['url'];
			$Element['attributes']['title'] = $Definition['title'];
		}

		//
		$Link = [
			'extent' => $extent,
			'element' => $Element,
		];

		// Extension
		$remainder = substr($Excerpt['text'], $Link['extent']);
		if ( preg_match('/^[ ]*{(' . $this->regexAttribute . '+)}/', $remainder, $matches) ) {
			$Link['element']['attributes'] += $this->parseAttributeData($matches[1]);
			$Link['extent'] += strlen($matches[0]);
		}

		//
		return $Link;
	}

	//<editor-fold desc="Abreviation">

	private $currentAbreviation;
	private $currentMeaning;

	protected function insertAbreviation(array $Element)
	{
		if ( isset($Element['text']) ) {
			$Element['elements'] = self::pregReplaceElements(
				'/\b' . preg_quote($this->currentAbreviation, '/') . '\b/',
				[
					[
						'name' => 'abbr',
						'attributes' => [
							'title' => $this->currentMeaning,
						],
						'text' => $this->currentAbreviation,
					]
				],
				$Element['text']
			);

			unset($Element['text']);
		}

		return $Element;
	}

	//</editor-fold>

	protected function inlineMarkup($Excerpt)
	{
		if ( $this->markupEscaped or $this->safeMode or strpos($Excerpt['text'], '>') === false ) {
			return null;
		}

		if ( $Excerpt['text'][1] === '/' and preg_match('/^<\/\w[\w-]*+[ ]*+>/s', $Excerpt['text'], $matches) ) {
			return [
				'element' => ['rawHtml' => $matches[0]],
				'extent' => strlen($matches[0]),
			];
		}

		if ( $Excerpt['text'][1] === '!' and preg_match('/^<!---?[^>-](?:-?+[^-])*-->/s', $Excerpt['text'], $matches) ) {
			return [
				'element' => ['rawHtml' => $matches[0]],
				'extent' => strlen($matches[0]),
			];
		}

		if ( $Excerpt['text'][1] !== ' ' and preg_match('/^<\w[\w-]*+(?:[ ]*+' . $this->regexHtmlAttribute . ')*+[ ]*+\/?>/s', $Excerpt['text'], $matches) ) {
			return [
				'element' => ['rawHtml' => $matches[0]],
				'extent' => strlen($matches[0]),
			];
		}

		return null;
	}

	protected function inlineSpecialCharacter($Excerpt)
	{
		if ( substr($Excerpt['text'], 1, 1) !== ' ' and strpos($Excerpt['text'], ';') !== false
			and preg_match('/^&(#?+[0-9a-zA-Z]++);/', $Excerpt['text'], $matches)
		) {
			return [
				'element' => ['rawHtml' => '&' . $matches[1] . ';'],
				'extent' => strlen($matches[0]),
			];
		}

		return null;
	}

	//<editor-fold desc="inline FootNoteMarker">

	//
	protected function inlineFootnoteMarker($Excerpt)
	{
		if ( preg_match('/^\[\^(.+?)\]/', $Excerpt['text'], $matches) ) {
			$name = $matches[1];

			if ( !isset($this->DefinitionData['Footnote'][$name]) ) {
				return null;
			}

			$this->DefinitionData['Footnote'][$name]['count']++;

			if ( !isset($this->DefinitionData['Footnote'][$name]['number']) ) {
				$this->DefinitionData['Footnote'][$name]['number'] = ++$this->footnoteCount; # » &
			}

			$Element = [
				'name' => 'sup',
				'attributes' => ['id' => 'fnref' . $this->DefinitionData['Footnote'][$name]['count'] . ':' . $name],
				'element' => [
					'name' => 'a',
					'attributes' => ['href' => '#fn:' . $name, 'class' => 'footnote-ref'],
					'text' => $this->DefinitionData['Footnote'][$name]['number'],
				],
			];

			return [
				'extent' => strlen($matches[0]),
				'element' => $Element,
			];
		}

		return null;
	}

	private $footnoteCount = 0;

	//</editor-fold>

	protected function inlineStrikethrough($Excerpt)
	{
		if ( !isset($Excerpt['text'][1]) ) {
			return null;
		}

		if ( $Excerpt['text'][1] === '~' and preg_match('/^~~(?=\S)(.+?)(?<=\S)~~/', $Excerpt['text'], $matches) ) {
			return [
				'extent' => strlen($matches[0]),
				'element' => [
					'name' => 'del',
					'handler' => [
						'function' => 'lineElements',
						'argument' => $matches[1],
						'destination' => 'elements',
					]
				],
			];
		}

		return null;
	}

	protected function inlineUrl($Excerpt)
	{
		if ( $this->urlsLinked !== true or !isset($Excerpt['text'][2]) or $Excerpt['text'][2] !== '/' ) {
			return null;
		}

		if ( strpos($Excerpt['context'], 'http') !== false
			and preg_match('/\bhttps?+:[\/]{2}[^\s<]+\b\/*+/ui', $Excerpt['context'], $matches, PREG_OFFSET_CAPTURE)
		) {
			$url = $matches[0][0];

			$Inline = [
				'extent' => strlen($matches[0][0]),
				'position' => $matches[0][1],
				'element' => [
					'name' => 'a',
					'text' => $url,
					'attributes' => [
						'href' => $url,
					],
				],
			];

			return $Inline;
		}

		return null;
	}

	protected function inlineUrlTag($Excerpt)
	{
		if ( strpos($Excerpt['text'], '>') !== false and preg_match('/^<(\w++:\/{2}[^ >]++)>/i', $Excerpt['text'], $matches) ) {
			$url = $matches[1];

			return [
				'extent' => strlen($matches[0]),
				'element' => [
					'name' => 'a',
					'text' => $url,
					'attributes' => [
						'href' => $url,
					],
				],
			];
		}

		return null;
	}

	# ~

	protected function unmarkedText($text)
	{
		$Inline = $this->inlineText($text);
		return $this->element($Inline['element']);
	}
	//</editor-fold>

	// ==========

	//<editor-fold desc="Handlers">

	//
	protected function handle(array $Element)
	{
		if ( isset($Element['handler']) ) {
			if ( !isset($Element['nonNestables']) ) {
				$Element['nonNestables'] = [];
			}

			if ( is_string($Element['handler']) ) {
				$function = $Element['handler'];
				$argument = $Element['text'];
				unset($Element['text']);
				$destination = 'rawHtml';
			} else {
				$function = $Element['handler']['function'];
				$argument = $Element['handler']['argument'];
				$destination = $Element['handler']['destination'];
			}

			$Element[$destination] = $this->{$function}($argument, $Element['nonNestables']);

			if ( $destination === 'handler' ) {
				$Element = $this->handle($Element);
			}

			unset($Element['handler']);
		}

		return $Element;
	}

	protected function handleElementRecursive(array $Element)
	{
		return $this->elementApplyRecursive([$this, 'handle'], $Element);
	}

	protected function handleElementsRecursive(array $Elements)
	{
		return $this->elementsApplyRecursive([$this, 'handle'], $Elements);
	}

	protected function elementApplyRecursive($closure, array $Element)
	{
		$Element = call_user_func($closure, $Element);

		if ( isset($Element['elements']) ) {
			$Element['elements'] = $this->elementsApplyRecursive($closure, $Element['elements']);
		} elseif ( isset($Element['element']) ) {
			$Element['element'] = $this->elementApplyRecursive($closure, $Element['element']);
		}

		return $Element;
	}

	protected function elementApplyRecursiveDepthFirst($closure, array $Element)
	{
		if ( isset($Element['elements']) ) {
			$Element['elements'] = $this->elementsApplyRecursiveDepthFirst($closure, $Element['elements']);
		} elseif ( isset($Element['element']) ) {
			$Element['element'] = $this->elementsApplyRecursiveDepthFirst($closure, $Element['element']);
		}

		$Element = call_user_func($closure, $Element);

		return $Element;
	}

	protected function elementsApplyRecursive($closure, array $Elements)
	{
		foreach ( $Elements as &$Element ) {
			$Element = $this->elementApplyRecursive($closure, $Element);
		}

		return $Elements;
	}

	protected function elementsApplyRecursiveDepthFirst($closure, array $Elements)
	{
		foreach ( $Elements as &$Element ) {
			$Element = $this->elementApplyRecursiveDepthFirst($closure, $Element);
		}

		return $Elements;
	}

	protected function element(array $Element)
	{
		if ( $this->safeMode ) {
			$Element = $this->sanitiseElement($Element);
		}

		# identity map if element has no handler
		$Element = $this->handle($Element);

		$hasName = isset($Element['name']);

		$markup = '';

		if ( $hasName ) {
			$markup .= '<' . $Element['name'];

			if ( isset($Element['attributes']) ) {
				foreach ( $Element['attributes'] as $name => $value ) {
					if ( $value === null ) {
						continue;
					}

					$markup .= " $name=\"" . self::escape($value) . '"';
				}
			}
		}

		$permitRawHtml = false;

		//
		if ( isset($Element['text']) ) {
			$text = $Element['text'];
		}
		// very strongly consider an alternative if you're writing an
		// extension
		elseif ( isset($Element['rawHtml']) ) {
			$text = $Element['rawHtml'];

			$allowRawHtmlInSafeMode = isset($Element['allowRawHtmlInSafeMode']) && $Element['allowRawHtmlInSafeMode'];
			$permitRawHtml = !$this->safeMode || $allowRawHtmlInSafeMode;
		}

		$hasContent = isset($text) || isset($Element['element']) || isset($Element['elements']);

		if ( $hasContent ) {
			$markup .= $hasName ? '>' : '';

			if ( isset($Element['elements']) ) {
				$markup .= $this->elements($Element['elements']);
			} elseif ( isset($Element['element']) ) {
				$markup .= $this->element($Element['element']);
			} else {
				if ( !$permitRawHtml ) {
					$markup .= self::escape($text, true);
				} else {
					$markup .= $text;
				}
			}

			$markup .= $hasName ? '</' . $Element['name'] . '>' : '';
		} elseif ( $hasName ) {
			$markup .= ' />';
		}

		return $markup;
	}

	protected function elements(array $Elements)
	{
		$markup = '';

		$autoBreak = true;

		foreach ( $Elements as $Element ) {
			if ( empty($Element) ) {
				continue;
			}

			$autoBreakNext = (isset($Element['autobreak'])
				? $Element['autobreak'] : isset($Element['name'])
			);
			// (autobreak === false) covers both sides of an element
			$autoBreak = !$autoBreak ? $autoBreak : $autoBreakNext;

			$markup .= ($autoBreak ? "\n" : '') . $this->element($Element);
			$autoBreak = $autoBreakNext;
		}

		$markup .= $autoBreak ? "\n" : '';

		return $markup;
	}

	# ~

	protected function li($lines)
	{
		$Elements = $this->linesElements($lines);

		if ( !in_array('', $lines)
			and isset($Elements[0]) and isset($Elements[0]['name'])
			and $Elements[0]['name'] === 'p'
		) {
			unset($Elements[0]['name']);
		}

		return $Elements;
	}
	//</editor-fold>

	#
	# AST Convenience
	#

	/**
	 * Replace occurrences $regexp with $Elements in $text. Return an array of
	 * elements representing the replacement.
	 */
	protected static function pregReplaceElements($regexp, $Elements, $text)
	{
		$newElements = [];

		while ( preg_match($regexp, $text, $matches, PREG_OFFSET_CAPTURE) ) {
			$offset = $matches[0][1];
			$before = substr($text, 0, $offset);
			$after = substr($text, $offset + strlen($matches[0][0]));

			$newElements[] = ['text' => $before];

			foreach ( $Elements as $Element ) {
				$newElements[] = $Element;
			}

			$text = $after;
		}

		$newElements[] = ['text' => $text];

		return $newElements;
	}

	#
	# Deprecated Methods
	#

	function parse($text)
	{
		$markup = $this->text($text);

		return $markup;
	}

	protected function sanitiseElement(array $Element)
	{
		static $goodAttribute = '/^[a-zA-Z0-9][a-zA-Z0-9-_]*+$/';
		static $safeUrlNameToAtt = [
			'a' => 'href',
			'img' => 'src',
		];

		if ( !isset($Element['name']) ) {
			unset($Element['attributes']);
			return $Element;
		}

		if ( isset($safeUrlNameToAtt[$Element['name']]) ) {
			$Element = $this->filterUnsafeUrlInAttribute($Element, $safeUrlNameToAtt[$Element['name']]);
		}

		if ( !empty($Element['attributes']) ) {
			foreach ( $Element['attributes'] as $att => $val ) {
				# filter out badly parsed attribute
				if ( !preg_match($goodAttribute, $att) ) {
					unset($Element['attributes'][$att]);
				} # dump onevent attribute
				elseif ( self::striAtStart($att, 'on') ) {
					unset($Element['attributes'][$att]);
				}
			}
		}

		return $Element;
	}

	protected function filterUnsafeUrlInAttribute(array $Element, $attribute)
	{
		foreach ( $this->safeLinksWhitelist as $scheme ) {
			if ( self::striAtStart($Element['attributes'][$attribute], $scheme) ) {
				return $Element;
			}
		}

		$Element['attributes'][$attribute] = str_replace(':', '%3A', $Element['attributes'][$attribute]);

		return $Element;
	}


	protected function buildFootnoteElement()
	{
		$Element = [
			'name' => 'div',
			'attributes' => ['class' => 'footnotes'],
			'elements' => [
				['name' => 'hr'],
				[
					'name' => 'ol',
					'elements' => [],
				],
			],
		];

		uasort($this->DefinitionData['Footnote'], 'self::sortFootnotes');

		foreach ( $this->DefinitionData['Footnote'] as $definitionId => $DefinitionData ) {
			if ( !isset($DefinitionData['number']) ) {
				continue;
			}

			$text = $DefinitionData['text'];

			$textElements = self::textElements($text);

			$numbers = range(1, $DefinitionData['count']);

			$backLinkElements = [];

			foreach ( $numbers as $number ) {
				$backLinkElements[] = ['text' => ' '];
				$backLinkElements[] = [
					'name' => 'a',
					'attributes' => [
						'href' => "#fnref$number:$definitionId",
						'rev' => 'footnote',
						'class' => 'footnote-backref',
					],
					'rawHtml' => '&#8617;',
					'allowRawHtmlInSafeMode' => true,
					'autobreak' => false,
				];
			}

			unset($backLinkElements[0]);

			$n = count($textElements) - 1;

			if ( $textElements[$n]['name'] === 'p' ) {
				$backLinkElements = array_merge(
					[
						[
							'rawHtml' => '&#160;',
							'allowRawHtmlInSafeMode' => true,
						],
					],
					$backLinkElements
				);

				unset($textElements[$n]['name']);

				$textElements[$n] = [
					'name' => 'p',
					'elements' => array_merge(
						[$textElements[$n]],
						$backLinkElements
					),
				];
			} else {
				$textElements[] = [
					'name' => 'p',
					'elements' => $backLinkElements
				];
			}

			$Element['elements'][1]['elements'] [] = [
				'name' => 'li',
				'attributes' => ['id' => 'fn:' . $definitionId],
				'elements' => array_merge(
					$textElements
				),
			];
		}

		return $Element;
	}

	# ~

	protected function parseAttributeData($attributeString)
	{
		$Data = [];

		$attributes = preg_split('/[ ]+/', $attributeString, -1, PREG_SPLIT_NO_EMPTY);

		foreach ( $attributes as $attribute ) {
			if ( $attribute[0] === '#' ) {
				$Data['id'] = substr($attribute, 1);
			} else # "."
			{
				$classes [] = substr($attribute, 1);
			}
		}

		if ( isset($classes) ) {
			$Data['class'] = implode(' ', $classes);
		}

		return $Data;
	}

	protected function addDdElement(array $Line, array $Block)
	{
		$text = substr($Line['text'], 1);
		$text = trim($text);

		unset($Block['dd']);

		$Block['dd'] = [
			'name' => 'dd',
			'handler' => [
				'function' => 'lineElements',
				'argument' => $text,
				'destination' => 'elements'
			],
		];

		if ( isset($Block['interrupted']) ) {
			$Block['dd']['handler']['function'] = 'textElements';

			unset($Block['interrupted']);
		}

		$Block['element']['bbb'] [] = &$Block['dd'];

		return $Block;
	}

	# ~

	protected function processTag($elementMarkup) # recursive
	{
		# http://stackoverflow.com/q/1148928/200145
		\libxml_use_internal_errors(true);

		$DOMDocument = new \DOMDocument;

		# http://stackoverflow.com/q/11309194/200145
		$elementMarkup = mb_convert_encoding($elementMarkup, 'HTML-ENTITIES', 'UTF-8');

		# http://stackoverflow.com/q/4879946/200145
		$DOMDocument->loadHTML($elementMarkup);
		$DOMDocument->removeChild($DOMDocument->doctype);
		$DOMDocument->replaceChild($DOMDocument->firstChild->firstChild->firstChild, $DOMDocument->firstChild);

		$elementText = '';

		if ( $DOMDocument->documentElement->getAttribute('markdown') === '1' ) {
			foreach ( $DOMDocument->documentElement->childNodes as $Node ) {
				$elementText .= $DOMDocument->saveHTML($Node);
			}

			$DOMDocument->documentElement->removeAttribute('markdown');

			$elementText = "\n" . $this->text($elementText) . "\n";
		} else {
			foreach ( $DOMDocument->documentElement->childNodes as $Node ) {
				$nodeMarkup = $DOMDocument->saveHTML($Node);

				if ( $Node instanceof DOMElement and !in_array($Node->nodeName, $this->textLevelElements) ) {
					$elementText .= $this->processTag($nodeMarkup);
				} else {
					$elementText .= $nodeMarkup;
				}
			}
		}

		# because we don't want for markup to get encoded
		$DOMDocument->documentElement->nodeValue = 'placeholder\x1A';

		$markup = $DOMDocument->saveHTML($DOMDocument->documentElement);
		$markup = str_replace('placeholder\x1A', $elementText, $markup);

		return $markup;
	}

	# ~

	protected function sortFootnotes($A, $B) # callback
	{
		return $A['number'] - $B['number'];
	}

	#
	# Fields
	#

	protected $regexAttribute = '(?:[#.][-\w]+[ ]*)';

	#
	# Static Methods
	#

	protected static function escape($text, $allowQuotes = false)
	{
		return htmlspecialchars($text, $allowQuotes ? ENT_NOQUOTES : ENT_QUOTES, 'UTF-8');
	}

	protected static function striAtStart($string, $needle)
	{
		$len = strlen($needle);

		if ( $len > strlen($string) ) {
			return false;
		} else {
			return strtolower(substr($string, 0, $len)) === strtolower($needle);
		}
	}

	static function instance($name = 'default')
	{
		if ( isset(self::$instances[$name]) ) {
			return self::$instances[$name];
		}

		$instance = new static();

		self::$instances[$name] = $instance;

		return $instance;
	}

	private static $instances = [];

	#
	# Fields
	#

	protected $DefinitionData;

	//<editor-fold desc="Read Only">

	protected $specialCharacters = [
		'\\', '`', '*', '_', '{', '}', '[', ']', '(', ')', '>', '#', '+', '-', '.', '!', '|', '~'
	];

	protected $StrongRegex = [
		'*' => '/^[*]{2}((?:\\\\\*|[^*]|[*][^*]*+[*])+?)[*]{2}(?![*])/s',
		'_' => '/^__((?:\\\\_|[^_]|_[^_]*+_)+?)__(?!_)/us',
	];

	protected $EmRegex = [
		'*' => '/^[*]((?:\\\\\*|[^*]|[*][*][^*]+?[*][*])+?)[*](?![*])/s',
		'_' => '/^_((?:\\\\_|[^_]|__[^_]*__)+?)_(?!_)\b/us',
	];

	protected $regexHtmlAttribute = '[a-zA-Z_:][\w:.-]*+(?:\s*+=\s*+(?:[^"\'=<>`\s]+|"[^"]*+"|\'[^\']*+\'))?+';

	protected $voidElements = [
		'area', 'base', 'br', 'col', 'command', 'embed', 'hr', 'img', 'input', 'link', 'meta', 'param', 'source',
	];

	protected $textLevelElements = [
		'a', 'br', 'bdo', 'abbr', 'blink', 'nextid', 'acronym', 'basefont',
		'b', 'em', 'big', 'cite', 'small', 'spacer', 'listing',
		'i', 'rp', 'del', 'code', 'strike', 'marquee',
		'q', 'rt', 'ins', 'font', 'strong',
		's', 'tt', 'kbd', 'mark',
		'u', 'xm', 'sub', 'nobr',
		'sup', 'ruby',
		'var', 'span',
		'wbr', 'time',
	];
	//</editor-fold>
}
//
