<?php
# Custom Function
//
namespace snkeng\core\site;

//
class htmlToMarkDown
{
	public $hasDB = false;

	//
	public function __construct($hasDB)
	{
		$this->hasDB = $hasDB;
	}


	public function convert($cText)
	{
		// Cleanup
		$cText =
			preg_replace(
				[
					"/\r/",
					"/&nbsp;*/",
					"/(<br([^>]*)>)+[\s]*/",
					"/<span([^>]*)>/",
					"/<\/span>/",
					"/&(amp;)+/",
				],
				[
					"",
					"",
					"\n",
					"",
					"",
					"&",
				],
				$cText
			);

		// Simple conversions
		$cText =
			preg_replace(
				[
					"/<b>([^<]*)<\/b>/",
					"/<u>([^<]*)<\/u>/",
					"/<i>([^<]*)<\/i>/",
					"/<a[^>]*href=\"([^\"]*)\"[^>]*>([^<]*)<\/a>/",
					"/<hr[\/ ]*>/",
					"/<(p|div)([^>]*)>[\s]*/",
					"/<\/(p|div)>[\s]*/",
				],
				[
					"**$1**",
					"_$1_",
					"_$1_",
					"[$2]($1)",
					"---\n",
					"",
					"\n\n",
				],
				$cText
			);
		// Simple conversions
		$cText =
			preg_replace(
				[
					"/<h1>(.*)<\/h1>[\s]*/m",
					"/<h2>(.*)<\/h2>[\s]*/m",
					"/<h3>(.*)<\/h3>[\s]*/m",
					"/<h4>(.*)<\/h4>[\s]*/m",
					"/<h5>(.*)<\/h5>[\s]*/m",
					"/<h6>(.*)<\/h6>[\s]*/m",
					"/<(p|div)([^>]*)>/",
					"/<\/(p|div)>[\s]*/",
				],
				[
					"# $1\n\n",
					"## $1\n\n",
					"### $1\n\n",
					"#### $1\n\n",
					"##### $1\n\n",
					"###### $1\n\n",
					"",
					"\n\n",
				],
				$cText
			);

		// Tables
		$cText = preg_replace_callback(
			'/<table[^>]*>.*<\/table>/si',
			function ($matches) {
				//
				$cTable =
					preg_replace(
						[
							"/<(table|thead|tbody|tfoot)[^>]*>[\s]*/",
							"/<\/(table|tbody)>[\s]*/",
							"/<(td|th)[^>]*>[\s]*/",
							"/<\/thead>[\s]*/",
							"/<tr[^>]*>[\s]*/",
							"/<\/(td|th)>[\s]*/",
							"/<\/tr>[\s]*/",
						],
						[
							"",
							"",
							"",
							">| ---- |\n",
							">| ",
							" | ",
							"\n",
						],
						$matches[0]
					);

				//
				$cTable = <<<TEXT
>[!TABLE] (wide)
{$cTable}
TEXT;


				//
				return $cTable;
			},
			$cText
		);

		// Images
		$cText = preg_replace_callback(
			'/<img([^>]*)>[\s]*/',
			function ($matches) {
				$nMatches = [];
				preg_match('/src="([^"]*)"/', $matches[0], $nMatches);

				$srcUrl = $nMatches[1];

				$newImage = '';
				$baseName = basename($srcUrl);

				// Advanced images
				if ( strpos($srcUrl, 'server/') ) {
					if ( $this->hasDB ) {
						$imgId = intval(explode('_', $baseName)[0]);

						//
						$imgData = \snkeng\core\engine\mysql::singleRowAssoc(
							"SELECT img_width AS width, img_height AS height FROM sc_site_images WHERE img_id={$imgId};",
							[
								'int' => ['width', 'height']
							]
						);

						$newImage = "\n>[!IMAGE] (site, $baseName, {$imgData['width']}, {$imgData['height']})\n\n";
					} else {
						$newImage = "\n>[!IMAGE] (site, $baseName, 16, 9)\n\n";
					}
				} // Direct images?
				else {
					// Local image?
					$width = '';
					$height = '';
					if ( $srcUrl[0] === '/' ) {
						list($width, $height) = getimagesize("img/flag.jpg");
					}

					$newImage = "\n\n![IMAGED] ({$srcUrl}, {$width}, {$height})\n\n";
				}

				//
				return $newImage;
			},
			$cText
		);

		// Youtube
		$cText = preg_replace_callback(
			'/<iframe([^>]*)>[\s]*/',
			function ($matches) {
				$nMatches = [];
				preg_match('/src="([^"]*)"/', $matches[0], $nMatches);

				//
				$nUrl = "";

				//
				if ( strpos($nMatches[1], 'youtube.com/embed/') !== false ) {
					$url = strtok($nMatches[1], '?');
					$vidId = strstr($url, 'embed/');
					$vidId = substr($vidId, 6);
					//
					$nUrl = "https://youtu.be/{$vidId}";
				} elseif ( strpos($nMatches[1], 'vimeo.com') !== false ) {
					$url = strtok($nMatches[1], '?');
					$vidId = strstr($url, 'video/');
					$vidId = substr($vidId, 6);
					//
					$nUrl = 'http://vimeo.com/' . $vidId;
				} else {
					return "\nNOT VALID IFRAME, WAS VIDEO? SRC:'{}' \n\n";
				}

				//
				return "\n>[!VIDEO] ({$nUrl})\n\n";
			},
			$cText
		);

		// List
		$cText = preg_replace_callback(
			'/<ol[^>]*>.*<\/ol>[\s]*/si',
			function ($matches) {
				//
				$cOl =
					preg_replace(
						[
							"/<ol[^>]*>[\s]*/",
							"/<\/ol>[\s]*/",
							"/<(li)[^>]*>[\s]*/",
							"/<\/li>[\s]*/",
						],
						[
							"",
							"",
							"- ",
							"\n",
						],
						$matches[0]
					);

				//


				//
				return $cOl;
			},
			$cText
		);

		// List
		$cText = preg_replace_callback(
			'/<ul[^>]*>.*<\/ul>[\s]*/si',
			function ($matches) {
				//
				$cOl =
					preg_replace(
						[
							"/<ul[^>]*>[\s]*/",
							"/<\/ul>[\s]*/",
							"/<(li)[^>]*>[\s]*/",
							"/<\/li>[\s]*/",
						],
						[
							"",
							"",
							"1. ",
							"\n",
						],
						$matches[0]
					);

				//


				//
				return $cOl;
			},
			$cText
		);

		// Mayor cleanup
		$cText =
			preg_replace(
				[
					"/<(\/)?(b|u|i|h1|h2|h3|h4|h5|h6)([^>]*)>/",
					"/[\n]{2,}/"
				],
				[
					"",
					"\n\n"
				],
				$cText
			);

		$cText = trim($cText);

		return $cText;
	}
}
