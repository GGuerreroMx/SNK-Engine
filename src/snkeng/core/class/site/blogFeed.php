<?php
//
namespace snkeng\core\site;

//
class blogFeed {

	//
	public static function printFeed($rssType) {
		global $mysql, $siteVars;
		//
		$debug = ( isset($_GET['debug']) && $_GET['debug']);
		$xml = '';

	// Información de última actualización (tomando referencia el último post publicado...)
		$sql_qry = <<<SQL
SELECT art_dt_pub AS dtPub, art_dt_mod AS dtMod
FROM sc_site_articles
WHERE art_type_primary='blog' AND art_status_published=1 AND art_dt_pub<NOW()
ORDER BY art_dt_pub DESC
LIMIT 1;
SQL;
		$dates = \snkeng\core\engine\mysql::singleRowAssoc($sql_qry);
		$dates['dtPub_t'] = strtotime($dates['dtPub']);
		$dates['dtMod_t'] = strtotime($dates['dtMod']);

	//
		$feedData = [
			's_title' => $siteVars['site']['name'],
			's_url' => $siteVars['site']['url'].'/',
			's_desc' => $siteVars['site']['desc'],
			's_authorName' => $siteVars['site']['name'],
			's_authorMail' => $siteVars['admin']['contact'],
			's_lang' => 'en-us',
			//
			'f_generator' => 'SNK Eng - Feed v1.2',
			'f_pubDate' => ( empty($dates['dtMod']) || $dates['dtMod_t']<$dates['dtMod_t'] ) ? $dates['dtPub_t'] : $dates['dtMod_t'],
			//
			'l_url' => $siteVars['site']['url']."/res/direct/site_core/res/img/st_logo_rss.jpg",
			'l_title' => $siteVars['site']['name'],
			'l_width' => '144',
			'l_height' => '78',
		];
		$feedData['sql'] = <<<SQL
SELECT
art.art_id AS id, art.art_title AS title, art.art_urltitle AS urltitle, art.art_url AS fullUrl,
art.art_dt_pub AS dtPub, art.art_dt_mod AS dtMod,
art.art_meta_desc AS metaDesc, art.art_content AS content,
cat.cat_title AS cTitle
FROM sc_site_articles AS art
INNER JOIN sc_site_category AS cat ON art.cat_id=cat.cat_id
WHERE art_type_primary='blog' AND art.art_status_published=1 AND art.art_dt_pub<now()
ORDER BY art.art_dt_pub DESC
LIMIT 20;
SQL;

	// header("Content-Type: text/plain; charset=UTF-8");
		if ( $rssType === 'rss2' ) {
			$feedData['f_cType'] = "application/rss+xml";
			//
			$feedData['f_pubDate_f'] = date("r", $dates['dtPub_t'] );
			$feedData['f_genDate_f'] = date("r", time() );
			//
			$struct = <<<XML
<item>
<title>!title;</title>
<link>{$siteVars['site']['url']}!fullUrl;</link>
<description>!metaDesc;</description>
<pubDate>!dtPubFeed;</pubDate>
<guid>{$siteVars['site']['url']}!fullUrl;</guid>
</item>\n
XML;

			//
			$data = '';
			if ( \snkeng\core\engine\mysql::execQuery($feedData['sql']) ) {
				while ( $datos = \snkeng\core\engine\mysql::$result->fetch_array() ) {
					$datos['dtPubFeed'] = date("r", strtotime($datos['dtPub']) );
					$datos['metaDesc'] = substr($datos['metaDesc'], 0, 150);
					$data.= stringPopulate($struct, $datos);
				}
			}
			//
			$xml = <<<XML
<?xml version="1.0"?>
<rss version="2.0">
<channel>
<title>{$feedData['s_title']}</title>
<link>{$feedData['s_url']}</link>
<description>{$feedData['s_desc']}</description>
<language>{$feedData['s_lang']}</language>
<pubDate>{$feedData['f_pubDate_f']}</pubDate>
<lastBuildDate>{$feedData['f_genDate_f']}</lastBuildDate>
<docs>http://blogs.law.harvard.edu/tech/rss</docs>
<generator>{$feedData['f_generator']}</generator>
<managingEditor>editor@example.com</managingEditor>
<webMaster>webmaster@example.com</webMaster>
{$data}</channel>
</rss>
XML;
		} elseif ( $rssType === 'atom' ) {
			$feedData['f_cType'] = "application/atom+xml";
			//
			$feedData['f_pubDate_f'] = date("c", $dates['dtPub_t'] );
			$feedData['f_genDate_f'] = date("c", time() );
			//
			$struct = <<<XML
<entry>
<title>!title;</title>
<link href="{$siteVars['site']['url']}!fullUrl;" />
<id>{$siteVars['site']['url']}!fullUrl;</id>
<updated>!dtPubFeed;</updated>
<summary>!metaDesc;</summary>
<content type="xhtml">
!content;
</content>
</entry>\n
XML;

			//
			$data = '';
			if ( \snkeng\core\engine\mysql::execQuery($feedData['sql']) ) {
				while ( $datos = \snkeng\core\engine\mysql::$result->fetch_array() ) {
					$datos['dtPubFeed'] = date("c", strtotime($datos['dtPub']) );
					$datos['metaDesc'] = substr($datos['metaDesc'], 0, 150);
					$data.= stringPopulate($struct, $datos);
				}
			}
			//
			$xml = <<<XML
<?xml version="1.0" encoding="utf-8"?>
<feed xmlns="http://www.w3.org/2005/Atom">
<title>{$feedData['s_title']}</title>
<subtitle>{$feedData['s_desc']}</subtitle>
<link href="{$siteVars['site']['url']}/blog/feed/atom/" rel="self" />
<link href="{$siteVars['site']['url']}/" />
<id>{$siteVars['site']['url']}</id>
<updated>{$feedData['f_pubDate_f']}</updated>
{$data}</feed>
XML;
		}
		//

		//
		$feedData['f_cType'] = ( $debug ) ? 'text/plain' : $feedData['f_cType'];
		header('HTTP/1.1 200 OK');
		header('Status: 200');
		header("Last-Modified: ".date(DATE_RFC2822, $feedData['f_pubDate']));
		header("Content-Type: {$feedData['f_cType']}; charset=UTF-8");
		echo($xml."\n");
		exit();
	}
}