<?php
//
namespace snkeng\core\site;

//
class tagSystem
{

	// Esenciales
	private $mysql;                            // Conexión a mysql

	private $objId;                            // Id para ver la relación con los tags

	// Información
	private $tagData = array();                // Tags disponibles
	private $tagSel = array();                // Tags seleccionados
	private $sql_op = "";                    // Query de las operaciones a realizar

	//
	// Constructor
	//

	public function __construct($objId)
	{
		// Conexión mysql
		$this->mysql = $mysql;

		// asignación de variables
		$this->objId = $objId;

		// Determinación de elementos existentes
		$sql_qry = "SELECT tagl_id, tagl_title FROM st_site_tags";
		if ( \snkeng\core\engine\mysql::execQuery($sql_qry) ) {
			while ( $data = \snkeng\core\engine\mysql::$result->fetch_array() ) {
				$tempArray = array();
				$tempArray['id'] = $data['tagl_id'];
				$tempArray['title'] = $data['tagl_title'];
				$this->tagData[] = $tempArray;
			}
		} else {
			echo("\n<br/ >\nERROR: No hay tags para hacer operaciones.\n<br/ >\n");
			if ( empty(\snkeng\core\engine\mysql::error) ) {
				echo("Vacio.");
			} else {

				echo(\snkeng\core\engine\mysql::createAdvancedError('NO fue posible leer información', 'full'));
			}
		}

		// Determinación de elementos seleccionados (puede ser vacio, no hay problema)
		$sql_qry = "SELECT tagl_id FROM st_tagrel WHERE p_id=$objId";
		if ( \snkeng\core\engine\mysql::execQuery($sql_qry) ) {
			while ( $data = \snkeng\core\engine\mysql::$result->fetch_array() ) {
				$this->tagSel[] = $data['tagl_id'];
			}
		} else {
			if ( !empty(\snkeng\core\engine\mysql::error) ) {
				echo("\n<br/ >\nERROR: Elementos disponibles.\n<br/ >\n");
				echo(\snkeng\core\engine\mysql::createAdvancedError('NO fue posible leer información', 'full'));
			}
		}

	}

	public function printFunc()
	{
		if ( isset($_POST['upTags']) ) {
			$this->buildQuery();
			echo("\n<br />\n");
			if ( empty($this->sql_op) ) {
				echo("No hubieron cambios que actualizar");
			} else {
				echo($this->sql_op . "<br />\n");
				if ( \snkeng\core\engine\mysql::submitMultiQuery($this->sql_op) ) {
					echo("Operación de actualización llevada exitosamente.");
				} else {
					echo("ERROR: No se llevo a cabo la operación de actualización.<br />\n");
					echo(\snkeng\core\engine\mysql::createAdvancedError('NO fue posible leer información', 'full'));
				}
			}
		} else {
			$this->printOptions();
		}
	}

	private function printOptions()
	{
		echo <<<EOD
<form id="" name="" method="post">
	<table cellpadding="3" cellspacing="1" class="sortable">
		<thead>
			<tr><th>Tag</th><th>Sel.</th></tr>
		</thead>
		<tbody>\n
EOD;
		$string = "\t\t\t<tr><input type=\"hidden\" name=\"%s_tId\" value=\"%s\" /><td>%s</td><td><input type=\"checkbox\" name=\"%s_sel\" value=\"1\" %s/></td></tr>\n";
		$i = 0;
		foreach ( $this->tagData as $tag ) {
			$status = "";
			foreach ( $this->tagSel as $sel ) {
				if ( $tag['id'] == $sel['id'] ) {
					$status = "checked ";
				}
			}
			// Imprimir
			printf($string, $i, $tag['id'], $tag['title'], $i, $status);
			$i++;
		}
		echo <<<EOD
		</tbody>
	</table>
	<br />
	<input type="hidden" name="tagsCount" value="$i" />
	<input name="upTags" type="submit" value="Actualizar" /><br />
</form>\n
EOD;
	}

	// Creación del query
	private function buildQuery()
	{
		$count = $_POST['tagsCount'];
		$tagNew = array();
		for ( $i = 0; $i < $count; $i++ ) {
			if ( $_POST[$i . '_sel'] == "1" ) {
				$tagNew[] = $_POST[$i . '_tId'];
			}
		}

		// Seleccionar componentes
		$sql_del = "";
		$sql_add = "";
		$subQuery = "";
		$pid = $this->objId;

		// Agregar
		$first = true;
		$tempArray = array_diff($tagNew, $this->tagSel);
		foreach ( $tempArray as $id ) {
			if ( $first ) {
				$first = false;
			} else {
				$sql_add .= ",\n";
			}
			$sql_add .= "($id, $pid)";
		}

		// Borrar
		$first = true;
		$tempArray = array_diff($this->tagSel, $tagNew);
		foreach ( $tempArray as $id ) {
			if ( $first ) {
				$first = false;
			} else {
				$sql_del .= " OR ";
			}
			$sql_del .= "tagl_id=$id";
		}
		if ( !empty($sql_add) ) {
			$subQuery .= "INSERT INTO st_tagrel\n(tagl_id, p_id) \nVALUES\n" . $sql_add . ";\n";
		}
		if ( !empty($sql_del) ) {
			$subQuery .= "DELETE FROM st_tagrel WHERE p_id=$pid AND (" . $sql_del . ");\n";
		}
		$this->sql_op = $subQuery;
	}
}

?>