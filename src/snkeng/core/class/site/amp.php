<?php
//
namespace snkeng\core\site;

//
class amp
{
	//
	public static function printPage($parameters)
	{
		global $siteVars;

		$defaults = [
			'template' => "/snkeng/site_core/templates/amp.php",
			'siteName' => $siteVars['site']['name'],
			'logo' => $siteVars['site']['img'],
			'title' => '',
			'metaWord' => '',
			'metaDesc' => '',
			'metaData' => '',
			'type' => 'Article',
			'twType' => 'Article',
			'image' => '',
			'urlCanonical' => '',
			'content_body' => '',
			'content_style' => '',
			'script_head' => '',
			'script_body' => '',
			'content_mode' => 'html',
			'svg_icons' => [],
			'google_add' => false,
		];
		$contents = array_merge($defaults, $parameters);

		// Debug original content for amp adaptation
		if ( isset($_GET['debug']) && $_GET['debug'] ) {
			header('Content-Type: text/plain; charset=utf-8');
			echo <<<TEXT
	CONTENIDO ORIGINAL:
	
	-----------------------------------
	{$contents['content_body']}
	-----------------------------------\n\n\n
TEXT;
			//
		}

		//
		if ( $contents['content_mode'] === 'html' ) {
			//<editor-fold desc="Youtube">

			// Second pass
			$orig = [
				'/<iframe.*(youtube\.com\/embed|youtu\.be)\/([\w\-]*).*<\/iframe>/',
			];
			$replace = [
				"<amp-youtube data-videoid=\"$2\" layout=\"responsive\" width=\"480\" height=\"270\"></amp-youtube>"
			];

			//
			$contents['content_body'] = preg_replace($orig, $replace, $contents['content_body']);

			//</editor-fold>


			//<editor-fold desc="Images">

			// Image fix
			$contents['content_body'] = preg_replace_callback(
				'/<img([^>]*)>/',
				function ($matches) {
					// Get image parameters
					preg_match_all('/(\w+)=[\'"]([^\'"]*)/', $matches[0],$attributeMatches,PREG_SET_ORDER);

					//
					$result = [
						'src' => '',
						'alt' => ''
					];

					//
					foreach ( $attributeMatches as $match ) {
						$attrName = $match[1];

						//parse the string value into an integer if it's numeric,
						// leave it as a string if it's not numeric,
						$attrValue = is_numeric($match[2])? (int)$match[2]: trim($match[2]);

						$result[$attrName] = $attrValue; //add match to results
					}

					// Return image with available parameters
					return <<<HTML
	<amp-img src="{$result['src']}" alt="{$result['alt']}" width="475" height="268" layout="responsive" class="m1">
	<noscript><img src="{$result['src']}" width="475" height="268" alt="{$result['alt']}"></noscript>
	</amp-img>
HTML;
					//
				},
				$contents['content_body']
			);

			//
			if ( isset($_GET['debug']) && $_GET['debug'] ) {
				echo <<<TEXT
	CONTENIDO MODIFICADO:
	
	-----------------------------------
	{$contents['content_body']}
	-----------------------------------\n\n\n
TEXT;
				//
				exit;
			}
			//</editor-fold>

		}
		else {
			// No youtube operations requiered
		}


		// Youtube JS activation
		if ( preg_match('/<amp-youtube/', $contents['content_body']) ) {
			$contents['script_head'] .= '<script async custom-element="amp-youtube" src="https://cdn.ampproject.org/v0/amp-youtube-0.1.js"></script>' . "\n";
		}


		// ADS activation
		if ( !empty($contents['google_ads']) ) {
			$contents['script_head'] .= '<script async custom-element="amp-auto-ads" src="https://cdn.ampproject.org/v0/amp-auto-ads-0.1.js"></script>' . "\n";
		}


		//<editor-fold desc="Google Analytics">

		//
		if ( !empty($_ENV['GOOGLE_ANALYTICS_V3']) ) {
			$contents['script_head'] .= '<script async custom-element="amp-analytics" src="https://cdn.ampproject.org/v0/amp-analytics-0.1.js"></script>' . "\n";
			$contents['script_body'].= <<<HTML
<amp-analytics type="gtag" data-credentials="include">
<script type="application/json">
{
	"vars":{
		"gtag_id":"{$_ENV['GOOGLE_ANALYTICS_V3']}",
		"config":{
			"{$_ENV['GOOGLE_ANALYTICS_V3']}": { "groups": "default" }
		}
	}
}
</script>
</amp-analytics>\n
HTML;
			//
		}
		//</editor-fold>

		// cargar template
		$templateData = (require $_SERVER['DOCUMENT_ROOT'] . $contents['template']);
		$contents['content_body'] = stringPopulate($templateData['template_body'], ['content_body' => $contents['content_body']]);
		$contents['svg_icons'] = array_merge($templateData['svg_icons'], $contents['svg_icons']);

		// Procesar íconos
		$svgIcons = '';
		foreach ( $contents['svg_icons'] as $icon ) {
			$svgIcons.= "<symbol id=\"{$icon[0]}\" viewBox=\"{$icon[1]}\">{$icon[2]}</symbol>\n";
		}


		//
		header('Content-Type: text/html; charset=utf-8');

		//
		echo <<<HTML
<!doctype html>
<html amp lang="es">
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width,initial-scale=1,minimum-scale=1,maximum-scale=1,user-scalable=no,minimal-ui">
	<title>{$contents['title']} | {$siteVars['site']['name']}</title>
	<meta property="words" content="{$contents['metaWord']}">
	<meta property="description" content="{$contents['metaDesc']}">
	<link rel="canonical" href="{$contents['urlCanonical']}" />
	<meta property="og:site_name" content="{$contents['siteName']}">
	<meta property="og:title" content="{$contents['title']}">
	<meta property="og:url" content="{$contents['urlCanonical']}">
	<meta property="og:description" content="{$contents['metaDesc']}">
	<meta property="og:image" content="{$contents['image']}">
	<meta property="og:type" content="{$contents['type']}">
	<meta name="twitter:card" content="{$contents['twType']}">
	<script type="application/ld+json">
{$contents['metaData']}
	</script>
    <script async src="https://cdn.ampproject.org/v0.js"></script>
	{$contents['script_head']}
    <style amp-boilerplate>body{-webkit-animation:-amp-start 8s steps(1,end) 0s 1 normal both;-moz-animation:-amp-start 8s steps(1,end) 0s 1 normal both;-ms-animation:-amp-start 8s steps(1,end) 0s 1 normal both;animation:-amp-start 8s steps(1,end) 0s 1 normal both}@-webkit-keyframes -amp-start{from{visibility:hidden}to{visibility:visible}}@-moz-keyframes -amp-start{from{visibility:hidden}to{visibility:visible}}@-ms-keyframes -amp-start{from{visibility:hidden}to{visibility:visible}}@-o-keyframes -amp-start{from{visibility:hidden}to{visibility:visible}}@keyframes -amp-start{from{visibility:hidden}to{visibility:visible}}</style>
	<noscript><style amp-boilerplate>body{-webkit-animation:none;-moz-animation:none;-ms-animation:none;animation:none}</style></noscript>
    <style amp-custom>
{$templateData['template_style']}
{$contents['content_style']}
    </style>
</head>


<body>
{$contents['script_body']}
{$contents['content_body']}
</body>

</html>

<!DOCTYPE svg SYSTEM "http://www.w3.org/Graphics/SVG/1.1/DTD/svg11.dtd">
<svg xmlns="http://www.w3.org/2000/svg" version="1.1" style="display:none;">
{$svgIcons}</svg>\n
HTML;
		//
		exit();
	}

	//
	public static function contentClear($content) {
		$orig = [
			'/<\/?span[^>]*>/', '/<\/?div[^>]*>/', '/style=\"[^\"]*\"/',
			'/<(table|thead|tbody|tfoot|tr|th|td|p)[^>]*>/', '/<a[^>]*href="([^"]*)"[^>]*>/'
		];
		$replace = [
			'', '', '',
			'<$1>', "<a href=\"$1\">"
		];
		//
		$content = preg_replace($orig, $replace, $content);

		return $content;
	}
	//
}
//
