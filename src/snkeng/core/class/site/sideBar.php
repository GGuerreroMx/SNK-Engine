<?php
//
namespace snkeng\core\site;

//
class sideBar
{
	// Variables de sesión
	public $mysql;					// SQL
	public $siteVars;				// Sitio

	public static function createSidebar($elements)
	{
		// Cachar variables globales
		$resultHTML = '';

		
		foreach ( $elements as $element )
		{
			$parameters = ( isset($element['props']) ) ? $element['props'] : [];

			switch ( $element['name'] )
			{
				case 'banner':
					$resultHTML.= self::build_banner($parameters);
					break;
				case 'blogMosts':
					$resultHTML.= self::build_blogMosts($parameters);
					break;
				case 'blogCategory':
					$resultHTML.= self::build_blogCategory($parameters);
					break;
				case 'blogSearch':
					$resultHTML.= self::build_blogSearch($parameters);
					break;
				//
				default:
					se_killWithError('snkCoreSiteSideBar Proc not defined');
					break;
			}
		}

		return $resultHTML;
	}

	//
	// INI: build_blogMosts
	//
	private static function build_blogMosts($params)
	{
		global $postLang, $mysql;

		$extraWhere = '';

		//
		if ( isset($params['timeLimit']) ) {
			$extraWhere.= " AND art_dt_pub > DATE_SUB(CURDATE(), INTERVAL {$params['timeLimit']} MONTH)";
		}

		//
		$artType = ( isset($params['artType']) ) ? $params['artType'] : 'blog';


		//
		$sql_qry = <<<SQL
SELECT
	art.art_title AS title, art.art_url AS urlFull,
	IF ( art.art_img_struct IS NULL OR art.art_img_struct = '', 'empty.jpg', art.art_img_struct ) AS image,
	DATE_FORMAT(art_dt_pub, '%e %M %Y') AS date
FROM sc_site_articles AS art
WHERE art.art_dt_pub<NOW() AND art.art_status_published=1 AND art.art_type_primary='{$artType}'{$extraWhere}
LIMIT 0,3;
SQL;

		//
		$text = <<<HTML
<a class="postLink2" se-nav="se_middle" href="!urlFull;">
	<picture class="imgCont" itemprop="image">
		<source type="image/webp" srcset="/res/image/site/w_320-m_fit-r_16,9/!image;.webp" /> 
		<img src="/res/image/site/w_320-m_fit-r_16,9/!image;.jpg" width="320" height="180" />
	</picture>
	<div class="pl_title">!title;</div>
</a>\n
HTML;

		//
		$testPost = \snkeng\core\engine\mysql::printSimpleQuery($sql_qry, $text, [
		]);

		//
		return <<<HTML
<div id="theMost" class="sb_mod">
<div class="title">{$postLang['theMost']}</div>
{$testPost}
</div>\n
HTML;
		//
	}
	// END: build_blogMosts
	//

	//
	// INI: build_blogMostsType
	//
	private static function build_blogMostsType($params)
	{
		global $postLang, $mysql;
		$text = <<<HTML
<a class='postLink' se-nav='se_middle' href='!urlFull;'>
<img class='postImg' src='!img;_w320.jpg' srcset="!img;_w320.jpg 320w, !img;_w832.jpg 832w" sizes="(max-width: 320px) 100vw, (max-width: 768px) 100vw, 260px" />
<div class='postContent'><span class='pl_title'>!title;</span><span class='pl_data'></span></div>
</a>\n
HTML;

		$sql_qry = <<<SQL
SELECT
	art.art_title AS title, art.art_url AS urlFull,
	art.art_img_struct AS img, DATE_FORMAT(art_dt_pub, '%e %M %Y') AS date
FROM sc_site_articles AS art
WHERE art.art_dt_pub<now() AND art.art_status_published=1 AND art.art_type_primary='blog'
ORDER BY {$params['order']}
LIMIT 0,3;
SQL;

		//
		return \snkeng\core\engine\mysql::printSimpleQuery($sql_qry, $text, []);
	}
	// END:build_blogMostsType
	//

	//
	// INI: build_blogCategory
	//
	private static function build_blogCategory($params)
	{
		global $postLang, $mysql;

		//
		$sql_qry = "SELECT
						cat_urltitle AS urltitle, cat_title AS title
					FROM sc_site_category
					WHERE cat_mode_type='blog'
					LIMIT 10;";

		//
		$text = "\t\t\t<a se-nav='se_middle' href='/blog/category/!urltitle;/'>!title;</a>\n";

		//
		$blogCats = \snkeng\core\engine\mysql::printSimpleQuery($sql_qry, $text, []);

		//
return <<<HTML
	<div class="sb_mod">
		<div class="title">{$postLang['categories']}</div>
		<div class="links">
{$blogCats}		</div>
	</div>\n
HTML;
	}
	// END: build_blogCategory
	//

	//
	// INI: build_blogSearch
	//
	private static function build_blogSearch($params)
	{
		global $postLang;
		//
		$_GET['sT'] = ( isset($_GET['sT']) ) ? $_GET['sT'] : '';
		//
return <<<HTML
    <div class="sb_mod">
		<form action="/blog/search/?" id="search" method="get">
			<div class="searchElement">
				<input type="text" name="sT" value="{$_GET['sT']}" placeholder="{$postLang['search']}..." maxlength="50" size="20">
				<span><svg class="icon inline"><use xlink:href="#fa-search" /></svg></span>
			</div>
		</form>
	</div>\n
HTML;
	}
	// END: build_blogSearch
	//

	//
	// INI: build_banner
	//
	private static function build_banner($params)
	{
		
		if ( !isset($params) ) {
			se_killWithError('Banner size not defined');
		}

		$returnData = '';

		// Verificar que existan resultados con las características y reiniciar en caso contrario
		$sql_select = <<<SQL
SELECT
	ban_id, ban_type, ban_size,
	ban_alt,
	ban_floc, ban_ftype, ban_url,
	ban_code
FROM sc_site_banners
WHERE ban_size={$params['size']}
	AND ban_active=1
	AND ban_showed=0
	AND ( ban_curclicks<ban_maxclicks OR ban_maxclicks=0 )
	AND ( ban_curprints<ban_maxprints OR ban_maxprints=0 )
	AND ( now()<ban_dtend OR ban_dtend IS NULL )
	AND ( now()>ban_dtini OR ban_dtini IS NULL )
LIMIT 1;
SQL;
		$bannerData = \snkeng\core\engine\mysql::singleRowAssoc($sql_select,
			[
				'errorKey' => '',
				'errorDesc' => 'Unable to read banner'
			]
		);

		//
		if ( empty($bannerData) ) {
			$sql_qry = "UPDATE sc_site_banners
						SET ban_showed=0
						WHERE ban_active=1 AND ban_size={$params['size']};";
			\snkeng\core\engine\mysql::submitQuery($sql_qry,
				[
					'errorKey' => '',
					'errorDesc' => ''
				]
			);

			// Retry
			$bannerData = \snkeng\core\engine\mysql::singleRowAssoc($sql_select,
				[
					'errorKey' => '',
					'errorDesc' => ''
				]
			);
		}

		//
		if ( !empty($bannerData) ) {
			// Actualizar banner
			$sql_qry = "UPDATE sc_site_banners
						SET ban_curprints=ban_curprints+1, ban_showed=1
						WHERE ban_id={$bannerData['ban_id']};";
			\snkeng\core\engine\mysql::submitQuery($sql_qry);

			//
			switch ( $bannerData['ban_type'] ) {
				// File
				case '1':
					$returnData = "<a class='se_banner' se-plugin='banner' href='{$bannerData['ban_url']}' target='_new' data-id='{$bannerData['ban_id']}'><img src='{$bannerData['ban_floc']}' alt='{$bannerData['ban_alt']}' /></a>\n";
					break;
				// Code
				case '2':
					$returnData = $bannerData['ban_code']."\n";
					break;
			}
		} else {
			$returnData = "<!-- BANNER ({$params['size']}): Not Available -->\n";
		}
		//
		return  $returnData;
	}
	// END:build_banner
	//
}
//
