<?php
//
namespace snkeng\core\site;

//
class markdown_form {
	//
	public static function getText($appName, $appMode, $appId, $lang) {
				//
		$qry_sel = <<<SQL
SELECT
	txt.txt_id AS id, txt.txt_id_parent AS parentId,
	txt.txt_dt_add AS dtAdd, txt.txt_dt_mod AS dtMod,
	txt.txt_app_name AS appName, txt.txt_app_mode AS appMode, txt.txt_app_id AS appId,
	txt.txt_lang AS txtLang, txt.txt_type AS txtType, txt.txt_content AS content
FROM sc_site_texts AS txt
WHERE txt.txt_app_name='{$appName}' AND txt.txt_app_mode='{$appMode}' AND txt.txt_app_id='{$appId}' AND txt.txt_lang='{$lang}' AND txt.txt_type='md'
LIMIT 1;
SQL;
		$textData = \snkeng\core\engine\mysql::singleRowAssoc(
			$qry_sel,
			[
				'int' => ['id', 'parentId'],
				'stripCSlashes' => ['content']
			],
			[
				'errorKey' => '',
				'errorDesc' => '',
			]
		);

		return $textData;
	}

	//
	public static function formPrint(string $appName, string $appMode, int $appId, string $lang, string $ajaxBaseUrl, string $windowsPageUrl, array $params = []) {
		\snkeng\core\engine\nav::pageFileModuleAdd('admin_modules', 'site', '/pages/site-adm-pages-markdownform.css');
		\snkeng\core\engine\nav::pageFileModuleAdd('admin_modules', 'site', '/pages/site-adm-pages-markdownform.mjs');

		//
		$defaults = [
			'extraInput' => [],
		];
		$settings = array_merge($defaults, $params);

		$extraHiddenValues = '';
		if ( $settings['extraInput'] ) {
			foreach ( $settings['extraInput'] as $key => $val )  {
				$extraHiddenValues.= "<input type='hidden' name='{$key}' value='{$val}' />\n";
			}
		}

		//
		return <<<HTML
<site-adm-pages-markdownform data-apiurl="{$ajaxBaseUrl}" data-imgurl="{$windowsPageUrl}">
	<div class="formContainer">
		<form se-elem="text" class="se_form" method="post" action="{$ajaxBaseUrl}/textMD" is="se-async-form">
			{$extraHiddenValues}
			<input type="hidden" name="txtId" value="">
			<input type="hidden" name="appId" value="{$appId}">
			<input type="hidden" name="lang" value="{$lang}">
			<textarea class="markdDownFormTextArea" name="content"></textarea>
			<button type="submit" class="btn"><svg class="icon inline mr"><use xlink:href="#fa-save" /></svg>Guardar</button>
			<output></output>
		</form>
	</div>
	<div class="options" se-elem="options">
		<se-simple-tab class="mdTabs" data-selectfirst="false" data-hiddable="true">
			<div role="tablist">
				<div role="tab" aria-selected="false">Insertar</div>
				<div role="tab" aria-selected="false">Referencia</div>
			</div>
			<div class="contents" se-elem="panelGroup">
				<!-- DIV -->
				<div role="tabpanel">
					<div class="tabTitle">Insertar</div>
					<div class="verticalListObjects">
						<details>
							<summary>Notificaciones</summary>
							<form class="se_form" data-addoperation="notification">
								<label>
									<div class="title">Ícono</div>
									<input type="text" name="icon" required />
								</label>
								<label>
									<div class="title">Título</div>
									<input type="text" name="title" required />
								</label>
								<label>
									<div class="title">Estilo adicional</div>
									<select name="style">
										<option value="">Ninguno</option>
										<option value="warning">Advertencia</option>
										<option value="alert">Alerta</option>
									</select>
								</label>
								<label>
									<div class="title">Contenido</div>
									<textarea name="content" required></textarea>
								</label>
								<button type="submit"><svg class="icon inline mr"><use xlink:href="#fa-save" /></svg>Insertar</button>
							</form>
						</details>
						<details>
							<summary>Notas</summary>
							<form class="se_form" data-addoperation="note">
								<label>
									<div class="title">Título</div>
									<input type="text" name="title" required />
								</label>
								<label>
									<div class="title">Estilo adicional</div>
									<select name="style">
										<option value="">Normal</option>
										<option value="blue">Azul</option>
										<option value="red">Rojo</option>
									</select>
								</label>
								<label>
									<div class="title">Contenido</div>
									<textarea name="content" required></textarea>
								</label>
								<button type="submit"><svg class="icon inline mr"><use xlink:href="#fa-save" /></svg>Insertar</button>
							</form>
						</details>
						<details>
							<summary>Citas</summary>
							<form class="se_form" data-addoperation="quote">
								<label>
									<div class="title">Contenido</div>
									<textarea name="content" required></textarea>
								</label>
								<label>
									<div class="title">Autor</div>
									<input type="text" name="author" required />
								</label>
								<label>
									<div class="title">Fuente</div>
									<input type="text" name="source" />
								</label>
								<label>
									<div class="title">URL</div>
									<input type="text" name="url" />
								</label>
								<button type="submit"><svg class="icon inline mr"><use xlink:href="#fa-save" /></svg>Insertar</button>
							</form>
						</details>
						<details>
							<summary>Video</summary>
							<form class="se_form" data-addoperation="video">
								<label>
									<div class="title">URL video</div>
									<input type="url" name="url" required />
								</label>
								<button type="submit"><svg class="icon inline mr"><use xlink:href="#fa-save" /></svg>Insertar</button>
							</form>
						</details>
						<details>
							<summary>Imagen</summary>
							<div>
							<button class="btn blue wide" data-markdownform-onclick="imageSelect"><svg class="icon inline mr"><use xlink:href="#fa-photo" /></svg>Seleccionar</button>
							<form class="se_form" data-addoperation="image">
								<input type="hidden" name="adjustable" />
								<label>
									<div class="title">Carpeta</div>
									<input type="text" name="folder" required />
								</label>
								<label>
									<div class="title">Nombre</div>
									<input type="text" name="fname" required />
								</label>
								<label>
									<div class="title">Ubicación</div>
									<input type="text" name="floc" />
								</label>
								<label>
									<div class="title">Ancho</div>
									<input type="number" name="width" min="0" max="6000" required />
								</label>
								<label>
									<div class="title">Alto</div>
									<input type="number" name="height" min="0" max="6000" required />
								</label>
								<label>
									<div class="title">Descripción de la imagen</div>
									<textarea name="description"></textarea>
								</label>
								<label>
									<div class="title">Pie de foto</div>
									<textarea name="footer"></textarea>
								</label>
								<label>
									<div class="title">Créditos</div>
									<input type="text" name="credit" />
								</label>
								<button type="submit"><svg class="icon inline mr"><use xlink:href="#fa-save" /></svg>Insertar</button>
							</form>
						</div>
						</details>
						<details>
							<summary>Tablas</summary>
							<div>
								<form class="se_form" data-addoperation="table">
									<div class="separator">
										<div class="title">Modificadores</div>
										<label><input type="checkbox" value="1" name="border">Bordes</label>
										<label><input type="checkbox" value="1" name="alternate">Fondo alternado</label>
										<label><input type="checkbox" value="1" name="fixed">Tamaños similares</label>
										<label><input type="checkbox" value="1" name="wide">Ancho completo</label>
									</div>
									<label>
										<div class="title">Columnas</div>
										<input type="number" name="cols" min="0" max="10" value="2" required />
									</label>
									<label>
										<div class="title">Renglones Cabeza</div>
										<input type="number" name="rows_head" min="0" max="50" value="1" required />
									</label>
									<label>
										<div class="title">Renglones Cuerpo</div>
										<input type="number" name="rows_body" min="0" max="50" value="2" required />
									</label>
									<label>
										<div class="title">Renglones Pie</div>
										<input type="number" name="rows_foot" min="0" max="50" value="0" required />
									</label>
									<label>
										<div class="title">Título tabla</div>
										<input type="text" name="title" />
									</label>
									<label>
										<div class="title">Descripción</div>
										<textarea name="description"></textarea>
									</label>
									<div class="gOMB">
										<div>Ejemplo</div>
										<div se-elem="buildedTable"></div>
									</div>
									<button type="submit"><svg class="icon inline mr"><use xlink:href="#fa-save" /></svg>Insertar</button>
								</form>
							</div>
						</details>
					</div>
				</div>
				<!-- DIV -->
				<div role="tabpanel">
					<div class="tabTitle">Referencia</div>
					<div class="verticalListObjects">
						<details>
							<summary>Formato básico</summary>
							<div>
								<div>Generalidades</div>
								<div>
									<p>Debe existir dos renglones de diferencia entre cada elemento. (títulos con parrafo, parrafo con listas, imágenes, videos, etc).</p>
									<p>El sistema hace los ajustes según las necesidades. Más de dos espacios son ignorados y se tratan como si fueran los dos.</p>
								</div>
								<div>Negritas</div>
								<table class="se_table border">
									<thead>
										<tr><th>Texto</th><th>HTML</th><th>Resultado</th></tr>
									</thead>
									<tbody>
										<tr><td><code>texto en **negritas**.</code></td><td><code>texto en &lt;strong&gt; negritas&lt;/strong&gt;.</code></td><td>texto en <strong>negritas</strong>.</td></tr>
										<tr><td><code>texto en __negritas__.</code></td><td><code>texto en &lt;strong&gt; negritas&lt;/strong&gt;.</code></td><td>texto en <strong>negritas</strong>.</td></tr>
									</tbody>
								</table>
								<div>Itálicas</div>
								<table class="se_table border">
									<thead>
										<tr><th>Texto</th><th>HTML</th><th>Resultado</th></tr>
									</thead>
									<tbody>
										<tr><td><code>texto *en itálicas*.</code></td><td><code>texto &lt;em&gt;en itálicas&lt;/em&gt;.</code></td><td>texto <em>en itálicas</em>.</td></tr>
										<tr><td><code>texto _en itálicas_.</code></td><td><code>texto &lt;em&gt;en itálicas&lt;/em&gt;.</code></td><td>texto <em>en itálicas</em>.</td></tr>
									</tbody>
								</table>
							</div>
						</details>
						<details>
							<summary>Títulos</summary>
							<div>
								<table class="se_table">
									<thead>
										<tr><th>Texto</th><th>HTML</th><th>Resultado</th></tr>
									</thead>
									<tbody>
										<tr><td><code># Titulo 1</code></td><td><code>&lt;h1&gt;Título 1&lt;/h1&gt;</code></td><td><h1>Título 1</h1></td></tr>
										<tr><td><code>## Titulo 2</code></td><td><code>&lt;h2&gt;Título 2&lt;/h2&gt;</code></td><td><h2>Título 2</h2></td></tr>
										<tr><td><code>### Titulo 3</code></td><td><code>&lt;h3&gt;Título 3&lt;/h3&gt;</code></td><td><h3>Título 3</h3></td></tr>
										<tr><td><code>#### Titulo 4</code></td><td><code>&lt;h4&gt;Título 4&lt;/h4&gt;</code></td><td><h4>Título 4</h4></td></tr>
										<tr><td><code>##### Titulo 5</code></td><td><code>&lt;h5&gt;Título 5&lt;/h5&gt;</code></td><td><h5>Título 5</h5></td></tr>
										<tr><td><code>###### Titulo 6</code></td><td><code>&lt;h6&gt;Título 6&lt;/h6&gt;</code></td><td><h6>Título 6</h6></td></tr>
									</tbody>
								</table>
							</div>
						</details>
						<details>
							<summary>Bloques de texto</summary>
							<div>
								Formato
								<code>&gt; texto</code>
								Resultado
								<blockquote>texto</blockquote>
							</div>
						</details>
						<details>
							<summary>Lineas horizontales</summary>
							<div>
								<div>Formato</div>
								<code>texto<br/>---<br/>texto</code>
								<div>Resultado</div>
								<blockquote>texto<hr>texto</blockquote>
							</div>
						</details>
						<details>
							<summary>Listas</summary>
							<div>
								<div>Para listas ordenadas se ponen números, pero el valor del número no importa.</div>
								<table class="se_table border">
									<thead>
										<tr><th>Texto</th><th>HTML</th><th>Resultado</th></tr>
									</thead>
									<tbody>
										<tr>
											<td><code>1. Primer item<br />2. Segundo item<br />3. Tercer item<br />4. Cuarto item</code></td>
											<td><code>&lt;ol&gt;<br />&lt;li&gt;Primer item&lt;/li&gt;<br /> &lt;li&gt;Segundo item&lt;/li&gt;<br /> &lt;li&gt;Tercer item&lt;/li&gt;<br /> &lt;li&gt;Cuarto item&lt;/li&gt;<br /> &lt;/ol&gt;</code></td>
											<td><ol><li>Primer item</li><li>Segundo item</li><li>Tercer item</li><li>Cuarto item</li></ol></td>
										</tr>
										<tr>
											<td><code>1. Primer item<br />1. Segundo item<br />1. Tercer item<br />1. Cuarto item</code></td>
											<td><code>&lt;ol&gt;<br />&lt;li&gt;Primer item&lt;/li&gt;<br /> &lt;li&gt;Segundo item&lt;/li&gt;<br /> &lt;li&gt;Tercer item&lt;/li&gt;<br /> &lt;li&gt;Cuarto item&lt;/li&gt;<br /> &lt;/ol&gt;</code></td>
											<td><ol><li>Primer item</li><li>Segundo item</li><li>Tercer item</li><li>Cuarto item</li></ol></td>
										</tr>
									</tbody>
								</table>
								<div>En listas ordendas se pueden usar los siguientes caracteres</div>
								<table class="se_table border">
									<thead>
										<tr><th>Texto</th><th>HTML</th><th>Resultado</th></tr>
									</thead>
									<tbody>
										<tr>
											<td><code>- Primer item<br />- Segundo item<br />- Tercer item<br />- Cuarto item</code></td>
											<td><code>&lt;ul&gt;<br />&lt;li&gt;Primer item&lt;/li&gt;<br /> &lt;li&gt;Segundo item&lt;/li&gt;<br /> &lt;li&gt;Tercer item&lt;/li&gt;<br /> &lt;li&gt;Cuarto item&lt;/li&gt;<br /> &lt;/ul&gt;</code></td>
											<td><ul><li>Primer item</li><li>Segundo item</li><li>Tercer item</li><li>Cuarto item</li></ul></td>
										</tr>
										<tr>
											<td><code>* Primer item<br />* Segundo item<br />* Tercer item<br />* Cuarto item</code></td>
											<td><code>&lt;ul&gt;<br />&lt;li&gt;Primer item&lt;/li&gt;<br /> &lt;li&gt;Segundo item&lt;/li&gt;<br /> &lt;li&gt;Tercer item&lt;/li&gt;<br /> &lt;li&gt;Cuarto item&lt;/li&gt;<br /> &lt;/ul&gt;</code></td>
											<td><ul><li>Primer item</li><li>Segundo item</li><li>Tercer item</li><li>Cuarto item</li></ul></td>
										</tr>
										<tr>
											<td><code>+ Primer item<br />+ Segundo item<br />+ Tercer item<br />+ Cuarto item</code></td>
											<td><code>&lt;ul&gt;<br />&lt;li&gt;Primer item&lt;/li&gt;<br /> &lt;li&gt;Segundo item&lt;/li&gt;<br /> &lt;li&gt;Tercer item&lt;/li&gt;<br /> &lt;li&gt;Cuarto item&lt;/li&gt;<br /> &lt;/ul&gt;</code></td>
											<td><ul><li>Primer item</li><li>Segundo item</li><li>Tercer item</li><li>Cuarto item</li></ul></td>
										</tr>
									</tbody>
								</table>
							</div>
						</details>
						<details>
							<summary>Tablas</summary>
							<div>
								<div>Insertar tablas tiene el siguiente formato.</div>
								<pre>
| Cabeza 1 | Cabeza 2 |
| --- | --- |
| Cuerpo 1 | Cuerpo 2 |
| Cuerpo 1.1 | Cuerpo 2.1 |
								</pre>
							</div>
						</details>
					</div>
				</div>
			</div>
		</se-simple-tab>
	</div>
</site-adm-pages-markdownform>
HTML;
		//
	}

}