<?php
//
namespace snkeng\core\general;

//
class dynamicTable
{
	public static function createElement($an_struct, $setProperties)
	{
		// ADD JS module
		\snkeng\core\engine\nav::pageFileModuleAdd('core', '', '/components-simple/se-dynamic-table.mjs');
		\snkeng\core\engine\nav::pageFileModuleAdd('core', '', '/components-simple/se-async-content.mjs');

		//

		// Initial conditions
		$defaultData = [
			'printStruct' => 'any',
			'preProcessFunc' => 'null',
			'printType' => '',
			'js_id' => 'asyncDataContainer',
			'js_url' => '',
			'limit' => null
		];
		//
		$cSettings = array_merge($defaultData, $setProperties);

		//
		$jsOps = [
			'scrollLoad' => 'true',
			'lim' => []
		];

		$objectContent = [
			'container' => '',
			'body' => '',
			'content' => '',
		];

		// Adjust limits
		if ( !empty($cSettings['postInclude']) ) {
			$jsOps['lim'] = array_merge($cSettings['postInclude'], $jsOps['lim']);
		}
		elseif ( !empty($cSettings['limit']) ) {
			$jsOps['lim'] = $cSettings['limit'];
		}
		$jsOps['lim'] = \json_encode($jsOps['lim'], JSON_UNESCAPED_UNICODE);
		$jsOps['s_id'] = ( isset($jsOps['s_id']) ) ? $jsOps['s_id'] : '';

		//
		$cSettings['actions'] = $cSettings['actions'] ?? '{}';
		$cSettings['jsModule'] = $cSettings['jsModule'] ?? '';
		$cSettings['js_defaults'] = ( !empty($cSettings['js_defaults']) ) ? $cSettings['js_defaults'] : '{}';

		// Navegación
		$navigation = '';
		if ( isset($cSettings['settings']['nav']) && $cSettings['settings']['nav'] ) {
			//
			$navigation .= <<<HTML
	<div class="pages">Mostrando:
		<span data-count="eIni">0</span> - <span data-count="eEnd">0</span> de
		<span data-count="eTot">0</span>
		<meta data-count="eCur" content="0" />
	</div>
	<div class="navs">
		<button class="tabBtn" data-dyntab-onclick="navigation" data-action="reload">&#x21BA;</button>
		<button class="tabBtn" data-dyntab-onclick="navigation" data-action="first">&#xAB;</button>
		<button class="tabBtn" data-dyntab-onclick="navigation" data-action="prev">&#x2039;</button>
		<button class="tabBtn" data-dyntab-onclick="navigation" data-action="next">&#x203A;</button>
		<button class="tabBtn" data-dyntab-onclick="navigation" data-action="last">&#xBB;</button>
		<input type="number" data-count="pCur" value="1" size="1" min="1" data-dyntab-onclick="cpage" /> /
		<span data-count="pTot">0</span>
	</div>\n
HTML;
		}

		// Formulario de cabecera
		$filter_advanced = '';
		$cFilterValue = '';
		if ( !empty($cSettings['adv_filter']) ) {
			//
			foreach ( $cSettings['adv_filter'] as $cFilterName => $cFilterProps ) {
				$cFilterHTML = '';

				//
				switch ( $cFilterProps['type'] ) {
					//
					case 'idGroup':
						$cFilterHTML = <<<HTML
<input name="{$cFilterName}" value="" />
HTML;
						break;
					//
					case 'dbList':
						$cFilterHTML = \snkeng\core\engine\mysql::printSimpleQuery($cFilterProps['sql'], '<option value="!id;">!title;</option>');
						$cFilterHTML = <<<HTML
<select name="{$cFilterName}"><option value="">--</option>{$cFilterHTML}</select>
HTML;
						break;
					//
					case 'dQuery':
						$cFilterHTML = <<<HTML
<input type="search" name="{$cFilterName}" data-url="{$cFilterProps['url']}" />
<div se-elem="optionList"></div>
HTML;
						break;
					//
					default:
						se_killWithError('ADV FILTER ERROR0', $cFilterProps);
						break;
				}

				//
				$filter_advanced .= <<<HTML
<label data-type="{$cFilterProps['type']}" data-filtered="0">
<div class="cont"><span class="title">{$cFilterProps['title']}</span><span class="desc"></span></div>
{$cFilterHTML}
<div class="cFilterValue"><span>{$cFilterValue}</span><button type="button" se-act="filter_remove"><svg class="icon inline ml"><use xlink:href="#fa-remove" /></svg></button></div>
</label>
HTML;
			}
			//
			$filter_advanced = <<<HTML
<div se-elem="adv_filter">
<dialog style="min-width:300px; min-height:400px;">
	<div class="se_tab vertical gOMB" se-plugin="simpleTab">
		<div class="tabs" role="tablist">
			<div>Filtros</div>
			<div>Orden</div>
		</div>
		<div class="contents" se-elem="panelGroup">
			<div class="advFilter">
				<h2>Filtros</h2>
				<div class="se_form">
				{$filter_advanced}
				</div>
			</div>
			<div>
				<h2>Orden</h2>
			</div>
		</div>
	</div>
	<button se-act="adv_filter_close" class="btn blue wide"><svg class="icon inline mr"><use xlink:href="#fa-remove" /></svg>Cerrar</button>
</dialog>
<button class="btn" se-act="adv_filter_open"><svg class="icon inline mr"><use xlink:href="#fa-filter" /></svg>Filtro avanzado</button>
</div>
HTML;
			//
		}

		// Modificar clase
		$objectContentClass = ( isset($cSettings['contentClass']) ) ? ' ' . $cSettings['contentClass'] : '';

		// Modificar clase
		$tableStyle = 'inline-block';
		if ( isset($cSettings['tableWide']) && $cSettings['tableWide'] ) {
			$tableStyle = 'block';
			$objectContentClass .= ' wide';
		}

		if ( $cSettings['printType'] === 'table' ) {
			$tabRow = 'tr';
			$tabBCell = 'td';
			$tabHCell = 'th';
		}
		else {
			$tabRow = 'div';
			$tabBCell = 'span';
			$tabHCell = 'span';
		}

		// Crear elementos de la cabeza
		$headTitles = '';
		$headFilters = '';
		$headSecondary = '';

		//
		if ( isset($cSettings['tableHeadAbove']) && !empty($cSettings['tableHeadAbove']) ) {
			$tempTitles = '';
			foreach ( $cSettings['tableHeadAbove'] as $elem ) {
				$tempTitles .= "<{$tabHCell} colspan='{$elem['span']}'>{$elem['name']}</{$tabHCell}>";
			}
			$headSecondary = "<{$tabRow}>{$tempTitles}</{$tabRow}>\n";
		}

		// Filters operations
		$hasFilters = false;
		foreach ( $cSettings['tableHead'] as $elem ) {
			// Head content
			$sortMod = '';
			if ( !empty($elem['sort']) ) {
				$sortMod = " class='sortable' data-name='{$elem['fName']}' data-sort=''";
			}
			$headTitles .= "<{$tabHCell}{$sortMod}>{$elem['name']}</{$tabHCell}>";

			// Filters
			if ( true ) {
				if ( !empty($elem['filter']) ) {
					$filter = '';
					switch ( $elem['filter'] ) {
						case 1:
						case 'text':
							$filter = '<input type="text" name="' . $elem['fName'] . '" />';
							$hasFilters = true;
							break;
						case 'list':
							$optText = '';
							foreach ( $elem['data'] as $val => $name ) {
								$optText .= "<option value='$val'>$name</option>";
							}
							$filter = "<select name='{$elem['fName']}'><option value=''>--</option>{$optText}</select>";
							$hasFilters = true;
							break;
						case 'checkbox':
							$filter = "<input type='checkbox' name='{$elem['fName']}' value='1' />";
							$hasFilters = true;
							break;
						case 'dateinterval':
							$filter = "<input type='date' name='{$elem['fName']}_ini' /> - <input type='date' name='{$elem['fName']}_end' />";
							$hasFilters = true;
							break;
						default:
							$filter = 'Filtro no definido';
							break;
					}
					$headFilters .= "<{$tabBCell}>{$filter}</{$tabBCell}>";
				}
				else {
					$headFilters .= "<{$tabBCell}></{$tabBCell}>";
				}
			}
		}

		//
		if ( !empty($hasFilters) ) {
			$headFilters = "<{$tabRow} class='filters' data-element='filter_basic'>" . $headFilters . "</{$tabRow}>\n";
		}

		// Crear
		if ( $cSettings['printType'] === 'table' ) {
			$colGroup = '';
			$first = true;
			if ( isset($cSettings['colGroup']) ) {
				foreach ( $cSettings['colGroup'] as $cGroup ) {
					$cgSpan = ( empty($cGroup['span']) ) ? 1 : $cGroup['span'];
					//
					$colGroup .= ( $first ) ? '' : "\n";
					$colGroup .= "<col span='{$cgSpan}' class='{$cGroup['class']}' />";
					$first = false;
				}
				$colGroup = "<colgroup>{$colGroup}</colgroup>";
			}


			//
			$objectContent['container'] = '<tr><td colspan="' . count($cSettings['tableHead']) . '">%s</td></tr>';

			//
			$objectContent['body'] .= <<<HTML
<div style="display:{$tableStyle}">

<div class="topMenu">
	<div class="filterAdvanced" data-element="filterAdvanced">{$filter_advanced}</div>
	<div class="topNavigation" data-element="topNavigation">{$navigation}</div>
</div>

<div se-ajaxelem="response"></div>

<se-async-content class="tableContainer" url="{$cSettings['js_url']}" istable="true">
<table class="se_table alternate selectable border{$objectContentClass}">
{$colGroup}
<template>{$an_struct}</template>
<thead>
{$headSecondary}{$headFilters}<tr class="titles" se-elem="sort">{$headTitles}</tr>
</thead>
<tbody data-type="container">
{$objectContent['content']}
</tbody>
</table>
</se-async-content>

</div>\n
HTML;
			//
		}
		else {
			//
			$objectContent['container'] = '<span><span>%s</span></span>';
			$objectContent['body'] .= <<<HTML
<!-- INI:autoTable:mod -->
<div style="display:{$tableStyle}">

<div class="topMenu">
<div class="filterAdvanced" data-element="filterAdvanced">{$filter_advanced}</div>
<div se-elem="navBar" class="topNavigation" data-element="topNavigation">{$navigation}</div>
</div>

<div se-ajaxelem="response"></div>

<se-async-content class="tableContainer" url="{$cSettings['js_url']}" istable="true">
<div class="se_table alternate selectable border{$objectContentClass}">
<template>{$an_struct}</template>
<div class="thead">
{$headSecondary}{$headFilters}<div class="titles" se-elem="sort">{$headTitles}</div>
</div>
<div class="tbody" data-type="container">
{$objectContent['content']}
</div>
</div>
</se-async-content>

</div>
<!-- END:autoTable:mod -->\n
HTML;
			//
		}

		$env = ( $_ENV['SE_DEBUG'] ) ? 1 : 0;

		//
		return <<<HTML
<!-- INI:DynamicTable -->
<se-dynamic-table class="se_ajaxSimple">
<script se-elem="defaults" type="application/json" >
{
	"jsonUrl":"{$cSettings['js_url']}",
	"act_defaults":{$cSettings['js_defaults']},
	"lim":{$jsOps['lim']},
	"s_id":"{$jsOps['s_id']}",
	"scroll_load":{$jsOps['scrollLoad']},
	"container":'{$objectContent['container']}',
	"actions":{$cSettings['actions']},
	"preProcessFunc":{$cSettings['preProcessFunc']}
}
</script>
<script type="module" data-debug="{$env}">
{$cSettings['jsModule']}
</script>
<!-- INI:Content -->
{$objectContent['body']}
<!-- END:Content -->
</se-dynamic-table>
<!-- END:DynamicTable -->\n
HTML;
		//
	}
}
