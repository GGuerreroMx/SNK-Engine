<?php
//
namespace snkeng\core\general;

//
class asyncDataJson
{
	//
	public static function printJson($an_qry, $method = 'GET')
	{

		//
		if ( empty($an_qry) ) {
			die("mjson - empty qry");
		}

		//
		$dVars = ( $method === 'GET' ) ? $_GET : $_POST;

		$dVars['isNew'] = ( isset($dVars['isNew']) && $dVars['isNew'] === 'true' );

		//
		$dVars['pCur'] = ( !empty($dVars['pCur']) ) ? intval($dVars['pCur']) : 0;

		$data = [
			'eIni' => 1,
			'eEnd' => 0,
			'eLim' => $an_qry['lim'],
			'eCur' => 0,
			'eTot' => 0,
			'pCur' => $dVars['pCur'],
			'pTot' => 1,
			'isNew' => $dVars['isNew']
		];

		// Crear query
		$sql_qry = self::parseSQL($an_qry, $dVars, false);

		//
		$response = [];

		$an_qry['toSimple'] = ( isset($an_qry['toSimple']) ) ? $an_qry['toSimple'] : [];

		// Get information
		$response['d'] = \snkeng\core\engine\mysql::returnArray($sql_qry, $an_qry['toSimple']);

		//
		$data['eCur'] += count($response['d']);

		// Adjust count
		$data['eIni'] = ( $data['pCur'] * $data['eLim'] ) + 1;
		$data['eEnd'] = $data['eIni'] + $data['eCur'] - 1;

		//
		if ( $data['isNew'] ) {
			// Obtener la cuenta
			$data['eTot'] = \snkeng\core\engine\mysql::singleNumber('SELECT FOUND_ROWS();');
			$data['pTot'] = ceil($data['eTot'] / $data['eLim']);
		}


		// Debug
		if ( $_ENV['SE_DEBUG'] ) {
			$response['debug']['query'] = $sql_qry;
		}

		// Reporte
		$response['s'] = ['t' => 1, 'date' => date('H:i:s')];
		$response['s']['time'] = time();
		//
		$response['p'] = $data;

		//
		\snkeng\core\engine\nav::printJSON($response);
	}

	//
	public static function parseSQL($sql, $dVars, $getCount)
	{

		//	WHERE
		$cLim = ['lim' => 'true'];
		$where = '';
		$first = true;
		if ( !empty($sql['where']) ) {
			foreach ( $sql['where'] as $varName => $lim ) {
				$cWhere = '';
				$val = null;

				// Determinar variable y origen
				if ( isset($lim['set']) && strlen($lim['set']) > 0 ) {
					// Variables que sólo se pueden definir (no permite alteraciones)
					// $val = ( $lim['vtype'] === 'int' || $lim['vtype'] === 'date' || $lim['stype'] === 'between' ) ? $lim['set'] : "'{$lim['set']}'";
					$val = $lim['set'];
					$cLim[$varName] = $val;
				}
				elseif ( isset($dVars['where'][$varName]) ) {
					// Modificadas por el query
					$val = $dVars['where'][$varName];
					$cLim[$varName] = $val;
				}
				elseif ( isset($lim['def']) && !empty($lim['def']) ) {
					// Por defecto
					$val = $lim['def'];
				}

				// echo("SET: S:{$lim['stype']}. T:{$lim['vtype']}. V:{$val}.\n");
				//
				if ( ( !empty($val) && strlen($val) > 0 ) || ( isset($lim['stype']) && $lim['stype'] === 'set' ) ) {
					//
					if ( !isset($lim['stype']) ) {
						echo( "ERROR EN DEFINICIÓN DE WHERE:\nNO STYPE IN ARRAY: '{$varName}'.\n\n" );
						debugVariable($sql['where']);
						se_killWithError('ERROR EN DEFINICIÓN', "where no definido: '{$lim['stype']}'. En {$lim['name']}");
					}

					// Operación según tipo
					switch ( $lim['stype'] ) {
						case 'eq':
						case 'diff':
						case 'lt':
						case 'lte':
						case 'gt':
						case 'gte':
							switch ( $lim['vtype'] ) {
								//
								case 'int':
									$val = intval($val);
									break;
								//
								case 'str':
									$val = '\'' . $val . '\'';
									break;
								//
								case 'date':
									$tVal = strtoupper($val);
									$val = \snkeng\core\engine\mysql::real_escape_string($val);
									$val = ( substr($tVal, 0, 4) === "DATE" || substr($tVal, 0, 3) === "NOW" ) ? $val : "'{$val}'";
									break;
								//
								case 'bool':
									$val = intval($val);
									break;
								//
								default:
									$val = "'" . \snkeng\core\engine\mysql::real_escape_string($val) . "'";
									break;
							}
							$comp = '';
							switch ( $lim['stype'] ) {
								case 'eq':
									$comp = '=';
									break;
								case 'diff':
									$comp = '!=';
									break;
								case 'lt':
									$comp = '<';
									break;
								case 'lte':
									$comp = '<=';
									break;
								case 'gt':
									$comp = '>';
									break;
								case 'gte':
									$comp = '>=';
									break;
							}
							$cWhere = $lim['db'] . $comp . $val;
							break;
						case 'between':
							$cWhere = $lim['db'] . ' BETWEEN ' . $val;
							break;
						case 'like':
							$cWhere = "{$lim['db']} LIKE '%{$val}%'";
							break;
						case 'in':
							$cWhere = "{$lim['db']} IN ({$val})";
							break;
						case 'match':
							$cWhere = "MATCH({$lim['db']}) AGAINST('{$val}')"; // ' added previously
							break;
						case 'substitute':
							$cWhere = str_replace('%s', $val, $lim['db']);
							break;
						case 'set':
							$cWhere = $lim['db'];
							break;
						default:
							// debugVariable($sql['where'], '');
							se_killWithError("manua_ajaxjson: where no definido: '{$lim['stype']}'. En {$lim['name']}");
							break;
					}
				}

				//
				if ( !empty($cWhere) ) {
					$where .= ( $first ) ? '' : ' AND ';
					$where .= $cWhere;
					$first = false;
				}
			}
		}

		//
		//	HAVING
		$having = '';
		$first = true;
		if ( !empty($sql['having']) ) {
			foreach ( $sql['having'] as $lim ) {
				$cHaving = '';
				$val = null;
				// Determinar si la variable existe
				if ( isset($dVars['having'][$varName]) ) {
					$val = $dVars['having'][$varName];
					$cLim[$varName] = $val;
				}
				elseif ( isset($lim['def']) && !empty($lim['def']) ) {
					$val = $lim['def'];
				}

				//
				if ( !empty($val) ) {
					// Operación según tipo
					switch ( $lim['stype'] ) {
						case 'eq':
						case 'lt':
						case 'lte':
						case 'gt':
						case 'gte':
							switch ( $lim['vtype'] ) {
								case 'int':
									$val = intval($val);
									break;
								case 'str':
									break;
								case 'date':
									$val = ( $val !== 'NOW()' && $val !== 'DATE(NOW())' ) ? "'" . \snkeng\core\engine\mysql::real_escape_string($val) . "'" : $val;
									break;
								default:
									$val = "'" . \snkeng\core\engine\mysql::real_escape_string($val) . "'";
									break;
							}
							switch ( $lim['stype'] ) {
								case 'eq':
									$comp = '=';
									break;
								case 'lt':
									$comp = '<';
									break;
								case 'lte':
									$comp = '<=';
									break;
								case 'gt':
									$comp = '>';
									break;
								case 'gte':
									$comp = '>=';
									break;
							}
							$cHaving = $lim['db'] . $comp . $val;
							break;
						case 'between':
							$cHaving = $lim['db'] . ' BETWEEN ' . $val;
							break;
						case 'like':
							$cHaving = "{$lim['db']} LIKE '%{$val}%'";
							break;
						case 'in':
							$cHaving = "{$lim['db']} IN ({$val})";
							break;
						case 'match':
							$cHaving = "MATCH({$lim['db']}) AGAINST('{$val}')";
							break;
						case 'substitute':
							$cHaving = str_replace('%s', $val, $lim['db']);
							break;
						default:
							debugVariable($sql['having']);
							die('manua_ajaxjson: limnotdef' . $lim['stype']);
							break;
					}
				}

				//
				if ( !empty($cHaving) ) {
					$having .= ( $first ) ? '' : ' AND ';
					$having .= $cHaving;
					$first = false;
				}
			}
		}

		//
		//	ORDER
		$order = '';
		$first = true;
		if ( !empty($sql['order']) ) {
			$orderArray = [];
			//
			if ( !empty($dvars['sort']) && $dVars['sort'] !== 'null' ) {
				$orderArray = $dVars['sort'];
			}
			elseif ( !empty($sql['default_order']) ) {
				$orderArray = $sql['default_order'];
			}

			//
			if ( !empty($orderArray) ) {
				foreach ( $orderArray as $cOrder ) {
					if ( isset($sql['order'][$cOrder[0]]) ) {
						$order .= ( $first ) ? '' : ', ';
						$order .= $sql['order'][$cOrder[0]]['db'] . ' ';
						$order .= ( $cOrder[1] === 'DESC' ) ? 'DESC' : 'ASC';
						$first = false;
					}
				}
			}
		}
		//
		// Ajuste por página
		$pageCurrent = ( isset($dVars['pCur']) ) ? intval($dVars['pCur']) : 0;
		if ( $pageCurrent !== 0 ) {
			$dataIni = ( $pageCurrent * $sql['lim'] );
		}
		else {
			$dataIni = 0;
		}

		// Found rows support


		// Escribir SQL
		$sql_qry = "SELECT ";
		if ( ( isset($dVars['isNew']) && $dVars['isNew'] ) || $getCount ) {
			$sql_qry .= "SQL_CALC_FOUND_ROWS \n";
		}
		$sql_qry .= "{$sql['sel']}\nFROM {$sql['from']}";
		if ( !empty($where) ) {
			$sql_qry .= "\nWHERE {$where}";
		}
		if ( !empty($sql['group']) ) {
			$sql_qry .= "\nGROUP BY {$sql['group']}";
		}
		if ( !empty($having) ) {
			$sql_qry .= "\nHAVING {$having}";
		}
		if ( !empty($order) ) {
			$sql_qry .= "\nORDER BY {$order}";
		}
		$sql_qry .= "\nLIMIT {$dataIni}, {$sql['lim']};";

		// debugVariable($sql_qry);
		return $sql_qry;
	}
}
//
