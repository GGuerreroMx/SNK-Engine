<?php
//
namespace snkeng\core\general;

//
class time
{
	public static function timeToSecs($text) {
		$curTime = explode(':', $text);

		//
		switch ( count($curTime) ) {
			case 1:
				$time = intval($curTime[0]);
				break;
			case 2:
				$time = intval($curTime[0]) * 60 + intval($curTime[1]);
				break;
			case 3:
				$time = intval($curTime[0]) * 3600 + intval($curTime[1]) * 60 + intval($curTime[2]);
				break;
			default:
				die("tiempo no válido.");
				break;
		}

		//
		return $time;
	}
}