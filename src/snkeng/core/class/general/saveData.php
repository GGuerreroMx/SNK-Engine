<?php
//
namespace snkeng\core\general;

// Save data
class saveData
{
	//<editor-fold desc="Texto básicas">

	// proteger array
	public static function protectData($sendArray)
	{
		$rArray = [];
		foreach ( $sendArray as $x => $y ) {
			$rArray[$x] = trim(\snkeng\core\engine\mysql::real_escape_string($y));
		}
		return $rArray;
	}

	// Suponiendo que ya están protegidas, quitar los htmls
	// Neat trim modificado para ser más estricto
	public static function htmlProtect($str, $n, $delim = '...')
	{
		$str = htmlspecialchars($str);
		if ( strlen($str) > $n ) {
			preg_match('/(.{' . ($n - 3) . '}.*?)\b/', $str, $matches);
			$str = rtrim($matches[1]) . $delim;
		}
		return $str;
	}

	// En el caso de agarrar las variables directamente del GET o POST
	public static function saveNeatTrim($str, $n, $delim = '...')
	{
		$str = \snkeng\core\engine\mysql::real_escape_string($str);
		if ( strlen($str) > $n ) {
			preg_match('/(.{' . ($n - 3) . '}.*?)\b/', $str, $matches);
			$str = rtrim($matches[1]) . $delim;
		}
		return $str;
	}

	//
	public static function neatTrim2($str, $n, $delim = '...')
	{
		if ( strlen($str) > $n ) {
			preg_match('/(.{' . ($n - 3) . '}.*?)\b/', $str, $matches);
			$str = rtrim($matches[1]) . $delim;
		}
		return $str;
	}

	//
	public static function neatTrimSimple($str, $n, $delim = '...')
	{
		if ( strlen($str) > $n ) {
			preg_match('/(.{' . ($n - 3) . '}.*?)\b/', $str, $matches);
			$str = rtrim($matches[1]) . $delim;
		}
		return $str;
	}

	// Trim bello
	public static function neatTrim($str, $n, $delim = '...')
	{
		$len = strlen($str);
		if ( $len > $n ) {
			preg_match('/(.{' . $n . '}.*?)\b/', $str, $matches);
			return rtrim($matches[1]) . $delim;
		} else {
			return $str;
		}
	}

	// Remover acentos
	public static function removeAccents($txt)
	{
		$normalizeChars = [
			'Á' => 'A', 'À' => 'A', 'Â' => 'A', 'Ã' => 'A', 'Å' => 'A', 'Ä' => 'A',
			'á' => 'a', 'à' => 'a', 'â' => 'a', 'ã' => 'a', 'å' => 'a', 'ä' => 'a',
			'É' => 'E', 'È' => 'E', 'Ê' => 'E', 'Ë' => 'E',
			'é' => 'e', 'è' => 'e', 'ê' => 'e', 'ë' => 'e',
			'Í' => 'I', 'Ì' => 'I', 'Î' => 'I', 'Ï' => 'I',
			'í' => 'i', 'ì' => 'i', 'î' => 'i', 'ï' => 'i',
			'Ó' => 'O', 'Ò' => 'O', 'Ô' => 'O', 'Õ' => 'O', 'Ö' => 'O', 'Ø' => 'O',
			'ó' => 'o', 'ò' => 'o', 'ô' => 'o', 'õ' => 'o', 'ö' => 'o', 'ø' => 'o',
			'Ú' => 'U', 'Ù' => 'U', 'Û' => 'U', 'Ü' => 'U',
			'ú' => 'u', 'ù' => 'u', 'û' => 'u', 'ü' => 'u',
			'Ç' => 'C', 'ç' => 'c',
			'Ñ' => 'N', 'ñ' => 'n',
			'Ý' => 'Y', 'ý' => 'y', 'ÿ' => 'y',
			'Æ' => 'AE', 'æ' => 'ae', 'ß' => 'sz', 'þ' => 'thorn', 'Ð' => 'Eth', 'ð' => 'eth'
		];
		$txt = strtr($txt, $normalizeChars);
		return $txt;
	}

	// Texto para título
	public static function buildFriendlyTitle($text)
	{
		// Operaciones sobre el nombre del archivo. Quitar Acentos, hacer minúsculas y quitar caracteres extraños
		$text = self::removeAccents($text);
		$text = strtolower($text);
		$text = preg_replace("/[-_+\s]/", "-", $text);
		$text = preg_replace("/[-]+/", "-", $text);
		$text = preg_replace("/[^a-z0-9-]/", "", $text);
		return $text;
	}

	// Limpiar texto
	public static function textCleaning($text, $regexp)
	{
		require __DIR__ . '/../variable/regexp.php';
		// Carga de variables
		global $regexpVarsClean;
		if ( empty($regexpVarsClean[$regexp]) ) {
			echo("Falta: {$regexp}\n\n\n");
			debugVariable($regexpVarsClean);
		}
		$text = preg_replace($regexpVarsClean[$regexp], '', $text);
		return $text;
	}

	//</editor-fold>

	//<editor-fold desc="Texto avanzado">

	// Operaciones de forma etendida para hacer las operaciones
	public static function textOperations($txt, $len, $strictCut, $delim, $reduceSpaces, $utfDecode, $htmlProtect, $sqlProtect, $regexCut = '', $addSlashes = false)
	{
		//
		$txt = trim($txt);
		$debug = false;

		// Debug
		if ( $debug ) {
			echo("-\nINI Debug:\n" . $txt . "\n");
		}

		// Quotes
		if ( function_exists('get_magic_quotes_gpc') && get_magic_quotes_gpc() ) {
			$txt = stripslashes($txt);
			if ( $debug ) {
				echo("-\nQuotes:\n" . $txt . "\n");
			}
		}

		// Remover caracteres no válidos
		if ( !empty($regexCut) ) {
			$txt = self::textCleaning($txt, $regexCut);
			if ( $debug ) {
				echo("-\nRegex:\n" . $txt . "\n");
			}
		}

		// Remover más de dos espaciados
		if ( $reduceSpaces ) {
			$search = array("/\r*/", "/\t+/", "/ +/", "/\n{2,}/");
			$replace = array("", " ", " ", "\n\n");

			$txt = preg_replace($search, $replace, $txt);
			if ( $debug ) {
				echo("-\nReduceSpaces:\n:" . $txt . "\n");
			}
		}

		// Quitar caracteres HTML
		if ( $htmlProtect ) {
			$txt = htmlspecialchars($txt, (1 | 2), 'UTF-8');
			if ( $debug ) {
				echo("-\nHTMLSpecialChars:\n" . $txt . "\n");
			}
		}

		// Soporte UTF8
		if ( $utfDecode ) {
			// $txt = $txt;
			if ( $debug ) {
				echo("-\nUTF8:\n" . $txt . "\n");
			}
		}

		// Protección SQL
		if ( $addSlashes ) {
			$txt = addslashes($txt);
			if ( $debug ) {
				echo("-\nAddSlashes:\n" . $txt . "\n");
			}
		}

		// Protección SQL
		if ( $sqlProtect ) {
			$txt = \snkeng\core\engine\mysql::real_escape_string($txt);
			if ( $debug ) {
				echo("-\nSQL - Escape:\n" . $txt . "\n");
			}
		}
		// Hacer funciones delim
		if ( $len !== 0 ) {
			$txtLen = strlen($txt);
			if ( $txtLen > $len ) {
				if ( $strictCut ) {
					$txt = substr($txt, 0, $len);
				} else {
					$rLen = 0;
					if ( empty($delim) ) {
						$delim = '...';
					} else {
						$rLen = strlen($delim);
					}
					preg_match('/(.{' . ($len - $rLen) . '}.*?)\b/', $txt, $matches);
					$txt = rtrim($matches[1]) . $delim;
				}
			}
			if ( $debug ) {
				echo("-\nLim Cut:\n" . $txt . "\n");
			}
		}

		// Revisar que el último caracter no sea "\"
		$txt = rtrim($txt, "\\");

		return $txt;
	}

	// Función reducida para aplicar las operaciones de texto
	// Por lo general ya sufrieron el SQL Protect
	public static function textOpSimple($text, $len = 500, $type = 'ajaxFormText')
	{
		switch ( $type ) {
			case 'html':
				$text = self::textOperations($text, $len, true, '', false, false, false, true, '', false);
				break;
			case 'simpleTitle':
				$text = self::textOperations($text, $len, false, '...', true, true, true, true, 'simpleText', false);
				break;
			case 'simpleText':
				$text = self::textOperations($text, $len, true, '...', true, true, true, true, 'simpleText', false);
				break;
			case 'urlTitle':
				$text = self::textOperations($text, $len, true, '', false, false, true, true, 'urlTitle', false);
				break;
			case 'email':
				$text = self::textOperations($text, $len, true, '', false, false, true, true, 'email', false);
				break;
			case 'ajaxFormText':
				$text = self::textOperations($text, $len, false, '...', true, true, true, true, '', false);
				break;
			default:
				$text = self::textOperations($text, $len, false, '...', true, true, false, true, '', false);
				break;
		}
		return $text;
	}

	// Función para la evaluación del texto
	public static function textCheck($text, $minL, $maxL, $regexp)
	{
		// Carga de variables
		global $regexpVars;
		$strLen = strlen($text);
		$errorNum = 0;

		// Hacer las pruebas
		if ( $errorNum === 0 && empty($text) ) {
			$errorNum = 1;
		}
		if ( $errorNum === 0 && $minL !== 0 && $strLen < $minL ) {
			$errorNum = 2;
		}
		if ( $errorNum === 0 && $maxL !== 0 && $strLen > $maxL ) {
			$errorNum = 3;
		}
		if ( $errorNum === 0 && !empty($regexp) && !empty($regexpVars[$regexp]) ) {
			$regexpTxt = $regexpVars[$regexp];
			if ( !empty($regexpTxt) && !preg_match($regexpVars[$regexp], $text) ) {
				$errorNum = 4;
			}
		}

		//
		return $errorNum;
	}

	// Construir un error
	public static function buildError($fName, $text, $minL, $maxL, $regexp)
	{
		$error = '';
		$errorNum = self::textCheck($text, $minL, $maxL, $regexp);
		switch ( $errorNum ) {
			case 0:
				break;
			case 1:
				$error = "Requerido.";
				break;
			case 2:
				$error = "El texto debe ser mínimo de {$minL} caracteres.";
				break;
			case 3:
				$error = "El texto debe ser máximo de {$maxL} caracteres.";
				break;
			case 4:
				global $regexpVars;
				$error = "El texto posee caracteres no válidos. - {$text} - {$regexpVars[$regexp]}";
				break;
		}
		if ( !empty($error) ) {
			$error = "{$fName}: {$error}";
		}
		return $error;
	}

	/**
	 * Procesar información de POST
	 *
	 * @param array $response Modifiable array which contains return information about condition.
	 * @param array $data Information to create the query
	 *
	 * @return array The processed information
	 */
	public static function postParsing(&$response, $data)
	{
		//
		if ( $_SERVER['CONTENT_TYPE'] === 'application/json' ) {
			// Read body and convert to json.
			$dataRecieved = json_decode(file_get_contents('php://input'), true);
		} else {
			$dataRecieved = &$_POST;
		}

		//
		if ( empty($data) ) {
			\snkeng\core\engine\nav::invalidPage('No data', 400);
		}

		//
		$rData = [];
		$eCount = 0;
		$errors = [];
		$errorText = '';
		foreach ( $data as $varName => $elem ) {
			//
			$val = null;
			//
			switch ( $elem['type'] ) {
				//
				case 'int':
					$val = (isset($dataRecieved[$varName])) ? $dataRecieved[$varName] : '';
					$elem['max'] = (isset($elem['max'])) ? $elem['max'] : 0;
					$elem['min'] = (isset($elem['min'])) ? $elem['min'] : 0;


					if ( isset($elem['validate']) && $elem['validate'] ) {
						if ( strlen($val) === 0 ) {
							$errors[] = $elem['name'] . ' no tiene valor.';
							$eCount++;
						} elseif ( isset($elem['notNull']) && $elem['notNull'] && intval($val) === 0 ) {
							$errors[] = $elem['name'] . ' tiene un valor de cero.';
							$eCount++;
						} elseif ( $elem['max'] && intval($val) > $elem['max'] ) {
							$errors[] = $elem['name'] . "es mayor a {$elem['max']}.";
							$eCount++;
						} elseif ( $elem['min'] && intval($val) < $elem['min'] ) {
							$errors[] = $elem['name'] . "es menor a {$elem['max']}.";
							$eCount++;
						}
					}
					$val = intval($val);
					break;
				//
				case 'float':
					$val = (isset($dataRecieved[$varName])) ? $dataRecieved[$varName] : '';
					if ( isset($elem['validate']) && $elem['validate'] ) {
						if ( strlen($val) === 0 ) {
							$errors[] = $elem['name'] . ' no tiene valor.';
							$eCount++;
						} elseif ( isset($elem['notNull']) && $elem['notNull'] && floatval($val) === 0 ) {
							$errors[] = $elem['name'] . ' tiene un valor de cero.';
							$eCount++;
						}
					}
					$val = floatval($val);
					break;
				//
				case 'bool':
					$val = (isset($dataRecieved[$varName]) && $dataRecieved[$varName] !== 'false' && boolval($dataRecieved[$varName])) ? 1 : 0;
					break;
				//
				case 'color':
					$val = (isset($dataRecieved[$varName])) ? strtoupper(preg_replace("/[^A-Za-z0-9 ]/", '', $dataRecieved[$varName])) : '';
					break;
				//
				case 'url':
					$val = (isset($dataRecieved[$varName])) ? trim($dataRecieved[$varName]) : '';
					//
					if ( isset($elem['validate']) && $elem['validate'] ) {
						if ( !filter_var($val, FILTER_VALIDATE_URL) ) {
							$errors[] = $elem['name'] . ' url no válido.';
							$eCount++;
							$val = null;
						}
					}
					break;
				//
				case 'email':
					$val = (isset($dataRecieved[$varName])) ? trim($dataRecieved[$varName]) : '';
					//
					if ( isset($elem['validate']) && $elem['validate'] ) {
						if ( !filter_var($val, FILTER_VALIDATE_EMAIL) ) {
							$errors[] = $elem['name'] . ' url no válido.';
							$eCount++;
							$val = null;
						}
					}
					break;
				//
				case 'moneyInt':
					$val = (isset($dataRecieved[$varName])) ? $dataRecieved[$varName] : '';
					if ( isset($elem['validate']) && $elem['validate'] ) {
						if ( strlen($val) === 0 ) {
							$errors[] = $elem['name'] . ' no tiene valor.';
							$eCount++;
						} elseif ( isset($elem['notNull']) && $elem['notNull'] && floatval($val) === 0 ) {
							$errors[] = $elem['name'] . ' tiene un valor de cero.';
							$eCount++;
						}
					}
					$val = (!empty($val)) ? intval(floatval($val) * 100) : 0;
					break;
				//
				case 'datetime':
					$val = (isset($dataRecieved[$varName])) ? $dataRecieved[$varName] : '';
					if ( !empty($val) ) {
						$dt = \DateTime::createFromFormat("Y-m-d H:i:s", $val);
						if ( !($dt !== false && !array_sum($dt->getLastErrors())) ) {
							$errors[] = $elem['name'] . " formato no válido. ({$val})";
							$eCount++;
							$val = null;
							break;
						}
						$val = $dt->format('Y-m-d H:i:s');
					} elseif ( isset($elem['validate']) && $elem['validate'] ) {
						$errors[] = $elem['name'] . ' no tiene valor.';
						$eCount++;
					}
					break;
				//
				case 'date':
					$val = (isset($dataRecieved[$varName])) ? $dataRecieved[$varName] : '';
					if ( !empty($val) ) {
						$dt = \DateTime::createFromFormat("Y-m-d", $val);
						if ( !($dt !== false && !array_sum($dt->getLastErrors())) ) {
							$errors[] = $elem['name'] . ' formato no válido.';
							$eCount++;
							$val = null;
							break;
						}
						$val = $dt->format('Y-m-d');
					} elseif ( isset($elem['validate']) && $elem['validate'] ) {
						$errors[] = $elem['name'] . ' no tiene valor.';
						$eCount++;
					}
					break;
				//
				case 'time':
					$val = (isset($dataRecieved[$varName])) ? $dataRecieved[$varName] : '';
					if ( !empty($val) ) {
						if ( !preg_match('/^([01]?[0-9]|2[0-3]):[0-5][0-9](:[0-5][0-9])?$/', $val) ) {
							$errors[] = $elem['name'] . ' formato no válido.';
							$eCount++;
							$val = null;
							break;
						}
					} elseif ( isset($elem['validate']) && $elem['validate'] ) {
						$errors[] = $elem['name'] . ' no tiene valor.';
						$eCount++;
					}
					break;
				//
				case 'json':
					$val = (isset($dataRecieved[$varName])) ? \json_decode($dataRecieved[$varName], true) : [];
					break;
				//
				case 'array':
					$val = (isset($dataRecieved[$varName])) ? $dataRecieved[$varName] : [];
					break;
				//
				case 'numberList':
					$val = (isset($dataRecieved[$varName])) ? $dataRecieved[$varName] : '';
					$val = explode(',', $val);
					if ( $val ) {
						for ( $i = 0; $i < sizeof($val); $i++ ) {
							$val[$i] = intval($val[$i]);
						}
					}
					break;

				//
				case 'file':
					$val = null;
					if ( $elem['validate'] && !isset($_FILES[$varName]) ) {
						$errors[] = $elem['name'] . ' no subido.';
						$eCount++;
					}

					//
					if ( isset($_FILES[$varName]) ) {
						$val = $_FILES[$varName];
						$val['fName'] = strtolower(pathinfo($val['name'], PATHINFO_FILENAME));
						$val['fType'] = strtolower(pathinfo($val['name'], PATHINFO_EXTENSION));
						$val['fSize'] = round($val['size'] / 1024, 2);
						//
						if ( $elem['validate'] ) {
							if ( !empty($elem['fType']) && !in_array($val['fType'], $elem['fType']) ) {
								$fileFormats = implode(',', $elem['fType']);
								$errors[] = "Formato de archivo no válido. Soportados: {$fileFormats}. Recibido:{$val['fType']}.";
								$eCount++;
							} elseif ( !empty($elem['mSize']) && $elem['mSize'] < $val['fSize'] ) {
								$errors[] = "Archivo excede el tamaño máximo. (Máximo: {$elem['mSize']} KBs, Recibido: {$val['fSize']} KBs.)";
								$eCount++;
							} elseif ( empty($val['tmp_name']) ) {
								$errors[] = "Parece haber un problema con el archivo y no fue posible subirlo, por favor intente de nuevo.";
								$eCount++;
							}
						}
					}

					break;

				//
				case 'files':
					$cError = false;
					$totalFiles = count($_FILES[$varName]['name']);

					// Re arreglo del array
					$val = [];
					$file_keys = array_keys($_FILES[$varName]);
					for ( $i = 0; $i < $totalFiles; $i++ ) {
						foreach ( $file_keys as $key ) {
							$val[$i][$key] = $_FILES[$varName][$key][$i];
						}
					}

					//
					for ( $i = 0; $i < $totalFiles; $i++ ) {
						$cFile = $val[$i];
						$cFile['fName'] = strtolower(pathinfo($cFile['name'], PATHINFO_FILENAME));
						$cFile['fType'] = strtolower(pathinfo($cFile['name'], PATHINFO_EXTENSION));
						$cFile['fSize'] = round($cFile['size'] / 1024, 2);

						//
						if ( $elem['validate'] ) {
							//
							if ( empty($cFile['fName']) ) {
								$errors[] = $elem['name'] . ' no subido.';
								$eCount++;
								$cError = true;
							}
							if ( !$cError && !empty($elem['fType']) && !in_array($cFile['fType'], $elem['fType']) ) {
								$fileFormats = implode(',', $elem['fType']);
								$errors[] = "Formato de archivo no válido. Soportados: {$fileFormats}. Recibido:{$cFile['fType']}.";
								$eCount++;
								$cError = true;
							}
							if ( !$cError && !empty($elem['mSize']) && $elem['mSize'] < $cFile['fSize'] ) {
								$errors[] = "Archivo excede el tamaño máximo. (Máximo: {$elem['mSize']} KBs, Recibido: {$cFile['fSize']} KBs.)";
								$eCount++;
								$cError = true;
							}
						}
					}
					break;

				// No mysql protecion USE WITH CAUTION
				case 'straightStr':
					$val = (isset($dataRecieved[$varName])) ? trim($dataRecieved[$varName]) : '';
					// Ajustes
					$elem['lMax'] = (isset($elem['lMax'])) ? $elem['lMax'] : 0;
					$elem['lMin'] = (isset($elem['lMin'])) ? $elem['lMin'] : 0;
					$elem['filter'] = (isset($elem['filter'])) ? $elem['filter'] : '';

					if ( isset($elem['validate']) && $elem['validate'] ) {
						$error = self::buildError($elem['name'], $val, $elem['lMin'], $elem['lMax'], $elem['filter']);
						if ( !empty($error) ) {
							$errors[] = $error;
							$eCount++;
						}
					}
					break;

				//
				default:
					$val = (isset($dataRecieved[$varName])) ? trim($dataRecieved[$varName]) : '';
					// Ajustes
					$elem['lMax'] = (isset($elem['lMax'])) ? $elem['lMax'] : 0;
					$elem['lMin'] = (isset($elem['lMin'])) ? $elem['lMin'] : 0;
					$elem['filter'] = (isset($elem['filter'])) ? $elem['filter'] : '';
					//
					if ( isset($elem['process']) && $elem['process'] ) {
						$val = self::textOpSimple($val, $elem['lMax'], $elem['filter']);
					} else {
						$val = \snkeng\core\engine\mysql::real_escape_string($val);
					}
					if ( isset($elem['validate']) && $elem['validate'] ) {
						$error = self::buildError($elem['name'], $val, $elem['lMin'], $elem['lMax'], $elem['filter']);
						if ( !empty($error) ) {
							$errors[] = $error;
							$eCount++;
						}
					}
					break;
			}

			// Agregar
			$rData[$varName] = $val;
		}
		//
		if ( $eCount !== 0 ) {
			foreach ( $errors as $e ) {
				$errorText .= "<li>{$e}</li>\n";
			}
			$response['debug']['parsing'] = ['eC' => $eCount, 'ex' => $errors];
			$response['s']['t'] = 0;
			$response['s']['d'] = 'Datos no válidos';
			$response['s']['ex'] = "<ul>\n$errorText</ul>";

			// Exit all actions, return error
			se_killWithError('Datos no válidos', "<ul>\n$errorText</ul>", 400);
		}
		return $rData;
	}

	/**
	 * File check and moving
	 *
	 * @param array $file Current file information (location, temp name).
	 * @param array $fileData Information about the file to upload (size) returned by function
	 * @param string $fileName New name of the file
	 * @param string $path System file path where file will be saved
	 * @param integer $fileSizeMax Maximum file size supported
	 * @param array $fileTypes Suported file types
	 *
	 * @return boolean If file move operation has been successful
	 */
	public static function fileManager($file, &$fileData, $fileName, $path, $fileSizeMax = 0, $fileTypes = [])
	{
		if ( empty($file) ) {
			$fileData['error'] = "Falta archivo";
			return false;
		}

		//
		$fileData['ext'] = strtolower(pathinfo($file['name'], PATHINFO_EXTENSION));
		$fileData['size'] = filesize($file['tmp_name']);
		$fileData['name'] = $fileName . '.' . $fileData['ext'];
		$fileData['path'] = $path . $fileData['name'];

		// debugVariable($file);
		if ( !empty($fileTypes) && !in_array($fileData['ext'], $fileTypes) ) {
			$fileFormats = implode(',', $fileTypes);
			$fileData['error'] = "Formato de archivo no válido. ({$fileFormats}: {$fileData['ext']})";
			return false;
		}

		//
		if ( $fileSizeMax !== 0 && $fileData['size'] > $fileSizeMax ) {
			$fileSize = round($fileData['size'] / 1024, 2);
			$fileSizeMaxTxt = round($fileSizeMax / 1024, 2);
			$fileData['error'] = "Archivo mayor a {$fileSizeMaxTxt}KBs. ({$fileSize})";
			return false;
		}

		// File
		if ( empty($fileData['error']) ) {
			// Revisar directorio
			if ( !is_dir($_SERVER['DOCUMENT_ROOT'] . $path) ) {
				mkdir($_SERVER['DOCUMENT_ROOT'] . $path);
			}

			// Mover archivo
			if ( !move_uploaded_file($file["tmp_name"], $_SERVER['DOCUMENT_ROOT'] . $fileData['path']) ) {
				$fileData['error'] = "El archivo no pudo ser guardado en el directorio.";
				return false;
			}
		}

		// Success!
		return true;
	}

	//</editor-fold>

	//<editor-fold desc="SQL básico">

	/**
	 * Insert information to table
	 *
	 * @param array $response Modifiable array which contains return information about condition.
	 * @param array $params Information to create the query
	 * @param array $pData ID to read.
	 * @param array $rData ID to read.
	 */
	public static function sql_table_addRow(array &$response, array $params, array $pData, array $rData = null) : void
	{
		$tableElems = '';
		$tableValues = '';
		$first = true;

		// Parse
		if ( !empty($params['elems']) ) {
			foreach ( $params['elems'] as $dbName => $vName ) {
				$tableElems .= ($first) ? '' : ', ';
				$tableValues .= ($first) ? '' : ', ';
				$tableElems .= $dbName;
				if ( gettype($pData[$vName]) === 'array' ) {
					$tableValues .= "'" . \snkeng\core\engine\mysql::real_escape_string(json_encode($pData[$vName], JSON_UNESCAPED_SLASHES | JSON_UNESCAPED_UNICODE)) . "'";
				} else {
					$tableValues .= (strlen($pData[$vName]) > 0) ? "'{$pData[$vName]}'" : 'NULL';
				}
				$response['d'][$vName] = $pData[$vName];
				$first = false;
			}
		} else {
			se_killWithError('SAVEDATA ERROR', 'No se detectan elementos en el query.');
		}

		// Fecha de adición
		if ( !empty($params['dtAdd']) ) {
			$tableElems .= ', ' . $params['dtAdd'];
			$tableValues .= ', NOW()';
			$response['d']['dtAdd'] = date('Y-m-d H:i:s');
		}

		// Next ID
		$response['d'][$params['tId']['nm']] = \snkeng\core\engine\mysql::getNextId($params['tName']);

		// Extra return (evitar empty bug)
		if ( $rData ) {
			foreach ( $rData as $name => $value ) {
				$response['d'][$name] = $value;
			}
		}

		// Procesar
		$ins_qry = "INSERT INTO {$params['tName']} ({$tableElems}) VALUES ({$tableValues});";

		// Debug
		$response['debug']['qry_add'] = $ins_qry;
		//
		\snkeng\core\engine\mysql::submitQuery($ins_qry, [
			'errorKey' => 'saveDataAddRow',
			'errorDesc' => 'No fue posible actualizar la información.'
		]);

		//
		$response['s']['d'] = 'Agregado';
	}

	/**
	 * Update row information
	 *
	 * @param array $response Modifiable array which contains return information about condition.
	 * @param array $params Information to create the query
	 * @param integer $idVal ID to read.
	 */
	public static function sql_table_updRow(&$response, $params, $pData)
	{
		$updString = '';
		$first = true;

		// Parse
		foreach ( $params['elems'] as $dbName => $vName ) {
			$updString .= ($first) ? '' : ', ';
			$updString .= "{$dbName}=";
			if ( gettype($pData[$vName]) === 'array' ) {
				$updString .= "'" . \snkeng\core\engine\mysql::real_escape_string(json_encode($pData[$vName], JSON_UNESCAPED_SLASHES | JSON_UNESCAPED_UNICODE)) . "'";
			} else {
				$updString .= (strlen($pData[$vName]) > 0) ? "'{$pData[$vName]}'" : 'NULL';
			}
			$response['d'][$vName] = $pData[$vName];
			$first = false;
		}

		// Fecha de modificación
		if ( !empty($params['dtMod']) ) {
			$updString .= ", {$params['dtMod']}=NOW()";
		}

		// ID
		$response['d'][$params['tId']['nm']] = $pData[$params['tId']['nm']];

		// Procesar
		$ins_qry = "UPDATE {$params['tName']} SET {$updString} WHERE {$params['tId']['db']}={$pData[$params['tId']['nm']]} LIMIT 1;";

		// Debug
		$response['debug']['qry_upd'] = $ins_qry;

		//
		\snkeng\core\engine\mysql::submitQuery($ins_qry, [
			'errorKey' => 'saveDataUpdRow',
			'errorDesc' => 'No fue posible actualizar la información.'
		]);

		//
		$response['s']['d'] = 'Actualizado';
	}

	/**
	 * Read a table row, based in the same structure of all other operations
	 *
	 * @param array $response Modifiable array which contains return information about condition.
	 * @param array $params Information to create the query
	 * @param integer $idVal ID to read.
	 */
	public static function sql_table_readRow(&$response, $params, $idVal)
	{
		//
		$sql_qry = "SELECT\n{$params['tId']['db']} AS {$params['tId']['nm']}";
		foreach ( $params['elems'] as $dbName => $vName ) {
			$sql_qry .= ", {$dbName} AS {$vName}";
		}
		$sql_qry .= "\nFROM {$params['tName']}\nWHERE {$params['tId']['db']}='{$idVal}'\nLIMIT 1;";

		//
		$toSimple = (isset($params['toSimple'])) ? $params['toSimple'] : [];

		//
		$response['d'] = \snkeng\core\engine\mysql::singleRowAssoc($sql_qry, $toSimple,
			[
				'errorKey' => 'tableReadRow',
				'errorDesc' => 'No fue posible leer la información.'
			]
		);

		// Debug
		$response['debug']['qry_read'] = $sql_qry;
	}

	/**
	 * Combines validation and inserting operation.
	 *
	 * @param array $response Modifiable array which contains return information about condition.
	 * @param array $params Information to create the query
	 * @param string $idName ID to delete.
	 */
	public static function sql_table_delRow(array &$response, array $params, string $idName = 'id')
	{
		//
		if ( empty($idName) ) {
			se_killWithError('Nombre del ID no válido.', '');
		}

		$idValue = intval($_POST[$idName]);
		//
		if ( empty($idValue) ) {
			se_killWithError('Valor del ID no válido.', '');
		}

		//
		$del_qry = "DELETE FROM {$params['tName']} WHERE {$params['tId']['db']}={$idValue} LIMIT 1;";

		// Debug
		$response['debug']['del_qry'] = $del_qry;
		\snkeng\core\engine\mysql::submitQuery($del_qry,
			[
				'errorKey' => '',
				'errorDesc' => ''
			]
		);

		//
		$response['s']['d'] = 'Borrado';
	}
	//</editor-fold>

	//<editor-fold desc="SQL Avanzado">

	/**
	 * Combines validation and inserting operation.
	 *
	 * @param array $response Modifiable array which contains return information about condition.
	 * @param array $postStructure Structure of elements to be filtered and processed
	 * @param array $sqlStructure SQL Structure for update
	 * @param array $params Adicional optinal parameters
	 */
	public static function sql_complex_rowAdd(array &$response, array $postStructure, array $sqlStructure, array $params = [])
	{
		//
		$defaults = [
			'setData' => [],
			'opsData' => null,
			'readData' => []
		];
		$settings = array_merge($defaults, $params);

		// Parse post variables
		$rData = self::postParsing($response, $postStructure);

		// Add defaults if any
		if ( !empty($settings['setData']) ) {
			$rData = array_merge($rData, $settings['setData']);
		}

		// Extra operations
		if ( is_callable($settings['opsData']) ) {
			$settings['opsData']($rData);
		}

		//
		self::sql_table_addRow($response, $sqlStructure, $rData);

		// Maybe Read?
		if ( !empty($settings['readData']) ) {
			self::sql_table_readRow($response, $settings['readData'], $response['d'][$sqlStructure['tId']['nm']]);
		}

		return $rData;
	}

	/**
	 * Combines validation and updating operation.
	 *
	 * @param array $response Modifiable array which contains return information about condition.
	 * @param array $postStructure Structure of elements to be filtered and processed
	 * @param array $sqlStructure SQL Structure for update
	 * @param array $params Adicional optinal parameters
	 */
	public static function sql_complex_rowUpd(array &$response, array $postStructure, array $sqlStructure, array $params = [])
	{
		//
		$defaults = [
			'setData' => [],
			'opsData' => null,
			'returnData' => null,
			'readData' => [],
		];

		$settings = array_merge($defaults, $params);

		// Post parsing
		$rData = self::postParsing($response, $postStructure);

		// Add defaults
		if ( !empty($settings['setData']) ) {
			$rData = array_merge($rData, $settings['setData']);
		}

		// Extra operations
		if ( is_callable($settings['opsData']) ) {
			$settings['opsData']($rData);
		}

		// Self check?
		if ( isset($sqlStructure['pId']) ) {
			self::sql_safe_check($response, $rData, $sqlStructure);
		}

		// Actualizar tabla
		self::sql_table_updRow($response, $sqlStructure, $rData);

		// Maybe Read?
		if ( !empty($settings['readData']) ) {
			self::sql_table_readRow($response, $settings['readData'], $response['d'][$sqlStructure['tId']['nm']]);
		}

		// Extra operations
		if ( is_callable($settings['returnData']) ) {
			$settings['returnData']($rData);
		}

		return $rData;
	}

	//
	public static function multiPostParsing(&$response, $postStructure)
	{
		//
		if ( $_SERVER['CONTENT_TYPE'] === 'application/json' ) {
			// Read body and convert to json.
			$allData = json_decode(file_get_contents('php://input'), true);
		} else {
			$allData = \json_decode($_POST['data'], true);
		}

		//
		// Procesar cada dato
		$eCount = 0;
		$errors = [];
		$errorText = '';
		foreach ( $allData as $id => &$data ) {
			if ( intval($id) ) {
				foreach ( $postStructure as $name => $elem ) {
					if ( $elem['type'] === 'int' ) {
						$data[$name] = (isset($data[$name])) ? intval($data[$name]) : 0;
						if ( isset($elem['validate']) && $elem['validate'] && $data[$name] === 0 ) {
							$errors[] = $elem['name'] . ' tiene valor nulo.';
							$eCount++;
						}
					} elseif ( $elem['type'] === 'float' ) {
						$data[$name] = floatval($data[$name]);
						if ( $elem['validate'] && $data[$name] === 0 ) {
							$errors[] = $elem['name'] . ' tiene valor nulo.';
							$eCount++;
						}
					} elseif ( $elem['type'] === 'bool' ) {
						$data[$name] = (isset($data[$name]) && boolval($data[$name])) ? 1 : 0;
					} elseif ( $elem['type'] === 'color' ) {
						$data[$name] = (isset($data[$name])) ? strtoupper(preg_replace("/[^A-Za-z0-9 ]/", '', $data[$name])) : '';
					} elseif ( $elem['type'] === 'json' ) {
						$data[$name] = (isset($data[$name])) ? \json_decode($data[$name], true) : [];
					} elseif ( $elem['type'] === 'array' ) {
						$data[$name] = (isset($data[$name])) ? $data[$name] : [];
					} elseif ( $elem['type'] === 'moneyInt' ) {
						$data[$name] = (isset($data[$name])) ? intval(floatval($data[$name]) * 100) : 0;
					} else {
						$data[$name] = trim($data[$name]);
						// Ajustes
						$elem['lMin'] = (isset($elem['lMin'])) ? $elem['lMin'] : 0;
						$elem['lMax'] = (isset($elem['lMax'])) ? $elem['lMax'] : 0;
						$elem['filter'] = (isset($elem['filter'])) ? $elem['filter'] : '';
						//
						if ( $elem['process'] ) {
							$data[$name] = self::textOpSimple($data[$name], $elem['lMax'], $elem['filter']);
						}


						if ( $elem['validate'] ) {
							$error = self::buildError($elem['name'], $data[$name], $elem['lMin'], $elem['lMax'], $elem['filter']);
							if ( !empty($error) ) {
								$errors[] = $error;
								$eCount++;
							}
						}
					}
				}
			}
		}
		//
		// Resultados del proceso
		if ( $eCount !== 0 ) {
			foreach ( $errors as $e ) {
				$errorText .= "<li>{$e}</li>\n";
			}
			$response['debug']['parsing'] = ['eC' => $eCount, 'ex' => $errors];
			$response['s']['t'] = 0;
			$response['s']['d'] = 'Datos no válidos';
			$response['s']['ex'] = "<ul>\n$errorText</ul>";
		}
		//
		return $allData;
	}

	// Actualizar múltiples
	public static function sql_complex_rowUpdMulti(&$response, $postStructure, $sqlStructure, $callback = null)
	{
		$allData = self::multiPostParsing($response, $postStructure);
		// debugVariable($allData);
		//
		// Generar guardado
		$error = '';
		$ins_qry = '';
		//
		// Construir n strings
		foreach ( $allData as $id => $cData ) {
			$first = true;
			$updString = '';
			// Parse por cada elemento
			foreach ( $sqlStructure['elems'] as $dbName => $vName ) {
				$updString .= ($first) ? '' : ', ';
				$updString .= "{$dbName}='{$cData[$vName]}'";
				$first = false;
			}

			// Fecha de modificación
			if ( !empty($sqlStructure['dtMod']) ) {
				$updString .= ", {$sqlStructure['dtMod']}=now()";
			}

			// Procesar
			$ins_qry .= "UPDATE {$sqlStructure['tName']} SET {$updString} WHERE {$sqlStructure['tId']['db']}={$id} LIMIT 1;\n";
		}

		//
		if ( empty($ins_qry) ) {
			se_killWithError('Query no existente');
		}

		//
		// debugVariable($ins_qry);

		// Ejecutar query
		\snkeng\core\engine\mysql::submitMultiQuery($ins_qry,
			[
				'errorKey' => 'asdf',
				'errorDesc' => 'No fue posible guardar la información.'
			]
		);

		// Debug
		$response['debug']['qry_upd'] = $ins_qry;

		// Resultado
		$response['s']['d'] = 'Actualizadas';

		//
		if ( is_callable($callback) ) {
			$callback();
		}
	}

	// Borrar
	public static function sql_complex_rowDel(&$response, $sqlStructure, $callback = null)
	{
		$oId = (isset($_POST[$sqlStructure['tId']['nm']])) ? intval($_POST[$sqlStructure['tId']['nm']]) : 0;

		//
		if ( empty($oId) ) {
			$response['s']['t'] = 0;
			$response['s']['d'] = 'No se obtuvo el id de la operación.';
			return;
		}

		//
		if ( isset($sqlStructure['pId']) ) {
			$pId = (isset($_POST[$sqlStructure['pId']['nm']])) ? intval($_POST[$sqlStructure['pId']['nm']]) : 0;

			// First validation
			if ( empty($pId) ) {
				$response['s']['t'] = 0;
				$response['s']['d'] = 'No se obtuvo el id del dueño.';
				return;
			}

			$rData = [
				$sqlStructure['tId']['nm'] => $oId,
				$sqlStructure['pId']['nm'] => $pId,
			];

			//
			self::sql_safe_check($response, $rData, $sqlStructure);
			if ( !$response['s']['t'] ) {
				return;
			}
		}

		// Borrar
		self::sql_table_delRow($response, $sqlStructure, $oId);
		if ( !$response['s']['t'] ) {
			return;
		}

		// Callback
		if ( is_callable($callback) ) {
			$callback();
		}
	}

	// Leer uno
	public static function sql_complex_rowRead(&$response, $sqlStructure, $postName = '')
	{
		$oId = (isset($_POST[$sqlStructure['tId']['nm']])) ? intval($_POST[$sqlStructure['tId']['nm']]) : 0;

		//
		if ( empty($oId) ) {
			$response['s']['t'] = 0;
			$response['s']['d'] = 'No se obtuvo el id de la operación.';
			return;
		}

		//
		if ( isset($sqlStructure['pId']) ) {
			$pId = (isset($_POST[$sqlStructure['pId']['nm']])) ? intval($_POST[$sqlStructure['pId']['nm']]) : 0;

			// First validation
			if ( empty($pId) ) {
				$response['s']['t'] = 0;
				$response['s']['d'] = 'No se obtuvo el id del dueño.';
				return;
			}

			$rData = [
				$sqlStructure['tId']['nm'] => $oId,
				$sqlStructure['pId']['nm'] => $pId,
			];

			//
			self::sql_safe_check($response, $rData, $sqlStructure);
			if ( !$response['s']['t'] ) {
				return;
			}
		}

		//
		self::sql_table_readRow($response, $sqlStructure, $oId);
	}

	//
	public static function sql_complex_uri_update_read(array &$response, int $objectId, array $dbStructure, array $postFilterStructure, array $params = [])
	{
		//
		switch ( $_SERVER['REQUEST_METHOD'] ) {
			case 'GET':
				//
				\snkeng\core\general\saveData::sql_table_readRow(
					$response,
					$dbStructure,
					$objectId
				);
				break;
			//
			case 'POST':
				//
				if ( !isset($params['setData']) ) {
					$params['setData'] = [];
				}

				$params['setData'][$dbStructure['tId']['nm']] = $objectId;

				//
				$response['d'] = \snkeng\core\general\saveData::sql_complex_rowUpd(
					$response,
					$postFilterStructure,
					$dbStructure,
					$params
				);
				break;
			default:
				debugVariable($_SERVER);

				\snkeng\core\engine\nav::killWithError('Invalid header', "Recieved header: {$_SERVER['HTTP_REQUEST_METHOD']}");
				break;
		}
	}


	//</editor-fold>

	//<editor-fold desc="SQL Avanzado">

	public static function sql_safe_check(&$response, $rData, $sqlStructure)
	{
		$sql_qry = "SELECT {$sqlStructure['pId']['db']} FROM {$sqlStructure['tName']} WHERE {$sqlStructure['tId']['db']}='{$rData[$sqlStructure['tId']['nm']]}';";
		$pId = \snkeng\core\engine\mysql::singleNumber($sql_qry);
		if ( $pId !== $rData[$sqlStructure['pId']['nm']] ) {
			$response['s']['t'] = 0;
			$response['s']['d'] = 'Permisos de operación insuficientes.';
			$response['debug']['authQuery'] = $sql_qry;
		}
	}

	//</editor-fold>
}
//
