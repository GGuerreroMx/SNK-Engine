<?php
//
namespace snkeng\core\socnet;

//
use function snkeng\core\socnet\se_invalidPage;

class object_read
{
	//
	public static function getObjFromName($objName) {
		//
				$data = [];

		// URL Simple
		$objNameSimple = strtolower(preg_replace('/[^a-zA-Z0-9]/', '', $objName));

		$sql_qry = <<<SQL
SELECT
	obj.a_id AS id, obj.a_id_parent AS idParent,
    obj.a_ikey AS ikey,
    obj.a_obj_name AS name, obj.a_url_name_full AS urlName,
    obj.a_type AS type,
    obj.a_img_crop AS imageSquare, obj.a_banner_crop AS imageBanner,
    obj.a_status AS statusGen,
    obj.a_status_active AS statusActive, obj.a_status_featured AS statusFeatured,
    obj.a_status_published AS statusPublished, obj.a_status_useradmin AS statusUserAdmin,
    obj.a_prop_physical AS propPhysical,
    obj.a_geopoint_alt AS geoPointAlt, obj.a_geopoint_lat AS geoPointLat, obj.a_geopoint_lng AS geoPointLng,
    obj.a_count_members AS countMembers, obj.a_count_subscribers AS countSubscribers, obj.a_count_commends AS countCommends
FROM sb_objects_obj AS obj
WHERE obj.a_url_name_simple='{$objNameSimple}'
LIMIT 1;
SQL;
		$data['obj'] = \snkeng\core\engine\mysql::singleRowAssoc($sql_qry, [
			'int' => [
				'id', 'idParent',
				'statusGen', 'statusActive', 'statusFeatured', 'statusPublished', 'statusUserAdmin',
				'propPhysical', 'countMembers', 'countSubscribers', 'countCommends'
			]
		]);
		\snkeng\core\engine\mysql::killIfError("No fue posible leer la información del objeto.",'snPermits_fullData01');

		//
		if ( empty($data['obj']) ) {
			se_invalidPage(404);
		}

		//
		$data['obj']['url'] = "/@{$data['obj']['urlName']}";

		// Relationship
		if ( empty($data['idParent']) ) {
			$data['rel'] = self::getRelationship($data['obj']['id'], $data['obj']['type']);
		} else {
			$data['rel'] = self::getRelationship($data['obj']['idParent'], 'other'); // Siempre sera diferente a 1 (usuario), ya que no hay idParents con ususarios (o no debería)
		}

		// Regresar datos
		return $data;
	}

	//
	public static function getObjFromId($objId, $objKey) {
		//
				$data = [];

		//
		$sql_qry = <<<SQL
SELECT
	obj.a_id AS id, obj.a_id_parent AS idParent,
    obj.a_ikey AS ikey,
    obj.a_obj_name AS name, obj.a_url_name_full AS urlName,
    obj.a_type AS objType,
    obj.a_img_crop AS imageSquare, obj.a_banner_crop AS imageBanner,
    obj.a_status AS statusGen,
    obj.a_status_active AS statusActive, obj.a_status_featured AS statusFeatured,
    obj.a_status_published AS statusPublished, obj.a_status_useradmin AS statusUserAdmin,
    obj.a_prop_physical AS propPhysical,
    obj.a_geopoint_alt AS geoPointAlt, obj.a_geopoint_lat AS geoPointLat, obj.a_geopoint_lng AS geoPointLng,
    obj.a_count_members AS countMembers, obj.a_count_subscribers AS countSubscribers, obj.a_count_commends AS countCommends
FROM sb_objects_obj AS obj
WHERE a_id='{$objId}'
LIMIT 1;
SQL;
		$data['obj'] = \snkeng\core\engine\mysql::singleRowAssoc($sql_qry,
			[
				'int' => [
					'id', 'idParent',
					'statusGen', 'statusActive', 'statusFeatured', 'statusPublished', 'statusUserAdmin',
					'propPhysical', 'countMembers', 'countSubscribers', 'countCommends'
				]
			],
			[
				'errorKey' => 'snPermits_fullData01',
				'errorDesc' => 'No fue posible leer la información del objeto.'
			]
		);

		//
		$data['obj']['url'] = "/@{$data['obj']['urlName']}/";

		// ID
		if ( $data['obj']['ikey'] !== $objKey ) { se_killWithError('SN: Objeto no válido.'); }

		// Relationship
		if ( empty($data['idParent']) ) {
			$data['rel'] = self::getRelationship($data['obj']['id'], $data['obj']['objType']);
		} else {
			$data['rel'] = self::getRelationship($data['obj']['idParent'], 2); // Siempre sera diferente a 1 (usuario), ya que no hay idParents con usuarios (o no debería)
		}

		// Regresar datos
		return $data;
	}

	//
	public static function getRelationship($objId, $objType) {
		//
		$rel =	[
			'id' => 0,
			'admin' => 0,
			'status' => 0,

			'isObj' => false,
			'isRelated' => false,
			//
			'blocked' => false,
			//
			'subscribed' => false,
			'commended' => false
		];

		//
		if ( !\snkeng\core\engine\login::check_loginStatus() ) {
			return $rel;
		}

		//
		$userData = \snkeng\core\engine\login::getUserData();
		// debugVariable($userData);

		// Comparar con el objeco que pretender ser
		/*
		if ( $siteVars['user']['as']['id'] === $objId )
		{
			// MISMO OBJETO
			$rel =	[
				'id' => 0,
				'adminLevel' => 1,
				'isObj' => true,
				'isRelated' => true,
				'blocked' => false,
				'subscribed' => true,
				'commended' => true
			];
			//
			if ( $objType === 1 ) {
				$rel['adminLevel'] = 1;
			} else {
				$cRel = self::userRelRead($objId, $siteVars['user']['as']['id']);
				$rel['adminLevel'] = $cRel['adminLevel'];
			}
		} else {
		*/
			// DIFERENTE OBJETO
			$cRel = self::userRelRead($objId, $userData['id']);
			//
			$rel['id'] = $cRel['id'];
			$rel['status'] = $cRel['status'];
			$rel['adminLevel'] = $cRel['adminLevel'];
			$rel['commended'] = $cRel['commended'];
			$rel['subscribed'] = $cRel['subscribed'];
			$rel['blocked'] = $cRel['blocked'];

			// El usuario conecta como usuario
			// if ( $siteVars['user']['as']['typeNum'] === 1 )
			// {
				// Proponer administración (supera los privilegios anteriores)
				if ( \snkeng\core\engine\login::check_loginLevel('sadmin') )
				{
					$rel['admin'] = 1;
					$rel['isRelated'] = true;
				}
			// }
		// }
		//
		return $rel;
	}

	//
	public static function userRelRead($objIdTarget, $objIdFrom)
	{
				// Variables
		$relData = [
			'id' => 0,
			'valid' => false,
			//
			'status' => 0,
			//
			'blocked' => 0,
			//
			'subscribed' => 0,
			'commended' => 0,
			//
			'adminLevel' => 0,
			'adminType' => '',
		];

		// Revisar
		$sql_qry = <<<SQL
SELECT
orel.orel_id AS id, orel.obj_id_target AS objIdTarget, orel.obj_id_from AS objIdFrom,
DATE(orel.orel_dt_add) AS dtAdd, DATE(orel.orel_dt_mod) AS dtMod,
orel.orel_status AS status, orel.orel_blocked AS blocked, orel.orel_subscribed AS subscribed, orel.orel_commend AS commended,
orel.orel_admin_level AS adminLevel, orel.orel_admin_type AS admType
FROM sb_objects_relationships AS orel
WHERE obj_id_target={$objIdTarget} AND obj_id_from={$objIdFrom}
LIMIT 1;
SQL;
		//
		$data = \snkeng\core\engine\mysql::singleRowAssoc($sql_qry,
			[
				'int' => ['id', 'status', 'blocked', 'subscribed', 'commended', 'adminLevel']
			],
			[
				'errorKey' => '',
				'errorDesc' => ''
			]
		);
		if ( !empty($data['id']) ) {
			$relData = $data;
			$relData['valid'] = true;
		}

		//
		return $relData;
	}

}
//
