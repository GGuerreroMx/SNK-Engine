<?php
//
namespace snkeng\core\socnet;

//
class object_properties
{
	//
	public static function se_objectPreferencesReadDirect($objId, $app, $appType, $defaults = '') {
				$sql_qry = "SELECT op_id AS id, op_value AS val FROM sb_objects_properties
				WHERE a_id={$objId} AND app_name='{$app}' AND app_type='{$appType}' LIMIT 1;";
		$data = \snkeng\core\engine\mysql::singleRowAssoc($sql_qry);
		if ( empty($data) ) {
			$ins_qry = "INSERT INTO sb_objects_properties
						(a_id, app_name, app_type, op_value)
						VALUES
						({$objId}, '{$app}', '{$appType}', '{$defaults}');";
			\snkeng\core\engine\mysql::submitQuery($ins_qry,
				[
					'errorKey' => 'SEObjPreferenceSave',
					'errorDesc' => 'Preferences could not be saved'
				]
			);


			//
			$data = [
				'id' => \snkeng\core\engine\mysql::getLastId(),
				'data' => $defaults
			];
		}
		//
		return $data;
	}
	//
	public static function readArray($objId, $app, $appType, $defaults = []) {
		
		//
		$sql_qry = <<<SQL
SELECT
	op_id AS id, op_value AS data, op_dt_mod AS dtMod
FROM sb_objects_properties
WHERE a_id={$objId} AND app_name='{$app}' AND app_type='{$appType}' LIMIT 1;
SQL;
		$data = \snkeng\core\engine\mysql::singleRowAssoc(
			$sql_qry,
			[
				'timeUnix' => ['dtMod'],
				'json' => ['data']
			],
			[
				'errorKey' => 'SEObjPreferenceSave',
				'errorDesc' => 'Preferences could not be read.'
			]
		);

		//
		if ( empty($data) ) {
			$json_data = \snkeng\core\engine\mysql::real_escape_string(json_encode($defaults, JSON_UNESCAPED_UNICODE));
			$ins_qry = <<<SQL
INSERT INTO sb_objects_properties
(a_id, app_name, app_type, op_value)
VALUES
({$objId}, '{$app}', '{$appType}', '{$json_data}');
SQL;
			//
			\snkeng\core\engine\mysql::submitQuery(
				$ins_qry,
				[
					'errorKey' => 'SEObjPreferenceSave',
					'errorDesc' => 'Preferences could not be saved'
				]
			);

			//
			$data = [
				'id' => \snkeng\core\engine\mysql::getLastId(),
				'dtMod' => time(),
				'data' => $defaults
			];
		}
		else {
			//
			foreach ( $defaults as $name => $value ) {
				$data['data'][$name] = ( isset($data['data'][$name]) ) ? $data['data'][$name] : $value;
			}
		}
		//
		return $data;
	}

	//
	public static function saveArray($userId, $app, $appType, $opVal) {
				$json_data = \snkeng\core\engine\mysql::real_escape_string(json_encode($opVal, JSON_UNESCAPED_UNICODE));
		$upd_qry = <<<SQL
UPDATE sb_objects_properties SET op_value='{$json_data}' WHERE a_id={$userId} AND app_name='{$app}' AND app_type='{$appType}' LIMIT 1;
SQL;
		\snkeng\core\engine\mysql::submitQuery($upd_qry,
			[
				'errorKey' => 'SEObjPreferenceSave',
				'errorDesc' => 'Preferences could not be updated.'
			]
		);
	}

	//
	public static function se_objectPreferencesSaveArrayById($opId, $opVal) {
				$json_data = \snkeng\core\engine\mysql::real_escape_string(json_encode($opVal, JSON_UNESCAPED_UNICODE));
		$upd_qry = "UPDATE sb_objects_properties SET op_value='{$json_data}' WHERE op_id={$opId};";

		//
		\snkeng\core\engine\mysql::submitQuery($upd_qry,
			[
				'errorKey' => 'SEObjPreferenceSave',
				'errorDesc' => 'Preferences could not be updated.'
			]
		);
	}
	//
	public static function se_objectPreferencesSaveDirect($opId, $opVal) {
				$json_data = \snkeng\core\engine\mysql::real_escape_string($opVal);
		$upd_qry = "UPDATE sb_objects_properties SET op_value='{$json_data}' WHERE op_id={$opId};";

		//
		\snkeng\core\engine\mysql::submitQuery($upd_qry,
			[
				'errorKey' => 'SEObjPreferenceSave',
				'errorDesc' => 'Preferences could not be updated.'
			]
		);
	}
}
//
