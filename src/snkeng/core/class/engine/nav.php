<?php
//
namespace snkeng\core\engine;

/**
 * Class nav
 */
class nav
{
	// Navigation
	public static string $pathStr = '';
	public static string $fragment = '';
	public static string $currentPath = '';
	public static $currentEl = '';
	//
	public static array $pathElementStruct = [];
	public static array $pathElementList = [];
	//
	public static array $paramsVars = [];
	public static array $navPath = [];

	// Async
	public static bool $async = false;
	public static string $asyncTargetName = '';

	//
	private static int $http_status_code = 200;

	// Cache
	private static int $lastModification = 0;
	private static bool $tryCacheRevision = true;
	private static bool $cachePrinted = false;
	private static string $cachePrivacy = 'public';
	private static string $cacheType = '';
	private static int $cacheTime = 86400; // 1 day

	// Template
	public static string $templateFile = '';
	public static bool $templatePrint = true;

	//
	public static bool $typeExtended = false;

	//<editor-fold desc="Navigation Basics">
	//
	public static function init(): void
	{
		$cURI = $_SERVER['REQUEST_URI'];

		// Fragments
		$hasFragment = strpos($cURI, "#");
		if ( $hasFragment ) {
			self::$fragment = substr($cURI, $hasFragment + 1);
			$cURI = substr($cURI, 0, $hasFragment);
		}

		// Get
		$hasGet = strpos($cURI, "?");
		if ( $hasGet ) {
			$cGet = substr($cURI, $hasGet + 1);
			$cURI = substr($cURI, 0, $hasGet);
			//
			parse_str($cGet, $_GET);
		}

		//
		self::$currentEl = '';

		//
		$cURI = trim($cURI, '/');
		$path_array = explode("/", $cURI);

		//
		self::$pathElementStruct = $path_array;
		self::$pathElementList = $path_array;
		self::$pathStr = (!empty(self::$pathElementList)) ? '/' . implode('/', self::$pathElementList) . '/' : '';

		//
		self::$pathElementStruct[0] = (isset(self::$pathElementStruct[0])) ? self::$pathElementStruct[0] : '';
		self::$currentEl = self::$pathElementStruct[0];

		//
		$_ENV['SE_ASYNC'] = false;

		//
		self::$typeExtended = (($_ENV['SE_DEBUG'] && !isset($_GET['compressed'])) || (!$_ENV['SE_DEBUG'] && isset($_GET['expanded'])));
	}

	//
	public static function next($advance = true)
	{
		if ( $advance ) {
			$pathVar = array_shift(self::$pathElementStruct);
			self::$currentEl = $pathVar;
			if ( !empty($pathVar) ) {
				self::$currentPath .= '/' . $pathVar;
			}
			return $pathVar;
		} else {
			return (!empty(self::$pathElementStruct[0])) ? self::$pathElementStruct[0] : null;
		}
	}

	//
	public static function append( string $value) : void
	{
		array_unshift(self::$pathElementStruct, self::$currentEl); // Return last appended
		// array_unshift(self::$pathElementList, self::$currentEl); // Return last appended
		self::$currentEl = $value;
	}

	//
	public static function navPathSet(string $name, string $url) : void {
		self::$navPath[$name] = $url;
	}
	//
	public static function navPathAdd(string | array $name, string | null $url = null) : void {
		$addPath = '/';
		if ( $url ) {
			$addPath.= $url;
		} else {
			$addPath.= self::current();
		}


		if ( gettype($name) === 'array' ) {
			foreach ($name AS $cName) {
				self::$navPath[$cName].= $addPath;
			}
		} else {
			self::$navPath[$name].= $addPath;
		}
	}

	public static function navPathGet(string $name) : string {
		return self::$navPath[$name];
	}

	//
	public static function current()
	{
		return self::$currentEl;
	}

	//
	public static function remaining($addSlashes = false) : string {
		$cPath = ( self::$pathElementStruct ) ? implode('/', self::$pathElementStruct) : '';
		if ( $addSlashes && $cPath ) {
			$cPath = "/{$cPath}/";
		}
		return $cPath;
	}

	//
	public static function setPathVariable(string $varName, $value) {
		self::$paramsVars[$varName] = $value;
	}

	//
	public static function getPathVariables() {
		return self::$paramsVars;
	}

	//
	public static function debug()
	{
		debugVariable(
			[
				'PATHSTR' => self::$pathStr,
				'CURRENT_PATH' => self::$currentPath,
				'CURRENT_ELEMENT' => self::$currentEl,
				'FRAGMENT' => self::$fragment,
				'VARS' => self::$pathElementStruct,
				'PATH' => self::$pathElementList,
			]
			, "path");
	}

	/**
	 * getParse
	 * Convert requested get parameters to an array with automatic handling of empty
	 *
	 * @param array $params multidimentional array. core array is ['name', 'type'] types are, str, int, bool
	 *
	 * @return array results from parsing
	 */
	public static function getParse(array $params)
	{
		$result = [];

		//
		foreach ( $params as $param ) {
			$tResult = (isset($_GET[$param[0]])) ? $_GET[$param[0]] : null;
			//
			switch ( $param[1] ) {
				case 'int':
					$tResult = intval($tResult);
					break;
				case 'bool':
					$tResult = boolval($tResult);
					break;
				default:
					$tResult = strval($tResult);
					break;
			}

			$result[$param[0]] = $tResult;
		}

		return $result;
	}

	//

	/**
	 * invalidMethod
	 * Automatic response to invalid page
	 */
	public static function invalidMethod()
	{
		self::killWithError(
			'Not valid method.',
			'',
			405,
			[
				'debugVar' => [
					'REQUEST' => self::$pathStr,
					'PATH' => self::$currentPath,
					'FAILED IN' => self::$currentEl
				]
			]
		);
	}


	/**
	 * inValidPage
	 * Automatic response to invalid page
	 *
	 * @param int $errorNumber type of invalid page error
	 */
	public static function invalidPage(string $extraInformation = '', int $errorNumber = 404)
	{
		self::killWithError(
			'Página no válida.',
			$extraInformation,
			$errorNumber,
			[
				'debugVar' => [
					'REQUEST' => self::$pathStr,
					'PATH' => self::$currentPath,
					'FAILED IN' => self::$currentEl
				]
			]
		);
	}

	private static bool $hasKill = false;

	//</editor-fold>

	/**
	 * killWithError
	 * - Allows to kill all the operations of current page
	 *
	 * @param string     $errTitle
	 * @param string     $errDetail
	 * @param int        $errNumber
	 * @param array|null $extraInformation
	 * @param mixed      $params
	 * @param bool       $simple (should not attempt to create a page)
	 *
	 * @return void
	 */
	public static function killWithError(
		string $errTitle, string $errDetail = '',
		int $errNumber = 400,
		array | null $extraInformation = [],
		array | null $params = [],
		bool $simple = false
	) : void
	{
		//
		self::cacheSetType('none');

		// Save to SQL database?
		if ( !empty($params['debugVar']) && !empty($params['debugToSQL']) ) {
			debugVarToSQL($errTitle, $params['debugVar']);
		}

		// Missin login operation
		if ( $errNumber === 1 ) {
			$tempUri = $_SERVER['REQUEST_URI'];
			if ( substr($tempUri, 0, 8) === "/partial" ) {
				$tempUri = substr($tempUri, strpos($tempUri, '/', 9));
			}

			//
			$_SESSION['login_redirect'] = $tempUri;
		}

		$errorDescription = '';

		// Will answer according to type of request. (either as a front end or api call)

		if ( self::$async && empty(self::$asyncTargetName) ) {
			global $response;
			//
			if ( !empty($params['debugVar']) && ($_ENV['SE_DEBUG'] || (class_exists('\snkeng\core\engine\login') && \snkeng\core\engine\login::check_loginLevel('sadmin'))) ) {
				$response['debug']['killData'] = $params['debugVar'];
			}

			//
			$json = [
				// "type" => "/errors/incorrect-user-pass",
				"timestamp" => date('Y-m-d H:i:s'),
				"title" => $errTitle,
				"detail" => $errDetail,
				"instance" =>  self::$currentPath,
				"status" => $errNumber,
			];

			$result = array_merge($extraInformation, $json);

			//
			printJSONData($result, $errNumber);
		}
		else {
			global $page;

			// Mostrar error deacuerdo a la situación
			switch ( $errNumber ) {
				//
				case 1:
					// Mysql called for template
					global $siteVars;
					header("Location: {$siteVars['site']['login_url']}", true, 307);
					exit;

				// Redirecciones
				case 301:
				case 307:
					$_SESSION['target_url'] = $_SERVER['REQUEST_URI'];
					header("Location: {$errTitle}", true, $errNumber);
					exit;

				//
				case 200:
					$errTitleDesc = "ERROR?";
					break;

				// 400 Bad Request
				case 400:
					$errTitleDesc = "BAD REQUEST";
					break;

				// No autorizado, sin credenciales
				case 401:
					$errTitleDesc = "UNAUTHORIZED";
					break;

				// No autorizado, credenciales insuficientes
				case 403:
					$errTitleDesc = "FORBIDDEN";
					break;

				// No encontrado
				case 404:
					$errTitleDesc = "NOT FOUND";
					break;

				// Gone
				case 410:
					$errTitleDesc = "GONE";
					break;

				//
				case 500:
				default:
					$errTitleDesc = "SERVER MALFUNCTION";
					break;
			}

			if ( !$page || $simple ) {
				//
				header('Content-Type: text/plain; charset=utf-8', true, $errNumber);

				echo <<<TEXT
$errNumber - $errTitleDesc
---
$errTitle
---
$errDetail
TEXT;

				//
				if ( isset($params['debugVar']) && ($_ENV['SE_DEBUG'] || (class_exists('\snkeng\core\engine\login') && \snkeng\core\engine\login::check_loginLevel('sadmin'))) ) {
					$debugVariable = debugConvertVariable($params['debugVar']);

					//
					echo <<<TEXT
\n\n\n
DEBUG:
------
VARTYPE: {$debugVariable['type']}
--
{$debugVariable['content']}
TEXT;
					//
				}

				exit();
			}
			else {
				self::$http_status_code = $errNumber;

				$page['head']['title'] = "{$errNumber} - {$errTitleDesc}";

				//
				$page['body'] = <<<HTML
<style>
#errorInfo { text-align:center; padding:3rem; }
#errorInfo .errorNumber { font-size:16rem; font-weight:bold; }
#errorInfo .errorTitle { font-size:6rem; font-weight:bold; margin-bottom:3rem; }
#errorInfo .errorInfo { font-size:3rem; font-weight:bold; }
#errorInfo .errorDesc { font-size:3rem; font-weight:bold; }
</style>
<div id="errorInfo" class="wpContent grid">
	<div class="gr_sz06 gr_ps03">
		<div class="errorNumber">{$errNumber}</div>
		<div class="errorTitle">{$errTitleDesc}</div>
		<div class="errorInfo">{$errTitle}</div>
		<div class="errorDesc">{$errDetail}</div>
	</div>
</div>\n
HTML;
				// Debug Data
				//
				if ( isset($params['debugVar']) && ($_ENV['SE_DEBUG'] || (class_exists('\snkeng\core\engine\login') && \snkeng\core\engine\login::check_loginLevel('sadmin'))) ) {
					$debugVariable = debugConvertVariable($params['debugVar']);

					//
					$page['body'] .= <<<HTML
<div class="wpContent">
	<h3>Debug information</h3>
	<div>{$debugVariable['type']}</div>
	<pre>{$debugVariable['content']}</pre>
</div>\n
HTML;
					//
				}

				if ( self::$hasKill ) {
					self::pagePrint();
				}


				self::$hasKill = true;

				// Leer template
				if ( \snkeng\core\engine\nav::$templatePrint && file_exists(\snkeng\core\engine\nav::$templateFile) ) {
					require \snkeng\core\engine\nav::$templateFile;
				}

				// Imprimir página
				self::pagePrint();
			}
		}
		//
		exit;
	}

	//<editor-fold desc="Async Ops">

	//
	public static function asyncCheck()
	{
		//
		if ( self::current() !== 'partial' ) {
			return;
		}

		//
		if ( count(self::$pathElementStruct) < 2 ) {
			se_killWithError('ASYNC BEHAVIOUR', 'Not enough arguments.');
		}

		//
		self::next();
		// Definir nombre
		self::asyncTargetSet(self::next());

		// Template behaviour
		self::$templatePrint = (self::$asyncTargetName === 'se_template_root');

		// Default remove
		self::$pathElementList = array_slice(self::$pathElementList, 2);
		self::$pathStr = (!empty(self::$pathElementList)) ? '/' . implode('/', self::$pathElementList) . '/' : '';

		//
		self::$pathElementList[0] = (isset(self::$pathElementList[0])) ? self::$pathElementList[0] : '';
		self::$currentEl = self::$pathElementList[0];
	}

	//
	public static function asyncSetTemplate($templateFile)
	{
		// Set template
		self::$templateFile = $templateFile;
	}

	//
	public static function asyncTargetSet($name)
	{
		self::$asyncTargetName = $name;
		self::$async = true;
		$_ENV['SE_ASYNC'] = true;
	}

	//
	public static function asyncTargetCheck($name)
	{
		if ( isset(self::$asyncTargetName) && self::$asyncTargetName === $name ) {
			self::pagePrint();
		}
	}

	//
	public static function asyncTemplateUpdate($file)
	{
		self::$templateFile = $file;
	}

	//
	public static function asyncTemplateUse()
	{

	}
	//</editor-fold>

	//<editor-fold desc="Cache Ops">

	//
	public static function cacheSetPrivacy($privacy)
	{
		self::$cachePrivacy = $privacy;
	}

	//
	public static function cacheSetType($type)
	{
		self::$cacheType = $type;
	}

	//
	public static function cacheSetTime($seconds_to_cache)
	{
		self::$cacheTime = $seconds_to_cache;
	}

	//
	public static function cachePrint()
	{
		//
		if ( self::$cachePrinted ) {
			return;
		}

		//
		if ( self::$cacheType === 'none' ) {
			header('Cache-Control: no-store, no-cache, must-revalidate, post-check=0, pre-check=0');
			header('Expires: Thu, 19 Nov 1981 08:52:00 GMT');
			header('Pragma: no-cache');
		} else {
			$params = '';
			//
			switch ( self::$cacheType ) {
				// Permite alojamiento local sin revisar
				case 'hard':
					$params = '';
					break;
				//
				default:
					$params = ', must-revalidate';
					break;
			}

			//
			header('Cache-Control: ' . self::$cachePrivacy . ', max-age=' . self::$cacheTime . $params);
			header('ETag: "' . md5(self::$lastModification) . '"');
			header('Last-Modified: ' . self::timeToGMT(self::$lastModification));
			header('Expires: ' . self::timeToGMT(time() + self::$cacheTime));
			header('Pragma: cache');
		}

		//
		self::$cachePrinted = true;
	}

	//
	private static function timeToGMT($timeToGMT)
	{
		return gmdate("D, d M Y H:i:s", $timeToGMT) . " GMT";
	}

	//
	public static function cacheCheckFile($file)
	{
		//
		if ( !self::$tryCacheRevision ) {
			return;
		}

		//
		self::$lastModification = max(filemtime($file), self::$lastModification);
	}

	//
	public static function cacheCheckUnixTime($unixTime) : void
	{
		//
		if ( !self::$tryCacheRevision ) {
			return;
		}

		//
		self::$lastModification = max($unixTime, self::$lastModification);
	}


	/**
	 * Add a new date to compare for cache purposes.
	 * @param string $dtLastMod
	 * @return void
	 */
	public static function cacheCheckDate(string $dtLastMod) : void
	{
		//
		if ( !self::$tryCacheRevision ) {
			return;
		}

		//
		self::$lastModification = max(strtotime($dtLastMod), self::$lastModification);
	}

	//

	/**
	 *
	 */
	public static function cacheFinalCheck()
	{
		$ifModifiedSince = (isset($_SERVER['HTTP_IF_MODIFIED_SINCE']) ? $_SERVER['HTTP_IF_MODIFIED_SINCE'] : false);

		// Revisar la plantilla
		if ( self::$templatePrint ) {
			self::cacheCheckFile(self::$templateFile);
		}

		// Force print cache, since this cache will define the true behaviour (not setted, force none)
		self::cachePrint();

		// Check if cache request exists, else print default cache
		if ( @strtotime($ifModifiedSince) === self::$lastModification ) {
			http_response_code(304);
			exit;
		}
	}
	//</editor-fold>

	//<editor-fold desc="Page variables">
	//
	public static $customBaseFile = null;

	//
	public static $pageStruct = [
		'head' => [
			'url' => '',
			'title' => '',
			'metaWord' => '',
			'metaDesc' => '',
			'extraHead' => '',
			'og' => [
				'type' => '',
				'twType' => '',
				'img' => '',
				'data' => []
			],
			'extra' => []
		],
		'body' => '',
		//
		'js' => '',
		'mt' => '',
		//
		'files' => [
			'group' => [],
			'ext' => [],
			'mjs' => [],
			'js' => [],
			'css' => [],
			'svg' => []
		]
	];

	//
	public static function pageGetStruct()
	{
		return self::$pageStruct;
	}
	//</editor-fold>

	//<editor-fold desc="Page Dinamyc Files">

	//
	/**
	 * Add a JS Module file to the request. Will adjust within function target folder.
	 *
	 * @param string $type Primary location of the MJS file. [se_site_modules, se_core_module, se_site_main, se_core_main]. Default means no processing (no advantage in automatic processing either).
	 * @param string $moduleName The module name, only applies if type is [se_site_modules, se_core_module]
	 * @param mixed  $files The file location within the corresponding file (in all 4 valid types, see documentation). If not valid type, must actually be the intended location. Must start with '/'
	 *
	 * @return void
	 */
	public static function pageFileModuleAdd(string $type, string $moduleName, mixed $files) : void
	{
		// If single file, create an array of the file
		if ( gettype($files) === 'string' ) {
			$files = [$files];
		}

		// Now add each file
		foreach ( $files as $fileLocation ) {
			$fileType = pathinfo($fileLocation, PATHINFO_EXTENSION);

			//
			$newFileLocation = ( self::$typeExtended ) ? "/snkeng" : "/res/modules";
			$newFileLocation.= "/{$type}";

			//
			if ( $type === 'site_modules' || $type === 'admin_modules' ) {
				$newFileLocation.= "/{$moduleName}";
			}

			//
			$newFileLocation.= ( self::$typeExtended ) ? "/res/modules" : "";

			//
			$newFileLocation.= $fileLocation;

			//
			self::pageAddFiles($newFileLocation, $fileType);
		}
	}

	//

	/**
	 * Adds a new SVG icon file to the list. File must be available in the proper folder or will throw an error.
	 * @param string $iconGroupName
	 * @return void
	 */
	public static function svgIconsAdd(string $iconGroupName) : void
	{
		global $page;

		$realFile = $_SERVER['DOCUMENT_ROOT'] . "/se_files/site_generated/svg-icons/final/{$iconGroupName}.svgz";

		if ( !file_exists($realFile) ) {
			self::killWithError("NAV ERROR: SVG-Icon group not found ({$iconGroupName}).", "FILE: $realFile", 500);
		}

		// Get last mod time
		$dtModUnix = filemtime($realFile);

		// Send to cache revision
		self::cacheCheckUnixTime($dtModUnix);

		//
		$dtModTxt = date("ymd-His", $dtModUnix);

		// Add directly to download list
		$page['files']['svg'][] = "/res/svg-icons/{$iconGroupName}.{$dtModTxt}.svgz";
	}

	//
	public static function pageFileGroupAdd(mixed $files) : void
	{
		self::pageAddFiles($files, 'group');
	}

	//
	public static function pageFileExternalAdd($files)
	{
		self::pageAddFiles($files, 'ext');
	}

	//
	private static function pageAddFiles(mixed $files, string $location) : void
	{
		global $page;

		//
		if ( empty($files) ) {
			return;
		}

		//
		if ( gettype($files) === 'string' ) {
			$files = [$files];
		}

		//
		foreach ( $files as $file ) {
			array_unshift($page['files'][$location], $file);
		}
	}

	//</editor-fold>

	//<editor-fold desc="Page printing">

	public static function getFilesFromFile(string $fileName) : array
	{
		$fileList = file($_SERVER['DOCUMENT_ROOT'] . $fileName);
		$nFileList = [];
		foreach ( $fileList as $cFile ) {
			$nFileList[] = $cFile;
		}

		return $nFileList;
	}

	//

	/**
	 * Create the file group according to the current environment (dev or prod)
	 * @return void
	 */
	public static function pageFileGroup()
	{
		global $page;
		$jsonFile = $_SERVER['DOCUMENT_ROOT'] . '/se_files/site_generated/bundles/files.json';
		if ( !file_exists($jsonFile) ) {
			\snkeng\core\engine\nav::killWithError("SCRIPT FILES MISSING (INSTALLATION ERROR)");
		}
		$files = \json_decode(file_get_contents($jsonFile), true);

		//
		$mjsArr = [];
		$jsArr = [];
		$cssArr = [];

		//
		if ( empty($files) ) {
			\snkeng\core\engine\nav::killWithError('No hay archivos script de grupo (error).');
		}

		// debugVariable($page['files']['group']);
		// debugVariable($files);

		//
		foreach ( $page['files']['group'] as $cGroup ) {
			//
			if ( !isset($files[$cGroup]) ) {
				//
				if ( $_ENV['SE_DEBUG'] ) {
					echo "File Group ERROR: not defined ({$cGroup}).\n\n";
					debugVariable($files, "Defined system files", true);
				} else {
					se_killWithError("File Group ERROR: not defined ({$cGroup})", '', 500);
				}
			}

			//
			$cGroupObject = $files[$cGroup];

			//
			if ( self::$typeExtended && $cGroupObject['data']['isFolder'] ) {
				// Read all folder (test environment)
				foreach ( scandir($_SERVER['DOCUMENT_ROOT'] . $cGroupObject['data']['origin']) as $cFileEl ) {
					// Skip non files
					if ( !is_file($_SERVER['DOCUMENT_ROOT'] . $cGroupObject['data']['origin'] . $cFileEl) ) {
						continue;
					}

					//
					$array = explode('.', $cFileEl);
					$fileExt = strtolower(end($array));

					//
					switch ( $fileExt ) {
						//
						case 'mjs':
							$mjsArr[] = $cGroupObject['data']['origin'] . $cFileEl;
							break;
						//
						case 'js':
							$jsArr[] = $cGroupObject['data']['origin'] . $cFileEl;
							break;
						//
						case 'css':
							$cssArr[] = $cGroupObject['data']['origin'] . $cFileEl;
							break;
						//
						case 'svg':
						case 'svgz':
							$page['files']['svg'][] = $cGroupObject['data']['origin'] . $cFileEl;
							break;
						//
						case 'txt':
							// Order is important, files in txt are placed before any other and respecting placing order. (mainly for css)
							switch ( $cFileEl ) {
								case 'css.txt':
									$fileList = self::getFilesFromFile($cGroupObject['data']['origin'] . $cFileEl);
									$cssArr = array_merge($cssArr, $fileList);
									break;
								case 'js.txt':
									$fileList = self::getFilesFromFile($cGroupObject['data']['origin'] . $cFileEl);
									$jsArr = array_merge($jsArr, $fileList);
									break;
							}
							break;
					}
				}
			}
			else {
				// Get files described by json
				if ( empty($cGroupObject['files']) ) { break; }
				foreach ( $cGroupObject['files'] as $fileType => $cFile ) {
					//
					switch ( $fileType ) {
						//
						case 'modules':
							foreach ( $cFile as $curFile ) {
								switch ( $curFile[0] ) {
									case 'css':
										$cssArr[] = ( self::$typeExtended ) ? $curFile[1] : $curFile[2];
										break;
									case 'mjs':
										$mjsArr[] = ( self::$typeExtended ) ? $curFile[1] : $curFile[2];
										break;
								}
							}
							break;
						//
						case 'mjs':
							if ( !$cFile['count'] ) { continue 2; }
							if ( self::$typeExtended ) {
								foreach ( $cFile['files'] as $cFiles ) {
									$mjsArr[] = $cFiles;
								}
							} else {
								$mjsArr[] = $cFile['fName'];
								self::cacheCheckUnixTime($cFile['utime']);
							}
							break;
						//
						case 'js':
							if ( !$cFile['count'] ) { continue 2; }
							if ( self::$typeExtended ) {
								foreach ( $cFile['files'] as $cFiles ) {
									$jsArr[] = $cFiles;
								}
							} else {
								$jsArr[] = $cFile['fName'];
								self::cacheCheckUnixTime($cFile['utime']);
							}
							break;
						//
						case 'css':
							if ( !$cFile['count'] ) { continue 2; }
							if ( self::$typeExtended ) {
								foreach ( $cFile['files'] as $cFiles ) {
									$cssArr[] = $cFiles;
								}
							} else {
								$cssArr[] = $cFile['fName'];
								self::cacheCheckUnixTime($cFile['utime']);
							}
							break;
						//
						case 'svg':
						case 'svgz':
							if ( !$cGroupObject['data']['isFolder'] ) {
								$page['files']['svg'][] = $cFile['fName'];
							}
							break;
					}
				}
			}
		}

		//
		$page['files']['mjs'] = array_merge($mjsArr, $page['files']['mjs']);
		$page['files']['js'] = array_merge($jsArr, $page['files']['js']);
		$page['files']['css'] = array_merge($cssArr, $page['files']['css']);
	}

	//
	public static function pagePrint()
	{
		global $page;
		// No hubo caches... solicitar
		self::cachePrint();

		// Procesar archivos
		self::pageFileGroup();

		//
		if ( self::$async ) {
			//
			printJSONData([
				'd' => $page,
				's' => [
					't' => 1
				]
			], self::$http_status_code);
			//
		} else {
			//
			http_response_code(self::$http_status_code);
			header('Content-Type: text/html; charset=utf-8');

			// Require file and print
			$baseFile = ( empty(self::$customBaseFile) ) ? $_SERVER['DOCUMENT_ROOT'] . '/snkeng/core/php/load/base_default.php' : $_SERVER['DOCUMENT_ROOT'] . self::$customBaseFile;
			require $baseFile;

			//
			echo page_build_print();
		}
		exit;
	}
	//</editor-fold>

	public static function printJSON($array, $httpCode = 200) {
		// No hubo caches... solicitar
		self::cachePrint();

		//
		$_SERVER["HTTP_USER_AGENT"] = ( !empty($_SERVER["HTTP_USER_AGENT"]) ) ? $_SERVER["HTTP_USER_AGENT"] : '';
		if ( preg_match("/msie\s(\d+).(\d+)/", strtolower($_SERVER["HTTP_USER_AGENT"]), $arr) )
		{
			header('Content-type: text/html; charset=utf-8');
		} else {
			header('Content-type: application/json; charset=utf-8');
		}

		//
		http_response_code($httpCode);

		// Quitar debug si es necesario
		if ( !empty($array['debug']) )
		{
			if ( !$_ENV['SE_DEBUG'] && ( !class_exists('\snkeng\core\engine\login') || !\snkeng\core\engine\login::check_loginLevel('sadmin') ) ) {
				unset($array['debug']);
			} else {
				$debugType = gettype($array['debug']);
				$search = ['/[\s]+/'];
				$destroy = [' '];
				switch ( $debugType ) {

					case 'string':
						$array['debug'] = preg_replace($search, $destroy, $array['debug']);
						break;

					case 'array':
						foreach ( $array['debug'] as &$value )
						{
							if ( is_string($value) ) {
								$value = preg_replace($search, $destroy, $value);
							}
						}
						break;
				}
			}
		}

		$array['timestamp'] = date("Y-m-d H:m:s");

		//
		echo(\json_encode($array, JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES));
		exit();
	}
}
//
