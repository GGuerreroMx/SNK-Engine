<?php
//
namespace snkeng\core\engine;

/**
 * Class nav
 */
class serverSideEvent {
	//
	public static function startServerSideEvent() {
		// stop session
		session_abort();

		// Avoid compression and enable auto flush
		@ini_set('zlib.output_compression', 0);
		@ini_set('implicit_flush', 1);
		for ($i = 0; $i < ob_get_level(); $i++) { ob_end_flush(); }
		ob_implicit_flush(true);

		//
		set_time_limit(0);

		//
		header('Content-Encoding: none');
		header("Cache-Control: no-store");
		header("Content-Type: text/event-stream");
		header('Connection: keep-alive');

		// First and only flush?
		flush();
	}

	//
	public static function sendMessage($message, string $event = null) {
		//
		if ( $event ) {
			echo 'event: '. $event . "\n";
		}

		//
		switch ( gettype($message) ) {
			//
			case 'NULL':
				$message = "null object";
				break;
			//
			case 'bool':
				$message = ( $message ) ? 'true' : 'false';
				break;
			//
			case "integer":
			case "double":
				$message = "n: $message";
				break;
			//
			case "string":
				break;
			//
			case "array":
				$message = json_encode($message);
				break;
			//
			default:
				$message = "Invalid type: " + gettype($message);
				break;
		}
		
		// Send message
		echo 'data: '. $message . "\n";
		echo "\n";

		// Flush contents	
		flush();
	}
}