<?php
//
namespace snkeng\core\engine;

/**
 * Class environment
 * One of the first functions called, not using many "sure to exist" functions and stopping on error.
 *
 * @package \snkeng\core\core
 */
class environment
{
	//
	public static function loadEnv( string $folder, string $specificName = '' ) {
		$fileName = $_SERVER['DOCUMENT_ROOT']."/{$folder}/{$specificName}.env";
		if ( !file_exists($fileName) ) {
			die("ENV file not found.");
		}

		//
		$fileData = file($fileName);

		//
		foreach ( $fileData as $cRow ) {
			$cRow = trim($cRow);
			// Skip comments
			if ( $cRow[0] === '#' ) { continue; }
			if ( !preg_match('/^([A-Z_0-9]+)\s*=\s*\"([^"]+)\"$/', $cRow, $matches) ) {
				die("Invalid environment variable: $cRow .");
			}
			// Assigni
			$_ENV[$matches[1]] = $matches[2];
		}

	}
}
// Fin de clase
