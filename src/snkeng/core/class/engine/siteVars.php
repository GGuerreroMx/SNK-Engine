<?php
// Actualizar archivos
namespace snkeng\core\engine;

//
class siteVars
{
	//
	static function read($vName) {
		$sql_qry = "SELECT var_value AS val FROM sb_variables
				WHERE var_name='{$vName}' LIMIT 1;";
		$data = \snkeng\core\engine\mysql::singleValue($sql_qry);

		//
		return $data;
	}

	//
	static function save($varName, $varValue) {
		//
		$varId = \snkeng\core\engine\mysql::singleNumber("SELECT var_id FROM sb_variables WHERE sv_var='{$varName}' LIMIT 1;");
		$json_data = \snkeng\core\engine\mysql::real_escape_string(json_encode($varValue, JSON_UNESCAPED_UNICODE));
		//
		if ( empty($varId) ) {
			$sql_qry = "INSERT INTO sb_variables (var_name, var_value) VALUES ('{$varName}', '{$json_data}');";
		} else {
			$sql_qry = "UPDATE sa_sitevars SET var_value='{$json_data}' WHERE sv_id={$varId};";
		}

		//
		\snkeng\core\engine\mysql::submitQuery($sql_qry,
			[
				'errorKey' => 'SESiteVariableSaveArray',
				'errorDesc' => 'Preferences could not be updated.'
			]
		);
	}

	//
	static function saveById($varId, $varVal) {
		$json_data = \snkeng\core\engine\mysql::real_escape_string(json_encode($varVal, JSON_UNESCAPED_UNICODE));
		$upd_qry = "UPDATE sb_variables SET var_value='{$json_data}' WHERE var_id={$varId};";

		//
		\snkeng\core\engine\mysql::submitQuery($upd_qry,
			[
				'errorKey' => 'SESiteVariableSaveById',
				'errorDesc' => 'Preferences could not be updated.'
			]
		);
	}
}