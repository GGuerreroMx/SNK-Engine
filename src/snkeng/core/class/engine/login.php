<?php
//
namespace snkeng\core\engine;

//
class login
{
	protected static $user = [
		'login' => [
			'levels' => [
				'uadmin' => false,
				'sadmin' => false,
				'admin' => false
			],
			'logged' => false,
			//
			'condition' => 0,
			'level' => 0,
			'admType' => '',
			//
			'justLogin' => false,
			'fLogin' => false,
			'loginFail' => false,
			'warning' => '',
			//
			'captcha' => false
		],
		'data' => [
			'id' => 0,
			'name_first' => '',
			'name_last' => '',
			'name_full' => '',
			'name_url' => '',
			'image_square' => '',
			'image_banner' => '',
		],
		'as' => [
			'self' => true,
			'type' => 'user',
			'id' => 0,
			'eKey' => '',
			'name_full' => '',
			'name_url' => '',
			'image_square' => '',
			'image_banner' => '',
			'url' => ''
		],
		'sn' => []
	];

	protected static $logTry = false;

	protected static $error = '';
	protected static $errorNum = 0;

	protected static $mysql;
	protected static $siteVars;

	protected static $userId;
	protected static $userIp;

	// Levels
	public static $rulesCurrentPermits = [];
	protected static $rulesCurrentPosition = '';

	//
	public static function debugUser()
	{
		debugVariable(self::$user);
	}

	//
	private static function systemReadyCheck()
	{
		if ( empty(self::$mysql) ) {
			global $mysql, $siteVars;
			self::$mysql =& $mysql;
			self::$siteVars =& $siteVars;
		}
	}

	//
	private static function loginStart()
	{
		self::systemReadyCheck();

		// Obtener IP Usuario
		if ( isset($_SERVER["REMOTE_ADDR"]) ) {
			self::$userIp = $_SERVER["REMOTE_ADDR"];
		} elseif ( isset($_SERVER["HTTP_X_FORWARDED_FOR"]) ) {
			self::$userIp = $_SERVER["HTTP_X_FORWARDED_FOR"];
		} elseif ( isset($_SERVER["HTTP_CLIENT_IP"]) ) {
			self::$userIp = $_SERVER["HTTP_CLIENT_IP"];
		}

		// Verificación del login
		if ( isset($_SESSION['user']['logged']) && $_SESSION['user']['logged'] ) {
			// Logeado ver si hay que cortar o verificar conexión
			self::loginWithSession();
		} elseif ( isset($_COOKIE['rememberme']) ) {
			// Revisar cookie
			self::loginWithCookie();
		}

		//
		self::$logTry = true;
	}

	//
	private static function loginStartCheck()
	{
		if ( !self::$logTry ) {
			self::loginStart();
		}
	}

	//
	public static function returnUserStatus()
	{
		// Check login
		self::loginStartCheck();

		return [
			's' => self::$user['login']['logged'],
			'data' => self::$user['data'],
			'as' => self::$user['as'],
		];
	}

	//
	public static function check_loginStatus()
	{
		// Check login
		self::loginStartCheck();

		return self::$user['login']['logged'];
	}

	//
	public static function loginRequired()
	{
		// Check login
		self::loginStartCheck();

		//
		if ( !self::$user['login']['logged'] ) {
			if ( isset($_SERVER['HTTP_REFERER']) ) {
				$_SESSION['login_redirect'] = $_SERVER['HTTP_REFERER'];
			}
			se_killWithError('No login', '', 1);
		}
	}

	//
	public static function loginExceded()
	{
		// Check login
		self::loginStartCheck();

		//
		if ( self::$user['login']['logged'] ) {
			se_killWithError('Not for loged in users.', '', 402);
		}
	}

	//
	public static function getLoginLevel()
	{
		//
		self::loginStartCheck();

		//
		return (self::$user['login']['logged']) ? self::$user['login']['level'] : 4;
	}

	//
	public static function getUserId()
	{
		//
		self::loginStartCheck();

		//
		return self::$user['data']['id'];
	}

	//
	public static function getUserData($email = false)
	{
		//
		self::loginStartCheck();

		//
		if ( self::$user['login']['logged'] ) {
			$tempUserData = self::$user['data'];
			if ( $email ) {
				$sql_qry = "SELECT am_mail FROM sb_users_mail WHERE a_id={$tempUserData['id']} AND am_type=1 LIMIT 1;";
				$tempUserData['email'] = \snkeng\core\engine\mysql::singleValue($sql_qry);
			}

			return $tempUserData;
		} else {
			return null;
		}
	}

	//
	public static function check_loginLevel($level)
	{
		// Check login
		self::loginStartCheck();

		//
		if ( !isset(self::$user['login']['levels'][$level]) ) {
			se_killWithError('User login check', "Invalid level ({$level}).", 500);
		}

		return self::$user['login']['levels'][$level];
	}

	//
	public static function kill_loginLevel($level)
	{
		if ( !self::check_loginLevel($level) ) {
			se_killWithError('Not enough permits.', "Required level:{$level}.", 401);
		}
	}

	//
	public static function permitsSetRules($fileName, $levelName = null)
	{
		//
		self::$rulesCurrentPosition = $levelName;
		if ( empty($levelName) ) {
			self::$rulesCurrentPosition = self::$user['login']['admType'];
		}

		//
		if ( !file_exists($fileName) ) {
			se_killWithError("[SYSTEM] USER PERMITS.", 'Incorrect file permits location', 500);
		}

		//
		$tempFullStructure = (require $fileName);


		if ( !array_key_exists(self::$rulesCurrentPosition, $tempFullStructure) ) {
			se_killWithError("[SYSTEM] USER PERMITS.", "Invalid permit level. (" . self::$rulesCurrentPosition . ').', 500);
		}

		//
		self::$rulesCurrentPermits = $tempFullStructure[self::$rulesCurrentPosition]['apps'];
	}

	//
	public static function permitsCheck($app, $op = null)
	{
		//
		if ( self::$user['login']['levels']['sadmin'] ) {
			return true;
		}

		//
		if ( $op ) {
			return (in_array($op, self::$rulesCurrentPermits[$app]) || self::$rulesCurrentPermits[$app][0] === 'all');
		} else {
			return (array_key_exists($app, self::$rulesCurrentPermits));
		}
	}

	//
	public static function permitsCheckKill($app, $op = null)
	{
		//
		if ( !self::permitsCheck($app, $op) ) {
			// debugVariable(self::$rulesCurrentPermits[$app]);
			se_killWithError('Insuficient permits', "Required ({$app}, {$op})", 401);
		}
	}


	//<editor-fold desc="Inicio sesión normal">

	// Verificar si tiene login actual
	private static function loginWithSession()
	{
		//
		$valid = true;
		//
		$rememberLogin = (isset($_SESSION['user']['remember_login'])) ? $_SESSION['user']['remember_login'] : false;
		// Verificar que sea el usuario
		$ulId = intval($_SESSION['user']['logId']);
		//
		if ( $ulId === 0 ) {
			se_killWithError('INVALID LOGIN ATTEMPT.', 'IMPROPPER COOKIE DATA');
		}
		//
		$sql_qry = "SELECT a_id, ul_session, ul_cookie
					FROM sb_users_logins
					WHERE ul_id={$ulId};";
		//
		$logData = \snkeng\core\engine\mysql::singleRowAssoc($sql_qry,
			[
				'int' => ['a_id']
			],
			[
				'errorKey' => '',
				'errorDesc' => 'Not valid login query'
			]
		);

		// Validar
		if ( $logData['ul_session'] !== session_id() ) {
			self::killLogin();
			$valid = false;
		}
		if ( $logData['ul_cookie'] !== $_SESSION['user']['cookie'] ) {
			self::killLogin();
			$valid = false;
		}

		//
		if ( $valid ) {
			self::$userId = $logData['a_id'];
			self::setSession($rememberLogin, false);
		}
	}

	// Login a través de cookie
	private static function loginWithCookie()
	{
		// Hay cookie
		if ( isset($_COOKIE['rememberme']) ) {
			// Vars
			list($logId, $cookie, $hash) = explode(':', $_COOKIE['rememberme']);

			// Hash
			if ( $hash == hash('sha256', $logId . $cookie . $_ENV['SE_COOKIE']) ) {
				$logId = intval($logId);
				if ( $logId === 0 ) {
					se_killWithError('Cookie login, no ID');
				}
				$sql_qry = "SELECT a_id, ul_cookie FROM sb_users_logins WHERE ul_id='{$logId}';";
				$logData = \snkeng\core\engine\mysql::singleRowAssoc($sql_qry,
					[
						'int' => ['a_id']
					],
					[
						'errorKey' => '',
						'errorDesc' => 'Not valid cookie'
					]
				);

				//
				self::$userId = $logData['a_id'];
				if ( $logData['a_id'] !== 0 && $logData['ul_cookie'] === $cookie ) {
					$userIp = self::$userIp;
					$query = "INSERT INTO sb_users_login_log (log_ip, log_dtlast, a_id, log_type) VALUES ('{$userIp}', now(), {$logData['a_id']}, 1);";
					\snkeng\core\engine\mysql::submitQuery($query);
					self::setSession(true, true);
				} else {
					self::cookieDestroy();
					self::killLogin();
				}
			} else {
				self::cookieDestroy();
			}
		}
	}
	//</editor-fold>

	//<editor-fold desc="Default Ops">
	// Definir sesión actual
	private static function setSession($remember, $initLog = false, $firstLoginEva = false)
	{
		//
		$userId = self::$userId;
		$userIp = self::$userIp;
		// Primera conexión
		// Guardar información de la conexión actual, cookies y extraer claves
		if ( $initLog ) {
			// Crear nueva cookie
			$cookie = se_randString(32);
			session_regenerate_id(true);

			//
			$sql_qry = "INSERT INTO sb_users_logins
						(a_id, ul_ip, ul_dtadd, ul_dtlast, ul_session, ul_cookie)
						VALUES
						({$userId}, '{$userIp}', now(), now(), '" . session_id() . "', '{$cookie}');";
			// debugVariable($sql_qry);
			\snkeng\core\engine\mysql::submitQuery($sql_qry);

			\snkeng\core\engine\mysql::killIfError('No se pudo actualizar el login.', 'loginMainSetSes01');
			// Variables iniciales de sessión
			$_SESSION['user']['logged'] = true;
			$_SESSION['user']['id'] = self::$userId;
			$_SESSION['user']['logId'] = \snkeng\core\engine\mysql::getLastId();
			$_SESSION['user']['cookie'] = $cookie;
			$_SESSION['user']['remember_login'] = $remember;

			// Borrar redirect
			unset($_SESSION['login_redirect']);
		}

		// Crear cookie de conexión
		if ( $remember ) {
			self::cookieCreate();
		}

		// Obtener propiedades del usuario
		$sql_qry = "SELECT
						adm.a_adm_level AS u_level, adm.a_adm_type AS admType, adm.a_status AS a_status,
						adm.a_fname AS name_first, adm.a_lname AS name_last,
						adm.a_obj_name AS name_full, adm.a_url_name_full AS name_url,
						adm.a_img_crop AS image
					FROM sb_objects_obj AS adm
					WHERE adm.a_id={$userId}
					LIMIT 1;";
		$datos = \snkeng\core\engine\mysql::singleRowAssoc($sql_qry,
			[
				'int' => ['u_level', 'a_status']
			],
			[
				'errorKey' => '',
				'errorDesc' => 'No fue posible obtener los datos del usuario'
			]
		);

		// debugVariable($sql_qry);
		$valid = true;
		// if ( intval($datos[2]) === 0 ) { $valid = false; self::$user['login']['condition'] = 2; self::$user['login']['warning'] = "La cuenta no ha sido validada. Verifique su correo electrónico."; }
		if ( intval($datos['a_status']) === 2 ) {
			$valid = false;
			self::$user['login']['condition'] = 3;
			self::$user['login']['warning'] = "Su cuenta por el momento no se encuentra autorizada para accesar.";
		}

		//
		if ( $valid ) {
			//
			self::$user['login']['logged'] = true;


			self::$user['login']['condition'] = $datos['a_status'];
			self::$user['login']['level'] = $datos['u_level'];
			self::$user['login']['admType'] = $datos['admType'];

			self::$user['login']['justLogin'] = $initLog;
			self::$user['login']['fLogin'] = $firstLoginEva;

			//
			self::$user['data']['id'] = self::$userId;

			// Asignación permisos reales
			if ( self::$user['login']['level'] === 0 ) {
				self::$user['login']['levels']['uadmin'] = true;
				self::$user['login']['levels']['sadmin'] = true;
				self::$user['login']['levels']['admin'] = true;
			} elseif ( self::$user['login']['level'] === 1 ) {
				self::$user['login']['levels']['sadmin'] = true;
				self::$user['login']['levels']['admin'] = true;
			} elseif ( self::$user['login']['level'] === 2 ) {
				self::$user['login']['levels']['admin'] = true;
			}

			//
			self::$user['data']['name_first'] = $datos['name_first'];
			self::$user['data']['name_last'] = $datos['name_last'];
			self::$user['data']['name_full'] = $datos['name_full'];
			self::$user['data']['name_url'] = $datos['name_url'];

			//
			self::$user['data']['image_square'] = $datos['image'];

			//
			self::$user['as']['self'] = true;
			self::$user['as']['type'] = 'user';
			self::$user['as']['id'] = self::$userId;
			self::$user['as']['ikey'] = '';
			self::$user['as']['name_full'] = $datos['name_full'];
			self::$user['as']['name_url'] = $datos['name_url'];
			self::$user['as']['url'] = "/@{$datos['name_url']}/";
			self::$user['as']['image_square'] = $datos['image'];

			//
			if ( $initLog ) {
				// Acciones primer login (si existen)
				$iniFile = $_SERVER['DOCUMENT_ROOT'] . '/snkeng/site_core/specs/user_login.php';
				if ( file_exists($iniFile) ) {
					require $iniFile;
				}
			}

			//
			/*
			if ( self::$siteVars['site']['modules']['socnet'] ) {
				$asId = (empty($_POST['asId'])) ? intval($_SESSION['asId']) : intval($_POST['asId']);
				if ( empty($asId) || $asId === self::$siteVars['user']['data']['id'] ) {
					// Remover como otro usuario
					unset($_SESSION['asId']);
				} else {
					$sql_qry = "SELECT obj_id, obj_ikey, obj_type, obj_name, obj_urlname
								FROM st_socnet_objects WHERE obj_id={$asId};";
					$objData = \snkeng\core\engine\mysql::singleRowAssoc($sql_qry);
					\snkeng\core\engine\mysql::killIfError("asUser - No se pudo leer la información del objeto", "full");
					if ( intval($objData['obj_type']) !== 1 ) {
						//
						$sql_qry = "SELECT urel_id, urel_status, urel_blocked, urel_suscribed
									FROM st_socnet_relationships
									WHERE obj_id_1={$asId} AND obj_id_2={self::$userId}
									LIMIT 1;";
						$relData = \snkeng\core\engine\mysql::singleRowAssoc($sql_qry);
						\snkeng\core\engine\mysql::killIfError("asUser - No se pudo leer relación.", "full");
						//
						if ( !empty($relData['urel_id']) && intval($relData['urel_status']) <= 2 && intval($relData['urel_blocked']) === 0 ) {
							// Checar el objeto
							self::$user['as']['user'] = false;
							self::$user['as']['id'] = intval($objData['obj_id']);
							self::$user['as']['ikey'] = $objData['obj_ikey'];
							self::$user['as']['typeNum'] = intval($objData['obj_type']);
							self::$user['as']['name'] = $objData['obj_name'];
							self::$user['as']['uname'] = $objData['obj_urlname'];
							//
							$folder = self::$siteVars['modset']['socnet']['objects'][self::$user['as']['typeNum']]['folder'];
							self::$user['as']['url'] = "/{$folder}/{$objData['obj_urlname']}/";
							global $siteVars;
							$_SESSION['asId'] = $siteVars['user']['as']['id'];
						} else {
							unset($_SESSION['asId']);
							die("asUser - No admin.({})");
						}
					} else {
						unset($_SESSION['asId']);
						die("asUser - Usuario.");
					}
				}
			}
			*/


			// Final asignment
			global $siteVars;
			$siteVars['user'] = self::$user;
		}
	}
	// INI: forceFirstLogin

	//	Da login después de crear la cuenta
	public static function forceFirstLogin($userId)
	{
		//
		self::systemReadyCheck();

		// Verificación del login
		if ( !isset($_SESSION['user']['logged']) ) {
			self::$userId = $userId;
			self::setSession(true, true, true);
		} else {
			debugVariable($_SESSION['user']);
			die('aparent login already exists.... why?');
		}
	}
	// END:forceFirstLogin
	//
	public static function killLogin()
	{
		unset($_SESSION['user']);
		unset($_SESSION['shopCart']);
		self::cookieDestroy();
	}
	//</editor-fold>

	//<editor-fold desc="Cookies">

	// Cookie Crear
	private static function cookieCreate()
	{
		$logId = $_SESSION['user']['logId'];
		$cookie = $_SESSION['user']['cookie'];
		$hash = hash('sha256', $logId . $cookie . $_ENV['SE_COOKIE']);
		//
		$cookieVars = implode(':', [$logId, $cookie, $hash]);
		// Tiempo = 30 días (24*30*3600)
		setcookie('rememberme', $cookieVars, time() + 2592000, '/', '', true, true);
		setcookie('logedin', 1, time() + 2592000, '/', '', true, false);
	}

	// Cookie Destruir
	private static function cookieDestroy()
	{
		// Fecha de diez años atras (365 * 10 * 3600)
		setcookie('rememberme', 0, time() - 13140000, '/', '', true, true);
		setcookie('logedin', 0, time() - 13140000, '/', '', true, false);
	}
	//</editor-fold>

	// Nuevo login clásico
	public static function loginWithPost(&$response, $uName, $uPass, $uSave)
	{
		self::loginStart();
		//
		$response['s'] = [
			't' => 1,
			'l' => 0,
			'c' => 0,
			'e' => ''
		];

		// Verificar número de intentos (debe ser sobre el usuario aparentemente)
		/*
		$date = date("Y-m-d H:i:s", time()-(3600));
		$sql_qry = "SELECT log_id FROM sb_users_login_log WHERE log_ip='{self::$userIp}' AND log_dtlast >='{$date}' LIMIT 1;";
		$logId = \snkeng\core\engine\mysql::singleNumber($sql_qry);
		if ( empty($logId) )
		{
			$sql_qry = "INSERT INTO sb_users_login_log (log_ip, log_dtlast) VALUES ('self::$userIp', now());";
			\snkeng\core\engine\mysql::submitQuery($sql_qry);
			$logId = \snkeng\core\engine\mysql::getLastId();
		} else {
			$sql_qry = "UPDATE sb_users_login_log SET log_attemps=log_attemps+1 WHERE log_id={$logId};";
			\snkeng\core\engine\mysql::submitQuery($sql_qry);
			$sql_qry = "SELECT log_attemps FROM sb_users_login_log WHERE log_id='{$logId}';";
			$attempts = \snkeng\core\engine\mysql::singleNumber($sql_qry);
			if ( $attempts > 10 ) {
				$skip = true;
				$captcha = true;
				self::$login['captcha'] = true;
			} elseif ($attempts > 5) {
				if ( !$captchaObj->captchaPass ) { $skip = true; }
				$captcha = true;
				self::$login['captcha'] = true;
			} elseif ( $attempts == 5 ) {
				$captcha = true;
				self::$login['captcha'] = true;
			}
		}
		*/

		// Convertir variables
		if ( filter_var($uName, FILTER_VALIDATE_EMAIL) === false ) {
			$uName = '';
		}
		if ( !preg_match('/^[A-z0-9@\.#_\-]+$/', $uPass) ) {
			$uPass = '';
		}
		//
		if ( empty($uName) || empty($uPass) ) {
			$response['s']['e'] = 'Usuario y/o contraseña no válido.';
			$response['debug'] = '1';
			return;
		}

		// Obtener seed (para generar password)
		$sql_qry = "SELECT adm.a_upass, adm.a_id
					FROM sb_users_mail AS admail
					INNER JOIN sb_objects_obj AS adm ON admail.a_id=adm.a_id
					WHERE am_mail='{$uName}'
					LIMIT 1;";
		$data = \snkeng\core\engine\mysql::singleRowAssoc($sql_qry,
			[
				'errorKey' => '',
				'errorDesc' => ''
			]
		);

		//
		if ( empty($data) ) {
			$response['s']['e'] = 'Usuario y/o contraseña no válido.';
			$response['debug'] = '1';
			return;
		}

		if ( $data['a_upass'] == crypt($uPass, $data['a_upass']) ) {
			self::$userId = intval($data['a_id']);
			// $sql_qry = "UPDATE sb_users_login_log SET log_attemps=0, log_type=0, a_id={self::$userId} WHERE log_id='{$logId}';";
			\snkeng\core\engine\mysql::submitQuery($sql_qry);
			self::setSession($uSave, true);

			$response['d'] = self::$user['as'];
			$response['s']['l'] = 1;
		} else {
			$response['s']['e'] = 'Usuario y/o contraseña no válido.';
			$response['debug'] = '2';
		}
	}

	// Nuevo login clásico
	public static function loginWithSocialNetwork($socialNetwork, $snId)
	{
		self::loginStart();

		// Verificar si hay usuario
		$sql_qry = "SELECT asn_id, a_id FROM sb_users_socnet WHERE asn_sn_id='{$snId}' AND asn_type='{$socialNetwork}' LIMIT 1;";
		$rowData = \snkeng\core\engine\mysql::singleRowAssoc($sql_qry);
		if ( !empty($rowData) ) {
			// Login
			self::$userId = intval($rowData['a_id']);
			self::setSession(true, true, false);

			//
			$sql_qry = "UPDATE sb_users_socnet SET asn_last_login=NOW() WHERE asn_id='{$rowData['asn_id']}' LIMIT 1;";
			\snkeng\core\engine\mysql::submitQuery($sql_qry,
				[
					'errorKey' => 'SNLogin',
					'errorDesc' => 'No fue posible iniciar sesión.'
				]
			);

			// Tiene que dar true, para evitar lock in
			return true;
		} else {
			//
			return false;
		}
	}
}
