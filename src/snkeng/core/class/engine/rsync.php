<?php
// Actualizar archivos
namespace snkeng\core\engine;

//
class rsync
{
	public static function syncToServer($sshName, $targetFolder)
	{
		$syncOps = [
			[
				// This is core and app
				"name" => "SNKENG",
				"folder" => "snkeng"
			],
			[
				// This is core and app
				"name" => "Vendor",
				"folder" => "vendor"
			],
			[
				// must be identical in both servers, svg icons, js, mjs, css and bundles
				"name" => "Files - Generated ",
				"folder" => "se_files/site_generated"
			],
		];

		// 
		\snkeng\core\engine\serverSideEvent::sendMessage("Initializing File Sync");

		// Run each operation independently
		foreach ($syncOps as $cOp) {
			//
			\snkeng\core\engine\serverSideEvent::sendMessage("SYNC: {$cOp['name']}");
			// 
			$rCommand = "rsync -pgrltzhO --stats --delete --chmod=774 --chown=www-data:www-data {$_SERVER['DOCUMENT_ROOT']}/{$cOp['folder']}/ {$sshName}:/var/www/{$targetFolder}/{$cOp['folder']}/ 2>&1 | tee /var/www/rsync/log.txt";

			// (Send command, for analysis)
			\snkeng\core\engine\serverSideEvent::sendMessage($rCommand);

			//
			$output = shell_exec($rCommand);

			//
			if ( !$output ) {
				\snkeng\core\engine\serverSideEvent::sendMessage("ERROR, NO SE EJECUTÓ EL COMMANDO: " . $rCommand, "error");
			} else {
				//
				\snkeng\core\engine\serverSideEvent::sendMessage("OUTPUT: " . $output, "results");
			}
		}
		//		
	}

	public static function runOtherCommands() {
		//
		$operations = [
			[
				"name" => "Sync App Folder",
				"exec" => 'rsync -rlptzhO --no-perms --stats --delete --owner=www-data --group=www-data /var/www/snako/public_html/se_site_core/ syncserver@cloudy.snako.dev:/var/www/snako.dev/public_html/se_site_core/ 2>&1 | tee /var/www/rsync/log.txt'
			],
			/*
			[
				"name" => "Sync Core Folder",
				"exec" => "rsync -rlptzhO --no-perms --stats --delete --owner=www-data --group=www-data /var/www/snako/public_html/se_core/ syncserver@cloudy.snako.dev:/var/www/snako.dev/public_html/se_core/"
			],
			[
				"name" => "Modules",
				"exec" => "rsync -rlptzhO --no-perms --stats --delete /var/www/snako/public_html/se_files/site_generated/modules/ syncserver@cloudy.snako.dev:/var/www/snako.dev/public_html/se_files/site_generated/modules/"
			],
			[
				"name" => "Bundles",
				"exec" => "rsync -rlptzhO --no-perms --stats --delete /var/www/snako/public_html/se_files/site_generated/bundles/ syncserver@cloudy.snako.dev:/var/www/snako.dev/public_html/se_files/site_generated/bundles/"
			],
			[
				"name" => "SVG",
				"exec" => "rsync -rlptzhO --no-perms --stats --delete /var/www/snako/public_html/se_files/site_generated/svg-icons/final/ syncserver@cloudy.snako.dev:/var/www/snako.dev/public_html/se_files/site_generated/svg-icons/final/"
			],
			*/
		];

		// Run each operation independently
		foreach ($operations as $cOp) {
			// 
			\snkeng\core\engine\serverSideEvent::sendMessage($cOp["name"]);

			$output = shell_exec($cOp["exec"]);

			if ( !$output ) {
				\snkeng\core\engine\serverSideEvent::sendMessage("ERROR, NO SE EJECUTÓ EL COMMANDO: " . $cOp["exec"]. "\n---\n", "error");
			}

			//
			\snkeng\core\engine\serverSideEvent::sendMessage("OUTPUT: " . $output, "results");
		}
	}
}
//
