<?php
// Imprimir una galería e incluir objeto para cargar el js
// Es necesario que en el objeto que llame a esta función se cargue la librería de la galería.
function printGallery($galId, $type = 'full', $url = "")
{
		$sql_qry = "SELECT gal_title, gal_desc, gal_fotos FROM st_gallery_object WHERE gal_id=$galId LIMIT 1";
	if ( ($datos = \snkeng\core\engine\mysql::singleRow($sql_qry) ) )
	{
echo <<<EOD
	<div class="galleryObj">
		<h1>{$datos[0]}</h1>
		{$datos[1]}
		<div id="gallery">\n
EOD;
		if ( intval($datos[2]) !== 0 )
		{
			$sql_qry = "SELECT gimg_title, gimg_content, gimg_small, gimg_big FROM st_gallery_images WHERE gal_id=$galId";
			if ( \snkeng\core\engine\mysql::execQuery($sql_qry) )
			{
				echo("\t\t".'<ul class="noscript">'."\n");
				$str = "\t\t\t".'<li><h1>%s</h1><p>%s</p><a href="%s"><img src="%s" alt="%s"/></a></li>'."\n";
				while ( $datos = \snkeng\core\engine\mysql::$result->fetch_assoc() )
				{
					printf($str, $datos['gimg_title'], $datos['gimg_content'], $datos['gimg_big'], $datos['gimg_small'], $datos['gimg_title']);
				}
				echo("\t\t</ul>\n");
			}
		} else { echo("La galería no contiene fotos todavía."); }
echo <<<EOD
		</div>
	</div>
<script>
	window.addEvent('domready', function(){ snkGalObject = new snkGallery('gallery', '$type', '$url', { offset: -1 }); });
</script>
EOD;
	} else { echo("ERROR: La galería no existe."); }
}
?>