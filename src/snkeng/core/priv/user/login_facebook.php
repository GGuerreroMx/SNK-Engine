<?php
//

use snkeng\core\external\sn_facebook_se;

$fb = new sn_facebook_se();
$user = $fb->getUser(true);

//
$redirectUrl = '/';

// Attemp login
if ( !\snkeng\core\engine\login::loginWithSocialNetwork('fb', $user['id']) ) {
	// User doesn't create user if it already exists

	//
	$userData = $fb->getUserCoreData();


	// debugVariable($userData);

	// Register Data
	$response = [];
	\snkeng\core\user\management::userCreate($response, $userData['first_name'], $userData['last_name'], 3, 'socnet', [
		'socnet' => 'fb',
		'snId' => $user['id']
	]);

	// copiado de la imagen
	// require $_SERVER['DOCUMENT_ROOT'] . '/snkeng/core/priv/user/user_picture.php';
	// user_picture::picture_from_internet($response['d']['id'], $userData['picture']['data']['url']);

	// Primera concexión oficial
	\snkeng\core\engine\login::forceFirstLogin($response['d']['id']);

	//
	if ( isset($_SESSION['login_redirect']) ) {
		$redirectUrl = $_SESSION['login_redirect'];
		unset($_SESSION['login_redirect']);
	}

}

// Redirección final
header('Location: ' . $redirectUrl);
exit;