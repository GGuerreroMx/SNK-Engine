<?php
//

use snkeng\core\external\sn_google_se;

$google = new sn_google_se();
$userData = $google->getUser();

//
$redirectUrl = '/';

// Attemp login
if ( !\snkeng\core\engine\login::loginWithSocialNetwork('gp', $userData['id']) ) {
	// User doesn't exists, create

	// Register Data
	$response = [];
	\snkeng\core\user\management::userCreate($response, $userData['first_name'], $userData['last_name'], 3, 'socnet', [
		'socnet' => 'gp',
		'snId' => $userData['id']
	]);

	// copiado de la imagen
	\snkeng\core\socnet\object_picture::picture_from_internet($response['d']['id'], $userData['picture']);

	// Primera concexión oficial
	\snkeng\core\engine\login::forceFirstLogin($response['d']['id']);

	//
	if ( isset($_SESSION['login_redirect']) ) {
		$redirectUrl = $_SESSION['login_redirect'];
		unset($_SESSION['login_redirect']);
	}

}

// Redirección final
header('Location: ' . $redirectUrl);
exit;