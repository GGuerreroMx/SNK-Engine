<?php
//

// Check if app exists
if ( !is_dir($appData['dir']) ) {
	\snkeng\core\engine\nav::invalidPage("No se encontró la app.");
}

// User allways has access to himself, check other permissions
if ( \snkeng\core\engine\nav::current() !== 'user' ) {
	\snkeng\core\engine\login::permitsCheck($appData['name']);
}

// Cargar admin
require $appData['dir'].'/routing/adm/pages/#map.php';

// Revisar si se necesita la estructura de admin
\snkeng\core\engine\nav::asyncTargetCheck('app_content');

// Adjust variables
$pageVars = ( require $appData['dir'].'/settings/a_vars.php' );

//
$page['head']['title'] = ( empty($page['head']['title']) ) ? '' : $page['head']['title'].' - ';
$page['head']['title'].= $pageVars['title'];

// Objects visibility
$exStyle = "";
$permsName = "Admin";
if ( !( \snkeng\core\engine\login::check_loginLevel('sadmin') || \snkeng\core\engine\login::$rulesCurrentPermits[$appData['name']][0] === 'all' ) ) {
	$exStyle = "\n\tdiv[data-appname] [data-permit] { display:none; }\n";
	foreach (\snkeng\core\engine\login::$rulesCurrentPermits[$appData['name']] as $permit ) {
		$exStyle.= "\tdiv[data-appname] [data-permit=\"{$permit}\"] { display:initial !important; }\n";
	}
	//

	$permsName = ( isset($siteVars['user']['perms']['name']) ) ? $siteVars['user']['perms']['name'] : '';
}


//
$page['body'] = <<<HTML
\n\n<!-- INI:APP STRUCTURE -->
<div class="appLoader" data-appname="{$appData['name']}" data-test="{$permsName}">\n
<!-- INI:APP CSS PERMITS -->
<style>{$exStyle}</style>
<!-- END:APP CSS PERMITS -->\n\n
<div class="appTitle">{$pageVars['title']}</div>\n\n
<!-- INI:APP MENU -->
<div class="nav3">
<div class="menuIcon"><svg class="icon inline"><use xlink:href="#fa-navicon" /></svg></div>
<div class="menu">
{$pageVars['menu']}
</div>
</div>
<!-- END:APP MENU -->\n\n
<!-- INI:APP CONTENT -->
<div id="app_content">
{$page['body']}
</div>
<!-- END:APP CONTENT -->\n\n
</div>
<!-- END:APP STRUCTURE -->\n\n\n
HTML;
//
